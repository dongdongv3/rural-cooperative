# 农合合作社

#### 介绍
农合合作社使用到net6 asp.net api,efcore,ids4,react,antd等技术

#### 功能
土地托管
库存管理
农机合作
工单管理

#### 安装教程

本目录所有代码文件均来自[PTBlack](https://www.ptblack.cloud)生成，
如需访问此项目***预览地址***请跳转至 [模板库](https://www.ptblack.cloud/AppGroup/AppTemplateListPage)

