import { reqGet } from '@/utils/requestUtil';
import { reqGetByPage } from '@/utils/requestUtil';
import { reqPost } from '@/utils/requestUtil';
import { optionsResoult } from '@/utils/optionsResoult';
import { optionsResoultStatus } from '@/utils/optionsResoult';

type SheyuanAddRequest = {
  userName: any;
  userCode: any;
  phoneNumber: any;
  joinTime: any;
  panqu: any;
  miangji: number;
  status: any;
  yue: number;
};

export async function sheyuanAdd(request: SheyuanAddRequest): Promise<optionsResoult> {
  return reqPost('/api/SheyuanGroupService/sheyuanAdd', request);
}

export async function sheyuanAdd_GetDefaultValue(request: any) {
  return reqPost('/api/SheyuanGroupService/sheyuanAdd_GetDefaultValue', request);
}

export async function sheyuanAdd_Panqu_DataSource(pageParameter: any) {
  return reqGet('/api/SheyuanGroupService/SheyuanAdd_Panqu_DataSource', pageParameter);
}

export async function sheyuanAdd_Status_DataSource(pageParameter: any) {
  return reqGet('/api/SheyuanGroupService/SheyuanAdd_Status_DataSource', pageParameter);
}

type SheyuanEditUpdateRequest = {
  id: any;
  userName: any;
  userCode: any;
  phoneNumber: any;
  joinTime: any;
  panqu: any;
  miangji: number;
  status: any;
  yue: number;
};

type SheyuanEditQueryRequest = {
  id: any;
};

type SheyuanEditResponse = {
  id: any;
  userName: any;
  userCode: any;
  phoneNumber: any;
  joinTime: any;
  panqu: any;
  miangji: number;
  status: any;
  yue: number;
};

export async function sheyuanEdit(request: SheyuanEditUpdateRequest): Promise<optionsResoult> {
  return reqPost('/api/SheyuanGroupService/sheyuanEdit', request);
}

export async function sheyuanEdit_GetValue(
  request: SheyuanEditQueryRequest,
): Promise<SheyuanEditResponse> {
  return reqPost('/api/SheyuanGroupService/sheyuanEdit_GetValue', request);
}

export async function sheyuanEdit_Panqu_DataSource(pageParameter: any) {
  return reqGet('/api/SheyuanGroupService/SheyuanEdit_Panqu_DataSource', pageParameter);
}

export async function sheyuanEdit_Status_DataSource(pageParameter: any) {
  return reqGet('/api/SheyuanGroupService/SheyuanEdit_Status_DataSource', pageParameter);
}

type SheyuanDetailQueryRequest = {
  id: any;
};

type SheyuanDetailResponse = {
  id: any;
  userName: any;
  userCode: any;
  phoneNumber: any;
  joinTime: any;
  panqu: any;
  miangji: number;
  status: any;
  yue: number;
};

export async function sheyuanDetail_GetValue(
  request: SheyuanDetailQueryRequest,
): Promise<SheyuanDetailResponse> {
  return reqPost('/api/SheyuanGroupService/sheyuanDetail_GetValue', request);
}

export async function sheyuanDetail_Panqu_DataSource(pageParameter: any) {
  return reqGet('/api/SheyuanGroupService/SheyuanDetail_Panqu_DataSource', pageParameter);
}

export async function sheyuanDetail_Status_DataSource(pageParameter: any) {
  return reqGet('/api/SheyuanGroupService/SheyuanDetail_Status_DataSource', pageParameter);
}

type SheyuanDeleteRequest = {
  id: any;
};

export async function sheyuanDelete(request: SheyuanDeleteRequest): Promise<optionsResoult> {
  return reqPost('/api/SheyuanGroupService/sheyuanDelete', request);
}

type SheyuanSearchRequest = {};

export async function sheyuanSearch(request: SheyuanSearchRequest, sort: any, filter: any) {
  return reqGetByPage('/api/SheyuanGroupService/SheyuanSearch', request, sort, filter);
}
export async function sheyuanSearch_GetDefaultValue(request: any) {
  return reqPost('/api/SheyuanGroupService/sheyuanSearch_GetDefaultValue', request);
}

export async function sheyuanSearch_Panqu_View_DataSource(pageParameter: any) {
  return reqGet('/api/SheyuanGroupService/SheyuanSearch_Panqu_View_DataSource', pageParameter);
}

export async function sheyuanSearch_Status_View_DataSource(pageParameter: any) {
  return reqGet('/api/SheyuanGroupService/SheyuanSearch_Status_View_DataSource', pageParameter);
}
