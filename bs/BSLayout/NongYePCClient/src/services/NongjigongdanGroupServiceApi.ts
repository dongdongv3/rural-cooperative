import { reqGet } from '@/utils/requestUtil';
import { reqGetByPage } from '@/utils/requestUtil';
import { reqPost } from '@/utils/requestUtil';
import { optionsResoult } from '@/utils/optionsResoult';
import { optionsResoultStatus } from '@/utils/optionsResoult';

type NongjigongdanAddRequest = {
  miangji: number;
  leixing: any;
  chuli: any;
  zongjia: number;
  zhuangtai: any;
  jeisuanzhuangtai: any;
};

export async function nongjigongdanAdd(request: NongjigongdanAddRequest): Promise<optionsResoult> {
  return reqPost('/api/NongjigongdanGroupService/nongjigongdanAdd', request);
}

export async function nongjigongdanAdd_GetDefaultValue(request: any) {
  return reqPost('/api/NongjigongdanGroupService/nongjigongdanAdd_GetDefaultValue', request);
}

export async function nongjigongdanAdd_leixing_DataSource(pageParameter: any) {
  return reqGet(
    '/api/NongjigongdanGroupService/NongjigongdanAdd_leixing_DataSource',
    pageParameter,
  );
}

export async function nongjigongdanAdd_chuli_DataSource(pageParameter: any) {
  return reqGet('/api/NongjigongdanGroupService/NongjigongdanAdd_chuli_DataSource', pageParameter);
}

export async function nongjigongdanAdd_zhuangtai_DataSource(pageParameter: any) {
  return reqGet(
    '/api/NongjigongdanGroupService/NongjigongdanAdd_zhuangtai_DataSource',
    pageParameter,
  );
}

export async function nongjigongdanAdd_jeisuanzhuangtai_DataSource(pageParameter: any) {
  return reqGet(
    '/api/NongjigongdanGroupService/NongjigongdanAdd_jeisuanzhuangtai_DataSource',
    pageParameter,
  );
}

type NongjigongdanEditUpdateRequest = {
  id: any;
  beginTime: any;
  miangji: number;
  leixing: any;
  chuli: any;
  zongjia: number;
  zhuangtai: any;
  jeisuanzhuangtai: any;
};

type NongjigongdanEditQueryRequest = {
  id: any;
};

type NongjigongdanEditResponse = {
  id: any;
  beginTime: any;
  miangji: number;
  leixing: any;
  chuli: any;
  zongjia: number;
  zhuangtai: any;
  jeisuanzhuangtai: any;
};

export async function nongjigongdanEdit(
  request: NongjigongdanEditUpdateRequest,
): Promise<optionsResoult> {
  return reqPost('/api/NongjigongdanGroupService/nongjigongdanEdit', request);
}

export async function nongjigongdanEdit_GetValue(
  request: NongjigongdanEditQueryRequest,
): Promise<NongjigongdanEditResponse> {
  return reqPost('/api/NongjigongdanGroupService/nongjigongdanEdit_GetValue', request);
}

export async function nongjigongdanEdit_leixing_DataSource(pageParameter: any) {
  return reqGet(
    '/api/NongjigongdanGroupService/NongjigongdanEdit_leixing_DataSource',
    pageParameter,
  );
}

export async function nongjigongdanEdit_chuli_DataSource(pageParameter: any) {
  return reqGet('/api/NongjigongdanGroupService/NongjigongdanEdit_chuli_DataSource', pageParameter);
}

export async function nongjigongdanEdit_zhuangtai_DataSource(pageParameter: any) {
  return reqGet(
    '/api/NongjigongdanGroupService/NongjigongdanEdit_zhuangtai_DataSource',
    pageParameter,
  );
}

export async function nongjigongdanEdit_jeisuanzhuangtai_DataSource(pageParameter: any) {
  return reqGet(
    '/api/NongjigongdanGroupService/NongjigongdanEdit_jeisuanzhuangtai_DataSource',
    pageParameter,
  );
}

type NongjigongdanDetailQueryRequest = {
  id: any;
};

type NongjigongdanDetailResponse = {
  id: any;
  createtimre: any;
  beginTime: any;
  miangji: number;
  leixing: any;
  chuli: any;
  zongjia: number;
  zhuangtai: any;
  jeisuanzhuangtai: any;
};

export async function nongjigongdanDetail_GetValue(
  request: NongjigongdanDetailQueryRequest,
): Promise<NongjigongdanDetailResponse> {
  return reqPost('/api/NongjigongdanGroupService/nongjigongdanDetail_GetValue', request);
}

export async function nongjigongdanDetail_leixing_DataSource(pageParameter: any) {
  return reqGet(
    '/api/NongjigongdanGroupService/NongjigongdanDetail_leixing_DataSource',
    pageParameter,
  );
}

export async function nongjigongdanDetail_chuli_DataSource(pageParameter: any) {
  return reqGet(
    '/api/NongjigongdanGroupService/NongjigongdanDetail_chuli_DataSource',
    pageParameter,
  );
}

export async function nongjigongdanDetail_zhuangtai_DataSource(pageParameter: any) {
  return reqGet(
    '/api/NongjigongdanGroupService/NongjigongdanDetail_zhuangtai_DataSource',
    pageParameter,
  );
}

export async function nongjigongdanDetail_jeisuanzhuangtai_DataSource(pageParameter: any) {
  return reqGet(
    '/api/NongjigongdanGroupService/NongjigongdanDetail_jeisuanzhuangtai_DataSource',
    pageParameter,
  );
}

type NongjigongdanDeleteRequest = {
  id: any;
};

export async function nongjigongdanDelete(
  request: NongjigongdanDeleteRequest,
): Promise<optionsResoult> {
  return reqPost('/api/NongjigongdanGroupService/nongjigongdanDelete', request);
}

type NongjigongdanSearchRequest = {};

export async function nongjigongdanSearch(
  request: NongjigongdanSearchRequest,
  sort: any,
  filter: any,
) {
  return reqGetByPage('/api/NongjigongdanGroupService/NongjigongdanSearch', request, sort, filter);
}
export async function nongjigongdanSearch_GetDefaultValue(request: any) {
  return reqPost('/api/NongjigongdanGroupService/nongjigongdanSearch_GetDefaultValue', request);
}

export async function nongjigongdanSearch_leixing_View_DataSource(pageParameter: any) {
  return reqGet(
    '/api/NongjigongdanGroupService/NongjigongdanSearch_leixing_View_DataSource',
    pageParameter,
  );
}

export async function nongjigongdanSearch_chuli_View_DataSource(pageParameter: any) {
  return reqGet(
    '/api/NongjigongdanGroupService/NongjigongdanSearch_chuli_View_DataSource',
    pageParameter,
  );
}

export async function nongjigongdanSearch_zhuangtai_View_DataSource(pageParameter: any) {
  return reqGet(
    '/api/NongjigongdanGroupService/NongjigongdanSearch_zhuangtai_View_DataSource',
    pageParameter,
  );
}

export async function nongjigongdanSearch_jeisuanzhuangtai_View_DataSource(pageParameter: any) {
  return reqGet(
    '/api/NongjigongdanGroupService/NongjigongdanSearch_jeisuanzhuangtai_View_DataSource',
    pageParameter,
  );
}
