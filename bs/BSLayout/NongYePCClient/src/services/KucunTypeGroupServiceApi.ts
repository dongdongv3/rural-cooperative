import { reqGet } from '@/utils/requestUtil';
import { reqGetByPage } from '@/utils/requestUtil';
import { reqPost } from '@/utils/requestUtil';
import { optionsResoult } from '@/utils/optionsResoult';
import { optionsResoultStatus } from '@/utils/optionsResoult';

type KucunTypeAddRequest = {
  name: any;
};

export async function kucunTypeAdd(request: KucunTypeAddRequest): Promise<optionsResoult> {
  return reqPost('/api/KucunTypeGroupService/kucunTypeAdd', request);
}

export async function kucunTypeAdd_GetDefaultValue(request: any) {
  return reqPost('/api/KucunTypeGroupService/kucunTypeAdd_GetDefaultValue', request);
}

type KucunTypeDeleteRequest = {
  id: any;
};

export async function kucunTypeDelete(request: KucunTypeDeleteRequest): Promise<optionsResoult> {
  return reqPost('/api/KucunTypeGroupService/kucunTypeDelete', request);
}

type KucunTypeSearchRequest = {};

export async function kucunTypeSearch(request: KucunTypeSearchRequest, sort: any, filter: any) {
  return reqGetByPage('/api/KucunTypeGroupService/KucunTypeSearch', request, sort, filter);
}
export async function kucunTypeSearch_GetDefaultValue(request: any) {
  return reqPost('/api/KucunTypeGroupService/kucunTypeSearch_GetDefaultValue', request);
}
