import { reqGet } from '@/utils/requestUtil';
import { reqGetByPage } from '@/utils/requestUtil';
import { reqPost } from '@/utils/requestUtil';
import { optionsResoult } from '@/utils/optionsResoult';
import { optionsResoultStatus } from '@/utils/optionsResoult';

type NongjifenleiAddRequest = {
  name: any;
};

export async function nongjifenleiAdd(request: NongjifenleiAddRequest): Promise<optionsResoult> {
  return reqPost('/api/NongjifenleiGroupService/nongjifenleiAdd', request);
}

export async function nongjifenleiAdd_GetDefaultValue(request: any) {
  return reqPost('/api/NongjifenleiGroupService/nongjifenleiAdd_GetDefaultValue', request);
}

type NongjifenleiDeleteRequest = {
  id: any;
};

export async function nongjifenleiDelete(
  request: NongjifenleiDeleteRequest,
): Promise<optionsResoult> {
  return reqPost('/api/NongjifenleiGroupService/nongjifenleiDelete', request);
}

type NongjifenleiSearchRequest = {};

export async function nongjifenleiSearch(
  request: NongjifenleiSearchRequest,
  sort: any,
  filter: any,
) {
  return reqGetByPage('/api/NongjifenleiGroupService/NongjifenleiSearch', request, sort, filter);
}
export async function nongjifenleiSearch_GetDefaultValue(request: any) {
  return reqPost('/api/NongjifenleiGroupService/nongjifenleiSearch_GetDefaultValue', request);
}
