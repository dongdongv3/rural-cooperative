import { reqGet } from '@/utils/requestUtil';

export interface responseMode {
  title: string;
  required: boolean;
  options: { key: string; value: string }[];
}

export async function GetInputRequest(): Promise<responseMode> {
  return await reqGet('/api/Init/GetInputRequest', {});
}

export async function PostValue(inputValue: string): Promise<string> {
  return reqGet('/api/Init/SetValue', { value: inputValue });
}
