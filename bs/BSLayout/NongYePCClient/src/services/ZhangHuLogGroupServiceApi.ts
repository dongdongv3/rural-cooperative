import { reqGet } from '@/utils/requestUtil';
import { reqGetByPage } from '@/utils/requestUtil';
import { reqPost } from '@/utils/requestUtil';
import { optionsResoult } from '@/utils/optionsResoult';
import { optionsResoultStatus } from '@/utils/optionsResoult';

type ZhangHuLogSearchSheyuanRequest = {};

export async function zhangHuLogSearchSheyuan(
  request: ZhangHuLogSearchSheyuanRequest,
  sort: any,
  filter: any,
) {
  return reqGetByPage('/api/ZhangHuLogGroupService/ZhangHuLogSearchSheyuan', request, sort, filter);
}
export async function zhangHuLogSearchSheyuan_GetDefaultValue(request: any) {
  return reqPost('/api/ZhangHuLogGroupService/zhangHuLogSearchSheyuan_GetDefaultValue', request);
}

export async function zhangHuLogSearchSheyuan_RefSheyuanId_Where_DataSource(pageParameter: any) {
  return reqGet(
    '/api/ZhangHuLogGroupService/ZhangHuLogSearchSheyuan_RefSheyuanId_Where_DataSource',
    pageParameter,
  );
}

export async function zhangHuLogSearchSheyuan_RefSheyuanId_View_DataSource(pageParameter: any) {
  return reqGet(
    '/api/ZhangHuLogGroupService/ZhangHuLogSearchSheyuan_RefSheyuanId_View_DataSource',
    pageParameter,
  );
}
