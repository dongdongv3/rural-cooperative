import { reqGet } from '@/utils/requestUtil';
import { reqGetByPage } from '@/utils/requestUtil';
import { reqPost } from '@/utils/requestUtil';
import { optionsResoult } from '@/utils/optionsResoult';
import { optionsResoultStatus } from '@/utils/optionsResoult';

type NongjihezuoAddRequest = {
  username: any;
  phoneNumber: any;
  leixing: any;
  xinghao: any;
  qianyuejia: number;
};

export async function nongjihezuoAdd(request: NongjihezuoAddRequest): Promise<optionsResoult> {
  return reqPost('/api/NongjihezuoGroupService/nongjihezuoAdd', request);
}

export async function nongjihezuoAdd_GetDefaultValue(request: any) {
  return reqPost('/api/NongjihezuoGroupService/nongjihezuoAdd_GetDefaultValue', request);
}

export async function nongjihezuoAdd_leixing_DataSource(pageParameter: any) {
  return reqGet('/api/NongjihezuoGroupService/NongjihezuoAdd_leixing_DataSource', pageParameter);
}

type NongjihezuoEditUpdateRequest = {
  id: any;
  username: any;
  phoneNumber: any;
  leixing: any;
  xinghao: any;
  qianyuejia: number;
};

type NongjihezuoEditQueryRequest = {
  id: any;
};

type NongjihezuoEditResponse = {
  id: any;
  username: any;
  phoneNumber: any;
  leixing: any;
  xinghao: any;
  qianyuejia: number;
};

export async function nongjihezuoEdit(
  request: NongjihezuoEditUpdateRequest,
): Promise<optionsResoult> {
  return reqPost('/api/NongjihezuoGroupService/nongjihezuoEdit', request);
}

export async function nongjihezuoEdit_GetValue(
  request: NongjihezuoEditQueryRequest,
): Promise<NongjihezuoEditResponse> {
  return reqPost('/api/NongjihezuoGroupService/nongjihezuoEdit_GetValue', request);
}

export async function nongjihezuoEdit_leixing_DataSource(pageParameter: any) {
  return reqGet('/api/NongjihezuoGroupService/NongjihezuoEdit_leixing_DataSource', pageParameter);
}

type NongjihezuoDetailQueryRequest = {
  id: any;
};

type NongjihezuoDetailResponse = {
  id: any;
  username: any;
  phoneNumber: any;
  leixing: any;
  xinghao: any;
  qianyuejia: number;
};

export async function nongjihezuoDetail_GetValue(
  request: NongjihezuoDetailQueryRequest,
): Promise<NongjihezuoDetailResponse> {
  return reqPost('/api/NongjihezuoGroupService/nongjihezuoDetail_GetValue', request);
}

export async function nongjihezuoDetail_leixing_DataSource(pageParameter: any) {
  return reqGet('/api/NongjihezuoGroupService/NongjihezuoDetail_leixing_DataSource', pageParameter);
}

type NongjihezuoDeleteRequest = {
  id: any;
};

export async function nongjihezuoDelete(
  request: NongjihezuoDeleteRequest,
): Promise<optionsResoult> {
  return reqPost('/api/NongjihezuoGroupService/nongjihezuoDelete', request);
}

type NongjihezuoSearchRequest = {};

export async function nongjihezuoSearch(request: NongjihezuoSearchRequest, sort: any, filter: any) {
  return reqGetByPage('/api/NongjihezuoGroupService/NongjihezuoSearch', request, sort, filter);
}
export async function nongjihezuoSearch_GetDefaultValue(request: any) {
  return reqPost('/api/NongjihezuoGroupService/nongjihezuoSearch_GetDefaultValue', request);
}

export async function nongjihezuoSearch_leixing_View_DataSource(pageParameter: any) {
  return reqGet(
    '/api/NongjihezuoGroupService/NongjihezuoSearch_leixing_View_DataSource',
    pageParameter,
  );
}
