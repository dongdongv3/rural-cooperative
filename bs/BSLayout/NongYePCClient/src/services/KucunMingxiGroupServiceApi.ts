import { reqGet } from '@/utils/requestUtil';
import { reqGetByPage } from '@/utils/requestUtil';
import { reqPost } from '@/utils/requestUtil';
import { optionsResoult } from '@/utils/optionsResoult';
import { optionsResoultStatus } from '@/utils/optionsResoult';

type KucunMingxiAddRequest = {
  wupinName: any;
  count: number;
  danwei: any;
  fenlei: any;
};

export async function kucunMingxiAdd(request: KucunMingxiAddRequest): Promise<optionsResoult> {
  return reqPost('/api/KucunMingxiGroupService/kucunMingxiAdd', request);
}

export async function kucunMingxiAdd_GetDefaultValue(request: any) {
  return reqPost('/api/KucunMingxiGroupService/kucunMingxiAdd_GetDefaultValue', request);
}

export async function kucunMingxiAdd_fenlei_DataSource(pageParameter: any) {
  return reqGet('/api/KucunMingxiGroupService/KucunMingxiAdd_fenlei_DataSource', pageParameter);
}

type KucunMingxiEditUpdateRequest = {
  id: any;
  wupinName: any;
  count: number;
  danwei: any;
  fenlei: any;
};

type KucunMingxiEditQueryRequest = {
  id: any;
};

type KucunMingxiEditResponse = {
  id: any;
  wupinName: any;
  count: number;
  danwei: any;
  fenlei: any;
};

export async function kucunMingxiEdit(
  request: KucunMingxiEditUpdateRequest,
): Promise<optionsResoult> {
  return reqPost('/api/KucunMingxiGroupService/kucunMingxiEdit', request);
}

export async function kucunMingxiEdit_GetValue(
  request: KucunMingxiEditQueryRequest,
): Promise<KucunMingxiEditResponse> {
  return reqPost('/api/KucunMingxiGroupService/kucunMingxiEdit_GetValue', request);
}

export async function kucunMingxiEdit_fenlei_DataSource(pageParameter: any) {
  return reqGet('/api/KucunMingxiGroupService/KucunMingxiEdit_fenlei_DataSource', pageParameter);
}

type KucunMingxiDetailQueryRequest = {
  id: any;
};

type KucunMingxiDetailResponse = {
  id: any;
  wupinName: any;
  count: number;
  danwei: any;
  fenlei: any;
};

export async function kucunMingxiDetail_GetValue(
  request: KucunMingxiDetailQueryRequest,
): Promise<KucunMingxiDetailResponse> {
  return reqPost('/api/KucunMingxiGroupService/kucunMingxiDetail_GetValue', request);
}

export async function kucunMingxiDetail_fenlei_DataSource(pageParameter: any) {
  return reqGet('/api/KucunMingxiGroupService/KucunMingxiDetail_fenlei_DataSource', pageParameter);
}

type KucunMingxiDeleteRequest = {
  id: any;
};

export async function kucunMingxiDelete(
  request: KucunMingxiDeleteRequest,
): Promise<optionsResoult> {
  return reqPost('/api/KucunMingxiGroupService/kucunMingxiDelete', request);
}

type KucunMingxiSearchRequest = {};

export async function kucunMingxiSearch(request: KucunMingxiSearchRequest, sort: any, filter: any) {
  return reqGetByPage('/api/KucunMingxiGroupService/KucunMingxiSearch', request, sort, filter);
}
export async function kucunMingxiSearch_GetDefaultValue(request: any) {
  return reqPost('/api/KucunMingxiGroupService/kucunMingxiSearch_GetDefaultValue', request);
}

export async function kucunMingxiSearch_fenlei_View_DataSource(pageParameter: any) {
  return reqGet(
    '/api/KucunMingxiGroupService/KucunMingxiSearch_fenlei_View_DataSource',
    pageParameter,
  );
}
