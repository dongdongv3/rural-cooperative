import { reqGet } from '@/utils/requestUtil';
import { reqGetByPage } from '@/utils/requestUtil';
import { reqPost } from '@/utils/requestUtil';
import { optionsResoult } from '@/utils/optionsResoult';
import { optionsResoultStatus } from '@/utils/optionsResoult';

type ChurukujiluAddRequest = {
  shengqingren: any;
  approvedUser: any;
  leixing: any;
  fenlei: any;
  wupin: any;
  shuliang: number;
};

export async function churukujiluAdd(request: ChurukujiluAddRequest): Promise<optionsResoult> {
  return reqPost('/api/ChurukujiluGroupService/churukujiluAdd', request);
}

export async function churukujiluAdd_GetDefaultValue(request: any) {
  return reqPost('/api/ChurukujiluGroupService/churukujiluAdd_GetDefaultValue', request);
}

export async function churukujiluAdd_ApprovedUser_DataSource(pageParameter: any) {
  return reqGet(
    '/api/ChurukujiluGroupService/ChurukujiluAdd_ApprovedUser_DataSource',
    pageParameter,
  );
}

export async function churukujiluAdd_leixing_DataSource(pageParameter: any) {
  return reqGet('/api/ChurukujiluGroupService/ChurukujiluAdd_leixing_DataSource', pageParameter);
}

export async function churukujiluAdd_fenlei_DataSource(pageParameter: any) {
  return reqGet('/api/ChurukujiluGroupService/ChurukujiluAdd_fenlei_DataSource', pageParameter);
}

export async function churukujiluAdd_wupin_DataSource(pageParameter: any) {
  return reqGet('/api/ChurukujiluGroupService/ChurukujiluAdd_wupin_DataSource', pageParameter);
}

type ChurukujiluEditUpdateRequest = {
  id: any;
  shengqingren: any;
  approvedUser: any;
  leixing: any;
  fenlei: any;
  wupin: any;
  shuliang: number;
};

type ChurukujiluEditQueryRequest = {
  id: any;
};

type ChurukujiluEditResponse = {
  id: any;
  shengqingren: any;
  approvedUser: any;
  leixing: any;
  fenlei: any;
  wupin: any;
  shuliang: number;
};

export async function churukujiluEdit(
  request: ChurukujiluEditUpdateRequest,
): Promise<optionsResoult> {
  return reqPost('/api/ChurukujiluGroupService/churukujiluEdit', request);
}

export async function churukujiluEdit_GetValue(
  request: ChurukujiluEditQueryRequest,
): Promise<ChurukujiluEditResponse> {
  return reqPost('/api/ChurukujiluGroupService/churukujiluEdit_GetValue', request);
}

export async function churukujiluEdit_ApprovedUser_DataSource(pageParameter: any) {
  return reqGet(
    '/api/ChurukujiluGroupService/ChurukujiluEdit_ApprovedUser_DataSource',
    pageParameter,
  );
}

export async function churukujiluEdit_leixing_DataSource(pageParameter: any) {
  return reqGet('/api/ChurukujiluGroupService/ChurukujiluEdit_leixing_DataSource', pageParameter);
}

export async function churukujiluEdit_fenlei_DataSource(pageParameter: any) {
  return reqGet('/api/ChurukujiluGroupService/ChurukujiluEdit_fenlei_DataSource', pageParameter);
}

export async function churukujiluEdit_wupin_DataSource(pageParameter: any) {
  return reqGet('/api/ChurukujiluGroupService/ChurukujiluEdit_wupin_DataSource', pageParameter);
}

type ChurukujiluDetailQueryRequest = {
  id: any;
};

type ChurukujiluDetailResponse = {
  id: any;
  createtime: any;
  shengqingren: any;
  approvedUser: any;
  leixing: any;
  fenlei: any;
  wupin: any;
  shuliang: number;
};

export async function churukujiluDetail_GetValue(
  request: ChurukujiluDetailQueryRequest,
): Promise<ChurukujiluDetailResponse> {
  return reqPost('/api/ChurukujiluGroupService/churukujiluDetail_GetValue', request);
}

export async function churukujiluDetail_ApprovedUser_DataSource(pageParameter: any) {
  return reqGet(
    '/api/ChurukujiluGroupService/ChurukujiluDetail_ApprovedUser_DataSource',
    pageParameter,
  );
}

export async function churukujiluDetail_leixing_DataSource(pageParameter: any) {
  return reqGet('/api/ChurukujiluGroupService/ChurukujiluDetail_leixing_DataSource', pageParameter);
}

export async function churukujiluDetail_fenlei_DataSource(pageParameter: any) {
  return reqGet('/api/ChurukujiluGroupService/ChurukujiluDetail_fenlei_DataSource', pageParameter);
}

export async function churukujiluDetail_wupin_DataSource(pageParameter: any) {
  return reqGet('/api/ChurukujiluGroupService/ChurukujiluDetail_wupin_DataSource', pageParameter);
}

type ChurukujiluDeleteRequest = {
  id: any;
};

export async function churukujiluDelete(
  request: ChurukujiluDeleteRequest,
): Promise<optionsResoult> {
  return reqPost('/api/ChurukujiluGroupService/churukujiluDelete', request);
}

type ChurukujiluSearchRequest = {};

export async function churukujiluSearch(request: ChurukujiluSearchRequest, sort: any, filter: any) {
  return reqGetByPage('/api/ChurukujiluGroupService/ChurukujiluSearch', request, sort, filter);
}
export async function churukujiluSearch_GetDefaultValue(request: any) {
  return reqPost('/api/ChurukujiluGroupService/churukujiluSearch_GetDefaultValue', request);
}

export async function churukujiluSearch_ApprovedUser_View_DataSource(pageParameter: any) {
  return reqGet(
    '/api/ChurukujiluGroupService/ChurukujiluSearch_ApprovedUser_View_DataSource',
    pageParameter,
  );
}

export async function churukujiluSearch_leixing_View_DataSource(pageParameter: any) {
  return reqGet(
    '/api/ChurukujiluGroupService/ChurukujiluSearch_leixing_View_DataSource',
    pageParameter,
  );
}

export async function churukujiluSearch_fenlei_View_DataSource(pageParameter: any) {
  return reqGet(
    '/api/ChurukujiluGroupService/ChurukujiluSearch_fenlei_View_DataSource',
    pageParameter,
  );
}

export async function churukujiluSearch_wupin_View_DataSource(pageParameter: any) {
  return reqGet(
    '/api/ChurukujiluGroupService/ChurukujiluSearch_wupin_View_DataSource',
    pageParameter,
  );
}
