import { reqGet } from '@/utils/requestUtil';
import { reqGetByPage } from '@/utils/requestUtil';
import { reqPost } from '@/utils/requestUtil';
import { optionsResoult } from '@/utils/optionsResoult';
import { optionsResoultStatus } from '@/utils/optionsResoult';

type GengdiTypeAddRequest = {
  pianquName: any;
};

export async function gengdiTypeAdd(request: GengdiTypeAddRequest): Promise<optionsResoult> {
  return reqPost('/api/GengdiTypeGroupService/gengdiTypeAdd', request);
}

export async function gengdiTypeAdd_GetDefaultValue(request: any) {
  return reqPost('/api/GengdiTypeGroupService/gengdiTypeAdd_GetDefaultValue', request);
}

type GengdiTypeDeleteRequest = {
  id: any;
};

export async function gengdiTypeDelete(request: GengdiTypeDeleteRequest): Promise<optionsResoult> {
  return reqPost('/api/GengdiTypeGroupService/gengdiTypeDelete', request);
}

type GengdiTypeSearchRequest = {};

export async function gengdiTypeSearch(request: GengdiTypeSearchRequest, sort: any, filter: any) {
  return reqGetByPage('/api/GengdiTypeGroupService/GengdiTypeSearch', request, sort, filter);
}
export async function gengdiTypeSearch_GetDefaultValue(request: any) {
  return reqPost('/api/GengdiTypeGroupService/gengdiTypeSearch_GetDefaultValue', request);
}
