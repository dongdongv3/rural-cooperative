import { reqGet } from '@/utils/requestUtil';
import { reqGetByPage } from '@/utils/requestUtil';
import { reqPost } from '@/utils/requestUtil';
import { optionsResoult } from '@/utils/optionsResoult';
import { optionsResoultStatus } from '@/utils/optionsResoult';

type SysUser_AccountAndPasswordAuth1AddSysUserRequest = {
  loginID: any;
  pwd: any;
};

export async function sysUser_AccountAndPasswordAuth1AddSysUser(
  request: SysUser_AccountAndPasswordAuth1AddSysUserRequest,
): Promise<optionsResoult> {
  return reqPost('/api/SysUserGroupService/sysUser_AccountAndPasswordAuth1AddSysUser', request);
}

export async function sysUser_AccountAndPasswordAuth1AddSysUser_GetDefaultValue(request: any) {
  return reqPost(
    '/api/SysUserGroupService/sysUser_AccountAndPasswordAuth1AddSysUser_GetDefaultValue',
    request,
  );
}

export async function sysUser_AccountAndPasswordAuth1AddSysUser_RefSysUserId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_AccountAndPasswordAuth1AddSysUser_RefSysUserId_DataSource',
    pageParameter,
  );
}

type SysUser_AccountAndPasswordAuth1EditSysUserUpdateRequest = {
  id: any;
  loginID: any;
  pwd: any;
};

type SysUser_AccountAndPasswordAuth1EditSysUserQueryRequest = {
  id: any;
};

type SysUser_AccountAndPasswordAuth1EditSysUserResponse = {
  id: any;
  loginID: any;
  pwd: any;
  refSysUserId: any;
};

export async function sysUser_AccountAndPasswordAuth1EditSysUser(
  request: SysUser_AccountAndPasswordAuth1EditSysUserUpdateRequest,
): Promise<optionsResoult> {
  return reqPost('/api/SysUserGroupService/sysUser_AccountAndPasswordAuth1EditSysUser', request);
}

export async function sysUser_AccountAndPasswordAuth1EditSysUser_GetValue(
  request: SysUser_AccountAndPasswordAuth1EditSysUserQueryRequest,
): Promise<SysUser_AccountAndPasswordAuth1EditSysUserResponse> {
  return reqPost(
    '/api/SysUserGroupService/sysUser_AccountAndPasswordAuth1EditSysUser_GetValue',
    request,
  );
}

export async function sysUser_AccountAndPasswordAuth1EditSysUser_RefSysUserId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_AccountAndPasswordAuth1EditSysUser_RefSysUserId_DataSource',
    pageParameter,
  );
}

type SysUser_AccountAndPasswordAuth1DeleteRequest = {
  id: any;
};

export async function sysUser_AccountAndPasswordAuth1Delete(
  request: SysUser_AccountAndPasswordAuth1DeleteRequest,
): Promise<optionsResoult> {
  return reqPost('/api/SysUserGroupService/sysUser_AccountAndPasswordAuth1Delete', request);
}

type SysUser_AccountAndPasswordAuth1DetailQueryRequest = {
  id: any;
};

type SysUser_AccountAndPasswordAuth1DetailResponse = {
  id: any;
  loginID: any;
  pwd: any;
  refSysUserId: any;
};

export async function sysUser_AccountAndPasswordAuth1Detail_GetValue(
  request: SysUser_AccountAndPasswordAuth1DetailQueryRequest,
): Promise<SysUser_AccountAndPasswordAuth1DetailResponse> {
  return reqPost(
    '/api/SysUserGroupService/sysUser_AccountAndPasswordAuth1Detail_GetValue',
    request,
  );
}

export async function sysUser_AccountAndPasswordAuth1Detail_RefSysUserId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_AccountAndPasswordAuth1Detail_RefSysUserId_DataSource',
    pageParameter,
  );
}

type SysUser_AccountAndPasswordAuth1SearchSysUserRequest = {};

export async function sysUser_AccountAndPasswordAuth1SearchSysUser(
  request: SysUser_AccountAndPasswordAuth1SearchSysUserRequest,
  sort: any,
  filter: any,
) {
  return reqGetByPage(
    '/api/SysUserGroupService/SysUser_AccountAndPasswordAuth1SearchSysUser',
    request,
    sort,
    filter,
  );
}
export async function sysUser_AccountAndPasswordAuth1SearchSysUser_GetDefaultValue(request: any) {
  return reqPost(
    '/api/SysUserGroupService/sysUser_AccountAndPasswordAuth1SearchSysUser_GetDefaultValue',
    request,
  );
}

export async function sysUser_AccountAndPasswordAuth1SearchSysUser_RefSysUserId_Where_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_AccountAndPasswordAuth1SearchSysUser_RefSysUserId_Where_DataSource',
    pageParameter,
  );
}

export async function sysUser_AccountAndPasswordAuth1SearchSysUser_RefSysUserId_View_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_AccountAndPasswordAuth1SearchSysUser_RefSysUserId_View_DataSource',
    pageParameter,
  );
}

type SysUser_UserRoleMapAddSysUserRequest = {
  refSysUser_RoleId: any;
};

export async function sysUser_UserRoleMapAddSysUser(
  request: SysUser_UserRoleMapAddSysUserRequest,
): Promise<optionsResoult> {
  return reqPost('/api/SysUserGroupService/sysUser_UserRoleMapAddSysUser', request);
}

export async function sysUser_UserRoleMapAddSysUser_GetDefaultValue(request: any) {
  return reqPost('/api/SysUserGroupService/sysUser_UserRoleMapAddSysUser_GetDefaultValue', request);
}

export async function sysUser_UserRoleMapAddSysUser_RefSysUserId_DataSource(pageParameter: any) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_UserRoleMapAddSysUser_RefSysUserId_DataSource',
    pageParameter,
  );
}

export async function sysUser_UserRoleMapAddSysUser_RefSysUser_RoleId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_UserRoleMapAddSysUser_RefSysUser_RoleId_DataSource',
    pageParameter,
  );
}

type SysUser_UserRoleMapAddSysUser_RoleRequest = {
  refSysUserId: any;
};

export async function sysUser_UserRoleMapAddSysUser_Role(
  request: SysUser_UserRoleMapAddSysUser_RoleRequest,
): Promise<optionsResoult> {
  return reqPost('/api/SysUserGroupService/sysUser_UserRoleMapAddSysUser_Role', request);
}

export async function sysUser_UserRoleMapAddSysUser_Role_GetDefaultValue(request: any) {
  return reqPost(
    '/api/SysUserGroupService/sysUser_UserRoleMapAddSysUser_Role_GetDefaultValue',
    request,
  );
}

export async function sysUser_UserRoleMapAddSysUser_Role_RefSysUserId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_UserRoleMapAddSysUser_Role_RefSysUserId_DataSource',
    pageParameter,
  );
}

export async function sysUser_UserRoleMapAddSysUser_Role_RefSysUser_RoleId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_UserRoleMapAddSysUser_Role_RefSysUser_RoleId_DataSource',
    pageParameter,
  );
}

type SysUser_UserRoleMapEditSysUserUpdateRequest = {
  id: any;
  refSysUser_RoleId: any;
};

type SysUser_UserRoleMapEditSysUserQueryRequest = {
  id: any;
};

type SysUser_UserRoleMapEditSysUserResponse = {
  id: any;
  refSysUserId: any;
  refSysUser_RoleId: any;
};

export async function sysUser_UserRoleMapEditSysUser(
  request: SysUser_UserRoleMapEditSysUserUpdateRequest,
): Promise<optionsResoult> {
  return reqPost('/api/SysUserGroupService/sysUser_UserRoleMapEditSysUser', request);
}

export async function sysUser_UserRoleMapEditSysUser_GetValue(
  request: SysUser_UserRoleMapEditSysUserQueryRequest,
): Promise<SysUser_UserRoleMapEditSysUserResponse> {
  return reqPost('/api/SysUserGroupService/sysUser_UserRoleMapEditSysUser_GetValue', request);
}

export async function sysUser_UserRoleMapEditSysUser_RefSysUserId_DataSource(pageParameter: any) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_UserRoleMapEditSysUser_RefSysUserId_DataSource',
    pageParameter,
  );
}

export async function sysUser_UserRoleMapEditSysUser_RefSysUser_RoleId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_UserRoleMapEditSysUser_RefSysUser_RoleId_DataSource',
    pageParameter,
  );
}

type SysUser_UserRoleMapEditSysUser_RoleUpdateRequest = {
  id: any;
  refSysUserId: any;
};

type SysUser_UserRoleMapEditSysUser_RoleQueryRequest = {
  id: any;
};

type SysUser_UserRoleMapEditSysUser_RoleResponse = {
  id: any;
  refSysUserId: any;
  refSysUser_RoleId: any;
};

export async function sysUser_UserRoleMapEditSysUser_Role(
  request: SysUser_UserRoleMapEditSysUser_RoleUpdateRequest,
): Promise<optionsResoult> {
  return reqPost('/api/SysUserGroupService/sysUser_UserRoleMapEditSysUser_Role', request);
}

export async function sysUser_UserRoleMapEditSysUser_Role_GetValue(
  request: SysUser_UserRoleMapEditSysUser_RoleQueryRequest,
): Promise<SysUser_UserRoleMapEditSysUser_RoleResponse> {
  return reqPost('/api/SysUserGroupService/sysUser_UserRoleMapEditSysUser_Role_GetValue', request);
}

export async function sysUser_UserRoleMapEditSysUser_Role_RefSysUserId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_UserRoleMapEditSysUser_Role_RefSysUserId_DataSource',
    pageParameter,
  );
}

export async function sysUser_UserRoleMapEditSysUser_Role_RefSysUser_RoleId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_UserRoleMapEditSysUser_Role_RefSysUser_RoleId_DataSource',
    pageParameter,
  );
}

type SysUser_UserRoleMapDeleteRequest = {
  id: any;
};

export async function sysUser_UserRoleMapDelete(
  request: SysUser_UserRoleMapDeleteRequest,
): Promise<optionsResoult> {
  return reqPost('/api/SysUserGroupService/sysUser_UserRoleMapDelete', request);
}

type SysUser_UserRoleMapDetailQueryRequest = {
  id: any;
};

type SysUser_UserRoleMapDetailResponse = {
  id: any;
  refSysUserId: any;
  refSysUser_RoleId: any;
};

export async function sysUser_UserRoleMapDetail_GetValue(
  request: SysUser_UserRoleMapDetailQueryRequest,
): Promise<SysUser_UserRoleMapDetailResponse> {
  return reqPost('/api/SysUserGroupService/sysUser_UserRoleMapDetail_GetValue', request);
}

export async function sysUser_UserRoleMapDetail_RefSysUserId_DataSource(pageParameter: any) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_UserRoleMapDetail_RefSysUserId_DataSource',
    pageParameter,
  );
}

export async function sysUser_UserRoleMapDetail_RefSysUser_RoleId_DataSource(pageParameter: any) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_UserRoleMapDetail_RefSysUser_RoleId_DataSource',
    pageParameter,
  );
}

type SysUser_UserRoleMapSearchSysUserRequest = {};

export async function sysUser_UserRoleMapSearchSysUser(
  request: SysUser_UserRoleMapSearchSysUserRequest,
  sort: any,
  filter: any,
) {
  return reqGetByPage(
    '/api/SysUserGroupService/SysUser_UserRoleMapSearchSysUser',
    request,
    sort,
    filter,
  );
}
export async function sysUser_UserRoleMapSearchSysUser_GetDefaultValue(request: any) {
  return reqPost(
    '/api/SysUserGroupService/sysUser_UserRoleMapSearchSysUser_GetDefaultValue',
    request,
  );
}

export async function sysUser_UserRoleMapSearchSysUser_RefSysUserId_Where_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_UserRoleMapSearchSysUser_RefSysUserId_Where_DataSource',
    pageParameter,
  );
}

export async function sysUser_UserRoleMapSearchSysUser_RefSysUserId_View_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_UserRoleMapSearchSysUser_RefSysUserId_View_DataSource',
    pageParameter,
  );
}

export async function sysUser_UserRoleMapSearchSysUser_RefSysUser_RoleId_View_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_UserRoleMapSearchSysUser_RefSysUser_RoleId_View_DataSource',
    pageParameter,
  );
}

type SysUser_UserRoleMapSearchSysUser_RoleRequest = {};

export async function sysUser_UserRoleMapSearchSysUser_Role(
  request: SysUser_UserRoleMapSearchSysUser_RoleRequest,
  sort: any,
  filter: any,
) {
  return reqGetByPage(
    '/api/SysUserGroupService/SysUser_UserRoleMapSearchSysUser_Role',
    request,
    sort,
    filter,
  );
}
export async function sysUser_UserRoleMapSearchSysUser_Role_GetDefaultValue(request: any) {
  return reqPost(
    '/api/SysUserGroupService/sysUser_UserRoleMapSearchSysUser_Role_GetDefaultValue',
    request,
  );
}

export async function sysUser_UserRoleMapSearchSysUser_Role_RefSysUser_RoleId_Where_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_UserRoleMapSearchSysUser_Role_RefSysUser_RoleId_Where_DataSource',
    pageParameter,
  );
}

export async function sysUser_UserRoleMapSearchSysUser_Role_RefSysUserId_View_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_UserRoleMapSearchSysUser_Role_RefSysUserId_View_DataSource',
    pageParameter,
  );
}

export async function sysUser_UserRoleMapSearchSysUser_Role_RefSysUser_RoleId_View_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_UserRoleMapSearchSysUser_Role_RefSysUser_RoleId_View_DataSource',
    pageParameter,
  );
}

type SysUser_RoleAuthMapAddSysUser_RoleRequest = {
  refSysUser_AuthId: any;
};

export async function sysUser_RoleAuthMapAddSysUser_Role(
  request: SysUser_RoleAuthMapAddSysUser_RoleRequest,
): Promise<optionsResoult> {
  return reqPost('/api/SysUserGroupService/sysUser_RoleAuthMapAddSysUser_Role', request);
}

export async function sysUser_RoleAuthMapAddSysUser_Role_GetDefaultValue(request: any) {
  return reqPost(
    '/api/SysUserGroupService/sysUser_RoleAuthMapAddSysUser_Role_GetDefaultValue',
    request,
  );
}

export async function sysUser_RoleAuthMapAddSysUser_Role_RefSysUser_RoleId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_RoleAuthMapAddSysUser_Role_RefSysUser_RoleId_DataSource',
    pageParameter,
  );
}

export async function sysUser_RoleAuthMapAddSysUser_Role_RefSysUser_AuthId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_RoleAuthMapAddSysUser_Role_RefSysUser_AuthId_DataSource',
    pageParameter,
  );
}

type SysUser_RoleAuthMapAddSysUser_AuthRequest = {
  refSysUser_RoleId: any;
};

export async function sysUser_RoleAuthMapAddSysUser_Auth(
  request: SysUser_RoleAuthMapAddSysUser_AuthRequest,
): Promise<optionsResoult> {
  return reqPost('/api/SysUserGroupService/sysUser_RoleAuthMapAddSysUser_Auth', request);
}

export async function sysUser_RoleAuthMapAddSysUser_Auth_GetDefaultValue(request: any) {
  return reqPost(
    '/api/SysUserGroupService/sysUser_RoleAuthMapAddSysUser_Auth_GetDefaultValue',
    request,
  );
}

export async function sysUser_RoleAuthMapAddSysUser_Auth_RefSysUser_RoleId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_RoleAuthMapAddSysUser_Auth_RefSysUser_RoleId_DataSource',
    pageParameter,
  );
}

export async function sysUser_RoleAuthMapAddSysUser_Auth_RefSysUser_AuthId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_RoleAuthMapAddSysUser_Auth_RefSysUser_AuthId_DataSource',
    pageParameter,
  );
}

type SysUser_RoleAuthMapEditSysUser_RoleUpdateRequest = {
  id: any;
  refSysUser_AuthId: any;
};

type SysUser_RoleAuthMapEditSysUser_RoleQueryRequest = {
  id: any;
};

type SysUser_RoleAuthMapEditSysUser_RoleResponse = {
  id: any;
  refSysUser_RoleId: any;
  refSysUser_AuthId: any;
};

export async function sysUser_RoleAuthMapEditSysUser_Role(
  request: SysUser_RoleAuthMapEditSysUser_RoleUpdateRequest,
): Promise<optionsResoult> {
  return reqPost('/api/SysUserGroupService/sysUser_RoleAuthMapEditSysUser_Role', request);
}

export async function sysUser_RoleAuthMapEditSysUser_Role_GetValue(
  request: SysUser_RoleAuthMapEditSysUser_RoleQueryRequest,
): Promise<SysUser_RoleAuthMapEditSysUser_RoleResponse> {
  return reqPost('/api/SysUserGroupService/sysUser_RoleAuthMapEditSysUser_Role_GetValue', request);
}

export async function sysUser_RoleAuthMapEditSysUser_Role_RefSysUser_RoleId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_RoleAuthMapEditSysUser_Role_RefSysUser_RoleId_DataSource',
    pageParameter,
  );
}

export async function sysUser_RoleAuthMapEditSysUser_Role_RefSysUser_AuthId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_RoleAuthMapEditSysUser_Role_RefSysUser_AuthId_DataSource',
    pageParameter,
  );
}

type SysUser_RoleAuthMapEditSysUser_AuthUpdateRequest = {
  id: any;
  refSysUser_RoleId: any;
};

type SysUser_RoleAuthMapEditSysUser_AuthQueryRequest = {
  id: any;
};

type SysUser_RoleAuthMapEditSysUser_AuthResponse = {
  id: any;
  refSysUser_RoleId: any;
  refSysUser_AuthId: any;
};

export async function sysUser_RoleAuthMapEditSysUser_Auth(
  request: SysUser_RoleAuthMapEditSysUser_AuthUpdateRequest,
): Promise<optionsResoult> {
  return reqPost('/api/SysUserGroupService/sysUser_RoleAuthMapEditSysUser_Auth', request);
}

export async function sysUser_RoleAuthMapEditSysUser_Auth_GetValue(
  request: SysUser_RoleAuthMapEditSysUser_AuthQueryRequest,
): Promise<SysUser_RoleAuthMapEditSysUser_AuthResponse> {
  return reqPost('/api/SysUserGroupService/sysUser_RoleAuthMapEditSysUser_Auth_GetValue', request);
}

export async function sysUser_RoleAuthMapEditSysUser_Auth_RefSysUser_RoleId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_RoleAuthMapEditSysUser_Auth_RefSysUser_RoleId_DataSource',
    pageParameter,
  );
}

export async function sysUser_RoleAuthMapEditSysUser_Auth_RefSysUser_AuthId_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_RoleAuthMapEditSysUser_Auth_RefSysUser_AuthId_DataSource',
    pageParameter,
  );
}

type SysUser_RoleAuthMapDeleteRequest = {
  id: any;
};

export async function sysUser_RoleAuthMapDelete(
  request: SysUser_RoleAuthMapDeleteRequest,
): Promise<optionsResoult> {
  return reqPost('/api/SysUserGroupService/sysUser_RoleAuthMapDelete', request);
}

type SysUser_RoleAuthMapDetailQueryRequest = {
  id: any;
};

type SysUser_RoleAuthMapDetailResponse = {
  id: any;
  refSysUser_RoleId: any;
  refSysUser_AuthId: any;
};

export async function sysUser_RoleAuthMapDetail_GetValue(
  request: SysUser_RoleAuthMapDetailQueryRequest,
): Promise<SysUser_RoleAuthMapDetailResponse> {
  return reqPost('/api/SysUserGroupService/sysUser_RoleAuthMapDetail_GetValue', request);
}

export async function sysUser_RoleAuthMapDetail_RefSysUser_RoleId_DataSource(pageParameter: any) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_RoleAuthMapDetail_RefSysUser_RoleId_DataSource',
    pageParameter,
  );
}

export async function sysUser_RoleAuthMapDetail_RefSysUser_AuthId_DataSource(pageParameter: any) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_RoleAuthMapDetail_RefSysUser_AuthId_DataSource',
    pageParameter,
  );
}

type SysUser_RoleAuthMapSearchSysUser_RoleRequest = {};

export async function sysUser_RoleAuthMapSearchSysUser_Role(
  request: SysUser_RoleAuthMapSearchSysUser_RoleRequest,
  sort: any,
  filter: any,
) {
  return reqGetByPage(
    '/api/SysUserGroupService/SysUser_RoleAuthMapSearchSysUser_Role',
    request,
    sort,
    filter,
  );
}
export async function sysUser_RoleAuthMapSearchSysUser_Role_GetDefaultValue(request: any) {
  return reqPost(
    '/api/SysUserGroupService/sysUser_RoleAuthMapSearchSysUser_Role_GetDefaultValue',
    request,
  );
}

export async function sysUser_RoleAuthMapSearchSysUser_Role_RefSysUser_RoleId_Where_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_RoleAuthMapSearchSysUser_Role_RefSysUser_RoleId_Where_DataSource',
    pageParameter,
  );
}

export async function sysUser_RoleAuthMapSearchSysUser_Role_RefSysUser_RoleId_View_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_RoleAuthMapSearchSysUser_Role_RefSysUser_RoleId_View_DataSource',
    pageParameter,
  );
}

export async function sysUser_RoleAuthMapSearchSysUser_Role_RefSysUser_AuthId_View_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_RoleAuthMapSearchSysUser_Role_RefSysUser_AuthId_View_DataSource',
    pageParameter,
  );
}

type SysUser_RoleAuthMapSearchSysUser_AuthRequest = {};

export async function sysUser_RoleAuthMapSearchSysUser_Auth(
  request: SysUser_RoleAuthMapSearchSysUser_AuthRequest,
  sort: any,
  filter: any,
) {
  return reqGetByPage(
    '/api/SysUserGroupService/SysUser_RoleAuthMapSearchSysUser_Auth',
    request,
    sort,
    filter,
  );
}
export async function sysUser_RoleAuthMapSearchSysUser_Auth_GetDefaultValue(request: any) {
  return reqPost(
    '/api/SysUserGroupService/sysUser_RoleAuthMapSearchSysUser_Auth_GetDefaultValue',
    request,
  );
}

export async function sysUser_RoleAuthMapSearchSysUser_Auth_RefSysUser_AuthId_Where_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_RoleAuthMapSearchSysUser_Auth_RefSysUser_AuthId_Where_DataSource',
    pageParameter,
  );
}

export async function sysUser_RoleAuthMapSearchSysUser_Auth_RefSysUser_RoleId_View_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_RoleAuthMapSearchSysUser_Auth_RefSysUser_RoleId_View_DataSource',
    pageParameter,
  );
}

export async function sysUser_RoleAuthMapSearchSysUser_Auth_RefSysUser_AuthId_View_DataSource(
  pageParameter: any,
) {
  return reqGet(
    '/api/SysUserGroupService/SysUser_RoleAuthMapSearchSysUser_Auth_RefSysUser_AuthId_View_DataSource',
    pageParameter,
  );
}

type SysUser_AuthAddRequest = {
  authName: any;
};

export async function sysUser_AuthAdd(request: SysUser_AuthAddRequest): Promise<optionsResoult> {
  return reqPost('/api/SysUserGroupService/sysUser_AuthAdd', request);
}

export async function sysUser_AuthAdd_GetDefaultValue(request: any) {
  return reqPost('/api/SysUserGroupService/sysUser_AuthAdd_GetDefaultValue', request);
}

type SysUser_AuthEditUpdateRequest = {
  id: any;
  authName: any;
};

type SysUser_AuthEditQueryRequest = {
  id: any;
};

type SysUser_AuthEditResponse = {
  id: any;
  authName: any;
};

export async function sysUser_AuthEdit(
  request: SysUser_AuthEditUpdateRequest,
): Promise<optionsResoult> {
  return reqPost('/api/SysUserGroupService/sysUser_AuthEdit', request);
}

export async function sysUser_AuthEdit_GetValue(
  request: SysUser_AuthEditQueryRequest,
): Promise<SysUser_AuthEditResponse> {
  return reqPost('/api/SysUserGroupService/sysUser_AuthEdit_GetValue', request);
}

type SysUser_AuthDeleteRequest = {
  id: any;
};

export async function sysUser_AuthDelete(
  request: SysUser_AuthDeleteRequest,
): Promise<optionsResoult> {
  return reqPost('/api/SysUserGroupService/sysUser_AuthDelete', request);
}

type SysUser_AuthDetailQueryRequest = {
  id: any;
};

type SysUser_AuthDetailResponse = {
  id: any;
  authName: any;
};

export async function sysUser_AuthDetail_GetValue(
  request: SysUser_AuthDetailQueryRequest,
): Promise<SysUser_AuthDetailResponse> {
  return reqPost('/api/SysUserGroupService/sysUser_AuthDetail_GetValue', request);
}

type SysUser_AuthSearchRequest = {};

export async function sysUser_AuthSearch(
  request: SysUser_AuthSearchRequest,
  sort: any,
  filter: any,
) {
  return reqGetByPage('/api/SysUserGroupService/SysUser_AuthSearch', request, sort, filter);
}
export async function sysUser_AuthSearch_GetDefaultValue(request: any) {
  return reqPost('/api/SysUserGroupService/sysUser_AuthSearch_GetDefaultValue', request);
}

type SysUser_RoleAddRequest = {
  roleName: any;
};

export async function sysUser_RoleAdd(request: SysUser_RoleAddRequest): Promise<optionsResoult> {
  return reqPost('/api/SysUserGroupService/sysUser_RoleAdd', request);
}

export async function sysUser_RoleAdd_GetDefaultValue(request: any) {
  return reqPost('/api/SysUserGroupService/sysUser_RoleAdd_GetDefaultValue', request);
}

type SysUser_RoleEditUpdateRequest = {
  id: any;
  roleName: any;
};

type SysUser_RoleEditQueryRequest = {
  id: any;
};

type SysUser_RoleEditResponse = {
  id: any;
  roleName: any;
};

export async function sysUser_RoleEdit(
  request: SysUser_RoleEditUpdateRequest,
): Promise<optionsResoult> {
  return reqPost('/api/SysUserGroupService/sysUser_RoleEdit', request);
}

export async function sysUser_RoleEdit_GetValue(
  request: SysUser_RoleEditQueryRequest,
): Promise<SysUser_RoleEditResponse> {
  return reqPost('/api/SysUserGroupService/sysUser_RoleEdit_GetValue', request);
}

type SysUser_RoleDeleteRequest = {
  id: any;
};

export async function sysUser_RoleDelete(
  request: SysUser_RoleDeleteRequest,
): Promise<optionsResoult> {
  return reqPost('/api/SysUserGroupService/sysUser_RoleDelete', request);
}

type SysUser_RoleDetailQueryRequest = {
  id: any;
};

type SysUser_RoleDetailResponse = {
  id: any;
  roleName: any;
};

export async function sysUser_RoleDetail_GetValue(
  request: SysUser_RoleDetailQueryRequest,
): Promise<SysUser_RoleDetailResponse> {
  return reqPost('/api/SysUserGroupService/sysUser_RoleDetail_GetValue', request);
}

type SysUser_RoleSearchRequest = {};

export async function sysUser_RoleSearch(
  request: SysUser_RoleSearchRequest,
  sort: any,
  filter: any,
) {
  return reqGetByPage('/api/SysUserGroupService/SysUser_RoleSearch', request, sort, filter);
}
export async function sysUser_RoleSearch_GetDefaultValue(request: any) {
  return reqPost('/api/SysUserGroupService/sysUser_RoleSearch_GetDefaultValue', request);
}

type SysUserAddRequest = {
  id: any;
  userName: any;
};

export async function sysUserAdd(request: SysUserAddRequest): Promise<optionsResoult> {
  return reqPost('/api/SysUserGroupService/sysUserAdd', request);
}

export async function sysUserAdd_GetDefaultValue(request: any) {
  return reqPost('/api/SysUserGroupService/sysUserAdd_GetDefaultValue', request);
}

type SysUserEditUpdateRequest = {
  id: any;
  userName: any;
};

type SysUserEditQueryRequest = {
  id: any;
};

type SysUserEditResponse = {
  id: any;
  userName: any;
};

export async function sysUserEdit(request: SysUserEditUpdateRequest): Promise<optionsResoult> {
  return reqPost('/api/SysUserGroupService/sysUserEdit', request);
}

export async function sysUserEdit_GetValue(
  request: SysUserEditQueryRequest,
): Promise<SysUserEditResponse> {
  return reqPost('/api/SysUserGroupService/sysUserEdit_GetValue', request);
}

type SysUserDeleteRequest = {
  id: any;
};

export async function sysUserDelete(request: SysUserDeleteRequest): Promise<optionsResoult> {
  return reqPost('/api/SysUserGroupService/sysUserDelete', request);
}

type SysUserDetailQueryRequest = {
  id: any;
};

type SysUserDetailResponse = {
  id: any;
  userName: any;
};

export async function sysUserDetail_GetValue(
  request: SysUserDetailQueryRequest,
): Promise<SysUserDetailResponse> {
  return reqPost('/api/SysUserGroupService/sysUserDetail_GetValue', request);
}

type SysUserSearchRequest = {};

export async function sysUserSearch(request: SysUserSearchRequest, sort: any, filter: any) {
  return reqGetByPage('/api/SysUserGroupService/SysUserSearch', request, sort, filter);
}
export async function sysUserSearch_GetDefaultValue(request: any) {
  return reqPost('/api/SysUserGroupService/sysUserSearch_GetDefaultValue', request);
}
