import React, { useEffect, useRef } from 'react';
import classNames from 'classnames';
import { Chart } from '@antv/g2';

export interface ChartViewProps {
  prefixCls?: string;
  className?: string;
  style?: React.CSSProperties;
  spec: any;
}

export const ChartView: React.FC<ChartViewProps> = ({
  prefixCls = 'antvsite-rc-chartview',
  className,
  style,
  spec,
  ...restProps
}) => {
  const chart = React.useRef<any>(null);
  const container = React.useRef<any>(null);
  const compClassName = classNames(`${prefixCls}`, className);

  const height = style?.height || 200;
  const width = style?.width || '100%';

  useEffect(() => {
    if (!chart.current) {
      chart.current = new Chart({
        theme: 'classic',
        container: container.current,
        autoFit: true,
      });
    }
    chart.current.options(spec);
    chart.current.render();
  });

  return (
    <div
      {...restProps}
      className={compClassName}
      ref={container}
      style={{ ...style, width, height, margin: 'auto' }}
    />
  );
};
