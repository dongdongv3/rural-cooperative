import { Tree } from 'antd';
import { useModel } from 'umi';

/**
 * 通过tree形式展示对象的信息
 */
interface ItemObjViewOptions {
  value?: any;
}

const GetTreeData = function (value: any, title?: string, key?: string) {
  const output: any = {};
  const titlestart = title ? title : '';

  if (!value) {
    output.title = titlestart + ' : 空';
    output.key = key + output.title;
    output.children = [];
    return output;
  }
  if (value.length) {
    //集合
    output.title = titlestart + ':[' + value.length + ']';
    output.key = output.title;
    output.children = [];
    value.forEach((one: any, index: number) => {
      const suboutput: any = {};
      {
        suboutput.title = index;
        suboutput.key = one.id;
        suboutput.children = [];
        //console.log(typedata);
        for (let tmp in one) {
          let item: any = {};
          if (typeof one[tmp] === typeof {}) {
            item = GetTreeData(one[tmp], tmp, title + tmp);
          } else {
            item = { title: `${tmp}:${one[tmp]}` };
          }
          item.key = key + item.title;
          suboutput.children.push(item);
        }
      }
      output.children.push(suboutput);
    });
    return output;
  } else {
    //属性
    output.title = titlestart + ':object';
    output.key = output.title;
    output.children = [];
    for (let tmp in value) {
      let item: any = {};
      if (typeof value[tmp] === typeof {}) {
        item = GetTreeData(value[tmp], tmp, title + tmp);
      } else {
        item = { title: `${tmp}:${value[tmp]}` };
      }
      item.key = key + item.title;
      output.children.push(item);
    }
    return output;
  }
};

const ItemObjView: React.FC<ItemObjViewOptions> = ({ value }) => {
  const { initialState } = useModel('@@initialState');
  const treedata = GetTreeData(value);
  return <Tree treeData={[treedata]} showLine={true}></Tree>;
  //return <span>abc</span>;
  //defaultExpandAll
};

export default ItemObjView;
