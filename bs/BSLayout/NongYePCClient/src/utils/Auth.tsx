import { mgr } from '../../config/oidc';
import { useModel } from 'umi';
import { Spin } from 'antd';
import { Alert } from 'antd';

interface AuthProperty {
  children: any;
  auths?: string[];
  roles?: string[];
  [property: string]: any;
}

//obj property have tag any
const haveAny = function (obj: any, tag: string[]) {
  for (let i = 0; i < tag.length; i++) {
    const one = tag[i];
    if (obj[one]) {
      return true;
    }
  }
  return false;
};

//认证包裹组件，被包裹的组件需要提供认证
export default (props: AuthProperty) => {
  const { initialState, loading } = useModel('@@initialState');
  if (!initialState?.isLogin) return null;
  if ((props.auths && props.auths.length > 0) || (props.roles && props.children.length > 0)) {
    //需要验证授权
    let successed = false;
    let errormessage = '';
    //验证授权的过程 begin
    if (props.auths && props.auths.length > 0) {
      //权限模式
      successed = haveAny(initialState.currentUser?.auths, props.auths);
      errormessage = '缺少权限：' + props.auths;
    } else if (props.roles && props.roles.length > 0) {
      //角色模式
      successed = haveAny(initialState.currentUser?.roles, props.roles);
      errormessage = '缺少角色：' + props.roles;
    }
    //验证授权的过程 end
    if (!successed) {
      //授权验证失败
      return null; //<Alert message="授权验证失败" description={errormessage} type="error" />;
    } else {
      //授权验证成功,返回成功
      return props.children;
    }
  } else {
    return props.children;
  }
};
