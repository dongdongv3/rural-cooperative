import React, { useState } from 'react';
import ModelLink from './ModelLink';
import ProForm from '@ant-design/pro-form';
import { Space } from 'antd';
import ReactJson from 'react-json-view';

const JsonObject = ({ value, onChange, children, ...other }) => {
  const [objvalue, setObjvalue] = useState(value || {});
  const save = (v) => {
    setObjvalue(v);
    if (onChange) {
      onChange(v);
    }
  };
  return (
    <Space>
      <ReactJson
        src={objvalue}
        displayDataTypes={false}
        name={null}
        collapsed={0}
        enableClipboard={false}
        iconStyle="square"
      />
      <ModelLink
        type="primary"
        buttonName="设置"
        modelTitle={'设置:'}
        BodyContent={({ onHideModal }) => {
          return (
            <ProForm
              onFinish={(e) => {
                save(e);
                onHideModal();
              }}
              initialValues={objvalue}
            >
              {children}
            </ProForm>
          );
        }}
        {...other}
      ></ModelLink>
    </Space>
  );
};

export const ProFormJsonObject: React.FC<{}> = ({ ...props }) => {
  return (
    <ProForm.Item {...props}>
      <JsonObject {...props} />
    </ProForm.Item>
  );
};
