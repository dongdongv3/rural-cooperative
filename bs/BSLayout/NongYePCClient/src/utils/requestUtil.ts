import { request } from 'umi';

const systemTypeName = eval('systemType');

const GetObjProperty = function (obj: any): string[] {
  const list: string[] = [];
  for (const name in obj) {
    list.push(name);
  }
  return list;
};

export async function reqBase(url: string, method: string, otherOptions: any) {
  if (systemTypeName == 'csClient') {
    const systemCall = eval('systemService');
    const output = await systemCall.callApi(url, otherOptions.data);
    return output;
  } else {
    const options = otherOptions;
    options.method = method;
    const output = await request(url, options);
    return output.data;
  }
}

const convertToQueryString = (propertyName, obj) => {
  let output = {};
  if (typeof obj == typeof {}) {
    for (let i in obj) {
      let xp = '';
      if (propertyName) {
        xp = propertyName + '.' + i;
      } else {
        xp = i;
      }
      if (!obj[i]) {
        output[xp] = obj[i];
      } else if (obj[i].length && typeof obj[i] == typeof {}) {
        for (let e = 0; e < obj[i].length; e++) {
          const xpp = xp + `[${e}]`;
          if (typeof obj[i][e] == typeof {}) {
            output = { ...output, ...convertToQueryString(xpp, obj[i][e]) };
          } else {
            output[xpp] = obj[i][e];
          }
        }
      } else if (typeof obj[i] == typeof {}) {
        output = { ...output, ...convertToQueryString(xp, obj[i]) };
      } else {
        output[xp] = obj[i];
      }
    }
  }
  return output;
};

export async function reqGet(url: string, queryData: any) {
  return reqBase(url, 'GET', { params: convertToQueryString('', queryData) });
}

export async function reqPost(url: string, postData: any) {
  return reqBase(url, 'POST', { data: postData });
}

export async function reqGetByPage(url: string, params: any, sort: any, filter: any) {
  const sortobj: any = {};
  const propertylist = GetObjProperty(sort);
  console.dir(propertylist);
  if (propertylist.length == 1) {
    sortobj['SortName'] = propertylist[0];
    sortobj['SortType'] = sort[propertylist[0]].replace('end', '');
  }
  return reqPost(url, { ...params, pageIndex: params.current, ...sortobj });
}
