import { optionsResoult } from './optionsResoult';
import { optionsResoultStatus } from './optionsResoult';
import { Alert } from 'antd';
import { Button } from 'antd';
import { Result } from 'antd';
import { Space } from 'antd';
import { Card } from 'antd';
import { PageContainer } from '@ant-design/pro-layout';
import { useState } from 'react';
import { useHistory, useLocation } from 'umi';

interface ResoultWapperProperty {
  onFinished?: () => void;
  [property: string]: any;
}

//高阶组件，负责包装一层成功或失败ui的显示效果，用于操作，会像内部组件提供setResoult函数用户设置数据加载状态
const ResoultWapper = (Sub: any, title?: string) => {
  const tmp: React.FC<ResoultWapperProperty> = ({ onFinished, ...other }) => {
    const [resoult, setResoult] = useState<optionsResoult | undefined>(undefined);
    const history = useHistory();
    const location = useLocation();
    const clickClose = () => {
      if (onFinished) {
        onFinished();
      }
      setResoult(undefined);
      if (location.query.successGoto && location.query.successGoto != 'undefined') {
        const gobackCount = parseInt(location.query.successGoto);
        if (isNaN(gobackCount)) {
          //不是数字，直接跳转
          history.push(location.query.successGoto);
        } else {
          //是后退的数字
          history.go(-gobackCount);
        }
      } else {
        history.goBack();
      }
    };
    let body = undefined;
    if (resoult && resoult.status == optionsResoultStatus.succeed) {
      body = (
        <Result
          title={resoult.message}
          status="success"
          extra={[
            <Button type="primary" key="console" onClick={clickClose}>
              关闭
            </Button>,
          ]}
        ></Result>
      );
    } else {
      body = (
        <Space direction="vertical" style={{ display: 'flex' }}>
          <Sub setResoult={setResoult} {...other}></Sub>
          {resoult && resoult.status == optionsResoultStatus.failed && (
            <Alert showIcon type="error" message={resoult.message}></Alert>
          )}
        </Space>
      );
    }
    return (
      <PageContainer title={title}>
        <Card>{body}</Card>
      </PageContainer>
    );
  };
  return tmp;
};

export default ResoultWapper;
