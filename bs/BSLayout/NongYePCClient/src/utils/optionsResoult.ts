export enum optionsResoultStatus {
  succeed = 1,
  failed = 2,
}

export interface optionsResoult {
  status: optionsResoultStatus;
  errorCode?: number;
  message: string;
}
