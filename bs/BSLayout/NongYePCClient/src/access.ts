/**
 * @see https://umijs.org/zh-CN/plugins/plugin-access
 * */
import routeData from '../config/routes';

//obj property have tag any
const haveAny = function (obj: any, tag: string[]) {
  for (let i = 0; i < tag.length; i++) {
    const one = tag[i];
    if (obj[one]) {
      return true;
    }
  }
  return false;
};

export default function access(initialState: { currentUser?: any; isLogin: boolean }) {
  const { currentUser } = initialState ?? {};
  const list = routeData.filter((x) => x.access && x.path);
  let output: any = {};
  list.forEach((o) => {
    let tmp = false;
    if (o.haveAuth && initialState.isLogin) {
      let authtmp = true,
        roletmp = true;
      if (o.auths && o.auths.length > 0) {
        authtmp = haveAny(initialState.currentUser?.auths, o.auths);
      }
      if (o.roles && o.roles.length > 0) {
        roletmp = haveAny(initialState.currentUser?.roles, o.roles);
      }
      tmp = authtmp && roletmp;
    }
    output[o.access] = tmp;
  });
  console.log('access', output);
  return output;
}
