import { useModel } from 'umi';
import { mgr } from '../../config/oidc';
import { Spin, Alert } from 'antd';

//obj property have tag any
const haveAny = function (obj: any, tag: string[]) {
  for (let i = 0; i < tag.length; i++) {
    const one = tag[i];
    if (obj[one]) {
      return true;
    }
  }
  return false;
};

//认证包裹组件，被包裹的组件需要提供认证
export default (props: any) => {
  const { initialState, loading } = useModel('@@initialState');
  if (!props) {
    return <Spin size="large" tip="错误" />;
  }
  if (loading) {
    return <Spin size="large" tip="正在获取认证数据" />;
  } else {
    let haveAuth = false; //是否需要认证
    let haveAuthN = false; //是否需要验证授权

    if (props.route.haveAuth) {
      haveAuth = true; //需要认证
      if (props.route.roles?.length > 0 || props.route.auths?.length > 0) {
        haveAuthN = true; //需要验证授权
      }
    }

    if (!haveAuth) {
      //不需要认证，返回成功
      return props.children;
    } else {
      //需要认证
      if (!initialState?.isLogin) {
        //没有认证成功,去登录
        if (confirm('当前页面需要认证，请确认是否登录账户')) {
          mgr.signinRedirect();
          return <Spin size="large" tip="正在跳转到登录页面" />;
        } else {
          return <Alert message="认证错误" description="放弃登录" type="error" />;
        }
      } else {
        //认证成功
        if (!haveAuthN) {
          //不需要授权,返回成功
          return props.children;
        } else {
          //需要验证授权
          let successed = false;
          let errormessage = '';
          //验证授权的过程 begin
          if (props.route.auths && props.route.auths.length > 0) {
            //权限模式
            successed = haveAny(initialState.currentUser?.auths, props.route.auths);
            errormessage = '缺少权限：' + props.route.auths;
          } else if (props.route.roles && props.route.roles.length > 0) {
            //角色模式
            successed = haveAny(initialState.currentUser?.roles, props.route.roles);
            errormessage = '缺少角色：' + props.route.roles;
          }
          //验证授权的过程 end

          if (!successed) {
            //授权验证失败
            return <Alert message="授权验证失败" description={errormessage} type="error" />;
          } else {
            //授权验证成功,返回成功
            return props.children;
          }
        }
      }
    }
  }
};
