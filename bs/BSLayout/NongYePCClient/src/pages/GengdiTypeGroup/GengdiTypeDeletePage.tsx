import GengdiTypeDeleteCom from '@/components/GengdiTypeGroup/GengdiTypeDeleteCom';
import { useLocation } from 'umi';

const gengdiTypeDeleteCom = () => {
  const location = useLocation();
  return <GengdiTypeDeleteCom params={location.query}></GengdiTypeDeleteCom>;
};

export default gengdiTypeDeleteCom;
