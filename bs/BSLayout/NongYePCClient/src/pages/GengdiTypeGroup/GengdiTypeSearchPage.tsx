import GengdiTypeSearchCom from '@/components/GengdiTypeGroup/GengdiTypeSearchCom';
import { useLocation } from 'umi';

const gengdiTypeSearchCom = () => {
  const location = useLocation();
  return <GengdiTypeSearchCom params={location.query}></GengdiTypeSearchCom>;
};

export default gengdiTypeSearchCom;
