import GengdiTypeAddCom from '@/components/GengdiTypeGroup/GengdiTypeAddCom';
import { useLocation } from 'umi';

const gengdiTypeAddCom = () => {
  const location = useLocation();
  return <GengdiTypeAddCom params={location.query}></GengdiTypeAddCom>;
};

export default gengdiTypeAddCom;
