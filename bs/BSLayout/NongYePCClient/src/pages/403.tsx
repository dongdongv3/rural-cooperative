import { Button, Result, Space } from 'antd';
import { mgr } from '../../config/oidc';
import React from 'react';
import { history } from 'umi';
import { useModel } from 'umi';

const UnAccessiblePage: React.FC = () => {
  const { initialState } = useModel('@@initialState');
  return (
    <Result
      status="403"
      title="403"
      subTitle="无权访问"
      extra={
        <>
          <Space>
            <Button onClick={() => history.push('/')}>回到首页</Button>
            {initialState.isLogin || (
              <Button type="primary" onClick={() => mgr.signinRedirect()}>
                登录
              </Button>
            )}
          </Space>
        </>
      }
    />
  );
};

export default UnAccessiblePage;
