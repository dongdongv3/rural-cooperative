import SysUserSearchCom from '@/components/SysUserGroup/SysUserSearchCom';
import { useLocation } from 'umi';

const sysUserSearchCom = () => {
  const location = useLocation();
  return <SysUserSearchCom params={location.query}></SysUserSearchCom>;
};

export default sysUserSearchCom;
