import SysUser_UserRoleMapEditSysUserCom from '@/components/SysUserGroup/SysUser_UserRoleMapEditSysUserCom';
import { useLocation } from 'umi';

const sysUser_UserRoleMapEditSysUserCom = () => {
  const location = useLocation();
  return (
    <SysUser_UserRoleMapEditSysUserCom params={location.query}></SysUser_UserRoleMapEditSysUserCom>
  );
};

export default sysUser_UserRoleMapEditSysUserCom;
