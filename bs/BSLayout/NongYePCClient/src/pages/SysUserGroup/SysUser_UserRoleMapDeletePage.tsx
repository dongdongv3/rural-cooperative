import SysUser_UserRoleMapDeleteCom from '@/components/SysUserGroup/SysUser_UserRoleMapDeleteCom';
import { useLocation } from 'umi';

const sysUser_UserRoleMapDeleteCom = () => {
  const location = useLocation();
  return <SysUser_UserRoleMapDeleteCom params={location.query}></SysUser_UserRoleMapDeleteCom>;
};

export default sysUser_UserRoleMapDeleteCom;
