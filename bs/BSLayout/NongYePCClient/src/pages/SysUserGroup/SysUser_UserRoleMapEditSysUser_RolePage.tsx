import SysUser_UserRoleMapEditSysUser_RoleCom from '@/components/SysUserGroup/SysUser_UserRoleMapEditSysUser_RoleCom';
import { useLocation } from 'umi';

const sysUser_UserRoleMapEditSysUser_RoleCom = () => {
  const location = useLocation();
  return (
    <SysUser_UserRoleMapEditSysUser_RoleCom
      params={location.query}
    ></SysUser_UserRoleMapEditSysUser_RoleCom>
  );
};

export default sysUser_UserRoleMapEditSysUser_RoleCom;
