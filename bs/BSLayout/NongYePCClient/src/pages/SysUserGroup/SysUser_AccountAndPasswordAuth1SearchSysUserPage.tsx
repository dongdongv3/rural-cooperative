import SysUser_AccountAndPasswordAuth1SearchSysUserCom from '@/components/SysUserGroup/SysUser_AccountAndPasswordAuth1SearchSysUserCom';
import { useLocation } from 'umi';

const sysUser_AccountAndPasswordAuth1SearchSysUserCom = () => {
  const location = useLocation();
  return (
    <SysUser_AccountAndPasswordAuth1SearchSysUserCom
      params={location.query}
    ></SysUser_AccountAndPasswordAuth1SearchSysUserCom>
  );
};

export default sysUser_AccountAndPasswordAuth1SearchSysUserCom;
