import SysUserDetailCom from '@/components/SysUserGroup/SysUserDetailCom';
import { useLocation } from 'umi';

const sysUserDetailCom = () => {
  const location = useLocation();
  return <SysUserDetailCom params={location.query}></SysUserDetailCom>;
};

export default sysUserDetailCom;
