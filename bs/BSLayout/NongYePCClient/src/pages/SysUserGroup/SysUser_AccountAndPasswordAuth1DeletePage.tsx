import SysUser_AccountAndPasswordAuth1DeleteCom from '@/components/SysUserGroup/SysUser_AccountAndPasswordAuth1DeleteCom';
import { useLocation } from 'umi';

const sysUser_AccountAndPasswordAuth1DeleteCom = () => {
  const location = useLocation();
  return (
    <SysUser_AccountAndPasswordAuth1DeleteCom
      params={location.query}
    ></SysUser_AccountAndPasswordAuth1DeleteCom>
  );
};

export default sysUser_AccountAndPasswordAuth1DeleteCom;
