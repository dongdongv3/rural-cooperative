import SysUser_AuthSearchCom from '@/components/SysUserGroup/SysUser_AuthSearchCom';
import { useLocation } from 'umi';

const sysUser_AuthSearchCom = () => {
  const location = useLocation();
  return <SysUser_AuthSearchCom params={location.query}></SysUser_AuthSearchCom>;
};

export default sysUser_AuthSearchCom;
