import SysUser_RoleAuthMapSearchSysUser_AuthCom from '@/components/SysUserGroup/SysUser_RoleAuthMapSearchSysUser_AuthCom';
import { useLocation } from 'umi';

const sysUser_RoleAuthMapSearchSysUser_AuthCom = () => {
  const location = useLocation();
  return (
    <SysUser_RoleAuthMapSearchSysUser_AuthCom
      params={location.query}
    ></SysUser_RoleAuthMapSearchSysUser_AuthCom>
  );
};

export default sysUser_RoleAuthMapSearchSysUser_AuthCom;
