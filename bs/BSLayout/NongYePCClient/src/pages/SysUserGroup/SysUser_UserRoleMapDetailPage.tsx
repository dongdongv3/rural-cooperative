import SysUser_UserRoleMapDetailCom from '@/components/SysUserGroup/SysUser_UserRoleMapDetailCom';
import { useLocation } from 'umi';

const sysUser_UserRoleMapDetailCom = () => {
  const location = useLocation();
  return <SysUser_UserRoleMapDetailCom params={location.query}></SysUser_UserRoleMapDetailCom>;
};

export default sysUser_UserRoleMapDetailCom;
