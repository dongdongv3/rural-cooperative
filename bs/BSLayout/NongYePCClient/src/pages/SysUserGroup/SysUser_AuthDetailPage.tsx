import SysUser_AuthDetailCom from '@/components/SysUserGroup/SysUser_AuthDetailCom';
import { useLocation } from 'umi';

const sysUser_AuthDetailCom = () => {
  const location = useLocation();
  return <SysUser_AuthDetailCom params={location.query}></SysUser_AuthDetailCom>;
};

export default sysUser_AuthDetailCom;
