import SysUser_RoleAuthMapSearchSysUser_RoleCom from '@/components/SysUserGroup/SysUser_RoleAuthMapSearchSysUser_RoleCom';
import { useLocation } from 'umi';

const sysUser_RoleAuthMapSearchSysUser_RoleCom = () => {
  const location = useLocation();
  return (
    <SysUser_RoleAuthMapSearchSysUser_RoleCom
      params={location.query}
    ></SysUser_RoleAuthMapSearchSysUser_RoleCom>
  );
};

export default sysUser_RoleAuthMapSearchSysUser_RoleCom;
