import SysUser_RoleDetailCom from '@/components/SysUserGroup/SysUser_RoleDetailCom';
import { useLocation } from 'umi';

const sysUser_RoleDetailCom = () => {
  const location = useLocation();
  return <SysUser_RoleDetailCom params={location.query}></SysUser_RoleDetailCom>;
};

export default sysUser_RoleDetailCom;
