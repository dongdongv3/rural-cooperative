import SysUserDeleteCom from '@/components/SysUserGroup/SysUserDeleteCom';
import { useLocation } from 'umi';

const sysUserDeleteCom = () => {
  const location = useLocation();
  return <SysUserDeleteCom params={location.query}></SysUserDeleteCom>;
};

export default sysUserDeleteCom;
