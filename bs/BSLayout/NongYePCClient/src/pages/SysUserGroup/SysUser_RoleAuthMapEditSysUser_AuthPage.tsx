import SysUser_RoleAuthMapEditSysUser_AuthCom from '@/components/SysUserGroup/SysUser_RoleAuthMapEditSysUser_AuthCom';
import { useLocation } from 'umi';

const sysUser_RoleAuthMapEditSysUser_AuthCom = () => {
  const location = useLocation();
  return (
    <SysUser_RoleAuthMapEditSysUser_AuthCom
      params={location.query}
    ></SysUser_RoleAuthMapEditSysUser_AuthCom>
  );
};

export default sysUser_RoleAuthMapEditSysUser_AuthCom;
