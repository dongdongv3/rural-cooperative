import SysUser_RoleDeleteCom from '@/components/SysUserGroup/SysUser_RoleDeleteCom';
import { useLocation } from 'umi';

const sysUser_RoleDeleteCom = () => {
  const location = useLocation();
  return <SysUser_RoleDeleteCom params={location.query}></SysUser_RoleDeleteCom>;
};

export default sysUser_RoleDeleteCom;
