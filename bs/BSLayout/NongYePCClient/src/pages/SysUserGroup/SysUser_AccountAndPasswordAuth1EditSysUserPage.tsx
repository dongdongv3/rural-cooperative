import SysUser_AccountAndPasswordAuth1EditSysUserCom from '@/components/SysUserGroup/SysUser_AccountAndPasswordAuth1EditSysUserCom';
import { useLocation } from 'umi';

const sysUser_AccountAndPasswordAuth1EditSysUserCom = () => {
  const location = useLocation();
  return (
    <SysUser_AccountAndPasswordAuth1EditSysUserCom
      params={location.query}
    ></SysUser_AccountAndPasswordAuth1EditSysUserCom>
  );
};

export default sysUser_AccountAndPasswordAuth1EditSysUserCom;
