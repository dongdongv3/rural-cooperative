import SysUserEditCom from '@/components/SysUserGroup/SysUserEditCom';
import { useLocation } from 'umi';

const sysUserEditCom = () => {
  const location = useLocation();
  return <SysUserEditCom params={location.query}></SysUserEditCom>;
};

export default sysUserEditCom;
