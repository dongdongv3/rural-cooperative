import SysUser_RoleAuthMapDetailCom from '@/components/SysUserGroup/SysUser_RoleAuthMapDetailCom';
import { useLocation } from 'umi';

const sysUser_RoleAuthMapDetailCom = () => {
  const location = useLocation();
  return <SysUser_RoleAuthMapDetailCom params={location.query}></SysUser_RoleAuthMapDetailCom>;
};

export default sysUser_RoleAuthMapDetailCom;
