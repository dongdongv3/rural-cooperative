import SysUser_RoleAuthMapAddSysUser_RoleCom from '@/components/SysUserGroup/SysUser_RoleAuthMapAddSysUser_RoleCom';
import { useLocation } from 'umi';

const sysUser_RoleAuthMapAddSysUser_RoleCom = () => {
  const location = useLocation();
  return (
    <SysUser_RoleAuthMapAddSysUser_RoleCom
      params={location.query}
    ></SysUser_RoleAuthMapAddSysUser_RoleCom>
  );
};

export default sysUser_RoleAuthMapAddSysUser_RoleCom;
