import SysUser_UserRoleMapAddSysUser_RoleCom from '@/components/SysUserGroup/SysUser_UserRoleMapAddSysUser_RoleCom';
import { useLocation } from 'umi';

const sysUser_UserRoleMapAddSysUser_RoleCom = () => {
  const location = useLocation();
  return (
    <SysUser_UserRoleMapAddSysUser_RoleCom
      params={location.query}
    ></SysUser_UserRoleMapAddSysUser_RoleCom>
  );
};

export default sysUser_UserRoleMapAddSysUser_RoleCom;
