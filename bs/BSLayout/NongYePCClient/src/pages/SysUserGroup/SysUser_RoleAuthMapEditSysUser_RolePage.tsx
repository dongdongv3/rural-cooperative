import SysUser_RoleAuthMapEditSysUser_RoleCom from '@/components/SysUserGroup/SysUser_RoleAuthMapEditSysUser_RoleCom';
import { useLocation } from 'umi';

const sysUser_RoleAuthMapEditSysUser_RoleCom = () => {
  const location = useLocation();
  return (
    <SysUser_RoleAuthMapEditSysUser_RoleCom
      params={location.query}
    ></SysUser_RoleAuthMapEditSysUser_RoleCom>
  );
};

export default sysUser_RoleAuthMapEditSysUser_RoleCom;
