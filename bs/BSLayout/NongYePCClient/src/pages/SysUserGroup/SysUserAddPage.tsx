import SysUserAddCom from '@/components/SysUserGroup/SysUserAddCom';
import { useLocation } from 'umi';

const sysUserAddCom = () => {
  const location = useLocation();
  return <SysUserAddCom params={location.query}></SysUserAddCom>;
};

export default sysUserAddCom;
