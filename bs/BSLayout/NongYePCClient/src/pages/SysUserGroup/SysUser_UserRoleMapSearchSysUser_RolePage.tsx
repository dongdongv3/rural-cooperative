import SysUser_UserRoleMapSearchSysUser_RoleCom from '@/components/SysUserGroup/SysUser_UserRoleMapSearchSysUser_RoleCom';
import { useLocation } from 'umi';

const sysUser_UserRoleMapSearchSysUser_RoleCom = () => {
  const location = useLocation();
  return (
    <SysUser_UserRoleMapSearchSysUser_RoleCom
      params={location.query}
    ></SysUser_UserRoleMapSearchSysUser_RoleCom>
  );
};

export default sysUser_UserRoleMapSearchSysUser_RoleCom;
