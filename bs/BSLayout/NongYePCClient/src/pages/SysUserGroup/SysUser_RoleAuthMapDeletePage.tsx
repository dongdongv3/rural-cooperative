import SysUser_RoleAuthMapDeleteCom from '@/components/SysUserGroup/SysUser_RoleAuthMapDeleteCom';
import { useLocation } from 'umi';

const sysUser_RoleAuthMapDeleteCom = () => {
  const location = useLocation();
  return <SysUser_RoleAuthMapDeleteCom params={location.query}></SysUser_RoleAuthMapDeleteCom>;
};

export default sysUser_RoleAuthMapDeleteCom;
