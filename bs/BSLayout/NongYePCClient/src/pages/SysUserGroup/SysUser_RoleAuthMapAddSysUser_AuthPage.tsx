import SysUser_RoleAuthMapAddSysUser_AuthCom from '@/components/SysUserGroup/SysUser_RoleAuthMapAddSysUser_AuthCom';
import { useLocation } from 'umi';

const sysUser_RoleAuthMapAddSysUser_AuthCom = () => {
  const location = useLocation();
  return (
    <SysUser_RoleAuthMapAddSysUser_AuthCom
      params={location.query}
    ></SysUser_RoleAuthMapAddSysUser_AuthCom>
  );
};

export default sysUser_RoleAuthMapAddSysUser_AuthCom;
