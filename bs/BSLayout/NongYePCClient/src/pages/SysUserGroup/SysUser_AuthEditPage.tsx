import SysUser_AuthEditCom from '@/components/SysUserGroup/SysUser_AuthEditCom';
import { useLocation } from 'umi';

const sysUser_AuthEditCom = () => {
  const location = useLocation();
  return <SysUser_AuthEditCom params={location.query}></SysUser_AuthEditCom>;
};

export default sysUser_AuthEditCom;
