import SysUser_RoleEditCom from '@/components/SysUserGroup/SysUser_RoleEditCom';
import { useLocation } from 'umi';

const sysUser_RoleEditCom = () => {
  const location = useLocation();
  return <SysUser_RoleEditCom params={location.query}></SysUser_RoleEditCom>;
};

export default sysUser_RoleEditCom;
