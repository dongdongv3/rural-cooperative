import SysUser_RoleSearchCom from '@/components/SysUserGroup/SysUser_RoleSearchCom';
import { useLocation } from 'umi';

const sysUser_RoleSearchCom = () => {
  const location = useLocation();
  return <SysUser_RoleSearchCom params={location.query}></SysUser_RoleSearchCom>;
};

export default sysUser_RoleSearchCom;
