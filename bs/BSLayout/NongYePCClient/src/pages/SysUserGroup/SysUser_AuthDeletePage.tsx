import SysUser_AuthDeleteCom from '@/components/SysUserGroup/SysUser_AuthDeleteCom';
import { useLocation } from 'umi';

const sysUser_AuthDeleteCom = () => {
  const location = useLocation();
  return <SysUser_AuthDeleteCom params={location.query}></SysUser_AuthDeleteCom>;
};

export default sysUser_AuthDeleteCom;
