import SysUser_UserRoleMapSearchSysUserCom from '@/components/SysUserGroup/SysUser_UserRoleMapSearchSysUserCom';
import { useLocation } from 'umi';

const sysUser_UserRoleMapSearchSysUserCom = () => {
  const location = useLocation();
  return (
    <SysUser_UserRoleMapSearchSysUserCom
      params={location.query}
    ></SysUser_UserRoleMapSearchSysUserCom>
  );
};

export default sysUser_UserRoleMapSearchSysUserCom;
