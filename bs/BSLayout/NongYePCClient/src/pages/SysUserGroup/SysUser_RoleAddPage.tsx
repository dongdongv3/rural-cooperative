import SysUser_RoleAddCom from '@/components/SysUserGroup/SysUser_RoleAddCom';
import { useLocation } from 'umi';

const sysUser_RoleAddCom = () => {
  const location = useLocation();
  return <SysUser_RoleAddCom params={location.query}></SysUser_RoleAddCom>;
};

export default sysUser_RoleAddCom;
