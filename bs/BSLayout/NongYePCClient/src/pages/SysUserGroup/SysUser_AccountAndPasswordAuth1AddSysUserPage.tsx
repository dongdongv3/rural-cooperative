import SysUser_AccountAndPasswordAuth1AddSysUserCom from '@/components/SysUserGroup/SysUser_AccountAndPasswordAuth1AddSysUserCom';
import { useLocation } from 'umi';

const sysUser_AccountAndPasswordAuth1AddSysUserCom = () => {
  const location = useLocation();
  return (
    <SysUser_AccountAndPasswordAuth1AddSysUserCom
      params={location.query}
    ></SysUser_AccountAndPasswordAuth1AddSysUserCom>
  );
};

export default sysUser_AccountAndPasswordAuth1AddSysUserCom;
