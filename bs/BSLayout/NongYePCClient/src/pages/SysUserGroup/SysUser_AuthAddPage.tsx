import SysUser_AuthAddCom from '@/components/SysUserGroup/SysUser_AuthAddCom';
import { useLocation } from 'umi';

const sysUser_AuthAddCom = () => {
  const location = useLocation();
  return <SysUser_AuthAddCom params={location.query}></SysUser_AuthAddCom>;
};

export default sysUser_AuthAddCom;
