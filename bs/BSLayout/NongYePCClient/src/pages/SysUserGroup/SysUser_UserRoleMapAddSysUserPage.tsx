import SysUser_UserRoleMapAddSysUserCom from '@/components/SysUserGroup/SysUser_UserRoleMapAddSysUserCom';
import { useLocation } from 'umi';

const sysUser_UserRoleMapAddSysUserCom = () => {
  const location = useLocation();
  return (
    <SysUser_UserRoleMapAddSysUserCom params={location.query}></SysUser_UserRoleMapAddSysUserCom>
  );
};

export default sysUser_UserRoleMapAddSysUserCom;
