import SheyuanDetailCom from '@/components/SheyuanGroup/SheyuanDetailCom';
import { useLocation } from 'umi';

const sheyuanDetailCom = () => {
  const location = useLocation();
  return <SheyuanDetailCom params={location.query}></SheyuanDetailCom>;
};

export default sheyuanDetailCom;
