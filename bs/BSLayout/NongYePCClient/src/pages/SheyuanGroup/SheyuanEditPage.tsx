import SheyuanEditCom from '@/components/SheyuanGroup/SheyuanEditCom';
import { useLocation } from 'umi';

const sheyuanEditCom = () => {
  const location = useLocation();
  return <SheyuanEditCom params={location.query}></SheyuanEditCom>;
};

export default sheyuanEditCom;
