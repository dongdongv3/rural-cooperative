import SheyuanDeleteCom from '@/components/SheyuanGroup/SheyuanDeleteCom';
import { useLocation } from 'umi';

const sheyuanDeleteCom = () => {
  const location = useLocation();
  return <SheyuanDeleteCom params={location.query}></SheyuanDeleteCom>;
};

export default sheyuanDeleteCom;
