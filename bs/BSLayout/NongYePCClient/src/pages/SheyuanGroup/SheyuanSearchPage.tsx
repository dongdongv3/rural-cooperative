import SheyuanSearchCom from '@/components/SheyuanGroup/SheyuanSearchCom';
import { useLocation } from 'umi';

const sheyuanSearchCom = () => {
  const location = useLocation();
  return <SheyuanSearchCom params={location.query}></SheyuanSearchCom>;
};

export default sheyuanSearchCom;
