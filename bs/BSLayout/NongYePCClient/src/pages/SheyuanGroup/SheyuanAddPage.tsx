import SheyuanAddCom from '@/components/SheyuanGroup/SheyuanAddCom';
import { useLocation } from 'umi';

const sheyuanAddCom = () => {
  const location = useLocation();
  return <SheyuanAddCom params={location.query}></SheyuanAddCom>;
};

export default sheyuanAddCom;
