import NongjigongdanDeleteCom from '@/components/NongjigongdanGroup/NongjigongdanDeleteCom';
import { useLocation } from 'umi';

const nongjigongdanDeleteCom = () => {
  const location = useLocation();
  return <NongjigongdanDeleteCom params={location.query}></NongjigongdanDeleteCom>;
};

export default nongjigongdanDeleteCom;
