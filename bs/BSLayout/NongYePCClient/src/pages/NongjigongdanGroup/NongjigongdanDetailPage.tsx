import NongjigongdanDetailCom from '@/components/NongjigongdanGroup/NongjigongdanDetailCom';
import { useLocation } from 'umi';

const nongjigongdanDetailCom = () => {
  const location = useLocation();
  return <NongjigongdanDetailCom params={location.query}></NongjigongdanDetailCom>;
};

export default nongjigongdanDetailCom;
