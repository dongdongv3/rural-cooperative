import NongjigongdanAddCom from '@/components/NongjigongdanGroup/NongjigongdanAddCom';
import { useLocation } from 'umi';

const nongjigongdanAddCom = () => {
  const location = useLocation();
  return <NongjigongdanAddCom params={location.query}></NongjigongdanAddCom>;
};

export default nongjigongdanAddCom;
