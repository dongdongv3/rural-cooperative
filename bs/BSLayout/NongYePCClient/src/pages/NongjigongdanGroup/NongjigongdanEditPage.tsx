import NongjigongdanEditCom from '@/components/NongjigongdanGroup/NongjigongdanEditCom';
import { useLocation } from 'umi';

const nongjigongdanEditCom = () => {
  const location = useLocation();
  return <NongjigongdanEditCom params={location.query}></NongjigongdanEditCom>;
};

export default nongjigongdanEditCom;
