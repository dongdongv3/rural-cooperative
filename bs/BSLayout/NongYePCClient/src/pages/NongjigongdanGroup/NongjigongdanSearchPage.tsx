import NongjigongdanSearchCom from '@/components/NongjigongdanGroup/NongjigongdanSearchCom';
import { useLocation } from 'umi';

const nongjigongdanSearchCom = () => {
  const location = useLocation();
  return <NongjigongdanSearchCom params={location.query}></NongjigongdanSearchCom>;
};

export default nongjigongdanSearchCom;
