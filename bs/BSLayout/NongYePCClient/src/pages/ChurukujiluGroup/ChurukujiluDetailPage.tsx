import ChurukujiluDetailCom from '@/components/ChurukujiluGroup/ChurukujiluDetailCom';
import { useLocation } from 'umi';

const churukujiluDetailCom = () => {
  const location = useLocation();
  return <ChurukujiluDetailCom params={location.query}></ChurukujiluDetailCom>;
};

export default churukujiluDetailCom;
