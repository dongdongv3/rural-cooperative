import ChurukujiluEditCom from '@/components/ChurukujiluGroup/ChurukujiluEditCom';
import { useLocation } from 'umi';

const churukujiluEditCom = () => {
  const location = useLocation();
  return <ChurukujiluEditCom params={location.query}></ChurukujiluEditCom>;
};

export default churukujiluEditCom;
