import ChurukujiluSearchCom from '@/components/ChurukujiluGroup/ChurukujiluSearchCom';
import { useLocation } from 'umi';

const churukujiluSearchCom = () => {
  const location = useLocation();
  return <ChurukujiluSearchCom params={location.query}></ChurukujiluSearchCom>;
};

export default churukujiluSearchCom;
