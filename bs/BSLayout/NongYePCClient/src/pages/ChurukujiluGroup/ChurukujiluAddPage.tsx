import ChurukujiluAddCom from '@/components/ChurukujiluGroup/ChurukujiluAddCom';
import { useLocation } from 'umi';

const churukujiluAddCom = () => {
  const location = useLocation();
  return <ChurukujiluAddCom params={location.query}></ChurukujiluAddCom>;
};

export default churukujiluAddCom;
