import ChurukujiluDeleteCom from '@/components/ChurukujiluGroup/ChurukujiluDeleteCom';
import { useLocation } from 'umi';

const churukujiluDeleteCom = () => {
  const location = useLocation();
  return <ChurukujiluDeleteCom params={location.query}></ChurukujiluDeleteCom>;
};

export default churukujiluDeleteCom;
