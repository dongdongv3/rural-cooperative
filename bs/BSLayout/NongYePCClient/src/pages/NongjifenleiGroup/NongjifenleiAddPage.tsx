import NongjifenleiAddCom from '@/components/NongjifenleiGroup/NongjifenleiAddCom';
import { useLocation } from 'umi';

const nongjifenleiAddCom = () => {
  const location = useLocation();
  return <NongjifenleiAddCom params={location.query}></NongjifenleiAddCom>;
};

export default nongjifenleiAddCom;
