import NongjifenleiDeleteCom from '@/components/NongjifenleiGroup/NongjifenleiDeleteCom';
import { useLocation } from 'umi';

const nongjifenleiDeleteCom = () => {
  const location = useLocation();
  return <NongjifenleiDeleteCom params={location.query}></NongjifenleiDeleteCom>;
};

export default nongjifenleiDeleteCom;
