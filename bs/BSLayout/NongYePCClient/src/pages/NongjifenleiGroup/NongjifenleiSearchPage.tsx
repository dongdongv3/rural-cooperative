import NongjifenleiSearchCom from '@/components/NongjifenleiGroup/NongjifenleiSearchCom';
import { useLocation } from 'umi';

const nongjifenleiSearchCom = () => {
  const location = useLocation();
  return <NongjifenleiSearchCom params={location.query}></NongjifenleiSearchCom>;
};

export default nongjifenleiSearchCom;
