import KucunMingxiSearchCom from '@/components/KucunMingxiGroup/KucunMingxiSearchCom';
import { useLocation } from 'umi';

const kucunMingxiSearchCom = () => {
  const location = useLocation();
  return <KucunMingxiSearchCom params={location.query}></KucunMingxiSearchCom>;
};

export default kucunMingxiSearchCom;
