import KucunMingxiDeleteCom from '@/components/KucunMingxiGroup/KucunMingxiDeleteCom';
import { useLocation } from 'umi';

const kucunMingxiDeleteCom = () => {
  const location = useLocation();
  return <KucunMingxiDeleteCom params={location.query}></KucunMingxiDeleteCom>;
};

export default kucunMingxiDeleteCom;
