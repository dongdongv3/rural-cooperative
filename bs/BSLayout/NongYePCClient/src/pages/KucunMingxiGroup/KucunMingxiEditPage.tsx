import KucunMingxiEditCom from '@/components/KucunMingxiGroup/KucunMingxiEditCom';
import { useLocation } from 'umi';

const kucunMingxiEditCom = () => {
  const location = useLocation();
  return <KucunMingxiEditCom params={location.query}></KucunMingxiEditCom>;
};

export default kucunMingxiEditCom;
