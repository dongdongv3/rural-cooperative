import KucunMingxiAddCom from '@/components/KucunMingxiGroup/KucunMingxiAddCom';
import { useLocation } from 'umi';

const kucunMingxiAddCom = () => {
  const location = useLocation();
  return <KucunMingxiAddCom params={location.query}></KucunMingxiAddCom>;
};

export default kucunMingxiAddCom;
