import KucunMingxiDetailCom from '@/components/KucunMingxiGroup/KucunMingxiDetailCom';
import { useLocation } from 'umi';

const kucunMingxiDetailCom = () => {
  const location = useLocation();
  return <KucunMingxiDetailCom params={location.query}></KucunMingxiDetailCom>;
};

export default kucunMingxiDetailCom;
