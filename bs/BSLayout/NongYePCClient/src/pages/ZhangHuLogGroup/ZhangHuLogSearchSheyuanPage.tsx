import ZhangHuLogSearchSheyuanCom from '@/components/ZhangHuLogGroup/ZhangHuLogSearchSheyuanCom';
import { useLocation } from 'umi';

const zhangHuLogSearchSheyuanCom = () => {
  const location = useLocation();
  return <ZhangHuLogSearchSheyuanCom params={location.query}></ZhangHuLogSearchSheyuanCom>;
};

export default zhangHuLogSearchSheyuanCom;
