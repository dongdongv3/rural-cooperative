import KucunTypeAddCom from '@/components/KucunTypeGroup/KucunTypeAddCom';
import { useLocation } from 'umi';

const kucunTypeAddCom = () => {
  const location = useLocation();
  return <KucunTypeAddCom params={location.query}></KucunTypeAddCom>;
};

export default kucunTypeAddCom;
