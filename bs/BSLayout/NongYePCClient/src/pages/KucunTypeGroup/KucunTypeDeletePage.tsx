import KucunTypeDeleteCom from '@/components/KucunTypeGroup/KucunTypeDeleteCom';
import { useLocation } from 'umi';

const kucunTypeDeleteCom = () => {
  const location = useLocation();
  return <KucunTypeDeleteCom params={location.query}></KucunTypeDeleteCom>;
};

export default kucunTypeDeleteCom;
