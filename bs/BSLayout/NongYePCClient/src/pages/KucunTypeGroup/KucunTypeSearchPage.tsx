import KucunTypeSearchCom from '@/components/KucunTypeGroup/KucunTypeSearchCom';
import { useLocation } from 'umi';

const kucunTypeSearchCom = () => {
  const location = useLocation();
  return <KucunTypeSearchCom params={location.query}></KucunTypeSearchCom>;
};

export default kucunTypeSearchCom;
