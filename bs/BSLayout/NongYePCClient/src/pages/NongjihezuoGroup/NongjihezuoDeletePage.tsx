import NongjihezuoDeleteCom from '@/components/NongjihezuoGroup/NongjihezuoDeleteCom';
import { useLocation } from 'umi';

const nongjihezuoDeleteCom = () => {
  const location = useLocation();
  return <NongjihezuoDeleteCom params={location.query}></NongjihezuoDeleteCom>;
};

export default nongjihezuoDeleteCom;
