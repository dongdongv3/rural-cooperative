import NongjihezuoEditCom from '@/components/NongjihezuoGroup/NongjihezuoEditCom';
import { useLocation } from 'umi';

const nongjihezuoEditCom = () => {
  const location = useLocation();
  return <NongjihezuoEditCom params={location.query}></NongjihezuoEditCom>;
};

export default nongjihezuoEditCom;
