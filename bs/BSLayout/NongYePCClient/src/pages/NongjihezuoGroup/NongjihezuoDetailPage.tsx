import NongjihezuoDetailCom from '@/components/NongjihezuoGroup/NongjihezuoDetailCom';
import { useLocation } from 'umi';

const nongjihezuoDetailCom = () => {
  const location = useLocation();
  return <NongjihezuoDetailCom params={location.query}></NongjihezuoDetailCom>;
};

export default nongjihezuoDetailCom;
