import NongjihezuoSearchCom from '@/components/NongjihezuoGroup/NongjihezuoSearchCom';
import { useLocation } from 'umi';

const nongjihezuoSearchCom = () => {
  const location = useLocation();
  return <NongjihezuoSearchCom params={location.query}></NongjihezuoSearchCom>;
};

export default nongjihezuoSearchCom;
