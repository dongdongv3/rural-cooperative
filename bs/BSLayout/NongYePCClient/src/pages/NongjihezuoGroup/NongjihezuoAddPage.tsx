import NongjihezuoAddCom from '@/components/NongjihezuoGroup/NongjihezuoAddCom';
import { useLocation } from 'umi';

const nongjihezuoAddCom = () => {
  const location = useLocation();
  return <NongjihezuoAddCom params={location.query}></NongjihezuoAddCom>;
};

export default nongjihezuoAddCom;
