import { GetInputRequest, PostValue, responseMode } from '@/services/initServiceApi';
import ProCard from '@ant-design/pro-card';
import ProForm, { ProFormInstance, ProFormSelect, ProFormText } from '@ant-design/pro-form';
import { Alert, Button, Card, Divider, Form } from 'antd';
import { useEffect, useRef, useState } from 'react';

interface ItemMode {
  model: responseMode;
  value: string | undefined;
}

function arrGroup(arr: any[], fn: (item: any) => any) {
  const obj = {};
  arr.forEach((item) => {
    const key = fn(item);
    obj[key] = obj[key] || [];
    obj[key].push(item);
  });
  const tmp = Object.keys(obj).map((k) => {
    return { key: k, value: obj[k] };
  });
  return tmp;
}

const Title_Error = '初始化过程错误';
const Title_Closed = '初始化已关闭';
const Title_Complate = '初始化已完成';

const InitPage = (props: any) => {
  const formRef = useRef<ProFormInstance>();
  const [inputContext, setInputContext] = useState<ItemMode[]>([]);
  const [loading, setLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const SetValue = async () => {
    setLoading(true);
    const value = formRef.current?.getFieldValue(inputContext[inputContext.length - 1].model.title);
    await PostValue(value)
      .then((v) => {
        if (v) {
          setErrorMessage(v);
        } else {
          inputContext[inputContext.length - 1].value = value;
          setInputContext([...inputContext]);
          TryGetInput();
          setErrorMessage('');
        }
      })
      .finally(() => {
        setLoading(false);
      });
  };
  const TryGetInput = async () => {
    GetInputRequest().then((d) => {
      setInputContext([...inputContext, { model: d, value: undefined }]);
    });
  };
  useEffect(() => {
    TryGetInput();
  }, []);
  return (
    <>
      <div>
        <ProCard>
          <h1 style={{ background: '#ededed' }}>初始化业务</h1>
          <ProForm
            formRef={formRef}
            submitter={{
              render: () => {
                return [
                  <Form.Item>
                    <Button
                      type="primary"
                      hidden={
                        inputContext[inputContext.length - 1]?.model.title == Title_Complate ||
                        inputContext[inputContext.length - 1]?.model.title == Title_Closed ||
                        inputContext[inputContext.length - 1]?.model.title == Title_Error ||
                        inputContext.length == 0
                      }
                      loading={loading}
                      onClick={SetValue}
                    >
                      确认
                    </Button>
                  </Form.Item>,
                ];
              },
            }}
          >
            {arrGroup(inputContext, (v) => (v?.model?.title + '').split('-')[0]).map((g) => {
              const inbody = g.value.map((e: any) => {
                let body = null;
                if (e.model.title == Title_Complate) {
                  body = (
                    <ProCard>
                      <>
                        <label style={{ color: 'red' }}>
                          这很重要！！！。请通过移除‘init -openInitWebUI’
                          命令重新启动后端服务来关闭初始化页面，
                        </label>
                        <br />
                        <label style={{ color: 'red' }}>
                          这很重要！！！。请通过移除‘init -openInitWebUI’
                          命令重新启动后端服务来关闭初始化页面，
                        </label>
                        <br />
                        <label style={{ color: 'red' }}>
                          这很重要！！！。请通过移除‘init -openInitWebUI’
                          命令重新启动后端服务来关闭初始化页面，
                        </label>
                        <br />
                        <br />
                        <a href="/">初始化已经完成，点击此处返回主页</a>
                      </>
                    </ProCard>
                  );
                } else if (e.model.title == Title_Closed) {
                  body = (
                    <ProCard>
                      <label style={{ color: 'red' }}>
                        若要开启初始化页面。请通过添加参数‘init -openInitWebUI’
                        命令重新启动后端服务来开启初始化页面，
                      </label>
                      <br />
                      <a href="/">初始化已关闭，点击此处返回主页</a>
                    </ProCard>
                  );
                } else if (e.model.title == Title_Error) {
                  body = (
                    <ProCard>
                      <label style={{ color: 'red' }}>
                        初始化过程中断,如需重新执行初始化请重启服务，错误信息如下:
                      </label>
                      <br />
                      {e.model.errorMessage}
                      <br />
                      <Divider />
                      <a href="/">初始化已关闭，点击此处返回主页</a>
                    </ProCard>
                  );
                } else if (e.model.options) {
                  body = (
                    <ProForm.Group>
                      <ProFormSelect
                        required={e.model.required}
                        name={e.model.title}
                        label={e.model?.title?.replace(g.key + '-', '')}
                        width="lg"
                        disabled={e.value != undefined}
                        fieldProps={{
                          options: e.model.options.map((x) => {
                            return { label: x.value, value: x.key };
                          }),
                        }}
                      />
                    </ProForm.Group>
                  );
                } else {
                  body = (
                    <ProForm.Group>
                      <ProFormText
                        required={e.model.required}
                        name={e.model.title}
                        label={e.model?.title?.replace(g.key + '-', '')}
                        width="lg"
                        disabled={e.value != undefined}
                      />
                    </ProForm.Group>
                  );
                }
                return body;
              });
              return <Card title={g.key}>{inbody}</Card>;
            })}
            {errorMessage != '' && errorMessage != undefined && (
              <Alert message={errorMessage} type="error" showIcon />
            )}
          </ProForm>
        </ProCard>
      </div>
    </>
  );
};

export default InitPage;
