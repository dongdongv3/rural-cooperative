import * as sysUserGroupServiceApi from '@/services/SysUserGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProForm } from '@ant-design/pro-components';
import { useState } from 'react';
import { useRef } from 'react';
import { ProFormText } from '@ant-design/pro-components';
import { ProFormSelect } from '@ant-design/pro-components';

interface inputProperty {
  params: {
    id: any;
    refSysUserId: any;
  };
  [property: string]: any;
}

const SysUser_AccountAndPasswordAuth1EditSysUser: React.FC<inputProperty> = ({
  params,
  setResoult,
}) => {
  const [record, setRecord] = useState<any>({ ...params });
  const formref = useRef<FormInstance>();
  const getValue = async () => {
    const outdata =
      await sysUserGroupServiceApi.sysUser_AccountAndPasswordAuth1EditSysUser_GetValue({
        ...params,
        pageParament: params,
      });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return convertData;
  };

  const saveValue = async (pageparams: any) => {
    setResoult(undefined);
    const convertData = { ...pageparams };
    const tmp = await sysUserGroupServiceApi.sysUser_AccountAndPasswordAuth1EditSysUser({
      ...convertData,
      pageParament: params,
    });
    setResoult(tmp);
  };

  return (
    <ProForm
      params={params}
      request={getValue}
      onFinish={saveValue}
      formRef={formref}
      submitter={{
        render: (props, doms) => {
          return [...doms];
        },
      }}
    >
      <ProFormText
        name={'id'}
        label={'Id'}
        hidden={true}
        rules={[{ type: 'string', max: 50 }]}
      ></ProFormText>
      <ProFormText
        name={'loginID'}
        label={'登录账户'}
        rules={[{ type: 'string', max: 50 }]}
      ></ProFormText>
      <ProFormText.Password name={'pwd'} label={'密码'} rules={[]}></ProFormText.Password>
      <ProFormSelect
        request={() =>
          sysUserGroupServiceApi.sysUser_AccountAndPasswordAuth1EditSysUser_RefSysUserId_DataSource(
            { ...(formref.current?.getFieldsValue() || {}), pageParament: params },
          )
        }
        dependencies={[]}
        name={'refSysUserId'}
        label={'系统用户-用户信息-用户名'}
        readonly={true}
        rules={[]}
      ></ProFormSelect>
    </ProForm>
  );
};

export default ResoultWapper(
  SysUser_AccountAndPasswordAuth1EditSysUser,
  '为系统用户-用户信息编辑账户密码认证1-认证通道',
);
