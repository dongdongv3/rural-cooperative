import * as sysUserGroupServiceApi from '@/services/SysUserGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ActionType } from '@ant-design/pro-components';
import { FormInstance } from '@ant-design/pro-components';
import { ProTable } from '@ant-design/pro-components';
import { PageContainer } from '@ant-design/pro-layout';
import { useRef } from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import LinkTo from '@/utils/LinkTo';
import { EditOutlined } from '@ant-design/icons';
import { DeleteOutlined } from '@ant-design/icons';
import { FileTextOutlined } from '@ant-design/icons';
import { SubnodeOutlined } from '@ant-design/icons';
import { FileAddOutlined } from '@ant-design/icons';

interface inputProperty {
  params?: {};
  [property: string]: any;
}

const Listview: React.FC<inputProperty> = ({ params }) => {
  const [defaultvalue, setdefaultvalue] = useState(undefined);
  const ref = useRef<ActionType>();
  const formref = useRef<FormInstance>();
  useEffect(() => {
    sysUserGroupServiceApi
      .sysUser_RoleSearch_GetDefaultValue({ pageParament: params })
      .then((data) => {
        const convertData = { ...data };
        setdefaultvalue(convertData);

        if (formref?.current) {
          formref?.current?.setFieldsValue(convertData);
          formref.current?.submit();
        } else {
          setTimeout(() => {
            ref.current?.reload();
          }, 100);
        }
      });
  }, [params]);

  const sysUser_RoleSearch = async (request: any, sort: any, filter: any) => {
    const convertData = { ...request, pageParament: params };
    let data = await sysUserGroupServiceApi.sysUser_RoleSearch(convertData, sort, filter);
    let tmp = data.data;
    const convertValue: any[] = [];
    tmp.forEach((x: any) => {
      convertValue.push({ ...x });
    });
    tmp = convertValue;
    data.data = tmp;
    return data;
  };

  const columns: any[] = [];
  columns.push({
    valueType: 'text',
    title: '角色名称',
    dataIndex: 'sysUser_Role_RoleName',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'text',
    title: 'Id',
    dataIndex: 'sysUser_Role_Id',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
    hideInTable: true,
  });
  columns.push({
    title: '操作',
    valueType: 'option',
    fixed: 'right',
    render: (text, record, _, action) => [
      <LinkTo
        key="SysUser_RoleEdit"
        buttonProp={{ type: 'primary' }}
        path="/SysUserGroup/SysUser_RoleEditPage"
        query={{ id: record.sysUser_Role_Id }}
        icon={<EditOutlined />}
        successGoto={1}
      >
        编辑
      </LinkTo>,
      <LinkTo
        key="SysUser_RoleDelete"
        buttonProp={{ danger: true, type: 'primary' }}
        path="/SysUserGroup/SysUser_RoleDeletePage"
        query={{ id: record.sysUser_Role_Id }}
        icon={<DeleteOutlined />}
        successGoto={1}
      >
        删除
      </LinkTo>,
      <LinkTo
        key="SysUser_RoleDetail"
        buttonProp={{ type: 'link' }}
        path="/SysUserGroup/SysUser_RoleDetailPage"
        query={{ id: record.sysUser_Role_Id }}
        icon={<FileTextOutlined />}
        successGoto={1}
      >
        详情
      </LinkTo>,
      <LinkTo
        key="SysUser_UserRoleMapSearchSysUser_Role"
        buttonProp={{ type: 'link' }}
        path="/SysUserGroup/SysUser_UserRoleMapSearchSysUser_RolePage"
        query={{ refSysUser_RoleId: record.sysUser_Role_Id }}
        icon={<SubnodeOutlined />}
        successGoto={1}
      >
        用户角色映射表管理
      </LinkTo>,
      <LinkTo
        key="SysUser_RoleAuthMapSearchSysUser_Role"
        buttonProp={{ type: 'link' }}
        path="/SysUserGroup/SysUser_RoleAuthMapSearchSysUser_RolePage"
        query={{ refSysUser_RoleId: record.sysUser_Role_Id }}
        icon={<SubnodeOutlined />}
        successGoto={1}
      >
        角色权限映射表管理
      </LinkTo>,
    ],
  });

  const searchConfig: any = false;
  return (
    <PageContainer title="查询角色">
      <ProTable
        columns={columns}
        actionRef={ref}
        formRef={formref}
        manualRequest={true}
        search={searchConfig}
        scroll={{ x: 'auto' }}
        request={sysUser_RoleSearch}
        pagination={{ showSizeChanger: true, defaultPageSize: 10, pageSizeOptions: [10, 20, 30] }}
        toolBarRender={() => [
          <LinkTo
            key="SysUser_RoleAdd"
            buttonProp={{ type: 'primary' }}
            path="/SysUserGroup/SysUser_RoleAddPage"
            query={{}}
            icon={<FileAddOutlined />}
            successGoto={1}
          >
            添加
          </LinkTo>,
        ]}
      ></ProTable>
    </PageContainer>
  );
};

export default Listview;
