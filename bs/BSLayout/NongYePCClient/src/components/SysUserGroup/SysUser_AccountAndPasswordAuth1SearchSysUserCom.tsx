import * as sysUserGroupServiceApi from '@/services/SysUserGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ActionType } from '@ant-design/pro-components';
import { FormInstance } from '@ant-design/pro-components';
import { ProTable } from '@ant-design/pro-components';
import { PageContainer } from '@ant-design/pro-layout';
import { useRef } from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import LinkTo from '@/utils/LinkTo';
import { EditOutlined } from '@ant-design/icons';
import { DeleteOutlined } from '@ant-design/icons';
import { FileTextOutlined } from '@ant-design/icons';
import { FileAddOutlined } from '@ant-design/icons';

interface inputProperty {
  params: {
    refSysUserId: any;
  };
  [property: string]: any;
}

const Listview: React.FC<inputProperty> = ({ params }) => {
  const [defaultvalue, setdefaultvalue] = useState(undefined);
  const ref = useRef<ActionType>();
  const formref = useRef<FormInstance>();
  useEffect(() => {
    sysUserGroupServiceApi
      .sysUser_AccountAndPasswordAuth1SearchSysUser_GetDefaultValue({ pageParament: params })
      .then((data) => {
        const convertData = { ...data };
        setdefaultvalue(convertData);

        if (formref?.current) {
          formref?.current?.setFieldsValue(convertData);
          formref.current?.submit();
        } else {
          setTimeout(() => {
            ref.current?.reload();
          }, 100);
        }
      });
  }, [params]);

  const sysUser_AccountAndPasswordAuth1SearchSysUser = async (
    request: any,
    sort: any,
    filter: any,
  ) => {
    const convertData = { ...request, pageParament: params };
    let data = await sysUserGroupServiceApi.sysUser_AccountAndPasswordAuth1SearchSysUser(
      convertData,
      sort,
      filter,
    );
    let tmp = data.data;
    const convertValue: any[] = [];
    tmp.forEach((x: any) => {
      convertValue.push({ ...x });
    });
    tmp = convertValue;
    data.data = tmp;
    return data;
  };

  const columns: any[] = [];
  columns.push({
    valueType: 'text',
    title: '登录账户',
    dataIndex: 'sysUser_AccountAndPasswordAuth1_LoginID',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'password',
    title: '密码',
    dataIndex: 'sysUser_AccountAndPasswordAuth1_Pwd',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'text',
    title: 'Id',
    dataIndex: 'sysUser_AccountAndPasswordAuth1_Id',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
    hideInTable: true,
  });
  columns.push({
    valueType: 'select',
    request: () =>
      sysUserGroupServiceApi.sysUser_AccountAndPasswordAuth1SearchSysUser_RefSysUserId_View_DataSource(
        { ...{}, pageParament: params },
      ),
    title: '系统用户-用户信息-用户名',
    dataIndex: 'sysUser_AccountAndPasswordAuth1_RefSysUserId',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    title: '操作',
    valueType: 'option',
    fixed: 'right',
    render: (text, record, _, action) => [
      <LinkTo
        key="SysUser_AccountAndPasswordAuth1EditSysUser"
        buttonProp={{ type: 'primary' }}
        path="/SysUserGroup/SysUser_AccountAndPasswordAuth1EditSysUserPage"
        query={{ id: record.sysUser_AccountAndPasswordAuth1_Id }}
        icon={<EditOutlined />}
        successGoto={1}
      >
        编辑
      </LinkTo>,
      <LinkTo
        key="SysUser_AccountAndPasswordAuth1Delete"
        buttonProp={{ danger: true, type: 'primary' }}
        path="/SysUserGroup/SysUser_AccountAndPasswordAuth1DeletePage"
        query={{ id: record.sysUser_AccountAndPasswordAuth1_Id }}
        icon={<DeleteOutlined />}
        successGoto={1}
      >
        删除
      </LinkTo>,
      <LinkTo
        key="SysUser_AccountAndPasswordAuth1Detail"
        buttonProp={{ type: 'link' }}
        path="/SysUserGroup/SysUser_AccountAndPasswordAuth1DetailPage"
        query={{ id: record.sysUser_AccountAndPasswordAuth1_Id }}
        icon={<FileTextOutlined />}
        successGoto={1}
      >
        详情
      </LinkTo>,
    ],
  });

  const searchConfig: any = false;
  return (
    <PageContainer title="根据系统用户-用户信息查询账户密码认证1-认证通道">
      <ProTable
        columns={columns}
        actionRef={ref}
        formRef={formref}
        manualRequest={true}
        search={searchConfig}
        scroll={{ x: 'auto' }}
        request={sysUser_AccountAndPasswordAuth1SearchSysUser}
        pagination={{ showSizeChanger: true, defaultPageSize: 10, pageSizeOptions: [10, 20, 30] }}
        toolBarRender={() => [
          <LinkTo
            key="SysUser_AccountAndPasswordAuth1AddSysUser"
            buttonProp={{ type: 'primary' }}
            path="/SysUserGroup/SysUser_AccountAndPasswordAuth1AddSysUserPage"
            query={{ refSysUserId: params.refSysUserId }}
            icon={<FileAddOutlined />}
            successGoto={1}
          >
            添加
          </LinkTo>,
        ]}
      ></ProTable>
    </PageContainer>
  );
};

export default Listview;
