import * as sysUserGroupServiceApi from '@/services/SysUserGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProDescriptions } from '@ant-design/pro-components';
import { useState } from 'react';

interface inputProperty {
  params: {
    id: any;
  };
  [property: string]: any;
}

const SysUser_UserRoleMapDetail: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const getValue = async () => {
    const outdata = await sysUserGroupServiceApi.sysUser_UserRoleMapDetail_GetValue({
      ...params,
      pageParament: params,
    });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return { data: convertData, success: true };
  };

  return (
    <ProDescriptions params={params} request={getValue} bordered={true}>
      <ProDescriptions.Item
        valueType={'select'}
        request={() =>
          sysUserGroupServiceApi.sysUser_UserRoleMapDetail_RefSysUserId_DataSource({
            ...{},
            pageParament: params,
          })
        }
        title={'系统用户-用户信息-用户名'}
        dataIndex={'refSysUserId'}
      />
      <ProDescriptions.Item
        valueType={'select'}
        request={() =>
          sysUserGroupServiceApi.sysUser_UserRoleMapDetail_RefSysUser_RoleId_DataSource({
            ...{},
            pageParament: params,
          })
        }
        title={'角色-角色名称'}
        dataIndex={'refSysUser_RoleId'}
      />
      <ProDescriptions.Item valueType="option"></ProDescriptions.Item>
    </ProDescriptions>
  );
};

export default ResoultWapper(SysUser_UserRoleMapDetail, '详情用户角色映射表');
