import * as sysUserGroupServiceApi from '@/services/SysUserGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProForm } from '@ant-design/pro-components';
import { useState } from 'react';
import { useRef } from 'react';
import { ProFormText } from '@ant-design/pro-components';
import { ProFormSelect } from '@ant-design/pro-components';

interface inputProperty {
  params: {
    id: any;
    refSysUser_AuthId: any;
  };
  [property: string]: any;
}

const SysUser_RoleAuthMapEditSysUser_Auth: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const formref = useRef<FormInstance>();
  const getValue = async () => {
    const outdata = await sysUserGroupServiceApi.sysUser_RoleAuthMapEditSysUser_Auth_GetValue({
      ...params,
      pageParament: params,
    });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return convertData;
  };

  const saveValue = async (pageparams: any) => {
    setResoult(undefined);
    const convertData = { ...pageparams };
    const tmp = await sysUserGroupServiceApi.sysUser_RoleAuthMapEditSysUser_Auth({
      ...convertData,
      pageParament: params,
    });
    setResoult(tmp);
  };

  return (
    <ProForm
      params={params}
      request={getValue}
      onFinish={saveValue}
      formRef={formref}
      submitter={{
        render: (props, doms) => {
          return [...doms];
        },
      }}
    >
      <ProFormText
        name={'id'}
        label={'Id'}
        hidden={true}
        rules={[{ type: 'string', max: 50 }]}
      ></ProFormText>
      <ProFormSelect
        request={() =>
          sysUserGroupServiceApi.sysUser_RoleAuthMapEditSysUser_Auth_RefSysUser_RoleId_DataSource({
            ...(formref.current?.getFieldsValue() || {}),
            pageParament: params,
          })
        }
        dependencies={[]}
        name={'refSysUser_RoleId'}
        label={'角色-角色名称'}
        rules={[]}
      ></ProFormSelect>
      <ProFormSelect
        request={() =>
          sysUserGroupServiceApi.sysUser_RoleAuthMapEditSysUser_Auth_RefSysUser_AuthId_DataSource({
            ...(formref.current?.getFieldsValue() || {}),
            pageParament: params,
          })
        }
        dependencies={[]}
        name={'refSysUser_AuthId'}
        label={'权限-权限名称'}
        readonly={true}
        rules={[]}
      ></ProFormSelect>
    </ProForm>
  );
};

export default ResoultWapper(SysUser_RoleAuthMapEditSysUser_Auth, '为权限编辑角色权限映射表');
