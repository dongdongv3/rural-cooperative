import * as sysUserGroupServiceApi from '@/services/SysUserGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProDescriptions } from '@ant-design/pro-components';
import { useState } from 'react';

interface inputProperty {
  params: {
    id: any;
  };
  [property: string]: any;
}

const SysUser_AccountAndPasswordAuth1Detail: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const getValue = async () => {
    const outdata = await sysUserGroupServiceApi.sysUser_AccountAndPasswordAuth1Detail_GetValue({
      ...params,
      pageParament: params,
    });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return { data: convertData, success: true };
  };

  return (
    <ProDescriptions params={params} request={getValue} bordered={true}>
      <ProDescriptions.Item valueType={'text'} title={'登录账户'} dataIndex={'loginID'} />
      <ProDescriptions.Item valueType={'password'} title={'密码'} dataIndex={'pwd'} />
      <ProDescriptions.Item
        valueType={'select'}
        request={() =>
          sysUserGroupServiceApi.sysUser_AccountAndPasswordAuth1Detail_RefSysUserId_DataSource({
            ...{},
            pageParament: params,
          })
        }
        title={'系统用户-用户信息-用户名'}
        dataIndex={'refSysUserId'}
      />
      <ProDescriptions.Item valueType="option"></ProDescriptions.Item>
    </ProDescriptions>
  );
};

export default ResoultWapper(SysUser_AccountAndPasswordAuth1Detail, '详情账户密码认证1-认证通道');
