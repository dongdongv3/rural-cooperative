import * as sysUserGroupServiceApi from '@/services/SysUserGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProDescriptions } from '@ant-design/pro-components';
import { useState } from 'react';

interface inputProperty {
  params: {
    id: any;
  };
  [property: string]: any;
}

const SysUser_RoleDetail: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const getValue = async () => {
    const outdata = await sysUserGroupServiceApi.sysUser_RoleDetail_GetValue({
      ...params,
      pageParament: params,
    });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return { data: convertData, success: true };
  };

  return (
    <ProDescriptions params={params} request={getValue} bordered={true}>
      <ProDescriptions.Item valueType={'text'} title={'角色名称'} dataIndex={'roleName'} />
      <ProDescriptions.Item valueType="option"></ProDescriptions.Item>
    </ProDescriptions>
  );
};

export default ResoultWapper(SysUser_RoleDetail, '详情角色');
