import * as sysUserGroupServiceApi from '@/services/SysUserGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProForm } from '@ant-design/pro-components';
import { useState } from 'react';
import { useRef } from 'react';
import { ProFormSelect } from '@ant-design/pro-components';

interface inputProperty {
  params: {
    refSysUser_RoleId: any;
  };
  [property: string]: any;
}

const SysUser_UserRoleMapAddSysUser_Role: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const formref = useRef<FormInstance>();
  const readDefaultValue = async () => {
    const outdata = await sysUserGroupServiceApi.sysUser_UserRoleMapAddSysUser_Role_GetDefaultValue(
      { pageParament: params },
    );
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return convertData;
  };

  const saveValue = async (pageparams: any) => {
    setResoult(undefined);
    const convertData = { ...pageparams, pageParament: params };
    const tmp = await sysUserGroupServiceApi.sysUser_UserRoleMapAddSysUser_Role(convertData);
    setResoult(tmp);
  };

  return (
    <ProForm params={params} request={readDefaultValue} onFinish={saveValue} formRef={formref}>
      <ProFormSelect
        request={() =>
          sysUserGroupServiceApi.sysUser_UserRoleMapAddSysUser_Role_RefSysUserId_DataSource({
            ...(formref.current?.getFieldsValue() || {}),
            pageParament: params,
          })
        }
        dependencies={[]}
        name={'refSysUserId'}
        label={'系统用户-用户信息-用户名'}
        rules={[]}
      ></ProFormSelect>
    </ProForm>
  );
};

export default ResoultWapper(SysUser_UserRoleMapAddSysUser_Role, '为角色添加用户角色映射表');
