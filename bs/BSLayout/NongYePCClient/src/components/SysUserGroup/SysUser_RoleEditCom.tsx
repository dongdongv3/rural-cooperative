import * as sysUserGroupServiceApi from '@/services/SysUserGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProForm } from '@ant-design/pro-components';
import { useState } from 'react';
import { useRef } from 'react';
import { ProFormText } from '@ant-design/pro-components';

interface inputProperty {
  params: {
    id: any;
  };
  [property: string]: any;
}

const SysUser_RoleEdit: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const formref = useRef<FormInstance>();
  const getValue = async () => {
    const outdata = await sysUserGroupServiceApi.sysUser_RoleEdit_GetValue({
      ...params,
      pageParament: params,
    });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return convertData;
  };

  const saveValue = async (pageparams: any) => {
    setResoult(undefined);
    const convertData = { ...pageparams };
    const tmp = await sysUserGroupServiceApi.sysUser_RoleEdit({
      ...convertData,
      pageParament: params,
    });
    setResoult(tmp);
  };

  return (
    <ProForm
      params={params}
      request={getValue}
      onFinish={saveValue}
      formRef={formref}
      submitter={{
        render: (props, doms) => {
          return [...doms];
        },
      }}
    >
      <ProFormText
        name={'id'}
        label={'Id'}
        hidden={true}
        rules={[{ type: 'string', max: 50 }]}
      ></ProFormText>
      <ProFormText
        name={'roleName'}
        label={'角色名称'}
        rules={[{ type: 'string', max: 50 }]}
      ></ProFormText>
    </ProForm>
  );
};

export default ResoultWapper(SysUser_RoleEdit, '编辑角色');
