import * as kucunMingxiGroupServiceApi from '@/services/KucunMingxiGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProDescriptions } from '@ant-design/pro-components';
import { useState } from 'react';

interface inputProperty {
  params: {
    id: any;
  };
  [property: string]: any;
}

const KucunMingxiDetail: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const getValue = async () => {
    const outdata = await kucunMingxiGroupServiceApi.kucunMingxiDetail_GetValue({
      ...params,
      pageParament: params,
    });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return { data: convertData, success: true };
  };

  return (
    <ProDescriptions params={params} request={getValue} bordered={true}>
      <ProDescriptions.Item valueType={'text'} title={'物品名称'} dataIndex={'wupinName'} />
      <ProDescriptions.Item valueType={'digit'} title={'剩余数量'} dataIndex={'count'} />
      <ProDescriptions.Item valueType={'text'} title={'单位'} dataIndex={'danwei'} />
      <ProDescriptions.Item
        valueType={'select'}
        request={() =>
          kucunMingxiGroupServiceApi.kucunMingxiDetail_fenlei_DataSource({
            ...{},
            pageParament: params,
          })
        }
        title={'分类'}
        dataIndex={'fenlei'}
      />
      <ProDescriptions.Item valueType="option"></ProDescriptions.Item>
    </ProDescriptions>
  );
};

export default ResoultWapper(KucunMingxiDetail, '详情库存明细');
