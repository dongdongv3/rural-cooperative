import * as kucunMingxiGroupServiceApi from '@/services/KucunMingxiGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ActionType } from '@ant-design/pro-components';
import { FormInstance } from '@ant-design/pro-components';
import { ProTable } from '@ant-design/pro-components';
import { PageContainer } from '@ant-design/pro-layout';
import { useRef } from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import LinkTo from '@/utils/LinkTo';
import { EditOutlined } from '@ant-design/icons';
import { DeleteOutlined } from '@ant-design/icons';
import { FileTextOutlined } from '@ant-design/icons';
import { FileAddOutlined } from '@ant-design/icons';

interface inputProperty {
  params?: {};
  [property: string]: any;
}

const Listview: React.FC<inputProperty> = ({ params }) => {
  const [defaultvalue, setdefaultvalue] = useState(undefined);
  const ref = useRef<ActionType>();
  const formref = useRef<FormInstance>();
  useEffect(() => {
    kucunMingxiGroupServiceApi
      .kucunMingxiSearch_GetDefaultValue({ pageParament: params })
      .then((data) => {
        const convertData = { ...data };
        setdefaultvalue(convertData);

        if (formref?.current) {
          formref?.current?.setFieldsValue(convertData);
          formref.current?.submit();
        } else {
          setTimeout(() => {
            ref.current?.reload();
          }, 100);
        }
      });
  }, [params]);

  const kucunMingxiSearch = async (request: any, sort: any, filter: any) => {
    const convertData = { ...request, pageParament: params };
    let data = await kucunMingxiGroupServiceApi.kucunMingxiSearch(convertData, sort, filter);
    let tmp = data.data;
    const convertValue: any[] = [];
    tmp.forEach((x: any) => {
      convertValue.push({ ...x });
    });
    tmp = convertValue;
    data.data = tmp;
    return data;
  };

  const columns: any[] = [];
  columns.push({
    valueType: 'text',
    title: '物品名称',
    dataIndex: 'kucunMingxi_WupinName',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'digit',
    title: '剩余数量',
    dataIndex: 'kucunMingxi_Count',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'text',
    title: '单位',
    dataIndex: 'kucunMingxi_Danwei',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'select',
    request: () =>
      kucunMingxiGroupServiceApi.kucunMingxiSearch_fenlei_View_DataSource({
        ...{},
        pageParament: params,
      }),
    title: '分类',
    dataIndex: 'kucunMingxi_Fenlei',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'text',
    title: 'Id',
    dataIndex: 'kucunMingxi_Id',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
    hideInTable: true,
  });
  columns.push({
    title: '操作',
    valueType: 'option',
    fixed: 'right',
    render: (text, record, _, action) => [
      <LinkTo
        key="KucunMingxiEdit"
        buttonProp={{ type: 'primary' }}
        path="/KucunMingxiGroup/KucunMingxiEditPage"
        query={{ id: record.kucunMingxi_Id }}
        icon={<EditOutlined />}
        successGoto={1}
      >
        编辑
      </LinkTo>,
      <LinkTo
        key="KucunMingxiDelete"
        buttonProp={{ danger: true, type: 'primary' }}
        path="/KucunMingxiGroup/KucunMingxiDeletePage"
        query={{ id: record.kucunMingxi_Id }}
        icon={<DeleteOutlined />}
        successGoto={1}
      >
        删除
      </LinkTo>,
      <LinkTo
        key="KucunMingxiDetail"
        buttonProp={{ type: 'link' }}
        path="/KucunMingxiGroup/KucunMingxiDetailPage"
        query={{ id: record.kucunMingxi_Id }}
        icon={<FileTextOutlined />}
        successGoto={1}
      >
        详情
      </LinkTo>,
    ],
  });

  const searchConfig: any = false;
  return (
    <PageContainer title="库存明细">
      <ProTable
        columns={columns}
        actionRef={ref}
        formRef={formref}
        manualRequest={true}
        search={searchConfig}
        scroll={{ x: 'auto' }}
        request={kucunMingxiSearch}
        pagination={{ showSizeChanger: true, defaultPageSize: 10, pageSizeOptions: [10, 20, 30] }}
        toolBarRender={() => [
          <LinkTo
            key="KucunMingxiAdd"
            buttonProp={{ type: 'primary' }}
            path="/KucunMingxiGroup/KucunMingxiAddPage"
            query={{}}
            icon={<FileAddOutlined />}
            successGoto={1}
          >
            添加
          </LinkTo>,
        ]}
      ></ProTable>
    </PageContainer>
  );
};

export default Listview;
