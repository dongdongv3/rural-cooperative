import * as kucunMingxiGroupServiceApi from '@/services/KucunMingxiGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProForm } from '@ant-design/pro-components';
import { useState } from 'react';
import { useRef } from 'react';
import { ProFormText } from '@ant-design/pro-components';
import { ProFormDigit } from '@ant-design/pro-components';
import { ProFormSelect } from '@ant-design/pro-components';

interface inputProperty {
  params?: {};
  [property: string]: any;
}

const KucunMingxiAdd: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const formref = useRef<FormInstance>();
  const readDefaultValue = async () => {
    const outdata = await kucunMingxiGroupServiceApi.kucunMingxiAdd_GetDefaultValue({
      pageParament: params,
    });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return convertData;
  };

  const saveValue = async (pageparams: any) => {
    setResoult(undefined);
    const convertData = { ...pageparams, pageParament: params };
    const tmp = await kucunMingxiGroupServiceApi.kucunMingxiAdd(convertData);
    setResoult(tmp);
  };

  return (
    <ProForm params={params} request={readDefaultValue} onFinish={saveValue} formRef={formref}>
      <ProFormText
        name={'wupinName'}
        label={'物品名称'}
        rules={[{ type: 'string', max: 50 }]}
      ></ProFormText>
      <ProFormDigit
        fieldProps={{ min: Number.MIN_SAFE_INTEGER, max: Number.MAX_SAFE_INTEGER }}
        name={'count'}
        label={'剩余数量'}
        rules={[{ min: 0, max: 1000000000, type: 'number' }]}
      ></ProFormDigit>
      <ProFormText
        name={'danwei'}
        label={'单位'}
        rules={[{ type: 'string', max: 10 }]}
      ></ProFormText>
      <ProFormSelect
        request={() =>
          kucunMingxiGroupServiceApi.kucunMingxiAdd_fenlei_DataSource({
            ...(formref.current?.getFieldsValue() || {}),
            pageParament: params,
          })
        }
        dependencies={[]}
        name={'fenlei'}
        label={'分类'}
        rules={[]}
      ></ProFormSelect>
    </ProForm>
  );
};

export default ResoultWapper(KucunMingxiAdd, '添加库存明细');
