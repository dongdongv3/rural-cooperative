import * as kucunMingxiGroupServiceApi from '@/services/KucunMingxiGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProForm } from '@ant-design/pro-components';
import { useState } from 'react';
import { useRef } from 'react';
import { ProFormText } from '@ant-design/pro-components';
import { ProFormDigit } from '@ant-design/pro-components';
import { ProFormSelect } from '@ant-design/pro-components';

interface inputProperty {
  params: {
    id: any;
  };
  [property: string]: any;
}

const KucunMingxiEdit: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const formref = useRef<FormInstance>();
  const getValue = async () => {
    const outdata = await kucunMingxiGroupServiceApi.kucunMingxiEdit_GetValue({
      ...params,
      pageParament: params,
    });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return convertData;
  };

  const saveValue = async (pageparams: any) => {
    setResoult(undefined);
    const convertData = { ...pageparams };
    const tmp = await kucunMingxiGroupServiceApi.kucunMingxiEdit({
      ...convertData,
      pageParament: params,
    });
    setResoult(tmp);
  };

  return (
    <ProForm
      params={params}
      request={getValue}
      onFinish={saveValue}
      formRef={formref}
      submitter={{
        render: (props, doms) => {
          return [...doms];
        },
      }}
    >
      <ProFormText
        name={'id'}
        label={'Id'}
        hidden={true}
        rules={[{ type: 'string', max: 50 }]}
      ></ProFormText>
      <ProFormText
        name={'wupinName'}
        label={'物品名称'}
        rules={[{ type: 'string', max: 50 }]}
      ></ProFormText>
      <ProFormDigit
        fieldProps={{ min: Number.MIN_SAFE_INTEGER, max: Number.MAX_SAFE_INTEGER }}
        name={'count'}
        label={'剩余数量'}
        rules={[{ min: 0, max: 1000000000, type: 'number' }]}
      ></ProFormDigit>
      <ProFormText
        name={'danwei'}
        label={'单位'}
        rules={[{ type: 'string', max: 10 }]}
      ></ProFormText>
      <ProFormSelect
        request={() =>
          kucunMingxiGroupServiceApi.kucunMingxiEdit_fenlei_DataSource({
            ...(formref.current?.getFieldsValue() || {}),
            pageParament: params,
          })
        }
        dependencies={[]}
        name={'fenlei'}
        label={'分类'}
        rules={[]}
      ></ProFormSelect>
    </ProForm>
  );
};

export default ResoultWapper(KucunMingxiEdit, '编辑库存明细');
