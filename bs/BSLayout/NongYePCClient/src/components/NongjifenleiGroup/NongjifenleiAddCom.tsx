import * as nongjifenleiGroupServiceApi from '@/services/NongjifenleiGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProForm } from '@ant-design/pro-components';
import { useState } from 'react';
import { useRef } from 'react';
import { ProFormText } from '@ant-design/pro-components';

interface inputProperty {
  params?: {};
  [property: string]: any;
}

const NongjifenleiAdd: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const formref = useRef<FormInstance>();
  const readDefaultValue = async () => {
    const outdata = await nongjifenleiGroupServiceApi.nongjifenleiAdd_GetDefaultValue({
      pageParament: params,
    });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return convertData;
  };

  const saveValue = async (pageparams: any) => {
    setResoult(undefined);
    const convertData = { ...pageparams, pageParament: params };
    const tmp = await nongjifenleiGroupServiceApi.nongjifenleiAdd(convertData);
    setResoult(tmp);
  };

  return (
    <ProForm params={params} request={readDefaultValue} onFinish={saveValue} formRef={formref}>
      <ProFormText
        name={'name'}
        label={'类型名'}
        rules={[{ type: 'string', max: 50 }]}
      ></ProFormText>
    </ProForm>
  );
};

export default ResoultWapper(NongjifenleiAdd, '添加农机分类');
