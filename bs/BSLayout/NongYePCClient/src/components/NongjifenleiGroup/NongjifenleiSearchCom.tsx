import * as nongjifenleiGroupServiceApi from '@/services/NongjifenleiGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ActionType } from '@ant-design/pro-components';
import { FormInstance } from '@ant-design/pro-components';
import { ProTable } from '@ant-design/pro-components';
import { PageContainer } from '@ant-design/pro-layout';
import { useRef } from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import LinkTo from '@/utils/LinkTo';
import { DeleteOutlined } from '@ant-design/icons';
import { FileAddOutlined } from '@ant-design/icons';

interface inputProperty {
  params?: {};
  [property: string]: any;
}

const Listview: React.FC<inputProperty> = ({ params }) => {
  const [defaultvalue, setdefaultvalue] = useState(undefined);
  const ref = useRef<ActionType>();
  const formref = useRef<FormInstance>();
  useEffect(() => {
    nongjifenleiGroupServiceApi
      .nongjifenleiSearch_GetDefaultValue({ pageParament: params })
      .then((data) => {
        const convertData = { ...data };
        setdefaultvalue(convertData);

        if (formref?.current) {
          formref?.current?.setFieldsValue(convertData);
          formref.current?.submit();
        } else {
          setTimeout(() => {
            ref.current?.reload();
          }, 100);
        }
      });
  }, [params]);

  const nongjifenleiSearch = async (request: any, sort: any, filter: any) => {
    const convertData = { ...request, pageParament: params };
    let data = await nongjifenleiGroupServiceApi.nongjifenleiSearch(convertData, sort, filter);
    let tmp = data.data;
    const convertValue: any[] = [];
    tmp.forEach((x: any) => {
      convertValue.push({ ...x });
    });
    tmp = convertValue;
    data.data = tmp;
    return data;
  };

  const columns: any[] = [];
  columns.push({
    valueType: 'text',
    title: '类型名',
    dataIndex: 'nongjifenlei_Name',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'text',
    title: 'Id',
    dataIndex: 'nongjifenlei_Id',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
    hideInTable: true,
  });
  columns.push({
    title: '操作',
    valueType: 'option',
    fixed: 'right',
    render: (text, record, _, action) => [
      <LinkTo
        key="NongjifenleiDelete"
        buttonProp={{ danger: true, type: 'primary' }}
        path="/NongjifenleiGroup/NongjifenleiDeletePage"
        query={{ id: record.nongjifenlei_Id }}
        icon={<DeleteOutlined />}
        successGoto={1}
      >
        删除
      </LinkTo>,
    ],
  });

  const searchConfig: any = false;
  return (
    <PageContainer title="农机分类">
      <ProTable
        columns={columns}
        actionRef={ref}
        formRef={formref}
        manualRequest={true}
        search={searchConfig}
        scroll={{ x: 'auto' }}
        request={nongjifenleiSearch}
        pagination={{ showSizeChanger: true, defaultPageSize: 10, pageSizeOptions: [10, 20, 30] }}
        toolBarRender={() => [
          <LinkTo
            key="NongjifenleiAdd"
            buttonProp={{ type: 'primary' }}
            path="/NongjifenleiGroup/NongjifenleiAddPage"
            query={{}}
            icon={<FileAddOutlined />}
            successGoto={1}
          >
            添加
          </LinkTo>,
        ]}
      ></ProTable>
    </PageContainer>
  );
};

export default Listview;
