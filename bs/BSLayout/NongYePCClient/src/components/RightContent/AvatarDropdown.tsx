import { outLogin } from '@/services/ant-design-pro/api';
import { LogoutOutlined, LoginOutlined, SettingOutlined, UserOutlined } from '@ant-design/icons';
import { Avatar, Menu, Spin } from 'antd';
import type { ItemType } from 'antd/lib/menu/hooks/useItems';
import { stringify } from 'querystring';
import type { MenuInfo } from 'rc-menu/lib/interface';
import React, { useCallback } from 'react';
import { history, useModel } from 'umi';
import HeaderDropdown from '../HeaderDropdown';
import styles from './index.less';
import { mgr } from '../../../config/oidc';

export type GlobalHeaderRightProps = {
  menu?: boolean;
};

/**
 * 退出登录，并且将当前的 url 保存
 */
const loginOut = async () => {
  mgr.signoutRedirect();
};

const AvatarDropdown: React.FC<GlobalHeaderRightProps> = ({ menu }) => {
  const { initialState, setInitialState } = useModel('@@initialState');
  const onMenuClick = useCallback(
    (event: MenuInfo) => {
      const { key } = event;
      if (key === 'logout') {
        setInitialState((s) => ({ ...s, currentUser: undefined }));
        loginOut();
        return;
      } else if (key == 'login') {
        mgr.signinRedirect();
      }
    },
    [setInitialState],
  );

  const loading = (
    <span className={`${styles.action} ${styles.account}`}>
      <Spin
        size="small"
        style={{
          marginLeft: 8,
          marginRight: 8,
        }}
      />
    </span>
  );

  let menuName = 'xxx';
  let loginOrlogout = {
    key: 'logout',
    icon: <LogoutOutlined />,
    label: '退出登录',
  };

  if (initialState && !initialState.isLogin) {
    loginOrlogout = {
      key: 'login',
      icon: <LoginOutlined />,
      label: '登录',
    };
    menuName = '未登录';
  } else {
    menuName = '你好 : ' + initialState?.currentUser.displayName;
  }

  const menuHeaderDropdown = (
    <Menu className={styles.menu} selectedKeys={[]} onClick={onMenuClick} items={[loginOrlogout]} />
  );

  return (
    <HeaderDropdown overlay={menuHeaderDropdown}>
      <span className={`${styles.action} ${styles.account}`}>
        {/* <Avatar size="small" className={styles.avatar} src={currentUser.avatar} alt="avatar" /> */}
        <span className={`${styles.name} anticon`}>{menuName}</span>
      </span>
    </HeaderDropdown>
  );
};

export default AvatarDropdown;
