import * as nongjihezuoGroupServiceApi from '@/services/NongjihezuoGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProForm } from '@ant-design/pro-components';
import { useState } from 'react';
import { useRef } from 'react';
import { ProFormText } from '@ant-design/pro-components';
import { ProFormSelect } from '@ant-design/pro-components';
import { ProFormMoney } from '@ant-design/pro-components';

interface inputProperty {
  params?: {};
  [property: string]: any;
}

const NongjihezuoAdd: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const formref = useRef<FormInstance>();
  const readDefaultValue = async () => {
    const outdata = await nongjihezuoGroupServiceApi.nongjihezuoAdd_GetDefaultValue({
      pageParament: params,
    });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return convertData;
  };

  const saveValue = async (pageparams: any) => {
    setResoult(undefined);
    const convertData = { ...pageparams, pageParament: params };
    const tmp = await nongjihezuoGroupServiceApi.nongjihezuoAdd(convertData);
    setResoult(tmp);
  };

  return (
    <ProForm params={params} request={readDefaultValue} onFinish={saveValue} formRef={formref}>
      <ProFormText
        name={'username'}
        label={'合作人名称'}
        rules={[{ type: 'string', max: 50 }]}
      ></ProFormText>
      <ProFormText
        name={'phoneNumber'}
        label={'合作人电话'}
        rules={[{ type: 'string', max: 50 }]}
      ></ProFormText>
      <ProFormSelect
        request={() =>
          nongjihezuoGroupServiceApi.nongjihezuoAdd_leixing_DataSource({
            ...(formref.current?.getFieldsValue() || {}),
            pageParament: params,
          })
        }
        dependencies={[]}
        name={'leixing'}
        label={'农机类型'}
        rules={[]}
      ></ProFormSelect>
      <ProFormText
        name={'xinghao'}
        label={'农机型号'}
        rules={[{ type: 'string', max: 50 }]}
      ></ProFormText>
      <ProFormMoney
        fieldProps={{ precision: 0 }}
        name={'qianyuejia'}
        label={'签约价(每亩)'}
        rules={[]}
      ></ProFormMoney>
    </ProForm>
  );
};

export default ResoultWapper(NongjihezuoAdd, '添加农机合作');
