import * as nongjihezuoGroupServiceApi from '@/services/NongjihezuoGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ActionType } from '@ant-design/pro-components';
import { FormInstance } from '@ant-design/pro-components';
import { ProTable } from '@ant-design/pro-components';
import { PageContainer } from '@ant-design/pro-layout';
import { useRef } from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import LinkTo from '@/utils/LinkTo';
import { EditOutlined } from '@ant-design/icons';
import { DeleteOutlined } from '@ant-design/icons';
import { FileTextOutlined } from '@ant-design/icons';
import { FileAddOutlined } from '@ant-design/icons';

interface inputProperty {
  params?: {};
  [property: string]: any;
}

const Listview: React.FC<inputProperty> = ({ params }) => {
  const [defaultvalue, setdefaultvalue] = useState(undefined);
  const ref = useRef<ActionType>();
  const formref = useRef<FormInstance>();
  useEffect(() => {
    nongjihezuoGroupServiceApi
      .nongjihezuoSearch_GetDefaultValue({ pageParament: params })
      .then((data) => {
        const convertData = { ...data };
        setdefaultvalue(convertData);

        if (formref?.current) {
          formref?.current?.setFieldsValue(convertData);
          formref.current?.submit();
        } else {
          setTimeout(() => {
            ref.current?.reload();
          }, 100);
        }
      });
  }, [params]);

  const nongjihezuoSearch = async (request: any, sort: any, filter: any) => {
    const convertData = { ...request, pageParament: params };
    let data = await nongjihezuoGroupServiceApi.nongjihezuoSearch(convertData, sort, filter);
    let tmp = data.data;
    const convertValue: any[] = [];
    tmp.forEach((x: any) => {
      convertValue.push({ ...x });
    });
    tmp = convertValue;
    data.data = tmp;
    return data;
  };

  const columns: any[] = [];
  columns.push({
    valueType: 'text',
    title: '合作人名称',
    dataIndex: 'nongjihezuo_Username',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'text',
    title: '合作人电话',
    dataIndex: 'nongjihezuo_PhoneNumber',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'select',
    request: () =>
      nongjihezuoGroupServiceApi.nongjihezuoSearch_leixing_View_DataSource({
        ...{},
        pageParament: params,
      }),
    title: '农机类型',
    dataIndex: 'nongjihezuo_Leixing',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'text',
    title: '农机型号',
    dataIndex: 'nongjihezuo_Xinghao',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'money',
    title: '签约价(每亩)',
    dataIndex: 'nongjihezuo_Qianyuejia',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'text',
    title: 'Id',
    dataIndex: 'nongjihezuo_Id',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
    hideInTable: true,
  });
  columns.push({
    title: '操作',
    valueType: 'option',
    fixed: 'right',
    render: (text, record, _, action) => [
      <LinkTo
        key="NongjihezuoEdit"
        buttonProp={{ type: 'primary' }}
        path="/NongjihezuoGroup/NongjihezuoEditPage"
        query={{ id: record.nongjihezuo_Id }}
        icon={<EditOutlined />}
        successGoto={1}
      >
        编辑
      </LinkTo>,
      <LinkTo
        key="NongjihezuoDelete"
        buttonProp={{ danger: true, type: 'primary' }}
        path="/NongjihezuoGroup/NongjihezuoDeletePage"
        query={{ id: record.nongjihezuo_Id }}
        icon={<DeleteOutlined />}
        successGoto={1}
      >
        删除
      </LinkTo>,
      <LinkTo
        key="NongjihezuoDetail"
        buttonProp={{ type: 'link' }}
        path="/NongjihezuoGroup/NongjihezuoDetailPage"
        query={{ id: record.nongjihezuo_Id }}
        icon={<FileTextOutlined />}
        successGoto={1}
      >
        详情
      </LinkTo>,
    ],
  });

  const searchConfig: any = false;
  return (
    <PageContainer title="农机合作">
      <ProTable
        columns={columns}
        actionRef={ref}
        formRef={formref}
        manualRequest={true}
        search={searchConfig}
        scroll={{ x: 'auto' }}
        request={nongjihezuoSearch}
        pagination={{ showSizeChanger: true, defaultPageSize: 10, pageSizeOptions: [10, 20, 30] }}
        toolBarRender={() => [
          <LinkTo
            key="NongjihezuoAdd"
            buttonProp={{ type: 'primary' }}
            path="/NongjihezuoGroup/NongjihezuoAddPage"
            query={{}}
            icon={<FileAddOutlined />}
            successGoto={1}
          >
            添加
          </LinkTo>,
        ]}
      ></ProTable>
    </PageContainer>
  );
};

export default Listview;
