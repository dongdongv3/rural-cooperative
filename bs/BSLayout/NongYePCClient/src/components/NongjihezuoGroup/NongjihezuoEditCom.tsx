import * as nongjihezuoGroupServiceApi from '@/services/NongjihezuoGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProForm } from '@ant-design/pro-components';
import { useState } from 'react';
import { useRef } from 'react';
import { ProFormText } from '@ant-design/pro-components';
import { ProFormSelect } from '@ant-design/pro-components';
import { ProFormMoney } from '@ant-design/pro-components';

interface inputProperty {
  params: {
    id: any;
  };
  [property: string]: any;
}

const NongjihezuoEdit: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const formref = useRef<FormInstance>();
  const getValue = async () => {
    const outdata = await nongjihezuoGroupServiceApi.nongjihezuoEdit_GetValue({
      ...params,
      pageParament: params,
    });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return convertData;
  };

  const saveValue = async (pageparams: any) => {
    setResoult(undefined);
    const convertData = { ...pageparams };
    const tmp = await nongjihezuoGroupServiceApi.nongjihezuoEdit({
      ...convertData,
      pageParament: params,
    });
    setResoult(tmp);
  };

  return (
    <ProForm
      params={params}
      request={getValue}
      onFinish={saveValue}
      formRef={formref}
      submitter={{
        render: (props, doms) => {
          return [...doms];
        },
      }}
    >
      <ProFormText
        name={'id'}
        label={'Id'}
        hidden={true}
        rules={[{ type: 'string', max: 50 }]}
      ></ProFormText>
      <ProFormText
        name={'username'}
        label={'合作人名称'}
        rules={[{ type: 'string', max: 50 }]}
      ></ProFormText>
      <ProFormText
        name={'phoneNumber'}
        label={'合作人电话'}
        rules={[{ type: 'string', max: 50 }]}
      ></ProFormText>
      <ProFormSelect
        request={() =>
          nongjihezuoGroupServiceApi.nongjihezuoEdit_leixing_DataSource({
            ...(formref.current?.getFieldsValue() || {}),
            pageParament: params,
          })
        }
        dependencies={[]}
        name={'leixing'}
        label={'农机类型'}
        rules={[]}
      ></ProFormSelect>
      <ProFormText
        name={'xinghao'}
        label={'农机型号'}
        rules={[{ type: 'string', max: 50 }]}
      ></ProFormText>
      <ProFormMoney
        fieldProps={{ precision: 0 }}
        name={'qianyuejia'}
        label={'签约价(每亩)'}
        rules={[]}
      ></ProFormMoney>
    </ProForm>
  );
};

export default ResoultWapper(NongjihezuoEdit, '编辑农机合作');
