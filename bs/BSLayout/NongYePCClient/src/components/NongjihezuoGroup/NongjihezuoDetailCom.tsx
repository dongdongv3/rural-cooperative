import * as nongjihezuoGroupServiceApi from '@/services/NongjihezuoGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProDescriptions } from '@ant-design/pro-components';
import { useState } from 'react';

interface inputProperty {
  params: {
    id: any;
  };
  [property: string]: any;
}

const NongjihezuoDetail: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const getValue = async () => {
    const outdata = await nongjihezuoGroupServiceApi.nongjihezuoDetail_GetValue({
      ...params,
      pageParament: params,
    });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return { data: convertData, success: true };
  };

  return (
    <ProDescriptions params={params} request={getValue} bordered={true}>
      <ProDescriptions.Item valueType={'text'} title={'合作人名称'} dataIndex={'username'} />
      <ProDescriptions.Item valueType={'text'} title={'合作人电话'} dataIndex={'phoneNumber'} />
      <ProDescriptions.Item
        valueType={'select'}
        request={() =>
          nongjihezuoGroupServiceApi.nongjihezuoDetail_leixing_DataSource({
            ...{},
            pageParament: params,
          })
        }
        title={'农机类型'}
        dataIndex={'leixing'}
      />
      <ProDescriptions.Item valueType={'text'} title={'农机型号'} dataIndex={'xinghao'} />
      <ProDescriptions.Item valueType={'money'} title={'签约价(每亩)'} dataIndex={'qianyuejia'} />
      <ProDescriptions.Item valueType="option"></ProDescriptions.Item>
    </ProDescriptions>
  );
};

export default ResoultWapper(NongjihezuoDetail, '详情农机合作');
