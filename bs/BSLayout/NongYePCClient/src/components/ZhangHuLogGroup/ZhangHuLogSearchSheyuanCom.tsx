import * as zhangHuLogGroupServiceApi from '@/services/ZhangHuLogGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ActionType } from '@ant-design/pro-components';
import { FormInstance } from '@ant-design/pro-components';
import { ProTable } from '@ant-design/pro-components';
import { PageContainer } from '@ant-design/pro-layout';
import { useRef } from 'react';
import { useState } from 'react';
import { useEffect } from 'react';

interface inputProperty {
  params: {
    refSheyuanId: any;
  };
  [property: string]: any;
}

const Listview: React.FC<inputProperty> = ({ params }) => {
  const [defaultvalue, setdefaultvalue] = useState(undefined);
  const ref = useRef<ActionType>();
  const formref = useRef<FormInstance>();
  useEffect(() => {
    zhangHuLogGroupServiceApi
      .zhangHuLogSearchSheyuan_GetDefaultValue({ pageParament: params })
      .then((data) => {
        const convertData = { ...data };
        setdefaultvalue(convertData);

        if (formref?.current) {
          formref?.current?.setFieldsValue(convertData);
          formref.current?.submit();
        } else {
          setTimeout(() => {
            ref.current?.reload();
          }, 100);
        }
      });
  }, [params]);

  const zhangHuLogSearchSheyuan = async (request: any, sort: any, filter: any) => {
    const convertData = { ...request, pageParament: params };
    let data = await zhangHuLogGroupServiceApi.zhangHuLogSearchSheyuan(convertData, sort, filter);
    let tmp = data.data;
    const convertValue: any[] = [];
    tmp.forEach((x: any) => {
      convertValue.push({ ...x });
    });
    tmp = convertValue;
    data.data = tmp;
    return data;
  };

  const columns: any[] = [];
  columns.push({
    valueType: 'dateTime',
    title: '创建时间',
    dataIndex: 'zhangHuLog_CreateTime',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'money',
    title: '操作金额',
    dataIndex: 'zhangHuLog_ActionMoney',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'text',
    title: '说明',
    dataIndex: 'zhangHuLog_Remark',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'text',
    title: 'Id',
    dataIndex: 'zhangHuLog_Id',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
    hideInTable: true,
  });
  columns.push({
    valueType: 'select',
    request: () =>
      zhangHuLogGroupServiceApi.zhangHuLogSearchSheyuan_RefSheyuanId_View_DataSource({
        ...{},
        pageParament: params,
      }),
    title: '社员-姓名',
    dataIndex: 'zhangHuLog_RefSheyuanId',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });

  const searchConfig: any = false;
  return (
    <PageContainer title="社员账户记录">
      <ProTable
        columns={columns}
        actionRef={ref}
        formRef={formref}
        manualRequest={true}
        search={searchConfig}
        scroll={{ x: 'auto' }}
        request={zhangHuLogSearchSheyuan}
        pagination={{ showSizeChanger: true, defaultPageSize: 10, pageSizeOptions: [10, 20, 30] }}
        toolBarRender={() => []}
      ></ProTable>
    </PageContainer>
  );
};

export default Listview;
