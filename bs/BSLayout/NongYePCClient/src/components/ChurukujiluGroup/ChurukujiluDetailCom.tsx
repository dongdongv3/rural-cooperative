import * as churukujiluGroupServiceApi from '@/services/ChurukujiluGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProDescriptions } from '@ant-design/pro-components';
import { useState } from 'react';

interface inputProperty {
  params: {
    id: any;
  };
  [property: string]: any;
}

const ChurukujiluDetail: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const getValue = async () => {
    const outdata = await churukujiluGroupServiceApi.churukujiluDetail_GetValue({
      ...params,
      pageParament: params,
    });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return { data: convertData, success: true };
  };

  return (
    <ProDescriptions params={params} request={getValue} bordered={true}>
      <ProDescriptions.Item valueType={'dateTime'} title={'发生时间'} dataIndex={'createtime'} />
      <ProDescriptions.Item valueType={'text'} title={'申请人'} dataIndex={'shengqingren'} />
      <ProDescriptions.Item
        valueType={'select'}
        request={() =>
          churukujiluGroupServiceApi.churukujiluDetail_ApprovedUser_DataSource({
            ...{},
            pageParament: params,
          })
        }
        title={'审批人'}
        dataIndex={'approvedUser'}
      />
      <ProDescriptions.Item
        valueType={'select'}
        request={() =>
          churukujiluGroupServiceApi.churukujiluDetail_leixing_DataSource({
            ...{},
            pageParament: params,
          })
        }
        title={'类型'}
        dataIndex={'leixing'}
      />
      <ProDescriptions.Item
        valueType={'select'}
        request={() =>
          churukujiluGroupServiceApi.churukujiluDetail_fenlei_DataSource({
            ...{},
            pageParament: params,
          })
        }
        title={'库存分类'}
        dataIndex={'fenlei'}
      />
      <ProDescriptions.Item
        valueType={'select'}
        request={() =>
          churukujiluGroupServiceApi.churukujiluDetail_wupin_DataSource({
            ...{},
            pageParament: params,
          })
        }
        title={'物品'}
        dataIndex={'wupin'}
      />
      <ProDescriptions.Item valueType={'digit'} title={'出入库数量'} dataIndex={'shuliang'} />
      <ProDescriptions.Item valueType="option"></ProDescriptions.Item>
    </ProDescriptions>
  );
};

export default ResoultWapper(ChurukujiluDetail, '详情出入库记录');
