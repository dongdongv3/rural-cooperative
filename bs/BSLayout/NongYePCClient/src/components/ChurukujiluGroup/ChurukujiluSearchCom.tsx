import * as churukujiluGroupServiceApi from '@/services/ChurukujiluGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ActionType } from '@ant-design/pro-components';
import { FormInstance } from '@ant-design/pro-components';
import { ProTable } from '@ant-design/pro-components';
import { PageContainer } from '@ant-design/pro-layout';
import { useRef } from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import LinkTo from '@/utils/LinkTo';
import { EditOutlined } from '@ant-design/icons';
import { DeleteOutlined } from '@ant-design/icons';
import { FileTextOutlined } from '@ant-design/icons';
import { FileAddOutlined } from '@ant-design/icons';

interface inputProperty {
  params?: {};
  [property: string]: any;
}

const Listview: React.FC<inputProperty> = ({ params }) => {
  const [defaultvalue, setdefaultvalue] = useState(undefined);
  const ref = useRef<ActionType>();
  const formref = useRef<FormInstance>();
  useEffect(() => {
    churukujiluGroupServiceApi
      .churukujiluSearch_GetDefaultValue({ pageParament: params })
      .then((data) => {
        const convertData = { ...data };
        setdefaultvalue(convertData);

        if (formref?.current) {
          formref?.current?.setFieldsValue(convertData);
          formref.current?.submit();
        } else {
          setTimeout(() => {
            ref.current?.reload();
          }, 100);
        }
      });
  }, [params]);

  const churukujiluSearch = async (request: any, sort: any, filter: any) => {
    const convertData = { ...request, pageParament: params };
    let data = await churukujiluGroupServiceApi.churukujiluSearch(convertData, sort, filter);
    let tmp = data.data;
    const convertValue: any[] = [];
    tmp.forEach((x: any) => {
      convertValue.push({ ...x });
    });
    tmp = convertValue;
    data.data = tmp;
    return data;
  };

  const columns: any[] = [];
  columns.push({
    valueType: 'dateTime',
    title: '发生时间',
    dataIndex: 'churukujilu_Createtime',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'text',
    title: '申请人',
    dataIndex: 'churukujilu_Shengqingren',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'select',
    request: () =>
      churukujiluGroupServiceApi.churukujiluSearch_ApprovedUser_View_DataSource({
        ...{},
        pageParament: params,
      }),
    title: '审批人',
    dataIndex: 'churukujilu_ApprovedUser',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'select',
    request: () =>
      churukujiluGroupServiceApi.churukujiluSearch_leixing_View_DataSource({
        ...{},
        pageParament: params,
      }),
    title: '类型',
    dataIndex: 'churukujilu_Leixing',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'select',
    request: () =>
      churukujiluGroupServiceApi.churukujiluSearch_fenlei_View_DataSource({
        ...{},
        pageParament: params,
      }),
    title: '库存分类',
    dataIndex: 'churukujilu_Fenlei',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'select',
    request: () =>
      churukujiluGroupServiceApi.churukujiluSearch_wupin_View_DataSource({
        ...{},
        pageParament: params,
      }),
    title: '物品',
    dataIndex: 'churukujilu_Wupin',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'digit',
    title: '出入库数量',
    dataIndex: 'churukujilu_Shuliang',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'text',
    title: 'Id',
    dataIndex: 'churukujilu_Id',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
    hideInTable: true,
  });
  columns.push({
    title: '操作',
    valueType: 'option',
    fixed: 'right',
    render: (text, record, _, action) => [
      <LinkTo
        key="ChurukujiluEdit"
        buttonProp={{ type: 'primary' }}
        path="/ChurukujiluGroup/ChurukujiluEditPage"
        query={{ id: record.churukujilu_Id }}
        icon={<EditOutlined />}
        successGoto={1}
      >
        编辑
      </LinkTo>,
      <LinkTo
        key="ChurukujiluDelete"
        buttonProp={{ danger: true, type: 'primary' }}
        path="/ChurukujiluGroup/ChurukujiluDeletePage"
        query={{ id: record.churukujilu_Id }}
        icon={<DeleteOutlined />}
        successGoto={1}
      >
        删除
      </LinkTo>,
      <LinkTo
        key="ChurukujiluDetail"
        buttonProp={{ type: 'link' }}
        path="/ChurukujiluGroup/ChurukujiluDetailPage"
        query={{ id: record.churukujilu_Id }}
        icon={<FileTextOutlined />}
        successGoto={1}
      >
        详情
      </LinkTo>,
    ],
  });

  const searchConfig: any = false;
  return (
    <PageContainer title="出入库">
      <ProTable
        columns={columns}
        actionRef={ref}
        formRef={formref}
        manualRequest={true}
        search={searchConfig}
        scroll={{ x: 'auto' }}
        request={churukujiluSearch}
        pagination={{ showSizeChanger: true, defaultPageSize: 10, pageSizeOptions: [10, 20, 30] }}
        toolBarRender={() => [
          <LinkTo
            key="ChurukujiluAdd"
            buttonProp={{ type: 'primary' }}
            path="/ChurukujiluGroup/ChurukujiluAddPage"
            query={{}}
            icon={<FileAddOutlined />}
            successGoto={1}
          >
            添加
          </LinkTo>,
        ]}
      ></ProTable>
    </PageContainer>
  );
};

export default Listview;
