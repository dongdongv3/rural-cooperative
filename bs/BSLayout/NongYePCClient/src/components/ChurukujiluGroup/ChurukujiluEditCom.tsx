import * as churukujiluGroupServiceApi from '@/services/ChurukujiluGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProForm } from '@ant-design/pro-components';
import { useState } from 'react';
import { useRef } from 'react';
import { ProFormText } from '@ant-design/pro-components';
import { ProFormSelect } from '@ant-design/pro-components';
import { ProFormDigit } from '@ant-design/pro-components';

interface inputProperty {
  params: {
    id: any;
  };
  [property: string]: any;
}

const ChurukujiluEdit: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const formref = useRef<FormInstance>();
  const getValue = async () => {
    const outdata = await churukujiluGroupServiceApi.churukujiluEdit_GetValue({
      ...params,
      pageParament: params,
    });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return convertData;
  };

  const saveValue = async (pageparams: any) => {
    setResoult(undefined);
    const convertData = { ...pageparams };
    const tmp = await churukujiluGroupServiceApi.churukujiluEdit({
      ...convertData,
      pageParament: params,
    });
    setResoult(tmp);
  };

  return (
    <ProForm
      params={params}
      request={getValue}
      onFinish={saveValue}
      formRef={formref}
      submitter={{
        render: (props, doms) => {
          return [...doms];
        },
      }}
    >
      <ProFormText
        name={'id'}
        label={'Id'}
        hidden={true}
        rules={[{ type: 'string', max: 50 }]}
      ></ProFormText>
      <ProFormText
        name={'shengqingren'}
        label={'申请人'}
        rules={[{ type: 'string', max: 50 }]}
      ></ProFormText>
      <ProFormSelect
        request={() =>
          churukujiluGroupServiceApi.churukujiluEdit_ApprovedUser_DataSource({
            ...(formref.current?.getFieldsValue() || {}),
            pageParament: params,
          })
        }
        dependencies={[]}
        name={'approvedUser'}
        label={'审批人'}
        rules={[]}
      ></ProFormSelect>
      <ProFormSelect
        request={() =>
          churukujiluGroupServiceApi.churukujiluEdit_leixing_DataSource({
            ...(formref.current?.getFieldsValue() || {}),
            pageParament: params,
          })
        }
        dependencies={[]}
        name={'leixing'}
        label={'类型'}
        rules={[]}
      ></ProFormSelect>
      <ProFormSelect
        request={() =>
          churukujiluGroupServiceApi.churukujiluEdit_fenlei_DataSource({
            ...(formref.current?.getFieldsValue() || {}),
            pageParament: params,
          })
        }
        dependencies={[]}
        name={'fenlei'}
        label={'库存分类'}
        rules={[]}
      ></ProFormSelect>
      <ProFormSelect
        request={() =>
          churukujiluGroupServiceApi.churukujiluEdit_wupin_DataSource({
            ...(formref.current?.getFieldsValue() || {}),
            pageParament: params,
          })
        }
        dependencies={[]}
        name={'wupin'}
        label={'物品'}
        rules={[]}
      ></ProFormSelect>
      <ProFormDigit
        fieldProps={{ min: Number.MIN_SAFE_INTEGER, max: Number.MAX_SAFE_INTEGER }}
        name={'shuliang'}
        label={'出入库数量'}
        rules={[]}
      ></ProFormDigit>
    </ProForm>
  );
};

export default ResoultWapper(ChurukujiluEdit, '编辑出入库记录');
