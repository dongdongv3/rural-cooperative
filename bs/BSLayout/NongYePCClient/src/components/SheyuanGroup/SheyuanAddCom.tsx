import * as sheyuanGroupServiceApi from '@/services/SheyuanGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProForm } from '@ant-design/pro-components';
import { useState } from 'react';
import { useRef } from 'react';
import moment from 'moment';
import { ProFormText } from '@ant-design/pro-components';
import { ProFormDatePicker } from '@ant-design/pro-components';
import { ProFormSelect } from '@ant-design/pro-components';
import { ProFormDigit } from '@ant-design/pro-components';
import { ProFormMoney } from '@ant-design/pro-components';

interface inputProperty {
  params?: {};
  [property: string]: any;
}

const SheyuanAdd: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const formref = useRef<FormInstance>();
  const readDefaultValue = async () => {
    const outdata = await sheyuanGroupServiceApi.sheyuanAdd_GetDefaultValue({
      pageParament: params,
    });
    const convertData = {
      ...outdata,
      joinTime:
        outdata.joinTime?._Year &&
        moment(outdata.joinTime?._Year, 'yyyy')
          .add(outdata.joinTime._Month - 1, 'months')
          .format(),
    };
    setRecord({ ...record, ...convertData });
    return convertData;
  };

  const saveValue = async (pageparams: any) => {
    setResoult(undefined);
    const convertData = {
      ...pageparams,
      pageParament: params,
      joinTime: {
        _Year: pageparams.joinTime?.split('-')[0],
        _Month: pageparams.joinTime?.split('-')[1],
      },
    };
    const tmp = await sheyuanGroupServiceApi.sheyuanAdd(convertData);
    setResoult(tmp);
  };

  return (
    <ProForm params={params} request={readDefaultValue} onFinish={saveValue} formRef={formref}>
      <ProFormText
        name={'userName'}
        label={'姓名'}
        rules={[{ type: 'string', max: 10 }]}
      ></ProFormText>
      <ProFormText
        name={'userCode'}
        label={'身份证'}
        rules={[{ type: 'string', max: 50 }]}
      ></ProFormText>
      <ProFormText
        name={'phoneNumber'}
        label={'联系电话'}
        rules={[{ type: 'string', max: 50 }]}
      ></ProFormText>
      <ProFormDatePicker.Month
        fieldProps={{ format: 'yyyy-M' }}
        name={'joinTime'}
        label={'加入日期'}
        rules={[]}
      ></ProFormDatePicker.Month>
      <ProFormSelect
        request={() =>
          sheyuanGroupServiceApi.sheyuanAdd_Panqu_DataSource({
            ...(formref.current?.getFieldsValue() || {}),
            pageParament: params,
          })
        }
        dependencies={[]}
        name={'panqu'}
        label={'片区'}
        rules={[]}
      ></ProFormSelect>
      <ProFormDigit
        fieldProps={{ min: Number.MIN_SAFE_INTEGER, max: Number.MAX_SAFE_INTEGER }}
        name={'miangji'}
        label={'面积'}
        rules={[{ min: 0, max: 10000000, type: 'number' }]}
      ></ProFormDigit>
      <ProFormSelect
        request={() =>
          sheyuanGroupServiceApi.sheyuanAdd_Status_DataSource({
            ...(formref.current?.getFieldsValue() || {}),
            pageParament: params,
          })
        }
        dependencies={[]}
        name={'status'}
        label={'状态'}
        rules={[]}
      ></ProFormSelect>
      <ProFormMoney
        fieldProps={{ precision: 0 }}
        name={'yue'}
        label={'账户余额'}
        rules={[]}
      ></ProFormMoney>
    </ProForm>
  );
};

export default ResoultWapper(SheyuanAdd, '添加社员');
