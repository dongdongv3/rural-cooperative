import * as sheyuanGroupServiceApi from '@/services/SheyuanGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ActionType } from '@ant-design/pro-components';
import { FormInstance } from '@ant-design/pro-components';
import { ProTable } from '@ant-design/pro-components';
import { PageContainer } from '@ant-design/pro-layout';
import { useRef } from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import moment from 'moment';
import LinkTo from '@/utils/LinkTo';
import { EditOutlined } from '@ant-design/icons';
import { DeleteOutlined } from '@ant-design/icons';
import { FileTextOutlined } from '@ant-design/icons';
import { SubnodeOutlined } from '@ant-design/icons';
import { FileAddOutlined } from '@ant-design/icons';

interface inputProperty {
  params?: {};
  [property: string]: any;
}

const Listview: React.FC<inputProperty> = ({ params }) => {
  const [defaultvalue, setdefaultvalue] = useState(undefined);
  const ref = useRef<ActionType>();
  const formref = useRef<FormInstance>();
  useEffect(() => {
    sheyuanGroupServiceApi.sheyuanSearch_GetDefaultValue({ pageParament: params }).then((data) => {
      const convertData = { ...data };
      setdefaultvalue(convertData);

      if (formref?.current) {
        formref?.current?.setFieldsValue(convertData);
        formref.current?.submit();
      } else {
        setTimeout(() => {
          ref.current?.reload();
        }, 100);
      }
    });
  }, [params]);

  const sheyuanSearch = async (request: any, sort: any, filter: any) => {
    const convertData = { ...request, pageParament: params };
    let data = await sheyuanGroupServiceApi.sheyuanSearch(convertData, sort, filter);
    let tmp = data.data;
    const convertValue: any[] = [];
    tmp.forEach((x: any) => {
      convertValue.push({
        ...x,
        sheyuan_JoinTime:
          x.sheyuan_JoinTime?._Year &&
          moment(x.sheyuan_JoinTime?._Year, 'yyyy')
            .add(x.sheyuan_JoinTime._Month - 1, 'months')
            .format(),
      });
    });
    tmp = convertValue;
    data.data = tmp;
    return data;
  };

  const columns: any[] = [];
  columns.push({
    valueType: 'text',
    title: '姓名',
    dataIndex: 'sheyuan_UserName',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'text',
    title: '身份证',
    dataIndex: 'sheyuan_UserCode',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'text',
    title: '联系电话',
    dataIndex: 'sheyuan_PhoneNumber',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'dateMonth',
    fieldProps: { format: 'YYYY-M' },
    title: '加入日期',
    dataIndex: 'sheyuan_JoinTime',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'select',
    request: () =>
      sheyuanGroupServiceApi.sheyuanSearch_Panqu_View_DataSource({ ...{}, pageParament: params }),
    title: '片区',
    dataIndex: 'sheyuan_Panqu',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'digit',
    title: '面积',
    dataIndex: 'sheyuan_Miangji',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'select',
    request: () =>
      sheyuanGroupServiceApi.sheyuanSearch_Status_View_DataSource({ ...{}, pageParament: params }),
    title: '状态',
    dataIndex: 'sheyuan_Status',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'money',
    title: '账户余额',
    dataIndex: 'sheyuan_Yue',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'text',
    title: 'Id',
    dataIndex: 'sheyuan_Id',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
    hideInTable: true,
  });
  columns.push({
    title: '操作',
    valueType: 'option',
    fixed: 'right',
    render: (text, record, _, action) => [
      <LinkTo
        key="SheyuanEdit"
        buttonProp={{ type: 'primary' }}
        path="/SheyuanGroup/SheyuanEditPage"
        query={{ id: record.sheyuan_Id }}
        icon={<EditOutlined />}
        successGoto={1}
      >
        编辑
      </LinkTo>,
      <LinkTo
        key="SheyuanDelete"
        buttonProp={{ danger: true, type: 'primary' }}
        path="/SheyuanGroup/SheyuanDeletePage"
        query={{ id: record.sheyuan_Id }}
        icon={<DeleteOutlined />}
        successGoto={1}
      >
        删除
      </LinkTo>,
      <LinkTo
        key="SheyuanDetail"
        buttonProp={{ type: 'link' }}
        path="/SheyuanGroup/SheyuanDetailPage"
        query={{ id: record.sheyuan_Id }}
        icon={<FileTextOutlined />}
        successGoto={1}
      >
        详情
      </LinkTo>,
      <LinkTo
        key="ZhangHuLogSearchSheyuan"
        buttonProp={{ type: 'link' }}
        path="/ZhangHuLogGroup/ZhangHuLogSearchSheyuanPage"
        query={{ refSheyuanId: record.sheyuan_Id }}
        icon={<SubnodeOutlined />}
        successGoto={1}
      >
        账户记录管理
      </LinkTo>,
    ],
  });

  const searchConfig: any = false;
  return (
    <PageContainer title="管理社员">
      <ProTable
        columns={columns}
        actionRef={ref}
        formRef={formref}
        manualRequest={true}
        search={searchConfig}
        scroll={{ x: 'auto' }}
        request={sheyuanSearch}
        pagination={{ showSizeChanger: true, defaultPageSize: 10, pageSizeOptions: [10, 20, 30] }}
        toolBarRender={() => [
          <LinkTo
            key="SheyuanAdd"
            buttonProp={{ type: 'primary' }}
            path="/SheyuanGroup/SheyuanAddPage"
            query={{}}
            icon={<FileAddOutlined />}
            successGoto={1}
          >
            添加
          </LinkTo>,
        ]}
      ></ProTable>
    </PageContainer>
  );
};

export default Listview;
