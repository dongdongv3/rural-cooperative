import * as sheyuanGroupServiceApi from '@/services/SheyuanGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProDescriptions } from '@ant-design/pro-components';
import { useState } from 'react';
import moment from 'moment';

interface inputProperty {
  params: {
    id: any;
  };
  [property: string]: any;
}

const SheyuanDetail: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const getValue = async () => {
    const outdata = await sheyuanGroupServiceApi.sheyuanDetail_GetValue({
      ...params,
      pageParament: params,
    });
    const convertData = {
      ...outdata,
      joinTime:
        outdata.joinTime?._Year &&
        moment(outdata.joinTime?._Year, 'yyyy')
          .add(outdata.joinTime._Month - 1, 'months')
          .format(),
    };
    setRecord({ ...record, ...convertData });
    return { data: convertData, success: true };
  };

  return (
    <ProDescriptions params={params} request={getValue} bordered={true}>
      <ProDescriptions.Item valueType={'text'} title={'姓名'} dataIndex={'userName'} />
      <ProDescriptions.Item valueType={'text'} title={'身份证'} dataIndex={'userCode'} />
      <ProDescriptions.Item valueType={'text'} title={'联系电话'} dataIndex={'phoneNumber'} />
      <ProDescriptions.Item
        valueType={'dateMonth'}
        fieldProps={{ format: 'YYYY-M' }}
        title={'加入日期'}
        dataIndex={'joinTime'}
      />
      <ProDescriptions.Item
        valueType={'select'}
        request={() =>
          sheyuanGroupServiceApi.sheyuanDetail_Panqu_DataSource({ ...{}, pageParament: params })
        }
        title={'片区'}
        dataIndex={'panqu'}
      />
      <ProDescriptions.Item valueType={'digit'} title={'面积'} dataIndex={'miangji'} />
      <ProDescriptions.Item
        valueType={'select'}
        request={() =>
          sheyuanGroupServiceApi.sheyuanDetail_Status_DataSource({ ...{}, pageParament: params })
        }
        title={'状态'}
        dataIndex={'status'}
      />
      <ProDescriptions.Item valueType={'money'} title={'账户余额'} dataIndex={'yue'} />
      <ProDescriptions.Item valueType="option"></ProDescriptions.Item>
    </ProDescriptions>
  );
};

export default ResoultWapper(SheyuanDetail, '详情社员');
