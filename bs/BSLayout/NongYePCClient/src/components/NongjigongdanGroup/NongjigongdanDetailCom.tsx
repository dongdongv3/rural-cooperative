import * as nongjigongdanGroupServiceApi from '@/services/NongjigongdanGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProDescriptions } from '@ant-design/pro-components';
import { useState } from 'react';

interface inputProperty {
  params: {
    id: any;
  };
  [property: string]: any;
}

const NongjigongdanDetail: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const getValue = async () => {
    const outdata = await nongjigongdanGroupServiceApi.nongjigongdanDetail_GetValue({
      ...params,
      pageParament: params,
    });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return { data: convertData, success: true };
  };

  return (
    <ProDescriptions params={params} request={getValue} bordered={true}>
      <ProDescriptions.Item valueType={'dateTime'} title={'创建时间'} dataIndex={'createtimre'} />
      <ProDescriptions.Item valueType={'dateTime'} title={'处理时间'} dataIndex={'beginTime'} />
      <ProDescriptions.Item valueType={'digit'} title={'面积'} dataIndex={'miangji'} />
      <ProDescriptions.Item
        valueType={'select'}
        request={() =>
          nongjigongdanGroupServiceApi.nongjigongdanDetail_leixing_DataSource({
            ...{},
            pageParament: params,
          })
        }
        title={'农机类型'}
        dataIndex={'leixing'}
      />
      <ProDescriptions.Item
        valueType={'select'}
        request={() =>
          nongjigongdanGroupServiceApi.nongjigongdanDetail_chuli_DataSource({
            ...{},
            pageParament: params,
          })
        }
        title={'接单农机'}
        dataIndex={'chuli'}
      />
      <ProDescriptions.Item valueType={'money'} title={'总价'} dataIndex={'zongjia'} />
      <ProDescriptions.Item
        valueType={'select'}
        request={() =>
          nongjigongdanGroupServiceApi.nongjigongdanDetail_zhuangtai_DataSource({
            ...{},
            pageParament: params,
          })
        }
        title={'状态'}
        dataIndex={'zhuangtai'}
      />
      <ProDescriptions.Item
        valueType={'select'}
        request={() =>
          nongjigongdanGroupServiceApi.nongjigongdanDetail_jeisuanzhuangtai_DataSource({
            ...{},
            pageParament: params,
          })
        }
        title={'结算状态'}
        dataIndex={'jeisuanzhuangtai'}
      />
      <ProDescriptions.Item valueType="option"></ProDescriptions.Item>
    </ProDescriptions>
  );
};

export default ResoultWapper(NongjigongdanDetail, '详情农机使用工单');
