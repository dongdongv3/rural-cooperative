import * as nongjigongdanGroupServiceApi from '@/services/NongjigongdanGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProForm } from '@ant-design/pro-components';
import { useState } from 'react';
import { useRef } from 'react';
import { ProFormDigit } from '@ant-design/pro-components';
import { ProFormSelect } from '@ant-design/pro-components';
import { ProFormMoney } from '@ant-design/pro-components';

interface inputProperty {
  params?: {};
  [property: string]: any;
}

const NongjigongdanAdd: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const formref = useRef<FormInstance>();
  const readDefaultValue = async () => {
    const outdata = await nongjigongdanGroupServiceApi.nongjigongdanAdd_GetDefaultValue({
      pageParament: params,
    });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return convertData;
  };

  const saveValue = async (pageparams: any) => {
    setResoult(undefined);
    const convertData = { ...pageparams, pageParament: params };
    const tmp = await nongjigongdanGroupServiceApi.nongjigongdanAdd(convertData);
    setResoult(tmp);
  };

  return (
    <ProForm params={params} request={readDefaultValue} onFinish={saveValue} formRef={formref}>
      <ProFormDigit
        fieldProps={{ min: Number.MIN_SAFE_INTEGER, max: Number.MAX_SAFE_INTEGER }}
        name={'miangji'}
        label={'面积'}
        rules={[{ min: 0, max: 10000000, type: 'number' }, { required: true }]}
      ></ProFormDigit>
      <ProFormSelect
        request={() =>
          nongjigongdanGroupServiceApi.nongjigongdanAdd_leixing_DataSource({
            ...(formref.current?.getFieldsValue() || {}),
            pageParament: params,
          })
        }
        dependencies={[]}
        name={'leixing'}
        label={'农机类型'}
        rules={[{ required: true }]}
      ></ProFormSelect>
      <ProFormSelect
        request={() =>
          nongjigongdanGroupServiceApi.nongjigongdanAdd_chuli_DataSource({
            ...(formref.current?.getFieldsValue() || {}),
            pageParament: params,
          })
        }
        dependencies={[]}
        name={'chuli'}
        label={'接单农机'}
        rules={[{ required: true }]}
      ></ProFormSelect>
      <ProFormMoney
        fieldProps={{ precision: 0 }}
        name={'zongjia'}
        label={'总价'}
        rules={[]}
      ></ProFormMoney>
      <ProFormSelect
        request={() =>
          nongjigongdanGroupServiceApi.nongjigongdanAdd_zhuangtai_DataSource({
            ...(formref.current?.getFieldsValue() || {}),
            pageParament: params,
          })
        }
        dependencies={[]}
        name={'zhuangtai'}
        label={'状态'}
        rules={[{ required: true }]}
      ></ProFormSelect>
      <ProFormSelect
        request={() =>
          nongjigongdanGroupServiceApi.nongjigongdanAdd_jeisuanzhuangtai_DataSource({
            ...(formref.current?.getFieldsValue() || {}),
            pageParament: params,
          })
        }
        dependencies={[]}
        name={'jeisuanzhuangtai'}
        label={'结算状态'}
        rules={[{ required: true }]}
      ></ProFormSelect>
    </ProForm>
  );
};

export default ResoultWapper(NongjigongdanAdd, '添加农机使用工单');
