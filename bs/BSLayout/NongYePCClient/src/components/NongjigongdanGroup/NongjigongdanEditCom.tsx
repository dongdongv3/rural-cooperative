import * as nongjigongdanGroupServiceApi from '@/services/NongjigongdanGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ProForm } from '@ant-design/pro-components';
import { useState } from 'react';
import { useRef } from 'react';
import { ProFormText } from '@ant-design/pro-components';
import { ProFormDateTimePicker } from '@ant-design/pro-components';
import { ProFormDigit } from '@ant-design/pro-components';
import { ProFormSelect } from '@ant-design/pro-components';
import { ProFormMoney } from '@ant-design/pro-components';

interface inputProperty {
  params: {
    id: any;
  };
  [property: string]: any;
}

const NongjigongdanEdit: React.FC<inputProperty> = ({ params, setResoult }) => {
  const [record, setRecord] = useState<any>({ ...params });
  const formref = useRef<FormInstance>();
  const getValue = async () => {
    const outdata = await nongjigongdanGroupServiceApi.nongjigongdanEdit_GetValue({
      ...params,
      pageParament: params,
    });
    const convertData = { ...outdata };
    setRecord({ ...record, ...convertData });
    return convertData;
  };

  const saveValue = async (pageparams: any) => {
    setResoult(undefined);
    const convertData = { ...pageparams };
    const tmp = await nongjigongdanGroupServiceApi.nongjigongdanEdit({
      ...convertData,
      pageParament: params,
    });
    setResoult(tmp);
  };

  return (
    <ProForm
      params={params}
      request={getValue}
      onFinish={saveValue}
      formRef={formref}
      submitter={{
        render: (props, doms) => {
          return [...doms];
        },
      }}
    >
      <ProFormText
        name={'id'}
        label={'Id'}
        hidden={true}
        rules={[{ type: 'string', max: 50 }]}
      ></ProFormText>
      <ProFormDateTimePicker
        name={'beginTime'}
        label={'处理时间'}
        rules={[]}
      ></ProFormDateTimePicker>
      <ProFormDigit
        fieldProps={{ min: Number.MIN_SAFE_INTEGER, max: Number.MAX_SAFE_INTEGER }}
        name={'miangji'}
        label={'面积'}
        rules={[{ min: 0, max: 10000000, type: 'number' }]}
      ></ProFormDigit>
      <ProFormSelect
        request={() =>
          nongjigongdanGroupServiceApi.nongjigongdanEdit_leixing_DataSource({
            ...(formref.current?.getFieldsValue() || {}),
            pageParament: params,
          })
        }
        dependencies={[]}
        name={'leixing'}
        label={'农机类型'}
        rules={[]}
      ></ProFormSelect>
      <ProFormSelect
        request={() =>
          nongjigongdanGroupServiceApi.nongjigongdanEdit_chuli_DataSource({
            ...(formref.current?.getFieldsValue() || {}),
            pageParament: params,
          })
        }
        dependencies={[]}
        name={'chuli'}
        label={'接单农机'}
        rules={[]}
      ></ProFormSelect>
      <ProFormMoney
        fieldProps={{ precision: 0 }}
        name={'zongjia'}
        label={'总价'}
        rules={[]}
      ></ProFormMoney>
      <ProFormSelect
        request={() =>
          nongjigongdanGroupServiceApi.nongjigongdanEdit_zhuangtai_DataSource({
            ...(formref.current?.getFieldsValue() || {}),
            pageParament: params,
          })
        }
        dependencies={[]}
        name={'zhuangtai'}
        label={'状态'}
        rules={[]}
      ></ProFormSelect>
      <ProFormSelect
        request={() =>
          nongjigongdanGroupServiceApi.nongjigongdanEdit_jeisuanzhuangtai_DataSource({
            ...(formref.current?.getFieldsValue() || {}),
            pageParament: params,
          })
        }
        dependencies={[]}
        name={'jeisuanzhuangtai'}
        label={'结算状态'}
        rules={[]}
      ></ProFormSelect>
    </ProForm>
  );
};

export default ResoultWapper(NongjigongdanEdit, '编辑农机使用工单');
