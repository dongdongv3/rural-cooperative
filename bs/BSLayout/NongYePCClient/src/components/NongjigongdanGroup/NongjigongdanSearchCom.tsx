import * as nongjigongdanGroupServiceApi from '@/services/NongjigongdanGroupServiceApi';
import ResoultWapper from '@/utils/resoultWapper';
import { ActionType } from '@ant-design/pro-components';
import { FormInstance } from '@ant-design/pro-components';
import { ProTable } from '@ant-design/pro-components';
import { PageContainer } from '@ant-design/pro-layout';
import { useRef } from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import LinkTo from '@/utils/LinkTo';
import { EditOutlined } from '@ant-design/icons';
import { DeleteOutlined } from '@ant-design/icons';
import { FileTextOutlined } from '@ant-design/icons';
import { FileAddOutlined } from '@ant-design/icons';

interface inputProperty {
  params?: {};
  [property: string]: any;
}

const Listview: React.FC<inputProperty> = ({ params }) => {
  const [defaultvalue, setdefaultvalue] = useState(undefined);
  const ref = useRef<ActionType>();
  const formref = useRef<FormInstance>();
  useEffect(() => {
    nongjigongdanGroupServiceApi
      .nongjigongdanSearch_GetDefaultValue({ pageParament: params })
      .then((data) => {
        const convertData = { ...data };
        setdefaultvalue(convertData);

        if (formref?.current) {
          formref?.current?.setFieldsValue(convertData);
          formref.current?.submit();
        } else {
          setTimeout(() => {
            ref.current?.reload();
          }, 100);
        }
      });
  }, [params]);

  const nongjigongdanSearch = async (request: any, sort: any, filter: any) => {
    const convertData = { ...request, pageParament: params };
    let data = await nongjigongdanGroupServiceApi.nongjigongdanSearch(convertData, sort, filter);
    let tmp = data.data;
    const convertValue: any[] = [];
    tmp.forEach((x: any) => {
      convertValue.push({ ...x });
    });
    tmp = convertValue;
    data.data = tmp;
    return data;
  };

  const columns: any[] = [];
  columns.push({
    valueType: 'dateTime',
    title: '创建时间',
    dataIndex: 'nongjigongdan_Createtimre',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'dateTime',
    title: '处理时间',
    dataIndex: 'nongjigongdan_BeginTime',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'digit',
    title: '面积',
    dataIndex: 'nongjigongdan_Miangji',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'select',
    request: () =>
      nongjigongdanGroupServiceApi.nongjigongdanSearch_leixing_View_DataSource({
        ...{},
        pageParament: params,
      }),
    title: '农机类型',
    dataIndex: 'nongjigongdan_Leixing',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'select',
    request: () =>
      nongjigongdanGroupServiceApi.nongjigongdanSearch_chuli_View_DataSource({
        ...{},
        pageParament: params,
      }),
    title: '接单农机',
    dataIndex: 'nongjigongdan_Chuli',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'money',
    title: '总价',
    dataIndex: 'nongjigongdan_Zongjia',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'select',
    request: () =>
      nongjigongdanGroupServiceApi.nongjigongdanSearch_zhuangtai_View_DataSource({
        ...{},
        pageParament: params,
      }),
    title: '状态',
    dataIndex: 'nongjigongdan_Zhuangtai',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'select',
    request: () =>
      nongjigongdanGroupServiceApi.nongjigongdanSearch_jeisuanzhuangtai_View_DataSource({
        ...{},
        pageParament: params,
      }),
    title: '结算状态',
    dataIndex: 'nongjigongdan_Jeisuanzhuangtai',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
  });
  columns.push({
    valueType: 'text',
    title: 'Id',
    dataIndex: 'nongjigongdan_Id',
    hideInSearch: true,
    hideInForm: true,
    sorter: true,
    hideInTable: true,
  });
  columns.push({
    title: '操作',
    valueType: 'option',
    fixed: 'right',
    render: (text, record, _, action) => [
      <LinkTo
        key="NongjigongdanEdit"
        buttonProp={{ type: 'primary' }}
        path="/NongjigongdanGroup/NongjigongdanEditPage"
        query={{ id: record.nongjigongdan_Id }}
        icon={<EditOutlined />}
        successGoto={1}
      >
        编辑
      </LinkTo>,
      <LinkTo
        key="NongjigongdanDelete"
        buttonProp={{ danger: true, type: 'primary' }}
        path="/NongjigongdanGroup/NongjigongdanDeletePage"
        query={{ id: record.nongjigongdan_Id }}
        icon={<DeleteOutlined />}
        successGoto={1}
      >
        删除
      </LinkTo>,
      <LinkTo
        key="NongjigongdanDetail"
        buttonProp={{ type: 'link' }}
        path="/NongjigongdanGroup/NongjigongdanDetailPage"
        query={{ id: record.nongjigongdan_Id }}
        icon={<FileTextOutlined />}
        successGoto={1}
      >
        详情
      </LinkTo>,
    ],
  });

  const searchConfig: any = false;
  return (
    <PageContainer title="农机使用工单">
      <ProTable
        columns={columns}
        actionRef={ref}
        formRef={formref}
        manualRequest={true}
        search={searchConfig}
        scroll={{ x: 'auto' }}
        request={nongjigongdanSearch}
        pagination={{ showSizeChanger: true, defaultPageSize: 10, pageSizeOptions: [10, 20, 30] }}
        toolBarRender={() => [
          <LinkTo
            key="NongjigongdanAdd"
            buttonProp={{ type: 'primary' }}
            path="/NongjigongdanGroup/NongjigongdanAddPage"
            query={{}}
            icon={<FileAddOutlined />}
            successGoto={1}
          >
            添加
          </LinkTo>,
        ]}
      ></ProTable>
    </PageContainer>
  );
};

export default Listview;
