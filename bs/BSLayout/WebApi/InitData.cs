using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using WebApi.Code.Data;
using WebApi.Code.Common;

namespace WebApi
{
  public class InitData
  {
    public static void DatInit(IServiceScope serviceScope)
    {
      var _heZuosheDB = serviceScope.ServiceProvider.GetRequiredService<HeZuosheDB>();
      Console.WriteLine("初始化单元：权限或角色列表初始化");
      {
        var tmp = CommonDataSource.AuthList_Auto_SysUser();
        tmp.ForEach(one =>
        {

          var auth = _heZuosheDB.SysUser_Auth.FirstOrDefault(x => x.AuthName == one.Key);
          if (auth == null)
          {
            auth = new SysUser_Auth
            {
              Id = Guid.NewGuid(),
              AuthName = one.Key,
            };
            _heZuosheDB.Add(auth);
          }
        });

        _heZuosheDB.SaveChanges();
        //初始化超管角色
        var authList = _heZuosheDB.SysUser_Auth.ToList();
        var autoCreateAdminRole = ReadSetting.GetValue<string>("角色初始化", "是否需要初始化超级管理员角色?", new string[] { "是", "否" });
        if (autoCreateAdminRole == "是")
        {
          var adminName = ReadSetting.GetValue<string>("角色初始化", "请输入超级管理员角色名称");
          var adminrole = _heZuosheDB.SysUser_Role.FirstOrDefault(x => x.RoleName == adminName);
          if (adminrole == null)
          {
            adminrole = new SysUser_Role
            {
              Id = Guid.NewGuid(),
              RoleName = adminName,
              SubSysUser_RoleAuthMap = authList.Select(x => new SysUser_RoleAuthMap { Id = Guid.NewGuid(), RefSysUser_Auth = x }).ToList()
            };
            _heZuosheDB.Add(adminrole);
          }
        }
        _heZuosheDB.SaveChanges();
      }
      Console.WriteLine("初始化单元：初始化用户");

      //初始化用户
      var autoCreateUser = ReadSetting.GetValue<string>("用户初始化", "是否需要初始化超级管理员?.", new string[] { "是", "否" });
      if (autoCreateUser == "是")
      {
        var adminDisplayName = ReadSetting.GetValue<string>("用户初始化", "请输入超级管理员显示名");

        var adminRoleName = ReadSetting.GetValue<string>("用户初始化", "请输入超级管理员拥有的角色名称", _heZuosheDB.SysUser_Role.Select(x => x.RoleName).ToList().ToArray());
        var adminRole = _heZuosheDB.SysUser_Role.FirstOrDefault(x => x.RoleName == adminRoleName);
        if (adminRole == null)
        {
          throw new Exception($"无法找到角色‘{adminRoleName}’");
        }
        var adminUser = _heZuosheDB.SysUser.Include(e => e.SubSysUser_AccountAndPasswordAuth1).FirstOrDefault(x => x.UserName == adminDisplayName);
        if (adminUser == null)
        {
          adminUser = new SysUser
          {
            Id = Guid.NewGuid().ToString(),
            UserName = adminDisplayName,

            SubSysUser_AccountAndPasswordAuth1 = new SysUser_AccountAndPasswordAuth1
            {
              Id = Guid.NewGuid(),
              LoginID = ReadSetting.GetValue<string>("用户初始化", "请输入超级管理员登录名"),
              Pwd = ReadSetting.GetValue<string>("用户初始化", "请输入超级管理员登录密码")
            },
            SubSysUser_UserRoleMap = new List<SysUser_UserRoleMap>{
              new SysUser_UserRoleMap{
                Id=Guid.NewGuid(),
                RefSysUser_Role=adminRole
              }
            }
          };
          _heZuosheDB.Add(adminUser);
        }
      }
      _heZuosheDB.SaveChanges();

    }
  }
}
