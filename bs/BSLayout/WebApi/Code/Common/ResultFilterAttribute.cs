using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApi.Code.Common
{
  /// <summary>
  /// 包装结果集
  /// </summary>
  public class ResultFilterAttribute : Attribute, IAsyncResultFilter
  {
    public async Task OnResultExecutionAsync(ResultExecutingContext context, ResultExecutionDelegate next)
    {
      if (context.Result is ObjectResult t)
      {
        if (context.Result is BadRequestObjectResult && t.Value is ValidationProblemDetails)
        {
          context.Result = new ObjectResult(new BaseRsp<object>
          {
            Code = -1,
            Data = t.Value,
            IsOk = false,
            Message = "模型验证错误"
          })
          { StatusCode = 400 };
        }
        else
        {
          context.Result = new ObjectResult(new BaseRsp<object>
          {
            Code = 1,
            Data = t.Value,
            IsOk = true,
            Message = "成功"
          });
        }
      }

      await next.Invoke();
    }
  }
}
