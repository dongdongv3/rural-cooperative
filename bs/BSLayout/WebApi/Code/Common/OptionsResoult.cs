using System;

namespace WebApi.Code.Common
{
  public class OptionsResoult
  {

    /// <summary>
    /// 操作结果的状态
    /// </summary>
    public OptionsResoultStatus Status { get; set; }
    /// <summary>
    /// 错误代码
    /// </summary>
    public int ErrorCode { get; set; }
    /// <summary>
    /// 操作结果的文本描述
    /// </summary>
    public string Message { get; set; }

  }
}
