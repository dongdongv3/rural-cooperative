using System;
using System.Collections.Generic;

namespace WebApi.Code.Common
{
  public class CommonDataSource
  {
    /// <summary>
    /// 预设置权限列表-自动生成
    /// </summary>
    public static List<KeyValuePair<string, string>> AuthList_Auto_SysUser()
    {
      var output = new List<KeyValuePair<string, string>>();
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/添加", "/SysUserGroup/添加"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/编辑", "/SysUserGroup/编辑"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/删除", "/SysUserGroup/删除"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/详情", "/SysUserGroup/详情"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/查询", "/SysUserGroup/查询"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/添加", "/SysUserGroup/添加"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/添加", "/SysUserGroup/添加"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/编辑", "/SysUserGroup/编辑"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/编辑", "/SysUserGroup/编辑"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/删除", "/SysUserGroup/删除"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/详情", "/SysUserGroup/详情"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/查询", "/SysUserGroup/查询"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/查询", "/SysUserGroup/查询"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/添加", "/SysUserGroup/添加"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/添加", "/SysUserGroup/添加"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/编辑", "/SysUserGroup/编辑"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/编辑", "/SysUserGroup/编辑"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/删除", "/SysUserGroup/删除"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/详情", "/SysUserGroup/详情"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/查询", "/SysUserGroup/查询"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/查询", "/SysUserGroup/查询"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/添加", "/SysUserGroup/添加"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/编辑", "/SysUserGroup/编辑"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/删除", "/SysUserGroup/删除"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/详情", "/SysUserGroup/详情"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/查询", "/SysUserGroup/查询"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/添加", "/SysUserGroup/添加"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/编辑", "/SysUserGroup/编辑"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/删除", "/SysUserGroup/删除"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/详情", "/SysUserGroup/详情"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/查询", "/SysUserGroup/查询"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/添加", "/SysUserGroup/添加"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/编辑", "/SysUserGroup/编辑"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/删除", "/SysUserGroup/删除"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/详情", "/SysUserGroup/详情"));
      output.Add(new KeyValuePair<string, string>("/SysUserGroup/查询", "/SysUserGroup/查询"));
      output.Add(new KeyValuePair<string, string>("/NongjifenleiGroup/添加", "/NongjifenleiGroup/添加"));
      output.Add(new KeyValuePair<string, string>("/NongjifenleiGroup/删除", "/NongjifenleiGroup/删除"));
      output.Add(new KeyValuePair<string, string>("/NongjifenleiGroup/查询", "/NongjifenleiGroup/查询"));
      output.Add(new KeyValuePair<string, string>("/NongjihezuoGroup/添加", "/NongjihezuoGroup/添加"));
      output.Add(new KeyValuePair<string, string>("/NongjihezuoGroup/编辑", "/NongjihezuoGroup/编辑"));
      output.Add(new KeyValuePair<string, string>("/NongjihezuoGroup/详情", "/NongjihezuoGroup/详情"));
      output.Add(new KeyValuePair<string, string>("/NongjihezuoGroup/删除", "/NongjihezuoGroup/删除"));
      output.Add(new KeyValuePair<string, string>("/NongjihezuoGroup/查询", "/NongjihezuoGroup/查询"));
      output.Add(new KeyValuePair<string, string>("/NongjigongdanGroup/添加", "/NongjigongdanGroup/添加"));
      output.Add(new KeyValuePair<string, string>("/NongjigongdanGroup/编辑", "/NongjigongdanGroup/编辑"));
      output.Add(new KeyValuePair<string, string>("/NongjigongdanGroup/详情", "/NongjigongdanGroup/详情"));
      output.Add(new KeyValuePair<string, string>("/NongjigongdanGroup/删除", "/NongjigongdanGroup/删除"));
      output.Add(new KeyValuePair<string, string>("/NongjigongdanGroup/查询", "/NongjigongdanGroup/查询"));
      output.Add(new KeyValuePair<string, string>("/GengdiTypeGroup/添加", "/GengdiTypeGroup/添加"));
      output.Add(new KeyValuePair<string, string>("/GengdiTypeGroup/删除", "/GengdiTypeGroup/删除"));
      output.Add(new KeyValuePair<string, string>("/GengdiTypeGroup/查询", "/GengdiTypeGroup/查询"));
      output.Add(new KeyValuePair<string, string>("/KucunTypeGroup/添加", "/KucunTypeGroup/添加"));
      output.Add(new KeyValuePair<string, string>("/KucunTypeGroup/删除", "/KucunTypeGroup/删除"));
      output.Add(new KeyValuePair<string, string>("/KucunTypeGroup/查询", "/KucunTypeGroup/查询"));
      output.Add(new KeyValuePair<string, string>("/KucunMingxiGroup/添加", "/KucunMingxiGroup/添加"));
      output.Add(new KeyValuePair<string, string>("/KucunMingxiGroup/编辑", "/KucunMingxiGroup/编辑"));
      output.Add(new KeyValuePair<string, string>("/KucunMingxiGroup/详情", "/KucunMingxiGroup/详情"));
      output.Add(new KeyValuePair<string, string>("/KucunMingxiGroup/删除", "/KucunMingxiGroup/删除"));
      output.Add(new KeyValuePair<string, string>("/KucunMingxiGroup/查询", "/KucunMingxiGroup/查询"));
      output.Add(new KeyValuePair<string, string>("/ChurukujiluGroup/添加", "/ChurukujiluGroup/添加"));
      output.Add(new KeyValuePair<string, string>("/ChurukujiluGroup/编辑", "/ChurukujiluGroup/编辑"));
      output.Add(new KeyValuePair<string, string>("/ChurukujiluGroup/详情", "/ChurukujiluGroup/详情"));
      output.Add(new KeyValuePair<string, string>("/ChurukujiluGroup/删除", "/ChurukujiluGroup/删除"));
      output.Add(new KeyValuePair<string, string>("/ChurukujiluGroup/查询", "/ChurukujiluGroup/查询"));
      output.Add(new KeyValuePair<string, string>("/ZhangHuLogGroup/查询", "/ZhangHuLogGroup/查询"));
      output.Add(new KeyValuePair<string, string>("/SheyuanGroup/添加", "/SheyuanGroup/添加"));
      output.Add(new KeyValuePair<string, string>("/SheyuanGroup/编辑", "/SheyuanGroup/编辑"));
      output.Add(new KeyValuePair<string, string>("/SheyuanGroup/详情", "/SheyuanGroup/详情"));
      output.Add(new KeyValuePair<string, string>("/SheyuanGroup/删除", "/SheyuanGroup/删除"));
      output.Add(new KeyValuePair<string, string>("/SheyuanGroup/查询", "/SheyuanGroup/查询"));
      return output;
    }
  }
}
