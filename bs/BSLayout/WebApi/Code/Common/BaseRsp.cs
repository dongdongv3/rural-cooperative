using System;

namespace WebApi.Code.Common
{
  public class BaseRsp<T>
  {
    public int Code { get; set; }
    public T Data { get; set; }
    public bool IsOk { get; set; }
    public string Message { get; set; }
  }
}
