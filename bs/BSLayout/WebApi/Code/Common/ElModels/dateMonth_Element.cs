using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace WebApi.Code.Common.ElModels
{
  public class dateMonth_Element
  {
    public int? _Year { get; set; }
    public int? _Month { get; set; }
  }
}
