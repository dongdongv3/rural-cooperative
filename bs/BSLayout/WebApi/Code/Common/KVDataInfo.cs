using System;
using System.Collections.Generic;

namespace WebApi.Code.Common
{
  public class KVDataInfo
  {
    public KVDataInfo()
    {
    }
    public KVDataInfo(object value, string label)
    {
      this.Value = value;
      this.Label = label;
    }
    /// <summary>
    /// 存储值
    /// </summary>
    public object Value { get; set; }
    /// <summary>
    /// 显示值
    /// </summary>
    public string Label { get; set; }
  }
}
