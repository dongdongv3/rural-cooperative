using System;
using System.Security.Cryptography;
using System.Text;

namespace WebApi.Code.Common.PwdProvider
{
  /// <summary>
  /// MD5加密服务
  /// </summary>
  public class MD5Encrypt : IPWDEncryptInterface
  {
    public string Encrypt(string key)
    {
      return Md5(key);
    }

    private string Md5(string key)
    {
      using (var md5 = MD5.Create())
      {
        var result = md5.ComputeHash(Encoding.UTF8.GetBytes(key));
        var strResult = BitConverter.ToString(result);
        string result3 = strResult.Replace("-", "");
        return result3;
      }
    }
  }
}
