using System;
using WebApi.Code.Common;

namespace WebApi.Code.Common.PwdProvider
{
  public class PWDEncryptProvider
  {
    /// <summary>
    /// 获取加密服务
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public static IPWDEncryptInterface GetPWDEncrypt(PWDEncryptType type)
    {
      switch (type)
      {
        case PWDEncryptType.None:
          return new NoneEncrypt();
        case PWDEncryptType.MD5:
          return new MD5Encrypt();
        case PWDEncryptType.MD52:
          return new MD52Encrypt();
        default:
          throw new AuthException("无效的密码加密提供程序");
      }
    }
    public enum PWDEncryptType
    {
      MD5,
      MD52,
      None
    }
  }
}
