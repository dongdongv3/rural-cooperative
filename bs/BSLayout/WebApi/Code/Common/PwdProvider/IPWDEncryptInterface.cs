using System;

namespace WebApi.Code.Common.PwdProvider
{
  public interface IPWDEncryptInterface
  {
    /// <summary>
    /// 密码加密服务
    /// </summary>
    string Encrypt(string key);
  }
}
