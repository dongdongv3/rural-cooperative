using System;
using Microsoft.AspNetCore.Http;
using WebApi.Code.Common.Authentication;

namespace WebApi.Code.Common
{
  public static class HttpContextExt
  {
    /// <summary>
    /// 当前请求的用户对象
    /// </summary>
    /// <param name="context"></param>
    /// <returns></returns>
    public static UserInfo UserInfo(this HttpContext context)
    {
      var user = context.Items["UserInfo"] as UserInfo;
      if (user == null) throw new AuthException("未经过认证");
      return user;
    }
    /// <summary>
    /// 填充当前请求的用户信息
    /// </summary>
    /// <param name="context"></param>
    /// <param name="userInfo"></param>
    public static void UserInfoSet(this HttpContext context, UserInfo userInfo)
    {
      context.Items["UserInfo"] = userInfo;
    }
  }
}
