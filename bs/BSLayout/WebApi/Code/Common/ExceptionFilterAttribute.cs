using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Hosting;

namespace WebApi.Code.Common
{
  public class ExceptionFilterAttribute : Microsoft.AspNetCore.Mvc.Filters.ExceptionFilterAttribute
  {
    /// <summary>
    /// 吞噬异常返回BaseRsp，默认true，
    /// 如果设置false则只记录未知异常
    /// </summary>
    public bool NoThrow { get; set; } = true;
    public override void OnException(ExceptionContext context)
    {
      var tmpid = Guid.NewGuid().ToString("N");
      var tmpidstr = tmpid.Substring(0, 10);
      var Logger = context.HttpContext.RequestServices.GetService(typeof(ILogger<ExceptionFilterAttribute>)) as ILogger<ExceptionFilterAttribute>;
      var environment = context.HttpContext.RequestServices.GetService(typeof(IWebHostEnvironment)) as IWebHostEnvironment;
      if (context.Exception != null)
      {
        if (context.Exception is ViewException d && NoThrow)
        {
          context.Result = new ObjectResult(new BaseRsp<string>
          {
            Code = d.Code,
            Data = "失败",
            IsOk = false,
            Message = d.Message
          })
          { StatusCode = 501 }; ;
          return;
        }
        if (context.Exception is AuthException d2 && NoThrow)
        {
          context.Result = new ObjectResult(new BaseRsp<string>
          {
            Code = d2.Code,
            Data = "失败",
            IsOk = false,
            Message = d2.Message
          })
          { StatusCode = 401 };
          return;
        }
        Logger.LogError(context.Exception, context.Exception.Message);
        if (NoThrow)
        {
          if (environment.IsDevelopment())
          {
            context.Result = new ObjectResult(new BaseRsp<string>
            {
              Code = -10,
              Data = "失败",
              IsOk = false,
              Message = context.Exception.ToString()
            })
            { StatusCode = 500 };
            return;
          }
          else
          {
            context.Result = new ObjectResult(new BaseRsp<string>
            {
              Code = -10,
              Data = "失败",
              IsOk = false,
              Message = $"系统未知错误[{tmpid}]"
            })
            { StatusCode = 500 };
            return;
          }
        }
        if (!NoThrow)
        {
          throw context.Exception;
        }
      }
      else
      {
        base.OnException(context);
      }
    }
  }
}
