using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace WebApi.Code.Common
{
  public static class LinqExtend
  {
    /// <summary>
    /// 排序
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="source"></param>
    /// <param name="sortExpression">排序列</param>
    /// <param name="sortDirection">排序方式 asc  desc</param>
    /// <returns></returns>
    public static IQueryable<T> Sort<T>(this IQueryable<T> source, string sortExpression, string sortDirection)
    {
      if (string.IsNullOrEmpty(sortDirection))
      {
        return source;
      }
      if (string.IsNullOrEmpty(sortExpression))
      {
        return source;
      }

      string sortingDir = string.Empty;
      string thenSortingDir = string.Empty;
      if (sortDirection.ToLower().Trim() == "asc")
      {
        sortingDir = "OrderBy";
        thenSortingDir = "ThenBy";
      }
      else if (sortDirection.ToLower().Trim() == "desc")
      {
        sortingDir = "OrderByDescending";
        thenSortingDir = "ThenByDescending";
      }

      if (sortingDir == string.Empty)
      {
        throw new ViewException($"无效的排序方式:{sortDirection}");
      }
      ParameterExpression param = Expression.Parameter(typeof(T), sortExpression);
      var propertyInfos = typeof(T).GetProperties();
      var pi = propertyInfos.FirstOrDefault(x => x.Name.Equals(sortExpression, StringComparison.CurrentCultureIgnoreCase));
      if (pi == null)
      {
        throw new ViewException($"无效的排序字段:{sortExpression}");
      }
      if (!pi.PropertyType.FullName.StartsWith("System"))
      {
        //其他类型
        var subp = pi.PropertyType.GetProperties().ToList();
        //Expression
        Expression expr = Expression.Call(typeof(Queryable), sortingDir, new Type[] { typeof(T), subp[0].PropertyType }, source.Expression, Expression.Lambda(Expression.Property(Expression.Property(param, sortExpression), subp[0].Name), param));
        for (int i = 1; i < subp.Count; i++)
        {
          var p = subp[i];
          expr = Expression.Call(typeof(Queryable), thenSortingDir, new Type[] { typeof(T), p.PropertyType }, expr, Expression.Lambda(Expression.Property(Expression.Property(param, sortExpression), p.Name), param));
        }
        IQueryable<T> query = source.AsQueryable().Provider.CreateQuery<T>(expr);
        return query;
      }
      else
      {
        Type[] types = new Type[2];
        types[0] = typeof(T);
        types[1] = pi.PropertyType;
        Expression expr = Expression.Call(typeof(Queryable), sortingDir, types, source.Expression, Expression.Lambda(Expression.Property(param, sortExpression), param));
        IQueryable<T> query = source.AsQueryable().Provider.CreateQuery<T>(expr);
        return query;
      }
    }
  }
}
