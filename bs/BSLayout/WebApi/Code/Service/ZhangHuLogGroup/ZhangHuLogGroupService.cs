using System;
using WebApi.Code.Common.ElModels;
using WebApi.Code.Interface.ZhangHuLogGroup;
using WebApi.Code.Interface.ZhangHuLogGroup.Models;
using WebApi.Code.Common;
using System.Collections.Generic;
using WebApi.Code.Common.Authentication;
using System.Linq;
using WebApi.Code.Data;

namespace WebApi.Code.Service.ZhangHuLogGroup
{
  public class ZhangHuLogGroupService : IZhangHuLogGroupInterface
  {
    public readonly HeZuosheDB _heZuosheDB;

    public ZhangHuLogGroupService(HeZuosheDB _heZuosheDB)
    {
      this._heZuosheDB = _heZuosheDB;
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<ZhangHuLogSearchSheyuanResponse> ZhangHuLogSearchSheyuan(ZhangHuLogSearchSheyuanQueryRequest request, UserInfo loginUser)
    {
      //填充变量
      if (string.IsNullOrEmpty(request.SortName)) request.SortName = "ZhangHuLog_CreateTime";
      if (string.IsNullOrEmpty(request.SortType)) request.SortType = "desc";
      if (request.PageIndex <= 0) request.PageIndex = 1;
      if (request.PageSize <= 0) request.PageSize = 10;

      //验证输入
      {
        var sortnameList = new List<string> { "ZhangHuLog_CreateTime", "ZhangHuLog_ActionMoney", "ZhangHuLog_Remark", "ZhangHuLog_Id", "ZhangHuLog_RefSheyuanId" };
        if (sortnameList.All(one => !one.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase)))
        {
          throw new ViewException($"无效的排序列{request.SortName}");
        }
        else
        {
          request.SortName = sortnameList.FirstOrDefault(x => x.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase));
        }
      }
      {
        var pageSizeList = new List<int> { 10, 20, 30 };
        if (pageSizeList.All(one => one != request.PageSize))
        {
          throw new ViewException($"无效的页大小{request.PageSize}");
        }
      }
      // select
      var select =
      from ZhangHuLog in _heZuosheDB.ZhangHuLog
      select new { ZhangHuLog };

      // where
      if (request.PageParament.RefSheyuanId != null)
      {
        select = select.Where(x => x.ZhangHuLog.RefSheyuanId == request.PageParament.RefSheyuanId);
      }

      // query
      var queryoutput = select.Select(x => new ZhangHuLogSearchSheyuanResponse
      {
        ZhangHuLog_CreateTime = x.ZhangHuLog.CreateTime,
        ZhangHuLog_ActionMoney = x.ZhangHuLog.ActionMoney,
        ZhangHuLog_Remark = x.ZhangHuLog.Remark,
        ZhangHuLog_Id = x.ZhangHuLog.Id,
        ZhangHuLog_RefSheyuanId = x.ZhangHuLog.RefSheyuanId,
      });

      // sort
      queryoutput = queryoutput.Sort(request.SortName, request.SortType);
      // pager
      var count = queryoutput.Count();
      queryoutput = queryoutput.Skip((request.PageIndex - 1) * request.PageSize)
      .Take(request.PageSize);

      return new PagerInfo<ZhangHuLogSearchSheyuanResponse>
      {
        Total = count,
        Data = queryoutput.ToList(),
        PageSize = request.PageSize,
        PageIndex = request.PageIndex,
      };
    }

    /// <summary>
    /// 查询
    /// </summary>
    public ZhangHuLogSearchSheyuanDefaultValueResponse ZhangHuLogSearchSheyuan_GetDefaultValue(ZhangHuLogSearchSheyuanPageparameter request, UserInfo loginUser)
    {
      return new ZhangHuLogSearchSheyuanDefaultValueResponse
      {
      };
    }

    /// <summary>
    /// '社员-姓名' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> ZhangHuLogSearchSheyuan_RefSheyuanId_View_DataSource(ZhangHuLogSearchSheyuanPageparameter request, UserInfo loginUser)
    {
      var select =
      from Sheyuan in _heZuosheDB.Sheyuan
      select new { Sheyuan }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Sheyuan.Id, x.Sheyuan.UserName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '社员-姓名' 字段的数据源-where
    /// </summary>
    public List<KVDataInfo> ZhangHuLogSearchSheyuan_RefSheyuanId_Where_DataSource(ZhangHuLogSearchSheyuanQueryRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Sheyuan in _heZuosheDB.Sheyuan
      select new { Sheyuan }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Sheyuan.Id, x.Sheyuan.UserName)
      {
      }).ToList();
      return queryoutput;
    }

  }
}
