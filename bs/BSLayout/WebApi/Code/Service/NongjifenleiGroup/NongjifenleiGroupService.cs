using System;
using WebApi.Code.Common.ElModels;
using WebApi.Code.Interface.NongjifenleiGroup;
using WebApi.Code.Interface.NongjifenleiGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Linq;
using System.Collections.Generic;
using WebApi.Code.Data;

namespace WebApi.Code.Service.NongjifenleiGroup
{
  public class NongjifenleiGroupService : INongjifenleiGroupInterface
  {
    public readonly HeZuosheDB _heZuosheDB;

    public NongjifenleiGroupService(HeZuosheDB _heZuosheDB)
    {
      this._heZuosheDB = _heZuosheDB;
    }

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult NongjifenleiAdd(NongjifenleiAddRequest request, UserInfo loginUser)
    {
      var nongjifenlei = new Nongjifenlei
      {
        Id = Guid.NewGuid(),
        Name = request.Name,
      };
      _heZuosheDB.Add(nongjifenlei);
      _heZuosheDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘添加’操作执行成功"
      };
    }

    /// <summary>
    /// 添加
    /// </summary>
    public NongjifenleiAddDefaultValueResponse NongjifenleiAdd_GetDefaultValue(NongjifenleiAddPageparameter request, UserInfo loginUser)
    {
      return new NongjifenleiAddDefaultValueResponse
      {
        Name = default(string?),
      };
    }

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult NongjifenleiDelete(NongjifenleiDeleteRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.Nongjifenlei.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { Nongjifenlei = entity };
      if (entity != null)
      {
        _heZuosheDB.Remove(entity);
        _heZuosheDB.SaveChanges();
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.succeed,
          ErrorCode = 0,
          Message = "‘删除’操作执行成功,受影响行数‘1’"
        };
      }
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.failed,
        ErrorCode = 1,
        Message = "‘删除’操作执行失败，无法找到目标数据,受影响行数‘0’"
      };
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<NongjifenleiSearchResponse> NongjifenleiSearch(NongjifenleiSearchQueryRequest request, UserInfo loginUser)
    {
      //填充变量
      if (string.IsNullOrEmpty(request.SortName)) request.SortName = "Nongjifenlei_Name";
      if (string.IsNullOrEmpty(request.SortType)) request.SortType = "desc";
      if (request.PageIndex <= 0) request.PageIndex = 1;
      if (request.PageSize <= 0) request.PageSize = 10;

      //验证输入
      {
        var sortnameList = new List<string> { "Nongjifenlei_Name", "Nongjifenlei_Id" };
        if (sortnameList.All(one => !one.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase)))
        {
          throw new ViewException($"无效的排序列{request.SortName}");
        }
        else
        {
          request.SortName = sortnameList.FirstOrDefault(x => x.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase));
        }
      }
      {
        var pageSizeList = new List<int> { 10, 20, 30 };
        if (pageSizeList.All(one => one != request.PageSize))
        {
          throw new ViewException($"无效的页大小{request.PageSize}");
        }
      }
      // select
      var select =
      from Nongjifenlei in _heZuosheDB.Nongjifenlei
      select new { Nongjifenlei };

      // where

      // query
      var queryoutput = select.Select(x => new NongjifenleiSearchResponse
      {
        Nongjifenlei_Name = x.Nongjifenlei.Name,
        Nongjifenlei_Id = x.Nongjifenlei.Id,
      });

      // sort
      queryoutput = queryoutput.Sort(request.SortName, request.SortType);
      // pager
      var count = queryoutput.Count();
      queryoutput = queryoutput.Skip((request.PageIndex - 1) * request.PageSize)
      .Take(request.PageSize);

      return new PagerInfo<NongjifenleiSearchResponse>
      {
        Total = count,
        Data = queryoutput.ToList(),
        PageSize = request.PageSize,
        PageIndex = request.PageIndex,
      };
    }

    /// <summary>
    /// 查询
    /// </summary>
    public NongjifenleiSearchDefaultValueResponse NongjifenleiSearch_GetDefaultValue(NongjifenleiSearchPageparameter request, UserInfo loginUser)
    {
      return new NongjifenleiSearchDefaultValueResponse
      {
      };
    }

  }
}
