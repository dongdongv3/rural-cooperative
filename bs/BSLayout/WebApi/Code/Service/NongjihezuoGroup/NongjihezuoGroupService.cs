using System;
using WebApi.Code.Common.ElModels;
using WebApi.Code.Interface.NongjihezuoGroup;
using WebApi.Code.Interface.NongjihezuoGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using WebApi.Code.Data;

namespace WebApi.Code.Service.NongjihezuoGroup
{
  public class NongjihezuoGroupService : INongjihezuoGroupInterface
  {
    public readonly HeZuosheDB _heZuosheDB;

    public NongjihezuoGroupService(HeZuosheDB _heZuosheDB)
    {
      this._heZuosheDB = _heZuosheDB;
    }

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult NongjihezuoAdd(NongjihezuoAddRequest request, UserInfo loginUser)
    {
      var nongjihezuo = new Nongjihezuo
      {
        Id = Guid.NewGuid(),
        Username = request.Username,
        PhoneNumber = request.PhoneNumber,
        Leixing = request.Leixing,
        Xinghao = request.Xinghao,
        Qianyuejia = request.Qianyuejia,
      };
      _heZuosheDB.Add(nongjihezuo);
      _heZuosheDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘添加’操作执行成功"
      };
    }

    /// <summary>
    /// 添加
    /// </summary>
    public NongjihezuoAddDefaultValueResponse NongjihezuoAdd_GetDefaultValue(NongjihezuoAddPageparameter request, UserInfo loginUser)
    {
      return new NongjihezuoAddDefaultValueResponse
      {
        Username = default(string?),
        PhoneNumber = default(string?),
        Leixing = default(string?),
        Xinghao = default(string?),
        Qianyuejia = default(decimal?),
      };
    }

    /// <summary>
    /// '农机类型' 字段的数据源
    /// </summary>
    public List<KVDataInfo> NongjihezuoAdd_leixing_DataSource(NongjihezuoAddRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Nongjifenlei in _heZuosheDB.Nongjifenlei
      select new { Nongjifenlei }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Nongjifenlei.Name, x.Nongjifenlei.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 编辑
    /// </summary>
    public NongjihezuoEditResponse NongjihezuoEdit_GetValue(NongjihezuoEditQueryRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.Nongjihezuo.FirstOrDefault(x => x.Id == request.Id);
      return new NongjihezuoEditResponse
      {
        Id = entity.Id,
        Username = entity.Username,
        PhoneNumber = entity.PhoneNumber,
        Leixing = entity.Leixing,
        Xinghao = entity.Xinghao,
        Qianyuejia = entity.Qianyuejia,
      };
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult NongjihezuoEdit(NongjihezuoEditUpdateRequest request, UserInfo loginUser)
    {
      var one = _heZuosheDB.Nongjihezuo.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { Nongjihezuo = one };
      if (one == null)
      {
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.failed,
          ErrorCode = -1,
          Message = "‘编辑’操作执行失败,未找到目标数据"
        };
      }
      one.Username = request.Username;
      one.PhoneNumber = request.PhoneNumber;
      one.Leixing = request.Leixing;
      one.Xinghao = request.Xinghao;
      one.Qianyuejia = request.Qianyuejia;
      _heZuosheDB.Entry(one).State = EntityState.Modified;
      _heZuosheDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘编辑’操作执行成功,受影响行数‘1’"
      };
    }

    /// <summary>
    /// '农机类型' 字段的数据源
    /// </summary>
    public List<KVDataInfo> NongjihezuoEdit_leixing_DataSource(NongjihezuoEditUpdateRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Nongjifenlei in _heZuosheDB.Nongjifenlei
      select new { Nongjifenlei }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Nongjifenlei.Name, x.Nongjifenlei.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 详情
    /// </summary>
    public NongjihezuoDetailResponse NongjihezuoDetail_GetValue(NongjihezuoDetailQueryRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.Nongjihezuo.FirstOrDefault(x => x.Id == request.Id);
      return new NongjihezuoDetailResponse
      {
        Id = entity.Id,
        Username = entity.Username,
        PhoneNumber = entity.PhoneNumber,
        Leixing = entity.Leixing,
        Xinghao = entity.Xinghao,
        Qianyuejia = entity.Qianyuejia,
      };
    }

    /// <summary>
    /// '农机类型' 字段的数据源
    /// </summary>
    public List<KVDataInfo> NongjihezuoDetail_leixing_DataSource(NongjihezuoDetailResponse_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Nongjifenlei in _heZuosheDB.Nongjifenlei
      select new { Nongjifenlei }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Nongjifenlei.Name, x.Nongjifenlei.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult NongjihezuoDelete(NongjihezuoDeleteRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.Nongjihezuo.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { Nongjihezuo = entity };
      if (entity != null)
      {
        _heZuosheDB.Remove(entity);
        _heZuosheDB.SaveChanges();
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.succeed,
          ErrorCode = 0,
          Message = "‘删除’操作执行成功,受影响行数‘1’"
        };
      }
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.failed,
        ErrorCode = 1,
        Message = "‘删除’操作执行失败，无法找到目标数据,受影响行数‘0’"
      };
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<NongjihezuoSearchResponse> NongjihezuoSearch(NongjihezuoSearchQueryRequest request, UserInfo loginUser)
    {
      //填充变量
      if (string.IsNullOrEmpty(request.SortName)) request.SortName = "Nongjihezuo_Username";
      if (string.IsNullOrEmpty(request.SortType)) request.SortType = "desc";
      if (request.PageIndex <= 0) request.PageIndex = 1;
      if (request.PageSize <= 0) request.PageSize = 10;

      //验证输入
      {
        var sortnameList = new List<string> { "Nongjihezuo_Username", "Nongjihezuo_PhoneNumber", "Nongjihezuo_Leixing", "Nongjihezuo_Xinghao", "Nongjihezuo_Qianyuejia", "Nongjihezuo_Id" };
        if (sortnameList.All(one => !one.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase)))
        {
          throw new ViewException($"无效的排序列{request.SortName}");
        }
        else
        {
          request.SortName = sortnameList.FirstOrDefault(x => x.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase));
        }
      }
      {
        var pageSizeList = new List<int> { 10, 20, 30 };
        if (pageSizeList.All(one => one != request.PageSize))
        {
          throw new ViewException($"无效的页大小{request.PageSize}");
        }
      }
      // select
      var select =
      from Nongjihezuo in _heZuosheDB.Nongjihezuo
      select new { Nongjihezuo };

      // where

      // query
      var queryoutput = select.Select(x => new NongjihezuoSearchResponse
      {
        Nongjihezuo_Username = x.Nongjihezuo.Username,
        Nongjihezuo_PhoneNumber = x.Nongjihezuo.PhoneNumber,
        Nongjihezuo_Leixing = x.Nongjihezuo.Leixing,
        Nongjihezuo_Xinghao = x.Nongjihezuo.Xinghao,
        Nongjihezuo_Qianyuejia = x.Nongjihezuo.Qianyuejia,
        Nongjihezuo_Id = x.Nongjihezuo.Id,
      });

      // sort
      queryoutput = queryoutput.Sort(request.SortName, request.SortType);
      // pager
      var count = queryoutput.Count();
      queryoutput = queryoutput.Skip((request.PageIndex - 1) * request.PageSize)
      .Take(request.PageSize);

      return new PagerInfo<NongjihezuoSearchResponse>
      {
        Total = count,
        Data = queryoutput.ToList(),
        PageSize = request.PageSize,
        PageIndex = request.PageIndex,
      };
    }

    /// <summary>
    /// 查询
    /// </summary>
    public NongjihezuoSearchDefaultValueResponse NongjihezuoSearch_GetDefaultValue(NongjihezuoSearchPageparameter request, UserInfo loginUser)
    {
      return new NongjihezuoSearchDefaultValueResponse
      {
      };
    }

    /// <summary>
    /// '农机类型' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> NongjihezuoSearch_leixing_View_DataSource(NongjihezuoSearchPageparameter request, UserInfo loginUser)
    {
      var select =
      from Nongjifenlei in _heZuosheDB.Nongjifenlei
      select new { Nongjifenlei }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Nongjifenlei.Name, x.Nongjifenlei.Name)
      {
      }).ToList();
      return queryoutput;
    }

  }
}
