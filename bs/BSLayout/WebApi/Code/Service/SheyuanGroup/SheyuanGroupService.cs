using System;
using WebApi.Code.Common.ElModels;
using WebApi.Code.Interface.SheyuanGroup;
using WebApi.Code.Interface.SheyuanGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using WebApi.Code.Data;

namespace WebApi.Code.Service.SheyuanGroup
{
  public class SheyuanGroupService : ISheyuanGroupInterface
  {
    public readonly HeZuosheDB _heZuosheDB;

    public SheyuanGroupService(HeZuosheDB _heZuosheDB)
    {
      this._heZuosheDB = _heZuosheDB;
    }

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult SheyuanAdd(SheyuanAddRequest request, UserInfo loginUser)
    {
      var sheyuan = new Sheyuan
      {
        Id = Guid.NewGuid(),
        UserName = request.UserName,
        UserCode = request.UserCode,
        PhoneNumber = request.PhoneNumber,
        JoinTime_Year = request.JoinTime._Year,
        JoinTime_Month = request.JoinTime._Month,
        Panqu = request.Panqu,
        Miangji = request.Miangji,
        Status = request.Status,
        Yue = request.Yue,
      };
      _heZuosheDB.Add(sheyuan);
      _heZuosheDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘添加’操作执行成功"
      };
    }

    /// <summary>
    /// 添加
    /// </summary>
    public SheyuanAddDefaultValueResponse SheyuanAdd_GetDefaultValue(SheyuanAddPageparameter request, UserInfo loginUser)
    {
      return new SheyuanAddDefaultValueResponse
      {
        UserName = default(string?),
        UserCode = default(string?),
        PhoneNumber = default(string?),
        JoinTime = default(dateMonth_Element),
        Panqu = default(string?),
        Miangji = default(double?),
        Status = default(string?),
        Yue = default(decimal?),
      };
    }

    /// <summary>
    /// '片区' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SheyuanAdd_Panqu_DataSource(SheyuanAddRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from GengdiType in _heZuosheDB.GengdiType
      select new { GengdiType }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.GengdiType.PianquName, x.GengdiType.PianquName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '状态' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SheyuanAdd_Status_DataSource()
    {
      var data = new List<KVDataInfo>();
      data.Add(new KVDataInfo("已加入", "已加入"));
      data.Add(new KVDataInfo("已退出", "已退出"));
      return data;
    }

    /// <summary>
    /// 编辑
    /// </summary>
    public SheyuanEditResponse SheyuanEdit_GetValue(SheyuanEditQueryRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.Sheyuan.FirstOrDefault(x => x.Id == request.Id);
      return new SheyuanEditResponse
      {
        Id = entity.Id,
        UserName = entity.UserName,
        UserCode = entity.UserCode,
        PhoneNumber = entity.PhoneNumber,
        JoinTime = new dateMonth_Element
        {
          _Year = entity.JoinTime_Year,
          _Month = entity.JoinTime_Month,
        },
        Panqu = entity.Panqu,
        Miangji = entity.Miangji,
        Status = entity.Status,
        Yue = entity.Yue,
      };
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult SheyuanEdit(SheyuanEditUpdateRequest request, UserInfo loginUser)
    {
      var one = _heZuosheDB.Sheyuan.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { Sheyuan = one };
      if (one == null)
      {
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.failed,
          ErrorCode = -1,
          Message = "‘编辑’操作执行失败,未找到目标数据"
        };
      }
      one.UserName = request.UserName;
      one.UserCode = request.UserCode;
      one.PhoneNumber = request.PhoneNumber;
      one.JoinTime_Year = request.JoinTime._Year;
      one.JoinTime_Month = request.JoinTime._Month;
      one.Panqu = request.Panqu;
      one.Miangji = request.Miangji;
      one.Status = request.Status;
      one.Yue = request.Yue;
      _heZuosheDB.Entry(one).State = EntityState.Modified;
      _heZuosheDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘编辑’操作执行成功,受影响行数‘1’"
      };
    }

    /// <summary>
    /// '片区' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SheyuanEdit_Panqu_DataSource(SheyuanEditUpdateRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from GengdiType in _heZuosheDB.GengdiType
      select new { GengdiType }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.GengdiType.PianquName, x.GengdiType.PianquName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '状态' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SheyuanEdit_Status_DataSource()
    {
      var data = new List<KVDataInfo>();
      data.Add(new KVDataInfo("已加入", "已加入"));
      data.Add(new KVDataInfo("已退出", "已退出"));
      return data;
    }

    /// <summary>
    /// 详情
    /// </summary>
    public SheyuanDetailResponse SheyuanDetail_GetValue(SheyuanDetailQueryRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.Sheyuan.FirstOrDefault(x => x.Id == request.Id);
      return new SheyuanDetailResponse
      {
        Id = entity.Id,
        UserName = entity.UserName,
        UserCode = entity.UserCode,
        PhoneNumber = entity.PhoneNumber,
        JoinTime = new dateMonth_Element
        {
          _Year = entity.JoinTime_Year,
          _Month = entity.JoinTime_Month,
        },
        Panqu = entity.Panqu,
        Miangji = entity.Miangji,
        Status = entity.Status,
        Yue = entity.Yue,
      };
    }

    /// <summary>
    /// '片区' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SheyuanDetail_Panqu_DataSource(SheyuanDetailResponse_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from GengdiType in _heZuosheDB.GengdiType
      select new { GengdiType }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.GengdiType.PianquName, x.GengdiType.PianquName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '状态' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SheyuanDetail_Status_DataSource()
    {
      var data = new List<KVDataInfo>();
      data.Add(new KVDataInfo("已加入", "已加入"));
      data.Add(new KVDataInfo("已退出", "已退出"));
      return data;
    }

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult SheyuanDelete(SheyuanDeleteRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.Sheyuan.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { Sheyuan = entity };
      if (entity != null)
      {
        _heZuosheDB.Remove(entity);
        _heZuosheDB.SaveChanges();
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.succeed,
          ErrorCode = 0,
          Message = "‘删除’操作执行成功,受影响行数‘1’"
        };
      }
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.failed,
        ErrorCode = 1,
        Message = "‘删除’操作执行失败，无法找到目标数据,受影响行数‘0’"
      };
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<SheyuanSearchResponse> SheyuanSearch(SheyuanSearchQueryRequest request, UserInfo loginUser)
    {
      //填充变量
      if (string.IsNullOrEmpty(request.SortName)) request.SortName = "Sheyuan_UserName";
      if (string.IsNullOrEmpty(request.SortType)) request.SortType = "desc";
      if (request.PageIndex <= 0) request.PageIndex = 1;
      if (request.PageSize <= 0) request.PageSize = 10;

      //验证输入
      {
        var sortnameList = new List<string> { "Sheyuan_UserName", "Sheyuan_UserCode", "Sheyuan_PhoneNumber", "Sheyuan_JoinTime", "Sheyuan_Panqu", "Sheyuan_Miangji", "Sheyuan_Status", "Sheyuan_Yue", "Sheyuan_Id" };
        if (sortnameList.All(one => !one.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase)))
        {
          throw new ViewException($"无效的排序列{request.SortName}");
        }
        else
        {
          request.SortName = sortnameList.FirstOrDefault(x => x.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase));
        }
      }
      {
        var pageSizeList = new List<int> { 10, 20, 30 };
        if (pageSizeList.All(one => one != request.PageSize))
        {
          throw new ViewException($"无效的页大小{request.PageSize}");
        }
      }
      // select
      var select =
      from Sheyuan in _heZuosheDB.Sheyuan
      select new { Sheyuan };

      // where

      // query
      var queryoutput = select.Select(x => new SheyuanSearchResponse
      {
        Sheyuan_UserName = x.Sheyuan.UserName,
        Sheyuan_UserCode = x.Sheyuan.UserCode,
        Sheyuan_PhoneNumber = x.Sheyuan.PhoneNumber,
        Sheyuan_JoinTime = new dateMonth_Element
        {
          _Year = x.Sheyuan.JoinTime_Year,
          _Month = x.Sheyuan.JoinTime_Month,
        },
        Sheyuan_Panqu = x.Sheyuan.Panqu,
        Sheyuan_Miangji = x.Sheyuan.Miangji,
        Sheyuan_Status = x.Sheyuan.Status,
        Sheyuan_Yue = x.Sheyuan.Yue,
        Sheyuan_Id = x.Sheyuan.Id,
      });

      // sort
      queryoutput = queryoutput.Sort(request.SortName, request.SortType);
      // pager
      var count = queryoutput.Count();
      queryoutput = queryoutput.Skip((request.PageIndex - 1) * request.PageSize)
      .Take(request.PageSize);

      return new PagerInfo<SheyuanSearchResponse>
      {
        Total = count,
        Data = queryoutput.ToList(),
        PageSize = request.PageSize,
        PageIndex = request.PageIndex,
      };
    }

    /// <summary>
    /// 查询
    /// </summary>
    public SheyuanSearchDefaultValueResponse SheyuanSearch_GetDefaultValue(SheyuanSearchPageparameter request, UserInfo loginUser)
    {
      return new SheyuanSearchDefaultValueResponse
      {
      };
    }

    /// <summary>
    /// '片区' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> SheyuanSearch_Panqu_View_DataSource(SheyuanSearchPageparameter request, UserInfo loginUser)
    {
      var select =
      from GengdiType in _heZuosheDB.GengdiType
      select new { GengdiType }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.GengdiType.PianquName, x.GengdiType.PianquName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '状态' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> SheyuanSearch_Status_View_DataSource()
    {
      var data = new List<KVDataInfo>();
      data.Add(new KVDataInfo("已加入", "已加入"));
      data.Add(new KVDataInfo("已退出", "已退出"));
      return data;
    }

  }
}
