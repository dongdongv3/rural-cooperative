using System;
using WebApi.Code.Common.Authentication;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Security.Claims;
using WebApi.Code.Data;
using WebApi.Code.Common;

namespace WebApi.Code.Service.Authentication
{
  public class UserReadProvider_SysUser : IUserReadInterface
  {
    private readonly IEnumerable<IAuthenticationProviderInterface> authenticationProviderInterfaces;
    private readonly HeZuosheDB heZuosheDB;
    public UserReadProvider_SysUser(IEnumerable<IAuthenticationProviderInterface> authenticationProviders, HeZuosheDB heZuosheDB)
    {
      this.authenticationProviderInterfaces = authenticationProviders;
      this.heZuosheDB = heZuosheDB;
    }

    public Task<UserInfo> AutoProvisionUser(string provider, string userId, List<Claim> claims)
    {
      throw new System.NotImplementedException();
    }

    public Task<UserInfo> FindByExternalProvider(string provider, string userId)
    {
      throw new System.NotImplementedException();
    }

    public IAuthenticationProviderInterface GetAuthenticationProvider(LoginType type)
    {
      foreach (var auth in authenticationProviderInterfaces)
      {
        if (auth.Type == type)
        {
          return auth;
        }
      }
      throw new AuthException("无效的登录提供程序");
    }

    public async Task<UserInfo> GetUserByID(string userID)
    {
      var user = await heZuosheDB.SysUser.FirstOrDefaultAsync(x => x.Id == userID);
      if (user == null)
      {
        throw new AuthException("无法查询到有效用户");
      }
      Dictionary<string, string> Role = new Dictionary<string, string>();
      Dictionary<string, string> Auth = new Dictionary<string, string>();

      //role auth模式
      var roleauth = await heZuosheDB.SysUser_UserRoleMap.Include(x => x.RefSysUser_Role).ThenInclude(x => x.SubSysUser_RoleAuthMap).ThenInclude(x => x.RefSysUser_Auth).Where(x => x.RefSysUserId == userID).ToListAsync();
      roleauth.ForEach(x => Role[x.RefSysUser_Role.RoleName] = x.Id.ToString());
      roleauth.SelectMany(x => x.RefSysUser_Role.SubSysUser_RoleAuthMap).ToList().ForEach(x => Auth[x.RefSysUser_Auth.AuthName] = x.RefSysUser_AuthId.ToString());

      return new UserInfo
      {
        UserName = user.UserName,
        Id = user.Id,
        Roles = Role,
        Auths = Auth,
      };
    }
  }
}
