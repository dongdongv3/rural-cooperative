using System;
using WebApi.Code.Common.ElModels;
using WebApi.Code.Interface.KucunTypeGroup;
using WebApi.Code.Interface.KucunTypeGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Linq;
using System.Collections.Generic;
using WebApi.Code.Data;

namespace WebApi.Code.Service.KucunTypeGroup
{
  public class KucunTypeGroupService : IKucunTypeGroupInterface
  {
    public readonly HeZuosheDB _heZuosheDB;

    public KucunTypeGroupService(HeZuosheDB _heZuosheDB)
    {
      this._heZuosheDB = _heZuosheDB;
    }

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult KucunTypeAdd(KucunTypeAddRequest request, UserInfo loginUser)
    {
      var kucunType = new KucunType
      {
        Id = Guid.NewGuid(),
        Name = request.Name,
      };
      _heZuosheDB.Add(kucunType);
      _heZuosheDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘添加’操作执行成功"
      };
    }

    /// <summary>
    /// 添加
    /// </summary>
    public KucunTypeAddDefaultValueResponse KucunTypeAdd_GetDefaultValue(KucunTypeAddPageparameter request, UserInfo loginUser)
    {
      return new KucunTypeAddDefaultValueResponse
      {
        Name = default(string?),
      };
    }

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult KucunTypeDelete(KucunTypeDeleteRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.KucunType.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { KucunType = entity };
      if (entity != null)
      {
        _heZuosheDB.Remove(entity);
        _heZuosheDB.SaveChanges();
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.succeed,
          ErrorCode = 0,
          Message = "‘删除’操作执行成功,受影响行数‘1’"
        };
      }
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.failed,
        ErrorCode = 1,
        Message = "‘删除’操作执行失败，无法找到目标数据,受影响行数‘0’"
      };
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<KucunTypeSearchResponse> KucunTypeSearch(KucunTypeSearchQueryRequest request, UserInfo loginUser)
    {
      //填充变量
      if (string.IsNullOrEmpty(request.SortName)) request.SortName = "KucunType_Name";
      if (string.IsNullOrEmpty(request.SortType)) request.SortType = "desc";
      if (request.PageIndex <= 0) request.PageIndex = 1;
      if (request.PageSize <= 0) request.PageSize = 10;

      //验证输入
      {
        var sortnameList = new List<string> { "KucunType_Name", "KucunType_Id" };
        if (sortnameList.All(one => !one.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase)))
        {
          throw new ViewException($"无效的排序列{request.SortName}");
        }
        else
        {
          request.SortName = sortnameList.FirstOrDefault(x => x.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase));
        }
      }
      {
        var pageSizeList = new List<int> { 10, 20, 30 };
        if (pageSizeList.All(one => one != request.PageSize))
        {
          throw new ViewException($"无效的页大小{request.PageSize}");
        }
      }
      // select
      var select =
      from KucunType in _heZuosheDB.KucunType
      select new { KucunType };

      // where

      // query
      var queryoutput = select.Select(x => new KucunTypeSearchResponse
      {
        KucunType_Name = x.KucunType.Name,
        KucunType_Id = x.KucunType.Id,
      });

      // sort
      queryoutput = queryoutput.Sort(request.SortName, request.SortType);
      // pager
      var count = queryoutput.Count();
      queryoutput = queryoutput.Skip((request.PageIndex - 1) * request.PageSize)
      .Take(request.PageSize);

      return new PagerInfo<KucunTypeSearchResponse>
      {
        Total = count,
        Data = queryoutput.ToList(),
        PageSize = request.PageSize,
        PageIndex = request.PageIndex,
      };
    }

    /// <summary>
    /// 查询
    /// </summary>
    public KucunTypeSearchDefaultValueResponse KucunTypeSearch_GetDefaultValue(KucunTypeSearchPageparameter request, UserInfo loginUser)
    {
      return new KucunTypeSearchDefaultValueResponse
      {
      };
    }

  }
}
