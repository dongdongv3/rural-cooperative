using System;
using WebApi.Code.Common.ElModels;
using WebApi.Code.Interface.KucunMingxiGroup;
using WebApi.Code.Interface.KucunMingxiGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using WebApi.Code.Data;

namespace WebApi.Code.Service.KucunMingxiGroup
{
  public class KucunMingxiGroupService : IKucunMingxiGroupInterface
  {
    public readonly HeZuosheDB _heZuosheDB;

    public KucunMingxiGroupService(HeZuosheDB _heZuosheDB)
    {
      this._heZuosheDB = _heZuosheDB;
    }

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult KucunMingxiAdd(KucunMingxiAddRequest request, UserInfo loginUser)
    {
      var kucunMingxi = new KucunMingxi
      {
        Id = Guid.NewGuid(),
        WupinName = request.WupinName,
        Count = request.Count,
        Danwei = request.Danwei,
        Fenlei = request.Fenlei,
      };
      _heZuosheDB.Add(kucunMingxi);
      _heZuosheDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘添加’操作执行成功"
      };
    }

    /// <summary>
    /// 添加
    /// </summary>
    public KucunMingxiAddDefaultValueResponse KucunMingxiAdd_GetDefaultValue(KucunMingxiAddPageparameter request, UserInfo loginUser)
    {
      return new KucunMingxiAddDefaultValueResponse
      {
        WupinName = default(string?),
        Count = default(int?),
        Danwei = default(string?),
        Fenlei = default(string?),
      };
    }

    /// <summary>
    /// '分类' 字段的数据源
    /// </summary>
    public List<KVDataInfo> KucunMingxiAdd_fenlei_DataSource(KucunMingxiAddRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from KucunType in _heZuosheDB.KucunType
      select new { KucunType }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.KucunType.Name, x.KucunType.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 编辑
    /// </summary>
    public KucunMingxiEditResponse KucunMingxiEdit_GetValue(KucunMingxiEditQueryRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.KucunMingxi.FirstOrDefault(x => x.Id == request.Id);
      return new KucunMingxiEditResponse
      {
        Id = entity.Id,
        WupinName = entity.WupinName,
        Count = entity.Count,
        Danwei = entity.Danwei,
        Fenlei = entity.Fenlei,
      };
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult KucunMingxiEdit(KucunMingxiEditUpdateRequest request, UserInfo loginUser)
    {
      var one = _heZuosheDB.KucunMingxi.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { KucunMingxi = one };
      if (one == null)
      {
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.failed,
          ErrorCode = -1,
          Message = "‘编辑’操作执行失败,未找到目标数据"
        };
      }
      one.WupinName = request.WupinName;
      one.Count = request.Count;
      one.Danwei = request.Danwei;
      one.Fenlei = request.Fenlei;
      _heZuosheDB.Entry(one).State = EntityState.Modified;
      _heZuosheDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘编辑’操作执行成功,受影响行数‘1’"
      };
    }

    /// <summary>
    /// '分类' 字段的数据源
    /// </summary>
    public List<KVDataInfo> KucunMingxiEdit_fenlei_DataSource(KucunMingxiEditUpdateRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from KucunType in _heZuosheDB.KucunType
      select new { KucunType }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.KucunType.Name, x.KucunType.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 详情
    /// </summary>
    public KucunMingxiDetailResponse KucunMingxiDetail_GetValue(KucunMingxiDetailQueryRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.KucunMingxi.FirstOrDefault(x => x.Id == request.Id);
      return new KucunMingxiDetailResponse
      {
        Id = entity.Id,
        WupinName = entity.WupinName,
        Count = entity.Count,
        Danwei = entity.Danwei,
        Fenlei = entity.Fenlei,
      };
    }

    /// <summary>
    /// '分类' 字段的数据源
    /// </summary>
    public List<KVDataInfo> KucunMingxiDetail_fenlei_DataSource(KucunMingxiDetailResponse_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from KucunType in _heZuosheDB.KucunType
      select new { KucunType }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.KucunType.Name, x.KucunType.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult KucunMingxiDelete(KucunMingxiDeleteRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.KucunMingxi.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { KucunMingxi = entity };
      if (entity != null)
      {
        _heZuosheDB.Remove(entity);
        _heZuosheDB.SaveChanges();
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.succeed,
          ErrorCode = 0,
          Message = "‘删除’操作执行成功,受影响行数‘1’"
        };
      }
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.failed,
        ErrorCode = 1,
        Message = "‘删除’操作执行失败，无法找到目标数据,受影响行数‘0’"
      };
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<KucunMingxiSearchResponse> KucunMingxiSearch(KucunMingxiSearchQueryRequest request, UserInfo loginUser)
    {
      //填充变量
      if (string.IsNullOrEmpty(request.SortName)) request.SortName = "KucunMingxi_WupinName";
      if (string.IsNullOrEmpty(request.SortType)) request.SortType = "desc";
      if (request.PageIndex <= 0) request.PageIndex = 1;
      if (request.PageSize <= 0) request.PageSize = 10;

      //验证输入
      {
        var sortnameList = new List<string> { "KucunMingxi_WupinName", "KucunMingxi_Count", "KucunMingxi_Danwei", "KucunMingxi_Fenlei", "KucunMingxi_Id" };
        if (sortnameList.All(one => !one.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase)))
        {
          throw new ViewException($"无效的排序列{request.SortName}");
        }
        else
        {
          request.SortName = sortnameList.FirstOrDefault(x => x.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase));
        }
      }
      {
        var pageSizeList = new List<int> { 10, 20, 30 };
        if (pageSizeList.All(one => one != request.PageSize))
        {
          throw new ViewException($"无效的页大小{request.PageSize}");
        }
      }
      // select
      var select =
      from KucunMingxi in _heZuosheDB.KucunMingxi
      select new { KucunMingxi };

      // where

      // query
      var queryoutput = select.Select(x => new KucunMingxiSearchResponse
      {
        KucunMingxi_WupinName = x.KucunMingxi.WupinName,
        KucunMingxi_Count = x.KucunMingxi.Count,
        KucunMingxi_Danwei = x.KucunMingxi.Danwei,
        KucunMingxi_Fenlei = x.KucunMingxi.Fenlei,
        KucunMingxi_Id = x.KucunMingxi.Id,
      });

      // sort
      queryoutput = queryoutput.Sort(request.SortName, request.SortType);
      // pager
      var count = queryoutput.Count();
      queryoutput = queryoutput.Skip((request.PageIndex - 1) * request.PageSize)
      .Take(request.PageSize);

      return new PagerInfo<KucunMingxiSearchResponse>
      {
        Total = count,
        Data = queryoutput.ToList(),
        PageSize = request.PageSize,
        PageIndex = request.PageIndex,
      };
    }

    /// <summary>
    /// 查询
    /// </summary>
    public KucunMingxiSearchDefaultValueResponse KucunMingxiSearch_GetDefaultValue(KucunMingxiSearchPageparameter request, UserInfo loginUser)
    {
      return new KucunMingxiSearchDefaultValueResponse
      {
      };
    }

    /// <summary>
    /// '分类' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> KucunMingxiSearch_fenlei_View_DataSource(KucunMingxiSearchPageparameter request, UserInfo loginUser)
    {
      var select =
      from KucunType in _heZuosheDB.KucunType
      select new { KucunType }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.KucunType.Name, x.KucunType.Name)
      {
      }).ToList();
      return queryoutput;
    }

  }
}
