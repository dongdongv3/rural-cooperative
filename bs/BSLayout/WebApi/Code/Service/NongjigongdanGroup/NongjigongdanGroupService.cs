using System;
using WebApi.Code.Common.ElModels;
using WebApi.Code.Interface.NongjigongdanGroup;
using WebApi.Code.Interface.NongjigongdanGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using WebApi.Code.Data;

namespace WebApi.Code.Service.NongjigongdanGroup
{
  public class NongjigongdanGroupService : INongjigongdanGroupInterface
  {
    public readonly HeZuosheDB _heZuosheDB;

    public NongjigongdanGroupService(HeZuosheDB _heZuosheDB)
    {
      this._heZuosheDB = _heZuosheDB;
    }

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult NongjigongdanAdd(NongjigongdanAddRequest request, UserInfo loginUser)
    {
      var nongjigongdan = new Nongjigongdan
      {
        Id = Guid.NewGuid(),
        Createtimre = DateTime.Now,
        Miangji = request.Miangji,
        Leixing = request.Leixing,
        Chuli = request.Chuli,
        Zongjia = request.Zongjia,
        Zhuangtai = request.Zhuangtai,
        Jeisuanzhuangtai = request.Jeisuanzhuangtai,
      };
      _heZuosheDB.Add(nongjigongdan);
      _heZuosheDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘添加’操作执行成功"
      };
    }

    /// <summary>
    /// 添加
    /// </summary>
    public NongjigongdanAddDefaultValueResponse NongjigongdanAdd_GetDefaultValue(NongjigongdanAddPageparameter request, UserInfo loginUser)
    {
      return new NongjigongdanAddDefaultValueResponse
      {
        Miangji = default(int?),
        Leixing = default(string?),
        Chuli = default(string?),
        Zongjia = default(decimal?),
        Zhuangtai = default(string?),
        Jeisuanzhuangtai = default(string?),
      };
    }

    /// <summary>
    /// '农机类型' 字段的数据源
    /// </summary>
    public List<KVDataInfo> NongjigongdanAdd_leixing_DataSource(NongjigongdanAddRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Nongjifenlei in _heZuosheDB.Nongjifenlei
      select new { Nongjifenlei }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Nongjifenlei.Name, x.Nongjifenlei.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '接单农机' 字段的数据源
    /// </summary>
    public List<KVDataInfo> NongjigongdanAdd_chuli_DataSource(NongjigongdanAddRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Nongjihezuo in _heZuosheDB.Nongjihezuo
      select new { Nongjihezuo }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Nongjihezuo.Username, x.Nongjihezuo.Username)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '状态' 字段的数据源
    /// </summary>
    public List<KVDataInfo> NongjigongdanAdd_zhuangtai_DataSource()
    {
      var data = new List<KVDataInfo>();
      data.Add(new KVDataInfo("待处理", "待处理"));
      data.Add(new KVDataInfo("已处理", "已处理"));
      return data;
    }

    /// <summary>
    /// '结算状态' 字段的数据源
    /// </summary>
    public List<KVDataInfo> NongjigongdanAdd_jeisuanzhuangtai_DataSource()
    {
      var data = new List<KVDataInfo>();
      data.Add(new KVDataInfo("未结算", "未结算"));
      data.Add(new KVDataInfo("已结算", "已结算"));
      return data;
    }

    /// <summary>
    /// 编辑
    /// </summary>
    public NongjigongdanEditResponse NongjigongdanEdit_GetValue(NongjigongdanEditQueryRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.Nongjigongdan.FirstOrDefault(x => x.Id == request.Id);
      return new NongjigongdanEditResponse
      {
        Id = entity.Id,
        BeginTime = entity.BeginTime,
        Miangji = entity.Miangji,
        Leixing = entity.Leixing,
        Chuli = entity.Chuli,
        Zongjia = entity.Zongjia,
        Zhuangtai = entity.Zhuangtai,
        Jeisuanzhuangtai = entity.Jeisuanzhuangtai,
      };
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult NongjigongdanEdit(NongjigongdanEditUpdateRequest request, UserInfo loginUser)
    {
      var one = _heZuosheDB.Nongjigongdan.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { Nongjigongdan = one };
      if (one == null)
      {
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.failed,
          ErrorCode = -1,
          Message = "‘编辑’操作执行失败,未找到目标数据"
        };
      }
      one.BeginTime = request.BeginTime;
      one.Miangji = request.Miangji;
      one.Leixing = request.Leixing;
      one.Chuli = request.Chuli;
      one.Zongjia = request.Zongjia;
      one.Zhuangtai = request.Zhuangtai;
      one.Jeisuanzhuangtai = request.Jeisuanzhuangtai;
      _heZuosheDB.Entry(one).State = EntityState.Modified;
      _heZuosheDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘编辑’操作执行成功,受影响行数‘1’"
      };
    }

    /// <summary>
    /// '农机类型' 字段的数据源
    /// </summary>
    public List<KVDataInfo> NongjigongdanEdit_leixing_DataSource(NongjigongdanEditUpdateRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Nongjifenlei in _heZuosheDB.Nongjifenlei
      select new { Nongjifenlei }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Nongjifenlei.Name, x.Nongjifenlei.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '接单农机' 字段的数据源
    /// </summary>
    public List<KVDataInfo> NongjigongdanEdit_chuli_DataSource(NongjigongdanEditUpdateRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Nongjihezuo in _heZuosheDB.Nongjihezuo
      select new { Nongjihezuo }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Nongjihezuo.Username, x.Nongjihezuo.Username)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '状态' 字段的数据源
    /// </summary>
    public List<KVDataInfo> NongjigongdanEdit_zhuangtai_DataSource()
    {
      var data = new List<KVDataInfo>();
      data.Add(new KVDataInfo("待处理", "待处理"));
      data.Add(new KVDataInfo("已处理", "已处理"));
      return data;
    }

    /// <summary>
    /// '结算状态' 字段的数据源
    /// </summary>
    public List<KVDataInfo> NongjigongdanEdit_jeisuanzhuangtai_DataSource()
    {
      var data = new List<KVDataInfo>();
      data.Add(new KVDataInfo("未结算", "未结算"));
      data.Add(new KVDataInfo("已结算", "已结算"));
      return data;
    }

    /// <summary>
    /// 详情
    /// </summary>
    public NongjigongdanDetailResponse NongjigongdanDetail_GetValue(NongjigongdanDetailQueryRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.Nongjigongdan.FirstOrDefault(x => x.Id == request.Id);
      return new NongjigongdanDetailResponse
      {
        Id = entity.Id,
        Createtimre = entity.Createtimre,
        BeginTime = entity.BeginTime,
        Miangji = entity.Miangji,
        Leixing = entity.Leixing,
        Chuli = entity.Chuli,
        Zongjia = entity.Zongjia,
        Zhuangtai = entity.Zhuangtai,
        Jeisuanzhuangtai = entity.Jeisuanzhuangtai,
      };
    }

    /// <summary>
    /// '农机类型' 字段的数据源
    /// </summary>
    public List<KVDataInfo> NongjigongdanDetail_leixing_DataSource(NongjigongdanDetailResponse_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Nongjifenlei in _heZuosheDB.Nongjifenlei
      select new { Nongjifenlei }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Nongjifenlei.Name, x.Nongjifenlei.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '接单农机' 字段的数据源
    /// </summary>
    public List<KVDataInfo> NongjigongdanDetail_chuli_DataSource(NongjigongdanDetailResponse_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from Nongjihezuo in _heZuosheDB.Nongjihezuo
      select new { Nongjihezuo }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Nongjihezuo.Username, x.Nongjihezuo.Username)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '状态' 字段的数据源
    /// </summary>
    public List<KVDataInfo> NongjigongdanDetail_zhuangtai_DataSource()
    {
      var data = new List<KVDataInfo>();
      data.Add(new KVDataInfo("待处理", "待处理"));
      data.Add(new KVDataInfo("已处理", "已处理"));
      return data;
    }

    /// <summary>
    /// '结算状态' 字段的数据源
    /// </summary>
    public List<KVDataInfo> NongjigongdanDetail_jeisuanzhuangtai_DataSource()
    {
      var data = new List<KVDataInfo>();
      data.Add(new KVDataInfo("未结算", "未结算"));
      data.Add(new KVDataInfo("已结算", "已结算"));
      return data;
    }

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult NongjigongdanDelete(NongjigongdanDeleteRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.Nongjigongdan.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { Nongjigongdan = entity };
      if (entity != null)
      {
        _heZuosheDB.Remove(entity);
        _heZuosheDB.SaveChanges();
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.succeed,
          ErrorCode = 0,
          Message = "‘删除’操作执行成功,受影响行数‘1’"
        };
      }
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.failed,
        ErrorCode = 1,
        Message = "‘删除’操作执行失败，无法找到目标数据,受影响行数‘0’"
      };
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<NongjigongdanSearchResponse> NongjigongdanSearch(NongjigongdanSearchQueryRequest request, UserInfo loginUser)
    {
      //填充变量
      if (string.IsNullOrEmpty(request.SortName)) request.SortName = "Nongjigongdan_Createtimre";
      if (string.IsNullOrEmpty(request.SortType)) request.SortType = "desc";
      if (request.PageIndex <= 0) request.PageIndex = 1;
      if (request.PageSize <= 0) request.PageSize = 10;

      //验证输入
      {
        var sortnameList = new List<string> { "Nongjigongdan_Createtimre", "Nongjigongdan_BeginTime", "Nongjigongdan_Miangji", "Nongjigongdan_Leixing", "Nongjigongdan_Chuli", "Nongjigongdan_Zongjia", "Nongjigongdan_Zhuangtai", "Nongjigongdan_Jeisuanzhuangtai", "Nongjigongdan_Id" };
        if (sortnameList.All(one => !one.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase)))
        {
          throw new ViewException($"无效的排序列{request.SortName}");
        }
        else
        {
          request.SortName = sortnameList.FirstOrDefault(x => x.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase));
        }
      }
      {
        var pageSizeList = new List<int> { 10, 20, 30 };
        if (pageSizeList.All(one => one != request.PageSize))
        {
          throw new ViewException($"无效的页大小{request.PageSize}");
        }
      }
      // select
      var select =
      from Nongjigongdan in _heZuosheDB.Nongjigongdan
      select new { Nongjigongdan };

      // where

      // query
      var queryoutput = select.Select(x => new NongjigongdanSearchResponse
      {
        Nongjigongdan_Createtimre = x.Nongjigongdan.Createtimre,
        Nongjigongdan_BeginTime = x.Nongjigongdan.BeginTime,
        Nongjigongdan_Miangji = x.Nongjigongdan.Miangji,
        Nongjigongdan_Leixing = x.Nongjigongdan.Leixing,
        Nongjigongdan_Chuli = x.Nongjigongdan.Chuli,
        Nongjigongdan_Zongjia = x.Nongjigongdan.Zongjia,
        Nongjigongdan_Zhuangtai = x.Nongjigongdan.Zhuangtai,
        Nongjigongdan_Jeisuanzhuangtai = x.Nongjigongdan.Jeisuanzhuangtai,
        Nongjigongdan_Id = x.Nongjigongdan.Id,
      });

      // sort
      queryoutput = queryoutput.Sort(request.SortName, request.SortType);
      // pager
      var count = queryoutput.Count();
      queryoutput = queryoutput.Skip((request.PageIndex - 1) * request.PageSize)
      .Take(request.PageSize);

      return new PagerInfo<NongjigongdanSearchResponse>
      {
        Total = count,
        Data = queryoutput.ToList(),
        PageSize = request.PageSize,
        PageIndex = request.PageIndex,
      };
    }

    /// <summary>
    /// 查询
    /// </summary>
    public NongjigongdanSearchDefaultValueResponse NongjigongdanSearch_GetDefaultValue(NongjigongdanSearchPageparameter request, UserInfo loginUser)
    {
      return new NongjigongdanSearchDefaultValueResponse
      {
      };
    }

    /// <summary>
    /// '农机类型' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> NongjigongdanSearch_leixing_View_DataSource(NongjigongdanSearchPageparameter request, UserInfo loginUser)
    {
      var select =
      from Nongjifenlei in _heZuosheDB.Nongjifenlei
      select new { Nongjifenlei }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Nongjifenlei.Name, x.Nongjifenlei.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '接单农机' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> NongjigongdanSearch_chuli_View_DataSource(NongjigongdanSearchPageparameter request, UserInfo loginUser)
    {
      var select =
      from Nongjihezuo in _heZuosheDB.Nongjihezuo
      select new { Nongjihezuo }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.Nongjihezuo.Username, x.Nongjihezuo.Username)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '状态' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> NongjigongdanSearch_zhuangtai_View_DataSource()
    {
      var data = new List<KVDataInfo>();
      data.Add(new KVDataInfo("待处理", "待处理"));
      data.Add(new KVDataInfo("已处理", "已处理"));
      return data;
    }

    /// <summary>
    /// '结算状态' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> NongjigongdanSearch_jeisuanzhuangtai_View_DataSource()
    {
      var data = new List<KVDataInfo>();
      data.Add(new KVDataInfo("未结算", "未结算"));
      data.Add(new KVDataInfo("已结算", "已结算"));
      return data;
    }

  }
}
