using System;
using WebApi.Code.Common.ElModels;
using WebApi.Code.Interface.ChurukujiluGroup;
using WebApi.Code.Interface.ChurukujiluGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using WebApi.Code.Data;

namespace WebApi.Code.Service.ChurukujiluGroup
{
  public class ChurukujiluGroupService : IChurukujiluGroupInterface
  {
    public readonly HeZuosheDB _heZuosheDB;

    public ChurukujiluGroupService(HeZuosheDB _heZuosheDB)
    {
      this._heZuosheDB = _heZuosheDB;
    }

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult ChurukujiluAdd(ChurukujiluAddRequest request, UserInfo loginUser)
    {
      var churukujilu = new Churukujilu
      {
        Id = Guid.NewGuid(),
        Createtime = DateTime.Now,
        Shengqingren = request.Shengqingren,
        ApprovedUser = request.ApprovedUser,
        Leixing = request.Leixing,
        Fenlei = request.Fenlei,
        Wupin = request.Wupin,
        Shuliang = request.Shuliang,
      };
      _heZuosheDB.Add(churukujilu);
      _heZuosheDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘添加’操作执行成功"
      };
    }

    /// <summary>
    /// 添加
    /// </summary>
    public ChurukujiluAddDefaultValueResponse ChurukujiluAdd_GetDefaultValue(ChurukujiluAddPageparameter request, UserInfo loginUser)
    {
      return new ChurukujiluAddDefaultValueResponse
      {
        Shengqingren = default(string?),
        ApprovedUser = default(string?),
        Leixing = default(string?),
        Fenlei = default(string?),
        Wupin = default(string?),
        Shuliang = default(int?),
      };
    }

    /// <summary>
    /// '审批人' 字段的数据源
    /// </summary>
    public List<KVDataInfo> ChurukujiluAdd_ApprovedUser_DataSource(ChurukujiluAddRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from SysUser in _heZuosheDB.SysUser
      select new { SysUser }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser.UserName, x.SysUser.UserName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '类型' 字段的数据源
    /// </summary>
    public List<KVDataInfo> ChurukujiluAdd_leixing_DataSource()
    {
      var data = new List<KVDataInfo>();
      data.Add(new KVDataInfo("出库", "出库"));
      data.Add(new KVDataInfo("入库", "入库"));
      return data;
    }

    /// <summary>
    /// '库存分类' 字段的数据源
    /// </summary>
    public List<KVDataInfo> ChurukujiluAdd_fenlei_DataSource(ChurukujiluAddRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from KucunType in _heZuosheDB.KucunType
      select new { KucunType }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.KucunType.Name, x.KucunType.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '物品' 字段的数据源
    /// </summary>
    public List<KVDataInfo> ChurukujiluAdd_wupin_DataSource(ChurukujiluAddRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from KucunMingxi in _heZuosheDB.KucunMingxi
      select new { KucunMingxi }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.KucunMingxi.WupinName, x.KucunMingxi.WupinName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 编辑
    /// </summary>
    public ChurukujiluEditResponse ChurukujiluEdit_GetValue(ChurukujiluEditQueryRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.Churukujilu.FirstOrDefault(x => x.Id == request.Id);
      return new ChurukujiluEditResponse
      {
        Id = entity.Id,
        Shengqingren = entity.Shengqingren,
        ApprovedUser = entity.ApprovedUser,
        Leixing = entity.Leixing,
        Fenlei = entity.Fenlei,
        Wupin = entity.Wupin,
        Shuliang = entity.Shuliang,
      };
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult ChurukujiluEdit(ChurukujiluEditUpdateRequest request, UserInfo loginUser)
    {
      var one = _heZuosheDB.Churukujilu.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { Churukujilu = one };
      if (one == null)
      {
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.failed,
          ErrorCode = -1,
          Message = "‘编辑’操作执行失败,未找到目标数据"
        };
      }
      one.Shengqingren = request.Shengqingren;
      one.ApprovedUser = request.ApprovedUser;
      one.Leixing = request.Leixing;
      one.Fenlei = request.Fenlei;
      one.Wupin = request.Wupin;
      one.Shuliang = request.Shuliang;
      _heZuosheDB.Entry(one).State = EntityState.Modified;
      _heZuosheDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘编辑’操作执行成功,受影响行数‘1’"
      };
    }

    /// <summary>
    /// '审批人' 字段的数据源
    /// </summary>
    public List<KVDataInfo> ChurukujiluEdit_ApprovedUser_DataSource(ChurukujiluEditUpdateRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from SysUser in _heZuosheDB.SysUser
      select new { SysUser }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser.UserName, x.SysUser.UserName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '类型' 字段的数据源
    /// </summary>
    public List<KVDataInfo> ChurukujiluEdit_leixing_DataSource()
    {
      var data = new List<KVDataInfo>();
      data.Add(new KVDataInfo("出库", "出库"));
      data.Add(new KVDataInfo("入库", "入库"));
      return data;
    }

    /// <summary>
    /// '库存分类' 字段的数据源
    /// </summary>
    public List<KVDataInfo> ChurukujiluEdit_fenlei_DataSource(ChurukujiluEditUpdateRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from KucunType in _heZuosheDB.KucunType
      select new { KucunType }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.KucunType.Name, x.KucunType.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '物品' 字段的数据源
    /// </summary>
    public List<KVDataInfo> ChurukujiluEdit_wupin_DataSource(ChurukujiluEditUpdateRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from KucunMingxi in _heZuosheDB.KucunMingxi
      select new { KucunMingxi }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.KucunMingxi.WupinName, x.KucunMingxi.WupinName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 详情
    /// </summary>
    public ChurukujiluDetailResponse ChurukujiluDetail_GetValue(ChurukujiluDetailQueryRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.Churukujilu.FirstOrDefault(x => x.Id == request.Id);
      return new ChurukujiluDetailResponse
      {
        Id = entity.Id,
        Createtime = entity.Createtime,
        Shengqingren = entity.Shengqingren,
        ApprovedUser = entity.ApprovedUser,
        Leixing = entity.Leixing,
        Fenlei = entity.Fenlei,
        Wupin = entity.Wupin,
        Shuliang = entity.Shuliang,
      };
    }

    /// <summary>
    /// '审批人' 字段的数据源
    /// </summary>
    public List<KVDataInfo> ChurukujiluDetail_ApprovedUser_DataSource(ChurukujiluDetailResponse_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from SysUser in _heZuosheDB.SysUser
      select new { SysUser }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser.UserName, x.SysUser.UserName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '类型' 字段的数据源
    /// </summary>
    public List<KVDataInfo> ChurukujiluDetail_leixing_DataSource()
    {
      var data = new List<KVDataInfo>();
      data.Add(new KVDataInfo("出库", "出库"));
      data.Add(new KVDataInfo("入库", "入库"));
      return data;
    }

    /// <summary>
    /// '库存分类' 字段的数据源
    /// </summary>
    public List<KVDataInfo> ChurukujiluDetail_fenlei_DataSource(ChurukujiluDetailResponse_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from KucunType in _heZuosheDB.KucunType
      select new { KucunType }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.KucunType.Name, x.KucunType.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '物品' 字段的数据源
    /// </summary>
    public List<KVDataInfo> ChurukujiluDetail_wupin_DataSource(ChurukujiluDetailResponse_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from KucunMingxi in _heZuosheDB.KucunMingxi
      select new { KucunMingxi }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.KucunMingxi.WupinName, x.KucunMingxi.WupinName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult ChurukujiluDelete(ChurukujiluDeleteRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.Churukujilu.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { Churukujilu = entity };
      if (entity != null)
      {
        _heZuosheDB.Remove(entity);
        _heZuosheDB.SaveChanges();
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.succeed,
          ErrorCode = 0,
          Message = "‘删除’操作执行成功,受影响行数‘1’"
        };
      }
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.failed,
        ErrorCode = 1,
        Message = "‘删除’操作执行失败，无法找到目标数据,受影响行数‘0’"
      };
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<ChurukujiluSearchResponse> ChurukujiluSearch(ChurukujiluSearchQueryRequest request, UserInfo loginUser)
    {
      //填充变量
      if (string.IsNullOrEmpty(request.SortName)) request.SortName = "Churukujilu_Createtime";
      if (string.IsNullOrEmpty(request.SortType)) request.SortType = "desc";
      if (request.PageIndex <= 0) request.PageIndex = 1;
      if (request.PageSize <= 0) request.PageSize = 10;

      //验证输入
      {
        var sortnameList = new List<string> { "Churukujilu_Createtime", "Churukujilu_Shengqingren", "Churukujilu_ApprovedUser", "Churukujilu_Leixing", "Churukujilu_Fenlei", "Churukujilu_Wupin", "Churukujilu_Shuliang", "Churukujilu_Id" };
        if (sortnameList.All(one => !one.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase)))
        {
          throw new ViewException($"无效的排序列{request.SortName}");
        }
        else
        {
          request.SortName = sortnameList.FirstOrDefault(x => x.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase));
        }
      }
      {
        var pageSizeList = new List<int> { 10, 20, 30 };
        if (pageSizeList.All(one => one != request.PageSize))
        {
          throw new ViewException($"无效的页大小{request.PageSize}");
        }
      }
      // select
      var select =
      from Churukujilu in _heZuosheDB.Churukujilu
      select new { Churukujilu };

      // where

      // query
      var queryoutput = select.Select(x => new ChurukujiluSearchResponse
      {
        Churukujilu_Createtime = x.Churukujilu.Createtime,
        Churukujilu_Shengqingren = x.Churukujilu.Shengqingren,
        Churukujilu_ApprovedUser = x.Churukujilu.ApprovedUser,
        Churukujilu_Leixing = x.Churukujilu.Leixing,
        Churukujilu_Fenlei = x.Churukujilu.Fenlei,
        Churukujilu_Wupin = x.Churukujilu.Wupin,
        Churukujilu_Shuliang = x.Churukujilu.Shuliang,
        Churukujilu_Id = x.Churukujilu.Id,
      });

      // sort
      queryoutput = queryoutput.Sort(request.SortName, request.SortType);
      // pager
      var count = queryoutput.Count();
      queryoutput = queryoutput.Skip((request.PageIndex - 1) * request.PageSize)
      .Take(request.PageSize);

      return new PagerInfo<ChurukujiluSearchResponse>
      {
        Total = count,
        Data = queryoutput.ToList(),
        PageSize = request.PageSize,
        PageIndex = request.PageIndex,
      };
    }

    /// <summary>
    /// 查询
    /// </summary>
    public ChurukujiluSearchDefaultValueResponse ChurukujiluSearch_GetDefaultValue(ChurukujiluSearchPageparameter request, UserInfo loginUser)
    {
      return new ChurukujiluSearchDefaultValueResponse
      {
      };
    }

    /// <summary>
    /// '审批人' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> ChurukujiluSearch_ApprovedUser_View_DataSource(ChurukujiluSearchPageparameter request, UserInfo loginUser)
    {
      var select =
      from SysUser in _heZuosheDB.SysUser
      select new { SysUser }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser.UserName, x.SysUser.UserName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '类型' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> ChurukujiluSearch_leixing_View_DataSource()
    {
      var data = new List<KVDataInfo>();
      data.Add(new KVDataInfo("出库", "出库"));
      data.Add(new KVDataInfo("入库", "入库"));
      return data;
    }

    /// <summary>
    /// '库存分类' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> ChurukujiluSearch_fenlei_View_DataSource(ChurukujiluSearchPageparameter request, UserInfo loginUser)
    {
      var select =
      from KucunType in _heZuosheDB.KucunType
      select new { KucunType }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.KucunType.Name, x.KucunType.Name)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '物品' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> ChurukujiluSearch_wupin_View_DataSource(ChurukujiluSearchPageparameter request, UserInfo loginUser)
    {
      var select =
      from KucunMingxi in _heZuosheDB.KucunMingxi
      select new { KucunMingxi }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.KucunMingxi.WupinName, x.KucunMingxi.WupinName)
      {
      }).ToList();
      return queryoutput;
    }

  }
}
