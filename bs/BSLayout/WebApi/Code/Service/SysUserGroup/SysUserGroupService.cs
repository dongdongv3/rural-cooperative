using System;
using WebApi.Code.Common.ElModels;
using WebApi.Code.Interface.SysUserGroup;
using WebApi.Code.Interface.SysUserGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using WebApi.Code.Data;

namespace WebApi.Code.Service.SysUserGroup
{
  public class SysUserGroupService : ISysUserGroupInterface
  {
    public readonly HeZuosheDB _heZuosheDB;

    public SysUserGroupService(HeZuosheDB _heZuosheDB)
    {
      this._heZuosheDB = _heZuosheDB;
    }

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult SysUser_AccountAndPasswordAuth1AddSysUser(SysUser_AccountAndPasswordAuth1AddSysUserRequest request, UserInfo loginUser)
    {
      var sysUser_AccountAndPasswordAuth1 = new SysUser_AccountAndPasswordAuth1
      {
        Id = Guid.NewGuid(),
        LoginID = request.LoginID,
        Pwd = request.Pwd,
        RefSysUserId = request.PageParament.RefSysUserId,
      };
      _heZuosheDB.Add(sysUser_AccountAndPasswordAuth1);
      _heZuosheDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘添加’操作执行成功"
      };
    }

    /// <summary>
    /// 添加
    /// </summary>
    public SysUser_AccountAndPasswordAuth1AddSysUserDefaultValueResponse SysUser_AccountAndPasswordAuth1AddSysUser_GetDefaultValue(SysUser_AccountAndPasswordAuth1AddSysUserPageparameter request, UserInfo loginUser)
    {
      return new SysUser_AccountAndPasswordAuth1AddSysUserDefaultValueResponse
      {
        LoginID = default(string?),
        Pwd = default(string?),
      };
    }

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_AccountAndPasswordAuth1AddSysUser_RefSysUserId_DataSource(SysUser_AccountAndPasswordAuth1AddSysUserRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from SysUser in _heZuosheDB.SysUser
      select new { SysUser }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser.Id, x.SysUser.UserName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 编辑
    /// </summary>
    public SysUser_AccountAndPasswordAuth1EditSysUserResponse SysUser_AccountAndPasswordAuth1EditSysUser_GetValue(SysUser_AccountAndPasswordAuth1EditSysUserQueryRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.SysUser_AccountAndPasswordAuth1.FirstOrDefault(x => x.Id == request.Id);
      return new SysUser_AccountAndPasswordAuth1EditSysUserResponse
      {
        Id = entity.Id,
        LoginID = entity.LoginID,
        Pwd = entity.Pwd,
        RefSysUserId = entity.RefSysUserId,
      };
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult SysUser_AccountAndPasswordAuth1EditSysUser(SysUser_AccountAndPasswordAuth1EditSysUserUpdateRequest request, UserInfo loginUser)
    {
      var one = _heZuosheDB.SysUser_AccountAndPasswordAuth1.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { SysUser_AccountAndPasswordAuth1 = one };
      if (one == null)
      {
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.failed,
          ErrorCode = -1,
          Message = "‘编辑’操作执行失败,未找到目标数据"
        };
      }
      one.LoginID = request.LoginID;
      one.Pwd = request.Pwd;
      _heZuosheDB.Entry(one).State = EntityState.Modified;
      _heZuosheDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘编辑’操作执行成功,受影响行数‘1’"
      };
    }

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_AccountAndPasswordAuth1EditSysUser_RefSysUserId_DataSource(SysUser_AccountAndPasswordAuth1EditSysUserUpdateRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from SysUser in _heZuosheDB.SysUser
      select new { SysUser }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser.Id, x.SysUser.UserName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult SysUser_AccountAndPasswordAuth1Delete(SysUser_AccountAndPasswordAuth1DeleteRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.SysUser_AccountAndPasswordAuth1.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { SysUser_AccountAndPasswordAuth1 = entity };
      if (entity != null)
      {
        _heZuosheDB.Remove(entity);
        _heZuosheDB.SaveChanges();
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.succeed,
          ErrorCode = 0,
          Message = "‘删除’操作执行成功,受影响行数‘1’"
        };
      }
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.failed,
        ErrorCode = 1,
        Message = "‘删除’操作执行失败，无法找到目标数据,受影响行数‘0’"
      };
    }

    /// <summary>
    /// 详情
    /// </summary>
    public SysUser_AccountAndPasswordAuth1DetailResponse SysUser_AccountAndPasswordAuth1Detail_GetValue(SysUser_AccountAndPasswordAuth1DetailQueryRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.SysUser_AccountAndPasswordAuth1.FirstOrDefault(x => x.Id == request.Id);
      return new SysUser_AccountAndPasswordAuth1DetailResponse
      {
        Id = entity.Id,
        LoginID = entity.LoginID,
        Pwd = entity.Pwd,
        RefSysUserId = entity.RefSysUserId,
      };
    }

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_AccountAndPasswordAuth1Detail_RefSysUserId_DataSource(SysUser_AccountAndPasswordAuth1DetailResponse_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from SysUser in _heZuosheDB.SysUser
      select new { SysUser }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser.Id, x.SysUser.UserName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<SysUser_AccountAndPasswordAuth1SearchSysUserResponse> SysUser_AccountAndPasswordAuth1SearchSysUser(SysUser_AccountAndPasswordAuth1SearchSysUserQueryRequest request, UserInfo loginUser)
    {
      //填充变量
      if (string.IsNullOrEmpty(request.SortName)) request.SortName = "SysUser_AccountAndPasswordAuth1_LoginID";
      if (string.IsNullOrEmpty(request.SortType)) request.SortType = "desc";
      if (request.PageIndex <= 0) request.PageIndex = 1;
      if (request.PageSize <= 0) request.PageSize = 10;

      //验证输入
      {
        var sortnameList = new List<string> { "SysUser_AccountAndPasswordAuth1_LoginID", "SysUser_AccountAndPasswordAuth1_Pwd", "SysUser_AccountAndPasswordAuth1_Id", "SysUser_AccountAndPasswordAuth1_RefSysUserId" };
        if (sortnameList.All(one => !one.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase)))
        {
          throw new ViewException($"无效的排序列{request.SortName}");
        }
        else
        {
          request.SortName = sortnameList.FirstOrDefault(x => x.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase));
        }
      }
      {
        var pageSizeList = new List<int> { 10, 20, 30 };
        if (pageSizeList.All(one => one != request.PageSize))
        {
          throw new ViewException($"无效的页大小{request.PageSize}");
        }
      }
      // select
      var select =
      from SysUser_AccountAndPasswordAuth1 in _heZuosheDB.SysUser_AccountAndPasswordAuth1
      select new { SysUser_AccountAndPasswordAuth1 };

      // where
      if (request.PageParament.RefSysUserId != null)
      {
        select = select.Where(x => x.SysUser_AccountAndPasswordAuth1.RefSysUserId == request.PageParament.RefSysUserId);
      }

      // query
      var queryoutput = select.Select(x => new SysUser_AccountAndPasswordAuth1SearchSysUserResponse
      {
        SysUser_AccountAndPasswordAuth1_LoginID = x.SysUser_AccountAndPasswordAuth1.LoginID,
        SysUser_AccountAndPasswordAuth1_Pwd = x.SysUser_AccountAndPasswordAuth1.Pwd,
        SysUser_AccountAndPasswordAuth1_Id = x.SysUser_AccountAndPasswordAuth1.Id,
        SysUser_AccountAndPasswordAuth1_RefSysUserId = x.SysUser_AccountAndPasswordAuth1.RefSysUserId,
      });

      // sort
      queryoutput = queryoutput.Sort(request.SortName, request.SortType);
      // pager
      var count = queryoutput.Count();
      queryoutput = queryoutput.Skip((request.PageIndex - 1) * request.PageSize)
      .Take(request.PageSize);

      return new PagerInfo<SysUser_AccountAndPasswordAuth1SearchSysUserResponse>
      {
        Total = count,
        Data = queryoutput.ToList(),
        PageSize = request.PageSize,
        PageIndex = request.PageIndex,
      };
    }

    /// <summary>
    /// 查询
    /// </summary>
    public SysUser_AccountAndPasswordAuth1SearchSysUserDefaultValueResponse SysUser_AccountAndPasswordAuth1SearchSysUser_GetDefaultValue(SysUser_AccountAndPasswordAuth1SearchSysUserPageparameter request, UserInfo loginUser)
    {
      return new SysUser_AccountAndPasswordAuth1SearchSysUserDefaultValueResponse
      {
      };
    }

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> SysUser_AccountAndPasswordAuth1SearchSysUser_RefSysUserId_View_DataSource(SysUser_AccountAndPasswordAuth1SearchSysUserPageparameter request, UserInfo loginUser)
    {
      var select =
      from SysUser in _heZuosheDB.SysUser
      select new { SysUser }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser.Id, x.SysUser.UserName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源-where
    /// </summary>
    public List<KVDataInfo> SysUser_AccountAndPasswordAuth1SearchSysUser_RefSysUserId_Where_DataSource(SysUser_AccountAndPasswordAuth1SearchSysUserQueryRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from SysUser in _heZuosheDB.SysUser
      select new { SysUser }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser.Id, x.SysUser.UserName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult SysUser_UserRoleMapAddSysUser(SysUser_UserRoleMapAddSysUserRequest request, UserInfo loginUser)
    {
      var sysUser_UserRoleMap = new SysUser_UserRoleMap
      {
        Id = Guid.NewGuid(),
        RefSysUserId = request.PageParament.RefSysUserId,
        RefSysUser_RoleId = request.RefSysUser_RoleId,
      };
      _heZuosheDB.Add(sysUser_UserRoleMap);
      _heZuosheDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘添加’操作执行成功"
      };
    }

    /// <summary>
    /// 添加
    /// </summary>
    public SysUser_UserRoleMapAddSysUserDefaultValueResponse SysUser_UserRoleMapAddSysUser_GetDefaultValue(SysUser_UserRoleMapAddSysUserPageparameter request, UserInfo loginUser)
    {
      return new SysUser_UserRoleMapAddSysUserDefaultValueResponse
      {
        RefSysUser_RoleId = default(Guid),
      };
    }

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_UserRoleMapAddSysUser_RefSysUserId_DataSource(SysUser_UserRoleMapAddSysUserRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from SysUser in _heZuosheDB.SysUser
      select new { SysUser }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser.Id, x.SysUser.UserName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_UserRoleMapAddSysUser_RefSysUser_RoleId_DataSource(SysUser_UserRoleMapAddSysUserRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from SysUser_Role in _heZuosheDB.SysUser_Role
      select new { SysUser_Role }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser_Role.Id, x.SysUser_Role.RoleName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult SysUser_UserRoleMapAddSysUser_Role(SysUser_UserRoleMapAddSysUser_RoleRequest request, UserInfo loginUser)
    {
      var sysUser_UserRoleMap = new SysUser_UserRoleMap
      {
        Id = Guid.NewGuid(),
        RefSysUserId = request.RefSysUserId,
        RefSysUser_RoleId = request.PageParament.RefSysUser_RoleId,
      };
      _heZuosheDB.Add(sysUser_UserRoleMap);
      _heZuosheDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘添加’操作执行成功"
      };
    }

    /// <summary>
    /// 添加
    /// </summary>
    public SysUser_UserRoleMapAddSysUser_RoleDefaultValueResponse SysUser_UserRoleMapAddSysUser_Role_GetDefaultValue(SysUser_UserRoleMapAddSysUser_RolePageparameter request, UserInfo loginUser)
    {
      return new SysUser_UserRoleMapAddSysUser_RoleDefaultValueResponse
      {
        RefSysUserId = default(string),
      };
    }

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_UserRoleMapAddSysUser_Role_RefSysUserId_DataSource(SysUser_UserRoleMapAddSysUser_RoleRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from SysUser in _heZuosheDB.SysUser
      select new { SysUser }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser.Id, x.SysUser.UserName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_UserRoleMapAddSysUser_Role_RefSysUser_RoleId_DataSource(SysUser_UserRoleMapAddSysUser_RoleRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from SysUser_Role in _heZuosheDB.SysUser_Role
      select new { SysUser_Role }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser_Role.Id, x.SysUser_Role.RoleName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 编辑
    /// </summary>
    public SysUser_UserRoleMapEditSysUserResponse SysUser_UserRoleMapEditSysUser_GetValue(SysUser_UserRoleMapEditSysUserQueryRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.SysUser_UserRoleMap.FirstOrDefault(x => x.Id == request.Id);
      return new SysUser_UserRoleMapEditSysUserResponse
      {
        Id = entity.Id,
        RefSysUserId = entity.RefSysUserId,
        RefSysUser_RoleId = entity.RefSysUser_RoleId,
      };
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult SysUser_UserRoleMapEditSysUser(SysUser_UserRoleMapEditSysUserUpdateRequest request, UserInfo loginUser)
    {
      var one = _heZuosheDB.SysUser_UserRoleMap.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { SysUser_UserRoleMap = one };
      if (one == null)
      {
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.failed,
          ErrorCode = -1,
          Message = "‘编辑’操作执行失败,未找到目标数据"
        };
      }
      one.RefSysUser_RoleId = request.RefSysUser_RoleId;
      _heZuosheDB.Entry(one).State = EntityState.Modified;
      _heZuosheDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘编辑’操作执行成功,受影响行数‘1’"
      };
    }

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_UserRoleMapEditSysUser_RefSysUserId_DataSource(SysUser_UserRoleMapEditSysUserUpdateRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from SysUser in _heZuosheDB.SysUser
      select new { SysUser }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser.Id, x.SysUser.UserName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_UserRoleMapEditSysUser_RefSysUser_RoleId_DataSource(SysUser_UserRoleMapEditSysUserUpdateRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from SysUser_Role in _heZuosheDB.SysUser_Role
      select new { SysUser_Role }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser_Role.Id, x.SysUser_Role.RoleName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 编辑
    /// </summary>
    public SysUser_UserRoleMapEditSysUser_RoleResponse SysUser_UserRoleMapEditSysUser_Role_GetValue(SysUser_UserRoleMapEditSysUser_RoleQueryRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.SysUser_UserRoleMap.FirstOrDefault(x => x.Id == request.Id);
      return new SysUser_UserRoleMapEditSysUser_RoleResponse
      {
        Id = entity.Id,
        RefSysUserId = entity.RefSysUserId,
        RefSysUser_RoleId = entity.RefSysUser_RoleId,
      };
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult SysUser_UserRoleMapEditSysUser_Role(SysUser_UserRoleMapEditSysUser_RoleUpdateRequest request, UserInfo loginUser)
    {
      var one = _heZuosheDB.SysUser_UserRoleMap.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { SysUser_UserRoleMap = one };
      if (one == null)
      {
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.failed,
          ErrorCode = -1,
          Message = "‘编辑’操作执行失败,未找到目标数据"
        };
      }
      one.RefSysUserId = request.RefSysUserId;
      _heZuosheDB.Entry(one).State = EntityState.Modified;
      _heZuosheDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘编辑’操作执行成功,受影响行数‘1’"
      };
    }

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_UserRoleMapEditSysUser_Role_RefSysUserId_DataSource(SysUser_UserRoleMapEditSysUser_RoleUpdateRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from SysUser in _heZuosheDB.SysUser
      select new { SysUser }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser.Id, x.SysUser.UserName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_UserRoleMapEditSysUser_Role_RefSysUser_RoleId_DataSource(SysUser_UserRoleMapEditSysUser_RoleUpdateRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from SysUser_Role in _heZuosheDB.SysUser_Role
      select new { SysUser_Role }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser_Role.Id, x.SysUser_Role.RoleName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult SysUser_UserRoleMapDelete(SysUser_UserRoleMapDeleteRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.SysUser_UserRoleMap.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { SysUser_UserRoleMap = entity };
      if (entity != null)
      {
        _heZuosheDB.Remove(entity);
        _heZuosheDB.SaveChanges();
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.succeed,
          ErrorCode = 0,
          Message = "‘删除’操作执行成功,受影响行数‘1’"
        };
      }
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.failed,
        ErrorCode = 1,
        Message = "‘删除’操作执行失败，无法找到目标数据,受影响行数‘0’"
      };
    }

    /// <summary>
    /// 详情
    /// </summary>
    public SysUser_UserRoleMapDetailResponse SysUser_UserRoleMapDetail_GetValue(SysUser_UserRoleMapDetailQueryRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.SysUser_UserRoleMap.FirstOrDefault(x => x.Id == request.Id);
      return new SysUser_UserRoleMapDetailResponse
      {
        Id = entity.Id,
        RefSysUserId = entity.RefSysUserId,
        RefSysUser_RoleId = entity.RefSysUser_RoleId,
      };
    }

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_UserRoleMapDetail_RefSysUserId_DataSource(SysUser_UserRoleMapDetailResponse_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from SysUser in _heZuosheDB.SysUser
      select new { SysUser }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser.Id, x.SysUser.UserName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_UserRoleMapDetail_RefSysUser_RoleId_DataSource(SysUser_UserRoleMapDetailResponse_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from SysUser_Role in _heZuosheDB.SysUser_Role
      select new { SysUser_Role }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser_Role.Id, x.SysUser_Role.RoleName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<SysUser_UserRoleMapSearchSysUserResponse> SysUser_UserRoleMapSearchSysUser(SysUser_UserRoleMapSearchSysUserQueryRequest request, UserInfo loginUser)
    {
      //填充变量
      if (string.IsNullOrEmpty(request.SortName)) request.SortName = "SysUser_UserRoleMap_Id";
      if (string.IsNullOrEmpty(request.SortType)) request.SortType = "desc";
      if (request.PageIndex <= 0) request.PageIndex = 1;
      if (request.PageSize <= 0) request.PageSize = 10;

      //验证输入
      {
        var sortnameList = new List<string> { "SysUser_UserRoleMap_Id", "SysUser_UserRoleMap_RefSysUserId", "SysUser_UserRoleMap_RefSysUser_RoleId" };
        if (sortnameList.All(one => !one.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase)))
        {
          throw new ViewException($"无效的排序列{request.SortName}");
        }
        else
        {
          request.SortName = sortnameList.FirstOrDefault(x => x.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase));
        }
      }
      {
        var pageSizeList = new List<int> { 10, 20, 30 };
        if (pageSizeList.All(one => one != request.PageSize))
        {
          throw new ViewException($"无效的页大小{request.PageSize}");
        }
      }
      // select
      var select =
      from SysUser_UserRoleMap in _heZuosheDB.SysUser_UserRoleMap
      select new { SysUser_UserRoleMap };

      // where
      if (request.PageParament.RefSysUserId != null)
      {
        select = select.Where(x => x.SysUser_UserRoleMap.RefSysUserId == request.PageParament.RefSysUserId);
      }

      // query
      var queryoutput = select.Select(x => new SysUser_UserRoleMapSearchSysUserResponse
      {
        SysUser_UserRoleMap_Id = x.SysUser_UserRoleMap.Id,
        SysUser_UserRoleMap_RefSysUserId = x.SysUser_UserRoleMap.RefSysUserId,
        SysUser_UserRoleMap_RefSysUser_RoleId = x.SysUser_UserRoleMap.RefSysUser_RoleId,
      });

      // sort
      queryoutput = queryoutput.Sort(request.SortName, request.SortType);
      // pager
      var count = queryoutput.Count();
      queryoutput = queryoutput.Skip((request.PageIndex - 1) * request.PageSize)
      .Take(request.PageSize);

      return new PagerInfo<SysUser_UserRoleMapSearchSysUserResponse>
      {
        Total = count,
        Data = queryoutput.ToList(),
        PageSize = request.PageSize,
        PageIndex = request.PageIndex,
      };
    }

    /// <summary>
    /// 查询
    /// </summary>
    public SysUser_UserRoleMapSearchSysUserDefaultValueResponse SysUser_UserRoleMapSearchSysUser_GetDefaultValue(SysUser_UserRoleMapSearchSysUserPageparameter request, UserInfo loginUser)
    {
      return new SysUser_UserRoleMapSearchSysUserDefaultValueResponse
      {
      };
    }

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> SysUser_UserRoleMapSearchSysUser_RefSysUserId_View_DataSource(SysUser_UserRoleMapSearchSysUserPageparameter request, UserInfo loginUser)
    {
      var select =
      from SysUser in _heZuosheDB.SysUser
      select new { SysUser }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser.Id, x.SysUser.UserName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> SysUser_UserRoleMapSearchSysUser_RefSysUser_RoleId_View_DataSource(SysUser_UserRoleMapSearchSysUserPageparameter request, UserInfo loginUser)
    {
      var select =
      from SysUser_Role in _heZuosheDB.SysUser_Role
      select new { SysUser_Role }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser_Role.Id, x.SysUser_Role.RoleName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源-where
    /// </summary>
    public List<KVDataInfo> SysUser_UserRoleMapSearchSysUser_RefSysUserId_Where_DataSource(SysUser_UserRoleMapSearchSysUserQueryRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from SysUser in _heZuosheDB.SysUser
      select new { SysUser }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser.Id, x.SysUser.UserName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<SysUser_UserRoleMapSearchSysUser_RoleResponse> SysUser_UserRoleMapSearchSysUser_Role(SysUser_UserRoleMapSearchSysUser_RoleQueryRequest request, UserInfo loginUser)
    {
      //填充变量
      if (string.IsNullOrEmpty(request.SortName)) request.SortName = "SysUser_UserRoleMap_Id";
      if (string.IsNullOrEmpty(request.SortType)) request.SortType = "desc";
      if (request.PageIndex <= 0) request.PageIndex = 1;
      if (request.PageSize <= 0) request.PageSize = 10;

      //验证输入
      {
        var sortnameList = new List<string> { "SysUser_UserRoleMap_Id", "SysUser_UserRoleMap_RefSysUserId", "SysUser_UserRoleMap_RefSysUser_RoleId" };
        if (sortnameList.All(one => !one.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase)))
        {
          throw new ViewException($"无效的排序列{request.SortName}");
        }
        else
        {
          request.SortName = sortnameList.FirstOrDefault(x => x.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase));
        }
      }
      {
        var pageSizeList = new List<int> { 10, 20, 30 };
        if (pageSizeList.All(one => one != request.PageSize))
        {
          throw new ViewException($"无效的页大小{request.PageSize}");
        }
      }
      // select
      var select =
      from SysUser_UserRoleMap in _heZuosheDB.SysUser_UserRoleMap
      select new { SysUser_UserRoleMap };

      // where
      if (request.PageParament.RefSysUser_RoleId != null)
      {
        select = select.Where(x => x.SysUser_UserRoleMap.RefSysUser_RoleId == request.PageParament.RefSysUser_RoleId);
      }

      // query
      var queryoutput = select.Select(x => new SysUser_UserRoleMapSearchSysUser_RoleResponse
      {
        SysUser_UserRoleMap_Id = x.SysUser_UserRoleMap.Id,
        SysUser_UserRoleMap_RefSysUserId = x.SysUser_UserRoleMap.RefSysUserId,
        SysUser_UserRoleMap_RefSysUser_RoleId = x.SysUser_UserRoleMap.RefSysUser_RoleId,
      });

      // sort
      queryoutput = queryoutput.Sort(request.SortName, request.SortType);
      // pager
      var count = queryoutput.Count();
      queryoutput = queryoutput.Skip((request.PageIndex - 1) * request.PageSize)
      .Take(request.PageSize);

      return new PagerInfo<SysUser_UserRoleMapSearchSysUser_RoleResponse>
      {
        Total = count,
        Data = queryoutput.ToList(),
        PageSize = request.PageSize,
        PageIndex = request.PageIndex,
      };
    }

    /// <summary>
    /// 查询
    /// </summary>
    public SysUser_UserRoleMapSearchSysUser_RoleDefaultValueResponse SysUser_UserRoleMapSearchSysUser_Role_GetDefaultValue(SysUser_UserRoleMapSearchSysUser_RolePageparameter request, UserInfo loginUser)
    {
      return new SysUser_UserRoleMapSearchSysUser_RoleDefaultValueResponse
      {
      };
    }

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> SysUser_UserRoleMapSearchSysUser_Role_RefSysUserId_View_DataSource(SysUser_UserRoleMapSearchSysUser_RolePageparameter request, UserInfo loginUser)
    {
      var select =
      from SysUser in _heZuosheDB.SysUser
      select new { SysUser }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser.Id, x.SysUser.UserName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> SysUser_UserRoleMapSearchSysUser_Role_RefSysUser_RoleId_View_DataSource(SysUser_UserRoleMapSearchSysUser_RolePageparameter request, UserInfo loginUser)
    {
      var select =
      from SysUser_Role in _heZuosheDB.SysUser_Role
      select new { SysUser_Role }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser_Role.Id, x.SysUser_Role.RoleName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源-where
    /// </summary>
    public List<KVDataInfo> SysUser_UserRoleMapSearchSysUser_Role_RefSysUser_RoleId_Where_DataSource(SysUser_UserRoleMapSearchSysUser_RoleQueryRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from SysUser_Role in _heZuosheDB.SysUser_Role
      select new { SysUser_Role }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser_Role.Id, x.SysUser_Role.RoleName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult SysUser_RoleAuthMapAddSysUser_Role(SysUser_RoleAuthMapAddSysUser_RoleRequest request, UserInfo loginUser)
    {
      var sysUser_RoleAuthMap = new SysUser_RoleAuthMap
      {
        Id = Guid.NewGuid(),
        RefSysUser_RoleId = request.PageParament.RefSysUser_RoleId,
        RefSysUser_AuthId = request.RefSysUser_AuthId,
      };
      _heZuosheDB.Add(sysUser_RoleAuthMap);
      _heZuosheDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘添加’操作执行成功"
      };
    }

    /// <summary>
    /// 添加
    /// </summary>
    public SysUser_RoleAuthMapAddSysUser_RoleDefaultValueResponse SysUser_RoleAuthMapAddSysUser_Role_GetDefaultValue(SysUser_RoleAuthMapAddSysUser_RolePageparameter request, UserInfo loginUser)
    {
      return new SysUser_RoleAuthMapAddSysUser_RoleDefaultValueResponse
      {
        RefSysUser_AuthId = default(Guid),
      };
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_RoleAuthMapAddSysUser_Role_RefSysUser_RoleId_DataSource(SysUser_RoleAuthMapAddSysUser_RoleRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from SysUser_Role in _heZuosheDB.SysUser_Role
      select new { SysUser_Role }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser_Role.Id, x.SysUser_Role.RoleName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '权限-权限名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_RoleAuthMapAddSysUser_Role_RefSysUser_AuthId_DataSource(SysUser_RoleAuthMapAddSysUser_RoleRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from SysUser_Auth in _heZuosheDB.SysUser_Auth
      select new { SysUser_Auth }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser_Auth.Id, x.SysUser_Auth.AuthName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult SysUser_RoleAuthMapAddSysUser_Auth(SysUser_RoleAuthMapAddSysUser_AuthRequest request, UserInfo loginUser)
    {
      var sysUser_RoleAuthMap = new SysUser_RoleAuthMap
      {
        Id = Guid.NewGuid(),
        RefSysUser_RoleId = request.RefSysUser_RoleId,
        RefSysUser_AuthId = request.PageParament.RefSysUser_AuthId,
      };
      _heZuosheDB.Add(sysUser_RoleAuthMap);
      _heZuosheDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘添加’操作执行成功"
      };
    }

    /// <summary>
    /// 添加
    /// </summary>
    public SysUser_RoleAuthMapAddSysUser_AuthDefaultValueResponse SysUser_RoleAuthMapAddSysUser_Auth_GetDefaultValue(SysUser_RoleAuthMapAddSysUser_AuthPageparameter request, UserInfo loginUser)
    {
      return new SysUser_RoleAuthMapAddSysUser_AuthDefaultValueResponse
      {
        RefSysUser_RoleId = default(Guid),
      };
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_RoleAuthMapAddSysUser_Auth_RefSysUser_RoleId_DataSource(SysUser_RoleAuthMapAddSysUser_AuthRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from SysUser_Role in _heZuosheDB.SysUser_Role
      select new { SysUser_Role }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser_Role.Id, x.SysUser_Role.RoleName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '权限-权限名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_RoleAuthMapAddSysUser_Auth_RefSysUser_AuthId_DataSource(SysUser_RoleAuthMapAddSysUser_AuthRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from SysUser_Auth in _heZuosheDB.SysUser_Auth
      select new { SysUser_Auth }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser_Auth.Id, x.SysUser_Auth.AuthName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 编辑
    /// </summary>
    public SysUser_RoleAuthMapEditSysUser_RoleResponse SysUser_RoleAuthMapEditSysUser_Role_GetValue(SysUser_RoleAuthMapEditSysUser_RoleQueryRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.SysUser_RoleAuthMap.FirstOrDefault(x => x.Id == request.Id);
      return new SysUser_RoleAuthMapEditSysUser_RoleResponse
      {
        Id = entity.Id,
        RefSysUser_RoleId = entity.RefSysUser_RoleId,
        RefSysUser_AuthId = entity.RefSysUser_AuthId,
      };
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult SysUser_RoleAuthMapEditSysUser_Role(SysUser_RoleAuthMapEditSysUser_RoleUpdateRequest request, UserInfo loginUser)
    {
      var one = _heZuosheDB.SysUser_RoleAuthMap.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { SysUser_RoleAuthMap = one };
      if (one == null)
      {
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.failed,
          ErrorCode = -1,
          Message = "‘编辑’操作执行失败,未找到目标数据"
        };
      }
      one.RefSysUser_AuthId = request.RefSysUser_AuthId;
      _heZuosheDB.Entry(one).State = EntityState.Modified;
      _heZuosheDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘编辑’操作执行成功,受影响行数‘1’"
      };
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_RoleAuthMapEditSysUser_Role_RefSysUser_RoleId_DataSource(SysUser_RoleAuthMapEditSysUser_RoleUpdateRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from SysUser_Role in _heZuosheDB.SysUser_Role
      select new { SysUser_Role }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser_Role.Id, x.SysUser_Role.RoleName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '权限-权限名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_RoleAuthMapEditSysUser_Role_RefSysUser_AuthId_DataSource(SysUser_RoleAuthMapEditSysUser_RoleUpdateRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from SysUser_Auth in _heZuosheDB.SysUser_Auth
      select new { SysUser_Auth }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser_Auth.Id, x.SysUser_Auth.AuthName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 编辑
    /// </summary>
    public SysUser_RoleAuthMapEditSysUser_AuthResponse SysUser_RoleAuthMapEditSysUser_Auth_GetValue(SysUser_RoleAuthMapEditSysUser_AuthQueryRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.SysUser_RoleAuthMap.FirstOrDefault(x => x.Id == request.Id);
      return new SysUser_RoleAuthMapEditSysUser_AuthResponse
      {
        Id = entity.Id,
        RefSysUser_RoleId = entity.RefSysUser_RoleId,
        RefSysUser_AuthId = entity.RefSysUser_AuthId,
      };
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult SysUser_RoleAuthMapEditSysUser_Auth(SysUser_RoleAuthMapEditSysUser_AuthUpdateRequest request, UserInfo loginUser)
    {
      var one = _heZuosheDB.SysUser_RoleAuthMap.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { SysUser_RoleAuthMap = one };
      if (one == null)
      {
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.failed,
          ErrorCode = -1,
          Message = "‘编辑’操作执行失败,未找到目标数据"
        };
      }
      one.RefSysUser_RoleId = request.RefSysUser_RoleId;
      _heZuosheDB.Entry(one).State = EntityState.Modified;
      _heZuosheDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘编辑’操作执行成功,受影响行数‘1’"
      };
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_RoleAuthMapEditSysUser_Auth_RefSysUser_RoleId_DataSource(SysUser_RoleAuthMapEditSysUser_AuthUpdateRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from SysUser_Role in _heZuosheDB.SysUser_Role
      select new { SysUser_Role }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser_Role.Id, x.SysUser_Role.RoleName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '权限-权限名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_RoleAuthMapEditSysUser_Auth_RefSysUser_AuthId_DataSource(SysUser_RoleAuthMapEditSysUser_AuthUpdateRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from SysUser_Auth in _heZuosheDB.SysUser_Auth
      select new { SysUser_Auth }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser_Auth.Id, x.SysUser_Auth.AuthName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult SysUser_RoleAuthMapDelete(SysUser_RoleAuthMapDeleteRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.SysUser_RoleAuthMap.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { SysUser_RoleAuthMap = entity };
      if (entity != null)
      {
        _heZuosheDB.Remove(entity);
        _heZuosheDB.SaveChanges();
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.succeed,
          ErrorCode = 0,
          Message = "‘删除’操作执行成功,受影响行数‘1’"
        };
      }
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.failed,
        ErrorCode = 1,
        Message = "‘删除’操作执行失败，无法找到目标数据,受影响行数‘0’"
      };
    }

    /// <summary>
    /// 详情
    /// </summary>
    public SysUser_RoleAuthMapDetailResponse SysUser_RoleAuthMapDetail_GetValue(SysUser_RoleAuthMapDetailQueryRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.SysUser_RoleAuthMap.FirstOrDefault(x => x.Id == request.Id);
      return new SysUser_RoleAuthMapDetailResponse
      {
        Id = entity.Id,
        RefSysUser_RoleId = entity.RefSysUser_RoleId,
        RefSysUser_AuthId = entity.RefSysUser_AuthId,
      };
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_RoleAuthMapDetail_RefSysUser_RoleId_DataSource(SysUser_RoleAuthMapDetailResponse_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from SysUser_Role in _heZuosheDB.SysUser_Role
      select new { SysUser_Role }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser_Role.Id, x.SysUser_Role.RoleName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '权限-权限名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_RoleAuthMapDetail_RefSysUser_AuthId_DataSource(SysUser_RoleAuthMapDetailResponse_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from SysUser_Auth in _heZuosheDB.SysUser_Auth
      select new { SysUser_Auth }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser_Auth.Id, x.SysUser_Auth.AuthName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<SysUser_RoleAuthMapSearchSysUser_RoleResponse> SysUser_RoleAuthMapSearchSysUser_Role(SysUser_RoleAuthMapSearchSysUser_RoleQueryRequest request, UserInfo loginUser)
    {
      //填充变量
      if (string.IsNullOrEmpty(request.SortName)) request.SortName = "SysUser_RoleAuthMap_Id";
      if (string.IsNullOrEmpty(request.SortType)) request.SortType = "desc";
      if (request.PageIndex <= 0) request.PageIndex = 1;
      if (request.PageSize <= 0) request.PageSize = 10;

      //验证输入
      {
        var sortnameList = new List<string> { "SysUser_RoleAuthMap_Id", "SysUser_RoleAuthMap_RefSysUser_RoleId", "SysUser_RoleAuthMap_RefSysUser_AuthId" };
        if (sortnameList.All(one => !one.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase)))
        {
          throw new ViewException($"无效的排序列{request.SortName}");
        }
        else
        {
          request.SortName = sortnameList.FirstOrDefault(x => x.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase));
        }
      }
      {
        var pageSizeList = new List<int> { 10, 20, 30 };
        if (pageSizeList.All(one => one != request.PageSize))
        {
          throw new ViewException($"无效的页大小{request.PageSize}");
        }
      }
      // select
      var select =
      from SysUser_RoleAuthMap in _heZuosheDB.SysUser_RoleAuthMap
      select new { SysUser_RoleAuthMap };

      // where
      if (request.PageParament.RefSysUser_RoleId != null)
      {
        select = select.Where(x => x.SysUser_RoleAuthMap.RefSysUser_RoleId == request.PageParament.RefSysUser_RoleId);
      }

      // query
      var queryoutput = select.Select(x => new SysUser_RoleAuthMapSearchSysUser_RoleResponse
      {
        SysUser_RoleAuthMap_Id = x.SysUser_RoleAuthMap.Id,
        SysUser_RoleAuthMap_RefSysUser_RoleId = x.SysUser_RoleAuthMap.RefSysUser_RoleId,
        SysUser_RoleAuthMap_RefSysUser_AuthId = x.SysUser_RoleAuthMap.RefSysUser_AuthId,
      });

      // sort
      queryoutput = queryoutput.Sort(request.SortName, request.SortType);
      // pager
      var count = queryoutput.Count();
      queryoutput = queryoutput.Skip((request.PageIndex - 1) * request.PageSize)
      .Take(request.PageSize);

      return new PagerInfo<SysUser_RoleAuthMapSearchSysUser_RoleResponse>
      {
        Total = count,
        Data = queryoutput.ToList(),
        PageSize = request.PageSize,
        PageIndex = request.PageIndex,
      };
    }

    /// <summary>
    /// 查询
    /// </summary>
    public SysUser_RoleAuthMapSearchSysUser_RoleDefaultValueResponse SysUser_RoleAuthMapSearchSysUser_Role_GetDefaultValue(SysUser_RoleAuthMapSearchSysUser_RolePageparameter request, UserInfo loginUser)
    {
      return new SysUser_RoleAuthMapSearchSysUser_RoleDefaultValueResponse
      {
      };
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> SysUser_RoleAuthMapSearchSysUser_Role_RefSysUser_RoleId_View_DataSource(SysUser_RoleAuthMapSearchSysUser_RolePageparameter request, UserInfo loginUser)
    {
      var select =
      from SysUser_Role in _heZuosheDB.SysUser_Role
      select new { SysUser_Role }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser_Role.Id, x.SysUser_Role.RoleName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '权限-权限名称' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> SysUser_RoleAuthMapSearchSysUser_Role_RefSysUser_AuthId_View_DataSource(SysUser_RoleAuthMapSearchSysUser_RolePageparameter request, UserInfo loginUser)
    {
      var select =
      from SysUser_Auth in _heZuosheDB.SysUser_Auth
      select new { SysUser_Auth }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser_Auth.Id, x.SysUser_Auth.AuthName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源-where
    /// </summary>
    public List<KVDataInfo> SysUser_RoleAuthMapSearchSysUser_Role_RefSysUser_RoleId_Where_DataSource(SysUser_RoleAuthMapSearchSysUser_RoleQueryRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from SysUser_Role in _heZuosheDB.SysUser_Role
      select new { SysUser_Role }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser_Role.Id, x.SysUser_Role.RoleName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<SysUser_RoleAuthMapSearchSysUser_AuthResponse> SysUser_RoleAuthMapSearchSysUser_Auth(SysUser_RoleAuthMapSearchSysUser_AuthQueryRequest request, UserInfo loginUser)
    {
      //填充变量
      if (string.IsNullOrEmpty(request.SortName)) request.SortName = "SysUser_RoleAuthMap_Id";
      if (string.IsNullOrEmpty(request.SortType)) request.SortType = "desc";
      if (request.PageIndex <= 0) request.PageIndex = 1;
      if (request.PageSize <= 0) request.PageSize = 10;

      //验证输入
      {
        var sortnameList = new List<string> { "SysUser_RoleAuthMap_Id", "SysUser_RoleAuthMap_RefSysUser_RoleId", "SysUser_RoleAuthMap_RefSysUser_AuthId" };
        if (sortnameList.All(one => !one.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase)))
        {
          throw new ViewException($"无效的排序列{request.SortName}");
        }
        else
        {
          request.SortName = sortnameList.FirstOrDefault(x => x.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase));
        }
      }
      {
        var pageSizeList = new List<int> { 10, 20, 30 };
        if (pageSizeList.All(one => one != request.PageSize))
        {
          throw new ViewException($"无效的页大小{request.PageSize}");
        }
      }
      // select
      var select =
      from SysUser_RoleAuthMap in _heZuosheDB.SysUser_RoleAuthMap
      select new { SysUser_RoleAuthMap };

      // where
      if (request.PageParament.RefSysUser_AuthId != null)
      {
        select = select.Where(x => x.SysUser_RoleAuthMap.RefSysUser_AuthId == request.PageParament.RefSysUser_AuthId);
      }

      // query
      var queryoutput = select.Select(x => new SysUser_RoleAuthMapSearchSysUser_AuthResponse
      {
        SysUser_RoleAuthMap_Id = x.SysUser_RoleAuthMap.Id,
        SysUser_RoleAuthMap_RefSysUser_RoleId = x.SysUser_RoleAuthMap.RefSysUser_RoleId,
        SysUser_RoleAuthMap_RefSysUser_AuthId = x.SysUser_RoleAuthMap.RefSysUser_AuthId,
      });

      // sort
      queryoutput = queryoutput.Sort(request.SortName, request.SortType);
      // pager
      var count = queryoutput.Count();
      queryoutput = queryoutput.Skip((request.PageIndex - 1) * request.PageSize)
      .Take(request.PageSize);

      return new PagerInfo<SysUser_RoleAuthMapSearchSysUser_AuthResponse>
      {
        Total = count,
        Data = queryoutput.ToList(),
        PageSize = request.PageSize,
        PageIndex = request.PageIndex,
      };
    }

    /// <summary>
    /// 查询
    /// </summary>
    public SysUser_RoleAuthMapSearchSysUser_AuthDefaultValueResponse SysUser_RoleAuthMapSearchSysUser_Auth_GetDefaultValue(SysUser_RoleAuthMapSearchSysUser_AuthPageparameter request, UserInfo loginUser)
    {
      return new SysUser_RoleAuthMapSearchSysUser_AuthDefaultValueResponse
      {
      };
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> SysUser_RoleAuthMapSearchSysUser_Auth_RefSysUser_RoleId_View_DataSource(SysUser_RoleAuthMapSearchSysUser_AuthPageparameter request, UserInfo loginUser)
    {
      var select =
      from SysUser_Role in _heZuosheDB.SysUser_Role
      select new { SysUser_Role }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser_Role.Id, x.SysUser_Role.RoleName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '权限-权限名称' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> SysUser_RoleAuthMapSearchSysUser_Auth_RefSysUser_AuthId_View_DataSource(SysUser_RoleAuthMapSearchSysUser_AuthPageparameter request, UserInfo loginUser)
    {
      var select =
      from SysUser_Auth in _heZuosheDB.SysUser_Auth
      select new { SysUser_Auth }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser_Auth.Id, x.SysUser_Auth.AuthName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// '权限-权限名称' 字段的数据源-where
    /// </summary>
    public List<KVDataInfo> SysUser_RoleAuthMapSearchSysUser_Auth_RefSysUser_AuthId_Where_DataSource(SysUser_RoleAuthMapSearchSysUser_AuthQueryRequest_ValidateNever request, UserInfo loginUser)
    {
      var select =
      from SysUser_Auth in _heZuosheDB.SysUser_Auth
      select new { SysUser_Auth }
      ;
      var queryoutput = select.Select(x => new KVDataInfo(x.SysUser_Auth.Id, x.SysUser_Auth.AuthName)
      {
      }).ToList();
      return queryoutput;
    }

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult SysUser_AuthAdd(SysUser_AuthAddRequest request, UserInfo loginUser)
    {
      var sysUser_Auth = new SysUser_Auth
      {
        Id = Guid.NewGuid(),
        AuthName = request.AuthName,
      };
      _heZuosheDB.Add(sysUser_Auth);
      _heZuosheDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘添加’操作执行成功"
      };
    }

    /// <summary>
    /// 添加
    /// </summary>
    public SysUser_AuthAddDefaultValueResponse SysUser_AuthAdd_GetDefaultValue(SysUser_AuthAddPageparameter request, UserInfo loginUser)
    {
      return new SysUser_AuthAddDefaultValueResponse
      {
        AuthName = default(string?),
      };
    }

    /// <summary>
    /// 编辑
    /// </summary>
    public SysUser_AuthEditResponse SysUser_AuthEdit_GetValue(SysUser_AuthEditQueryRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.SysUser_Auth.FirstOrDefault(x => x.Id == request.Id);
      return new SysUser_AuthEditResponse
      {
        Id = entity.Id,
        AuthName = entity.AuthName,
      };
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult SysUser_AuthEdit(SysUser_AuthEditUpdateRequest request, UserInfo loginUser)
    {
      var one = _heZuosheDB.SysUser_Auth.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { SysUser_Auth = one };
      if (one == null)
      {
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.failed,
          ErrorCode = -1,
          Message = "‘编辑’操作执行失败,未找到目标数据"
        };
      }
      one.AuthName = request.AuthName;
      _heZuosheDB.Entry(one).State = EntityState.Modified;
      _heZuosheDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘编辑’操作执行成功,受影响行数‘1’"
      };
    }

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult SysUser_AuthDelete(SysUser_AuthDeleteRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.SysUser_Auth.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { SysUser_Auth = entity };
      if (entity != null)
      {
        _heZuosheDB.Remove(entity);
        _heZuosheDB.SaveChanges();
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.succeed,
          ErrorCode = 0,
          Message = "‘删除’操作执行成功,受影响行数‘1’"
        };
      }
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.failed,
        ErrorCode = 1,
        Message = "‘删除’操作执行失败，无法找到目标数据,受影响行数‘0’"
      };
    }

    /// <summary>
    /// 详情
    /// </summary>
    public SysUser_AuthDetailResponse SysUser_AuthDetail_GetValue(SysUser_AuthDetailQueryRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.SysUser_Auth.FirstOrDefault(x => x.Id == request.Id);
      return new SysUser_AuthDetailResponse
      {
        Id = entity.Id,
        AuthName = entity.AuthName,
      };
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<SysUser_AuthSearchResponse> SysUser_AuthSearch(SysUser_AuthSearchQueryRequest request, UserInfo loginUser)
    {
      //填充变量
      if (string.IsNullOrEmpty(request.SortName)) request.SortName = "SysUser_Auth_AuthName";
      if (string.IsNullOrEmpty(request.SortType)) request.SortType = "desc";
      if (request.PageIndex <= 0) request.PageIndex = 1;
      if (request.PageSize <= 0) request.PageSize = 10;

      //验证输入
      {
        var sortnameList = new List<string> { "SysUser_Auth_AuthName", "SysUser_Auth_Id" };
        if (sortnameList.All(one => !one.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase)))
        {
          throw new ViewException($"无效的排序列{request.SortName}");
        }
        else
        {
          request.SortName = sortnameList.FirstOrDefault(x => x.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase));
        }
      }
      {
        var pageSizeList = new List<int> { 10, 20, 30 };
        if (pageSizeList.All(one => one != request.PageSize))
        {
          throw new ViewException($"无效的页大小{request.PageSize}");
        }
      }
      // select
      var select =
      from SysUser_Auth in _heZuosheDB.SysUser_Auth
      select new { SysUser_Auth };

      // where

      // query
      var queryoutput = select.Select(x => new SysUser_AuthSearchResponse
      {
        SysUser_Auth_AuthName = x.SysUser_Auth.AuthName,
        SysUser_Auth_Id = x.SysUser_Auth.Id,
      });

      // sort
      queryoutput = queryoutput.Sort(request.SortName, request.SortType);
      // pager
      var count = queryoutput.Count();
      queryoutput = queryoutput.Skip((request.PageIndex - 1) * request.PageSize)
      .Take(request.PageSize);

      return new PagerInfo<SysUser_AuthSearchResponse>
      {
        Total = count,
        Data = queryoutput.ToList(),
        PageSize = request.PageSize,
        PageIndex = request.PageIndex,
      };
    }

    /// <summary>
    /// 查询
    /// </summary>
    public SysUser_AuthSearchDefaultValueResponse SysUser_AuthSearch_GetDefaultValue(SysUser_AuthSearchPageparameter request, UserInfo loginUser)
    {
      return new SysUser_AuthSearchDefaultValueResponse
      {
      };
    }

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult SysUser_RoleAdd(SysUser_RoleAddRequest request, UserInfo loginUser)
    {
      var sysUser_Role = new SysUser_Role
      {
        Id = Guid.NewGuid(),
        RoleName = request.RoleName,
      };
      _heZuosheDB.Add(sysUser_Role);
      _heZuosheDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘添加’操作执行成功"
      };
    }

    /// <summary>
    /// 添加
    /// </summary>
    public SysUser_RoleAddDefaultValueResponse SysUser_RoleAdd_GetDefaultValue(SysUser_RoleAddPageparameter request, UserInfo loginUser)
    {
      return new SysUser_RoleAddDefaultValueResponse
      {
        RoleName = default(string?),
      };
    }

    /// <summary>
    /// 编辑
    /// </summary>
    public SysUser_RoleEditResponse SysUser_RoleEdit_GetValue(SysUser_RoleEditQueryRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.SysUser_Role.FirstOrDefault(x => x.Id == request.Id);
      return new SysUser_RoleEditResponse
      {
        Id = entity.Id,
        RoleName = entity.RoleName,
      };
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult SysUser_RoleEdit(SysUser_RoleEditUpdateRequest request, UserInfo loginUser)
    {
      var one = _heZuosheDB.SysUser_Role.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { SysUser_Role = one };
      if (one == null)
      {
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.failed,
          ErrorCode = -1,
          Message = "‘编辑’操作执行失败,未找到目标数据"
        };
      }
      one.RoleName = request.RoleName;
      _heZuosheDB.Entry(one).State = EntityState.Modified;
      _heZuosheDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘编辑’操作执行成功,受影响行数‘1’"
      };
    }

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult SysUser_RoleDelete(SysUser_RoleDeleteRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.SysUser_Role.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { SysUser_Role = entity };
      if (entity != null)
      {
        _heZuosheDB.Remove(entity);
        _heZuosheDB.SaveChanges();
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.succeed,
          ErrorCode = 0,
          Message = "‘删除’操作执行成功,受影响行数‘1’"
        };
      }
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.failed,
        ErrorCode = 1,
        Message = "‘删除’操作执行失败，无法找到目标数据,受影响行数‘0’"
      };
    }

    /// <summary>
    /// 详情
    /// </summary>
    public SysUser_RoleDetailResponse SysUser_RoleDetail_GetValue(SysUser_RoleDetailQueryRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.SysUser_Role.FirstOrDefault(x => x.Id == request.Id);
      return new SysUser_RoleDetailResponse
      {
        Id = entity.Id,
        RoleName = entity.RoleName,
      };
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<SysUser_RoleSearchResponse> SysUser_RoleSearch(SysUser_RoleSearchQueryRequest request, UserInfo loginUser)
    {
      //填充变量
      if (string.IsNullOrEmpty(request.SortName)) request.SortName = "SysUser_Role_RoleName";
      if (string.IsNullOrEmpty(request.SortType)) request.SortType = "desc";
      if (request.PageIndex <= 0) request.PageIndex = 1;
      if (request.PageSize <= 0) request.PageSize = 10;

      //验证输入
      {
        var sortnameList = new List<string> { "SysUser_Role_RoleName", "SysUser_Role_Id" };
        if (sortnameList.All(one => !one.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase)))
        {
          throw new ViewException($"无效的排序列{request.SortName}");
        }
        else
        {
          request.SortName = sortnameList.FirstOrDefault(x => x.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase));
        }
      }
      {
        var pageSizeList = new List<int> { 10, 20, 30 };
        if (pageSizeList.All(one => one != request.PageSize))
        {
          throw new ViewException($"无效的页大小{request.PageSize}");
        }
      }
      // select
      var select =
      from SysUser_Role in _heZuosheDB.SysUser_Role
      select new { SysUser_Role };

      // where

      // query
      var queryoutput = select.Select(x => new SysUser_RoleSearchResponse
      {
        SysUser_Role_RoleName = x.SysUser_Role.RoleName,
        SysUser_Role_Id = x.SysUser_Role.Id,
      });

      // sort
      queryoutput = queryoutput.Sort(request.SortName, request.SortType);
      // pager
      var count = queryoutput.Count();
      queryoutput = queryoutput.Skip((request.PageIndex - 1) * request.PageSize)
      .Take(request.PageSize);

      return new PagerInfo<SysUser_RoleSearchResponse>
      {
        Total = count,
        Data = queryoutput.ToList(),
        PageSize = request.PageSize,
        PageIndex = request.PageIndex,
      };
    }

    /// <summary>
    /// 查询
    /// </summary>
    public SysUser_RoleSearchDefaultValueResponse SysUser_RoleSearch_GetDefaultValue(SysUser_RoleSearchPageparameter request, UserInfo loginUser)
    {
      return new SysUser_RoleSearchDefaultValueResponse
      {
      };
    }

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult SysUserAdd(SysUserAddRequest request, UserInfo loginUser)
    {
      var sysUser = new SysUser
      {
        Id = request.Id,
        UserName = request.UserName,
      };
      _heZuosheDB.Add(sysUser);
      _heZuosheDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘添加’操作执行成功"
      };
    }

    /// <summary>
    /// 添加
    /// </summary>
    public SysUserAddDefaultValueResponse SysUserAdd_GetDefaultValue(SysUserAddPageparameter request, UserInfo loginUser)
    {
      return new SysUserAddDefaultValueResponse
      {
        Id = default(string),
        UserName = default(string?),
      };
    }

    /// <summary>
    /// 编辑
    /// </summary>
    public SysUserEditResponse SysUserEdit_GetValue(SysUserEditQueryRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.SysUser.FirstOrDefault(x => x.Id == request.Id);
      return new SysUserEditResponse
      {
        Id = entity.Id,
        UserName = entity.UserName,
      };
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult SysUserEdit(SysUserEditUpdateRequest request, UserInfo loginUser)
    {
      var one = _heZuosheDB.SysUser.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { SysUser = one };
      if (one == null)
      {
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.failed,
          ErrorCode = -1,
          Message = "‘编辑’操作执行失败,未找到目标数据"
        };
      }
      one.Id = request.Id;
      one.UserName = request.UserName;
      _heZuosheDB.Entry(one).State = EntityState.Modified;
      _heZuosheDB.SaveChanges();
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.succeed,
        ErrorCode = 0,
        Message = "‘编辑’操作执行成功,受影响行数‘1’"
      };
    }

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult SysUserDelete(SysUserDeleteRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.SysUser.FirstOrDefault(x => x.Id == request.Id);
      var dbvalue = new { SysUser = entity };
      if (entity != null)
      {
        _heZuosheDB.Remove(entity);
        _heZuosheDB.SaveChanges();
        return new OptionsResoult
        {
          Status = OptionsResoultStatus.succeed,
          ErrorCode = 0,
          Message = "‘删除’操作执行成功,受影响行数‘1’"
        };
      }
      return new OptionsResoult
      {
        Status = OptionsResoultStatus.failed,
        ErrorCode = 1,
        Message = "‘删除’操作执行失败，无法找到目标数据,受影响行数‘0’"
      };
    }

    /// <summary>
    /// 详情
    /// </summary>
    public SysUserDetailResponse SysUserDetail_GetValue(SysUserDetailQueryRequest request, UserInfo loginUser)
    {
      var entity = _heZuosheDB.SysUser.FirstOrDefault(x => x.Id == request.Id);
      return new SysUserDetailResponse
      {
        Id = entity.Id,
        UserName = entity.UserName,
      };
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<SysUserSearchResponse> SysUserSearch(SysUserSearchQueryRequest request, UserInfo loginUser)
    {
      //填充变量
      if (string.IsNullOrEmpty(request.SortName)) request.SortName = "SysUser_UserName";
      if (string.IsNullOrEmpty(request.SortType)) request.SortType = "desc";
      if (request.PageIndex <= 0) request.PageIndex = 1;
      if (request.PageSize <= 0) request.PageSize = 10;

      //验证输入
      {
        var sortnameList = new List<string> { "SysUser_UserName", "SysUser_Id" };
        if (sortnameList.All(one => !one.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase)))
        {
          throw new ViewException($"无效的排序列{request.SortName}");
        }
        else
        {
          request.SortName = sortnameList.FirstOrDefault(x => x.Equals(request.SortName, StringComparison.CurrentCultureIgnoreCase));
        }
      }
      {
        var pageSizeList = new List<int> { 10, 20, 30 };
        if (pageSizeList.All(one => one != request.PageSize))
        {
          throw new ViewException($"无效的页大小{request.PageSize}");
        }
      }
      // select
      var select =
      from SysUser in _heZuosheDB.SysUser
      select new { SysUser };

      // where

      // query
      var queryoutput = select.Select(x => new SysUserSearchResponse
      {
        SysUser_UserName = x.SysUser.UserName,
        SysUser_Id = x.SysUser.Id,
      });

      // sort
      queryoutput = queryoutput.Sort(request.SortName, request.SortType);
      // pager
      var count = queryoutput.Count();
      queryoutput = queryoutput.Skip((request.PageIndex - 1) * request.PageSize)
      .Take(request.PageSize);

      return new PagerInfo<SysUserSearchResponse>
      {
        Total = count,
        Data = queryoutput.ToList(),
        PageSize = request.PageSize,
        PageIndex = request.PageIndex,
      };
    }

    /// <summary>
    /// 查询
    /// </summary>
    public SysUserSearchDefaultValueResponse SysUserSearch_GetDefaultValue(SysUserSearchPageparameter request, UserInfo loginUser)
    {
      return new SysUserSearchDefaultValueResponse
      {
      };
    }

  }
}
