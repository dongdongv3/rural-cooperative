using System;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Code.Data
{
  /// <summary>
  /// 合作社数据库
  /// </summary>
  public class HeZuosheDB : DbContext
  {
    public HeZuosheDB(DbContextOptions<HeZuosheDB> options) : base(options) { }
    /// <summary>
    /// 系统用户-用户信息
    /// </summary>
    public DbSet<SysUser> SysUser { get; set; }
    /// <summary>
    /// 账户密码认证1-认证通道
    /// </summary>
    public DbSet<SysUser_AccountAndPasswordAuth1> SysUser_AccountAndPasswordAuth1 { get; set; }
    /// <summary>
    /// 角色
    /// </summary>
    public DbSet<SysUser_Role> SysUser_Role { get; set; }
    /// <summary>
    /// 权限
    /// </summary>
    public DbSet<SysUser_Auth> SysUser_Auth { get; set; }
    /// <summary>
    /// 角色权限映射表
    /// </summary>
    public DbSet<SysUser_RoleAuthMap> SysUser_RoleAuthMap { get; set; }
    /// <summary>
    /// 用户角色映射表
    /// </summary>
    public DbSet<SysUser_UserRoleMap> SysUser_UserRoleMap { get; set; }
    /// <summary>
    /// 账户记录
    /// </summary>
    public DbSet<ZhangHuLog> ZhangHuLog { get; set; }
    /// <summary>
    /// 社员
    /// </summary>
    public DbSet<Sheyuan> Sheyuan { get; set; }
    /// <summary>
    /// 耕地片区
    /// </summary>
    public DbSet<GengdiType> GengdiType { get; set; }
    /// <summary>
    /// 库存分类
    /// </summary>
    public DbSet<KucunType> KucunType { get; set; }
    /// <summary>
    /// 库存明细
    /// </summary>
    public DbSet<KucunMingxi> KucunMingxi { get; set; }
    /// <summary>
    /// 出入库记录
    /// </summary>
    public DbSet<Churukujilu> Churukujilu { get; set; }
    /// <summary>
    /// 农机分类
    /// </summary>
    public DbSet<Nongjifenlei> Nongjifenlei { get; set; }
    /// <summary>
    /// 农机合作
    /// </summary>
    public DbSet<Nongjihezuo> Nongjihezuo { get; set; }
    /// <summary>
    /// 农机使用工单
    /// </summary>
    public DbSet<Nongjigongdan> Nongjigongdan { get; set; }
    /// <summary>
    /// 文件上传
    /// </summary>
    public DbSet<FileUpLoad> FileUpLoad { get; set; }
  }
}
