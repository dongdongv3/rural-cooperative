using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace WebApi.Code.Data
{
  public class KucunMingxi
  {
    /// <summary>
    /// Id
    /// </summary>
    [Key]
    [Required]
    public Guid Id { get; set; }
    /// <summary>
    /// 物品名称
    /// </summary>
    [MaxLength(50)]
    public string? WupinName { get; set; }
    /// <summary>
    /// 剩余数量
    /// </summary>
    public int? Count { get; set; }
    /// <summary>
    /// 单位
    /// </summary>
    [MaxLength(10)]
    public string? Danwei { get; set; }
    /// <summary>
    /// 分类
    /// </summary>
    [MaxLength(50)]
    public string? Fenlei { get; set; }
  }
}
