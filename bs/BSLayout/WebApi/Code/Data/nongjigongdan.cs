using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace WebApi.Code.Data
{
  public class Nongjigongdan
  {
    /// <summary>
    /// Id
    /// </summary>
    [Key]
    [Required]
    public Guid Id { get; set; }
    /// <summary>
    /// 创建时间
    /// </summary>
    public DateTime? Createtimre { get; set; }
    /// <summary>
    /// 处理时间
    /// </summary>
    public DateTime? BeginTime { get; set; }
    /// <summary>
    /// 面积
    /// </summary>
    public int? Miangji { get; set; }
    /// <summary>
    /// 农机类型
    /// </summary>
    [MaxLength(50)]
    public string? Leixing { get; set; }
    /// <summary>
    /// 接单农机
    /// </summary>
    [MaxLength(50)]
    public string? Chuli { get; set; }
    /// <summary>
    /// 总价
    /// </summary>
    public decimal? Zongjia { get; set; }
    /// <summary>
    /// 状态
    /// </summary>
    [MaxLength(50)]
    public string? Zhuangtai { get; set; }
    /// <summary>
    /// 结算状态
    /// </summary>
    [MaxLength(50)]
    public string? Jeisuanzhuangtai { get; set; }
  }
}
