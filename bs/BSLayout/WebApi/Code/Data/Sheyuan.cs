using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace WebApi.Code.Data
{
  public class Sheyuan
  {
    /// <summary>
    /// Id
    /// </summary>
    [Key]
    [Required]
    public Guid Id { get; set; }
    /// <summary>
    /// 姓名
    /// </summary>
    [MaxLength(10)]
    public string? UserName { get; set; }
    /// <summary>
    /// 身份证
    /// </summary>
    [MaxLength(50)]
    public string? UserCode { get; set; }
    /// <summary>
    /// 联系电话
    /// </summary>
    [MaxLength(50)]
    public string? PhoneNumber { get; set; }
    /// <summary>
    /// 加入日期
    /// </summary>
    public int? JoinTime_Year { get; set; }
    /// <summary>
    /// 加入日期
    /// </summary>
    public int? JoinTime_Month { get; set; }
    /// <summary>
    /// 片区
    /// </summary>
    [MaxLength(50)]
    public string? Panqu { get; set; }
    /// <summary>
    /// 面积
    /// </summary>
    public double? Miangji { get; set; }
    /// <summary>
    /// 状态
    /// </summary>
    [MaxLength(50)]
    public string? Status { get; set; }
    /// <summary>
    /// 账户余额
    /// </summary>
    public decimal? Yue { get; set; }
    /// <summary>
    /// 账户记录
    /// </summary>
    public List<ZhangHuLog> SubZhangHuLog { get; set; }
  }
}
