using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace WebApi.Code.Data
{
  public class SysUser_RoleAuthMap
  {
    /// <summary>
    /// Id
    /// </summary>
    [Key]
    [Required]
    public Guid Id { get; set; }
    /// <summary>
    /// 角色
    /// </summary>
    public Guid RefSysUser_RoleId { get; set; }
    /// <summary>
    /// SysUser_Role
    /// </summary>
    public SysUser_Role RefSysUser_Role { get; set; }
    /// <summary>
    /// 权限
    /// </summary>
    public Guid RefSysUser_AuthId { get; set; }
    /// <summary>
    /// SysUser_Auth
    /// </summary>
    public SysUser_Auth RefSysUser_Auth { get; set; }
  }
}
