using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace WebApi.Code.Data
{
  public class Nongjihezuo
  {
    /// <summary>
    /// Id
    /// </summary>
    [Key]
    [Required]
    public Guid Id { get; set; }
    /// <summary>
    /// 合作人名称
    /// </summary>
    [MaxLength(50)]
    public string? Username { get; set; }
    /// <summary>
    /// 合作人电话
    /// </summary>
    [MaxLength(50)]
    public string? PhoneNumber { get; set; }
    /// <summary>
    /// 农机类型
    /// </summary>
    [MaxLength(50)]
    public string? Leixing { get; set; }
    /// <summary>
    /// 农机型号
    /// </summary>
    [MaxLength(50)]
    public string? Xinghao { get; set; }
    /// <summary>
    /// 签约价(每亩)
    /// </summary>
    public decimal? Qianyuejia { get; set; }
  }
}
