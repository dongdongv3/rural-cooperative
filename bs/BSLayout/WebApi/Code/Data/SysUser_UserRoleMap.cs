using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace WebApi.Code.Data
{
  public class SysUser_UserRoleMap
  {
    /// <summary>
    /// Id
    /// </summary>
    [Key]
    [Required]
    public Guid Id { get; set; }
    /// <summary>
    /// 系统用户-用户信息
    /// </summary>
    public string RefSysUserId { get; set; }
    /// <summary>
    /// SysUser
    /// </summary>
    public SysUser RefSysUser { get; set; }
    /// <summary>
    /// 角色
    /// </summary>
    public Guid RefSysUser_RoleId { get; set; }
    /// <summary>
    /// SysUser_Role
    /// </summary>
    public SysUser_Role RefSysUser_Role { get; set; }
  }
}
