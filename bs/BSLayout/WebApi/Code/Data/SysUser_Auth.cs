using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace WebApi.Code.Data
{
  public class SysUser_Auth
  {
    /// <summary>
    /// Id
    /// </summary>
    [Key]
    [Required]
    public Guid Id { get; set; }
    /// <summary>
    /// 权限名称
    /// </summary>
    [MaxLength(255)]
    public string? AuthName { get; set; }
    /// <summary>
    /// 角色权限映射表
    /// </summary>
    public List<SysUser_RoleAuthMap> SubSysUser_RoleAuthMap { get; set; }
  }
}
