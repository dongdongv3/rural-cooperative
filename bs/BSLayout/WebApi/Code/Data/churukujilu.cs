using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace WebApi.Code.Data
{
  public class Churukujilu
  {
    /// <summary>
    /// Id
    /// </summary>
    [Key]
    [Required]
    public Guid Id { get; set; }
    /// <summary>
    /// 发生时间
    /// </summary>
    public DateTime? Createtime { get; set; }
    /// <summary>
    /// 申请人
    /// </summary>
    [MaxLength(50)]
    public string? Shengqingren { get; set; }
    /// <summary>
    /// 审批人
    /// </summary>
    [MaxLength(50)]
    public string? ApprovedUser { get; set; }
    /// <summary>
    /// 类型
    /// </summary>
    [MaxLength(50)]
    public string? Leixing { get; set; }
    /// <summary>
    /// 库存分类
    /// </summary>
    [MaxLength(50)]
    public string? Fenlei { get; set; }
    /// <summary>
    /// 物品
    /// </summary>
    [MaxLength(50)]
    public string? Wupin { get; set; }
    /// <summary>
    /// 出入库数量
    /// </summary>
    public int? Shuliang { get; set; }
  }
}
