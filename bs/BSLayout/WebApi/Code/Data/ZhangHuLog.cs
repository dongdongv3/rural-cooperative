using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace WebApi.Code.Data
{
  public class ZhangHuLog
  {
    /// <summary>
    /// Id
    /// </summary>
    [Key]
    [Required]
    public Guid Id { get; set; }
    /// <summary>
    /// 创建时间
    /// </summary>
    public DateTime? CreateTime { get; set; }
    /// <summary>
    /// 操作金额
    /// </summary>
    public decimal? ActionMoney { get; set; }
    /// <summary>
    /// 说明
    /// </summary>
    [MaxLength(200)]
    public string? Remark { get; set; }
    /// <summary>
    /// 社员
    /// </summary>
    public Guid RefSheyuanId { get; set; }
    /// <summary>
    /// Sheyuan
    /// </summary>
    public Sheyuan RefSheyuan { get; set; }
  }
}
