using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace WebApi.Code.Data
{
  public class SysUser
  {
    /// <summary>
    /// Id
    /// </summary>
    [Key]
    [MaxLength(50)]
    [Required]
    public string Id { get; set; }
    /// <summary>
    /// 用户名
    /// </summary>
    [MaxLength(50)]
    public string? UserName { get; set; }
    /// <summary>
    /// 账户密码认证1-认证通道
    /// </summary>
    public SysUser_AccountAndPasswordAuth1 SubSysUser_AccountAndPasswordAuth1 { get; set; }
    /// <summary>
    /// 用户角色映射表
    /// </summary>
    public List<SysUser_UserRoleMap> SubSysUser_UserRoleMap { get; set; }
  }
}
