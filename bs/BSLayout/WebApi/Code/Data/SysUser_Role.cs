using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace WebApi.Code.Data
{
  public class SysUser_Role
  {
    /// <summary>
    /// Id
    /// </summary>
    [Key]
    [Required]
    public Guid Id { get; set; }
    /// <summary>
    /// 角色名称
    /// </summary>
    [MaxLength(50)]
    public string? RoleName { get; set; }
    /// <summary>
    /// 用户角色映射表
    /// </summary>
    public List<SysUser_UserRoleMap> SubSysUser_UserRoleMap { get; set; }
    /// <summary>
    /// 角色权限映射表
    /// </summary>
    public List<SysUser_RoleAuthMap> SubSysUser_RoleAuthMap { get; set; }
  }
}
