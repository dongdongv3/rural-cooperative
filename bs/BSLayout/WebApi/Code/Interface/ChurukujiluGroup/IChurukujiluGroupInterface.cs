using System;
using WebApi.Code.Interface.ChurukujiluGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Code.Interface.ChurukujiluGroup
{
  public interface IChurukujiluGroupInterface
  {
    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult ChurukujiluAdd(ChurukujiluAddRequest request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public ChurukujiluAddDefaultValueResponse ChurukujiluAdd_GetDefaultValue(ChurukujiluAddPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '审批人' 字段的数据源
    /// </summary>
    public List<KVDataInfo> ChurukujiluAdd_ApprovedUser_DataSource(ChurukujiluAddRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '类型' 字段的数据源
    /// </summary>
    public List<KVDataInfo> ChurukujiluAdd_leixing_DataSource();

    /// <summary>
    /// '库存分类' 字段的数据源
    /// </summary>
    public List<KVDataInfo> ChurukujiluAdd_fenlei_DataSource(ChurukujiluAddRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '物品' 字段的数据源
    /// </summary>
    public List<KVDataInfo> ChurukujiluAdd_wupin_DataSource(ChurukujiluAddRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// </summary>
    public ChurukujiluEditResponse ChurukujiluEdit_GetValue(ChurukujiluEditQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult ChurukujiluEdit(ChurukujiluEditUpdateRequest request, UserInfo loginUser);

    /// <summary>
    /// '审批人' 字段的数据源
    /// </summary>
    public List<KVDataInfo> ChurukujiluEdit_ApprovedUser_DataSource(ChurukujiluEditUpdateRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '类型' 字段的数据源
    /// </summary>
    public List<KVDataInfo> ChurukujiluEdit_leixing_DataSource();

    /// <summary>
    /// '库存分类' 字段的数据源
    /// </summary>
    public List<KVDataInfo> ChurukujiluEdit_fenlei_DataSource(ChurukujiluEditUpdateRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '物品' 字段的数据源
    /// </summary>
    public List<KVDataInfo> ChurukujiluEdit_wupin_DataSource(ChurukujiluEditUpdateRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 详情
    /// </summary>
    public ChurukujiluDetailResponse ChurukujiluDetail_GetValue(ChurukujiluDetailQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// '审批人' 字段的数据源
    /// </summary>
    public List<KVDataInfo> ChurukujiluDetail_ApprovedUser_DataSource(ChurukujiluDetailResponse_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '类型' 字段的数据源
    /// </summary>
    public List<KVDataInfo> ChurukujiluDetail_leixing_DataSource();

    /// <summary>
    /// '库存分类' 字段的数据源
    /// </summary>
    public List<KVDataInfo> ChurukujiluDetail_fenlei_DataSource(ChurukujiluDetailResponse_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '物品' 字段的数据源
    /// </summary>
    public List<KVDataInfo> ChurukujiluDetail_wupin_DataSource(ChurukujiluDetailResponse_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult ChurukujiluDelete(ChurukujiluDeleteRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<ChurukujiluSearchResponse> ChurukujiluSearch(ChurukujiluSearchQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// </summary>
    public ChurukujiluSearchDefaultValueResponse ChurukujiluSearch_GetDefaultValue(ChurukujiluSearchPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '审批人' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> ChurukujiluSearch_ApprovedUser_View_DataSource(ChurukujiluSearchPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '类型' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> ChurukujiluSearch_leixing_View_DataSource();

    /// <summary>
    /// '库存分类' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> ChurukujiluSearch_fenlei_View_DataSource(ChurukujiluSearchPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '物品' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> ChurukujiluSearch_wupin_View_DataSource(ChurukujiluSearchPageparameter request, UserInfo loginUser);

  }
}
