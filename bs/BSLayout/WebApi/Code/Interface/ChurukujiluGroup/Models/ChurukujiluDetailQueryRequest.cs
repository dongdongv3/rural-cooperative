using System;

namespace WebApi.Code.Interface.ChurukujiluGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 详情
  /// /// 查询输入模型
  /// /// </summary>
  /// </summary>
  public class ChurukujiluDetailQueryRequest : ChurukujiluDetailPageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
  }
}
