using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Code.Interface.ChurukujiluGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// 入参模型
  /// /// </summary>
  /// </summary>
  public class ChurukujiluAddRequest : ChurukujiluAddPageparameter
  {
    /// <summary>
    /// 申请人
    /// </summary>
    [MaxLength(50)]
    public string? Shengqingren { get; set; }
    /// <summary>
    /// 审批人
    /// </summary>
    public string? ApprovedUser { get; set; }
    /// <summary>
    /// 类型
    /// </summary>
    public string? Leixing { get; set; }
    /// <summary>
    /// 库存分类
    /// </summary>
    public string? Fenlei { get; set; }
    /// <summary>
    /// 物品
    /// </summary>
    public string? Wupin { get; set; }
    /// <summary>
    /// 出入库数量
    /// </summary>
    public int? Shuliang { get; set; }
  }
}
