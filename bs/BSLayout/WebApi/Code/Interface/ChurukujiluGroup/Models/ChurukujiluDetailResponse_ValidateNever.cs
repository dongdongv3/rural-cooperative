using System;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;

namespace WebApi.Code.Interface.ChurukujiluGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 详情
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  [ValidateNever]
  public class ChurukujiluDetailResponse_ValidateNever : ChurukujiluDetailResponse
  {
  }
}
