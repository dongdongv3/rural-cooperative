using System;

namespace WebApi.Code.Interface.ChurukujiluGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 查询
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class ChurukujiluSearchResponse : ChurukujiluSearchPageparameter
  {
    /// <summary>
    /// 出入库记录 - 发生时间
    /// </summary>
    public DateTime? Churukujilu_Createtime { get; set; }
    /// <summary>
    /// 出入库记录 - 申请人
    /// </summary>
    public string? Churukujilu_Shengqingren { get; set; }
    /// <summary>
    /// 出入库记录 - 审批人
    /// </summary>
    public string? Churukujilu_ApprovedUser { get; set; }
    /// <summary>
    /// 出入库记录 - 类型
    /// </summary>
    public string? Churukujilu_Leixing { get; set; }
    /// <summary>
    /// 出入库记录 - 库存分类
    /// </summary>
    public string? Churukujilu_Fenlei { get; set; }
    /// <summary>
    /// 出入库记录 - 物品
    /// </summary>
    public string? Churukujilu_Wupin { get; set; }
    /// <summary>
    /// 出入库记录 - 出入库数量
    /// </summary>
    public int? Churukujilu_Shuliang { get; set; }
    /// <summary>
    /// 出入库记录 - Id
    /// </summary>
    public Guid Churukujilu_Id { get; set; }
  }
}
