using System;

namespace WebApi.Code.Interface.KucunMingxiGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 查询
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class KucunMingxiSearchResponse : KucunMingxiSearchPageparameter
  {
    /// <summary>
    /// 库存明细 - 物品名称
    /// </summary>
    public string? KucunMingxi_WupinName { get; set; }
    /// <summary>
    /// 库存明细 - 剩余数量
    /// </summary>
    public int? KucunMingxi_Count { get; set; }
    /// <summary>
    /// 库存明细 - 单位
    /// </summary>
    public string? KucunMingxi_Danwei { get; set; }
    /// <summary>
    /// 库存明细 - 分类
    /// </summary>
    public string? KucunMingxi_Fenlei { get; set; }
    /// <summary>
    /// 库存明细 - Id
    /// </summary>
    public Guid KucunMingxi_Id { get; set; }
  }
}
