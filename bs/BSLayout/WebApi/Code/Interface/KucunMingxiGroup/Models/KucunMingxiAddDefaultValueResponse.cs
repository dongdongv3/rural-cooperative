using System;

namespace WebApi.Code.Interface.KucunMingxiGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// DefaultValue 输出模型
  /// /// </summary>
  /// </summary>
  public class KucunMingxiAddDefaultValueResponse : KucunMingxiAddPageparameter
  {
    /// <summary>
    /// 物品名称
    /// </summary>
    public string? WupinName { get; set; }
    /// <summary>
    /// 剩余数量
    /// </summary>
    public int? Count { get; set; }
    /// <summary>
    /// 单位
    /// </summary>
    public string? Danwei { get; set; }
    /// <summary>
    /// 分类
    /// </summary>
    public string? Fenlei { get; set; }
  }
}
