using System;

namespace WebApi.Code.Interface.KucunMingxiGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 编辑
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class KucunMingxiEditResponse : KucunMingxiEditPageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
    /// <summary>
    /// 物品名称
    /// </summary>
    public string? WupinName { get; set; }
    /// <summary>
    /// 剩余数量
    /// </summary>
    public int? Count { get; set; }
    /// <summary>
    /// 单位
    /// </summary>
    public string? Danwei { get; set; }
    /// <summary>
    /// 分类
    /// </summary>
    public string? Fenlei { get; set; }
  }
}
