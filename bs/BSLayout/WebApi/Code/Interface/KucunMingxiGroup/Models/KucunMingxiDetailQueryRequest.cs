using System;

namespace WebApi.Code.Interface.KucunMingxiGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 详情
  /// /// 查询输入模型
  /// /// </summary>
  /// </summary>
  public class KucunMingxiDetailQueryRequest : KucunMingxiDetailPageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
  }
}
