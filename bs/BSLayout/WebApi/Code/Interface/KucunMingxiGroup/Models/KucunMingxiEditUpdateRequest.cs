using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Code.Interface.KucunMingxiGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 编辑
  /// /// 更新输入模型
  /// /// </summary>
  /// </summary>
  public class KucunMingxiEditUpdateRequest : KucunMingxiEditPageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
    /// <summary>
    /// 物品名称
    /// </summary>
    [MaxLength(50)]
    public string? WupinName { get; set; }
    /// <summary>
    /// 剩余数量
    /// </summary>
    [Range(0, 1000000000)]
    public int? Count { get; set; }
    /// <summary>
    /// 单位
    /// </summary>
    [MaxLength(10)]
    public string? Danwei { get; set; }
    /// <summary>
    /// 分类
    /// </summary>
    public string? Fenlei { get; set; }
  }
}
