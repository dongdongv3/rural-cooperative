using System;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;

namespace WebApi.Code.Interface.KucunMingxiGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 详情
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  [ValidateNever]
  public class KucunMingxiDetailResponse_ValidateNever : KucunMingxiDetailResponse
  {
  }
}
