using System;
using WebApi.Code.Interface.KucunMingxiGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Code.Interface.KucunMingxiGroup
{
  public interface IKucunMingxiGroupInterface
  {
    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult KucunMingxiAdd(KucunMingxiAddRequest request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public KucunMingxiAddDefaultValueResponse KucunMingxiAdd_GetDefaultValue(KucunMingxiAddPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '分类' 字段的数据源
    /// </summary>
    public List<KVDataInfo> KucunMingxiAdd_fenlei_DataSource(KucunMingxiAddRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// </summary>
    public KucunMingxiEditResponse KucunMingxiEdit_GetValue(KucunMingxiEditQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult KucunMingxiEdit(KucunMingxiEditUpdateRequest request, UserInfo loginUser);

    /// <summary>
    /// '分类' 字段的数据源
    /// </summary>
    public List<KVDataInfo> KucunMingxiEdit_fenlei_DataSource(KucunMingxiEditUpdateRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 详情
    /// </summary>
    public KucunMingxiDetailResponse KucunMingxiDetail_GetValue(KucunMingxiDetailQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// '分类' 字段的数据源
    /// </summary>
    public List<KVDataInfo> KucunMingxiDetail_fenlei_DataSource(KucunMingxiDetailResponse_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult KucunMingxiDelete(KucunMingxiDeleteRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<KucunMingxiSearchResponse> KucunMingxiSearch(KucunMingxiSearchQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// </summary>
    public KucunMingxiSearchDefaultValueResponse KucunMingxiSearch_GetDefaultValue(KucunMingxiSearchPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '分类' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> KucunMingxiSearch_fenlei_View_DataSource(KucunMingxiSearchPageparameter request, UserInfo loginUser);

  }
}
