using System;

namespace WebApi.Code.Interface.ZhangHuLogGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 查询
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class ZhangHuLogSearchSheyuanResponse : ZhangHuLogSearchSheyuanPageparameter
  {
    /// <summary>
    /// 账户记录 - 创建时间
    /// </summary>
    public DateTime? ZhangHuLog_CreateTime { get; set; }
    /// <summary>
    /// 账户记录 - 操作金额
    /// </summary>
    public decimal? ZhangHuLog_ActionMoney { get; set; }
    /// <summary>
    /// 账户记录 - 说明
    /// </summary>
    public string? ZhangHuLog_Remark { get; set; }
    /// <summary>
    /// 账户记录 - Id
    /// </summary>
    public Guid ZhangHuLog_Id { get; set; }
    /// <summary>
    /// 账户记录 - 社员-姓名
    /// </summary>
    public Guid ZhangHuLog_RefSheyuanId { get; set; }
  }
}
