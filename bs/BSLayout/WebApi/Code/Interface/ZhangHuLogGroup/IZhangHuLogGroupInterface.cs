using System;
using WebApi.Code.Interface.ZhangHuLogGroup.Models;
using WebApi.Code.Common;
using System.Collections.Generic;
using WebApi.Code.Common.Authentication;

namespace WebApi.Code.Interface.ZhangHuLogGroup
{
  public interface IZhangHuLogGroupInterface
  {
    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<ZhangHuLogSearchSheyuanResponse> ZhangHuLogSearchSheyuan(ZhangHuLogSearchSheyuanQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// </summary>
    public ZhangHuLogSearchSheyuanDefaultValueResponse ZhangHuLogSearchSheyuan_GetDefaultValue(ZhangHuLogSearchSheyuanPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '社员-姓名' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> ZhangHuLogSearchSheyuan_RefSheyuanId_View_DataSource(ZhangHuLogSearchSheyuanPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '社员-姓名' 字段的数据源-where
    /// </summary>
    public List<KVDataInfo> ZhangHuLogSearchSheyuan_RefSheyuanId_Where_DataSource(ZhangHuLogSearchSheyuanQueryRequest_ValidateNever request, UserInfo loginUser);

  }
}
