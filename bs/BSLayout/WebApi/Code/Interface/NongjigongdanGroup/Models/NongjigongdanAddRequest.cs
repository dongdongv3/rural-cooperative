using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Code.Interface.NongjigongdanGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// 入参模型
  /// /// </summary>
  /// </summary>
  public class NongjigongdanAddRequest : NongjigongdanAddPageparameter
  {
    /// <summary>
    /// 面积
    /// </summary>
    [Range(0, 10000000)]
    [Required]
    public int? Miangji { get; set; }
    /// <summary>
    /// 农机类型
    /// </summary>
    [Required]
    public string? Leixing { get; set; }
    /// <summary>
    /// 接单农机
    /// </summary>
    [Required]
    public string? Chuli { get; set; }
    /// <summary>
    /// 总价
    /// </summary>
    public decimal? Zongjia { get; set; }
    /// <summary>
    /// 状态
    /// </summary>
    [Required]
    public string? Zhuangtai { get; set; }
    /// <summary>
    /// 结算状态
    /// </summary>
    [Required]
    public string? Jeisuanzhuangtai { get; set; }
  }
}
