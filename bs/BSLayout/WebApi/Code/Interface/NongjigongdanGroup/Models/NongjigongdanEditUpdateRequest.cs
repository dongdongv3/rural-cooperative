using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Code.Interface.NongjigongdanGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 编辑
  /// /// 更新输入模型
  /// /// </summary>
  /// </summary>
  public class NongjigongdanEditUpdateRequest : NongjigongdanEditPageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
    /// <summary>
    /// 处理时间
    /// </summary>
    public DateTime? BeginTime { get; set; }
    /// <summary>
    /// 面积
    /// </summary>
    [Range(0, 10000000)]
    public int? Miangji { get; set; }
    /// <summary>
    /// 农机类型
    /// </summary>
    public string? Leixing { get; set; }
    /// <summary>
    /// 接单农机
    /// </summary>
    public string? Chuli { get; set; }
    /// <summary>
    /// 总价
    /// </summary>
    public decimal? Zongjia { get; set; }
    /// <summary>
    /// 状态
    /// </summary>
    public string? Zhuangtai { get; set; }
    /// <summary>
    /// 结算状态
    /// </summary>
    public string? Jeisuanzhuangtai { get; set; }
  }
}
