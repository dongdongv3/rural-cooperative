using System;

namespace WebApi.Code.Interface.NongjigongdanGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 查询
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class NongjigongdanSearchResponse : NongjigongdanSearchPageparameter
  {
    /// <summary>
    /// 农机使用工单 - 创建时间
    /// </summary>
    public DateTime? Nongjigongdan_Createtimre { get; set; }
    /// <summary>
    /// 农机使用工单 - 处理时间
    /// </summary>
    public DateTime? Nongjigongdan_BeginTime { get; set; }
    /// <summary>
    /// 农机使用工单 - 面积
    /// </summary>
    public int? Nongjigongdan_Miangji { get; set; }
    /// <summary>
    /// 农机使用工单 - 农机类型
    /// </summary>
    public string? Nongjigongdan_Leixing { get; set; }
    /// <summary>
    /// 农机使用工单 - 接单农机
    /// </summary>
    public string? Nongjigongdan_Chuli { get; set; }
    /// <summary>
    /// 农机使用工单 - 总价
    /// </summary>
    public decimal? Nongjigongdan_Zongjia { get; set; }
    /// <summary>
    /// 农机使用工单 - 状态
    /// </summary>
    public string? Nongjigongdan_Zhuangtai { get; set; }
    /// <summary>
    /// 农机使用工单 - 结算状态
    /// </summary>
    public string? Nongjigongdan_Jeisuanzhuangtai { get; set; }
    /// <summary>
    /// 农机使用工单 - Id
    /// </summary>
    public Guid Nongjigongdan_Id { get; set; }
  }
}
