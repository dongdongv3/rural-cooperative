using System;
using WebApi.Code.Interface.NongjigongdanGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Code.Interface.NongjigongdanGroup
{
  public interface INongjigongdanGroupInterface
  {
    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult NongjigongdanAdd(NongjigongdanAddRequest request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public NongjigongdanAddDefaultValueResponse NongjigongdanAdd_GetDefaultValue(NongjigongdanAddPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '农机类型' 字段的数据源
    /// </summary>
    public List<KVDataInfo> NongjigongdanAdd_leixing_DataSource(NongjigongdanAddRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '接单农机' 字段的数据源
    /// </summary>
    public List<KVDataInfo> NongjigongdanAdd_chuli_DataSource(NongjigongdanAddRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '状态' 字段的数据源
    /// </summary>
    public List<KVDataInfo> NongjigongdanAdd_zhuangtai_DataSource();

    /// <summary>
    /// '结算状态' 字段的数据源
    /// </summary>
    public List<KVDataInfo> NongjigongdanAdd_jeisuanzhuangtai_DataSource();

    /// <summary>
    /// 编辑
    /// </summary>
    public NongjigongdanEditResponse NongjigongdanEdit_GetValue(NongjigongdanEditQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult NongjigongdanEdit(NongjigongdanEditUpdateRequest request, UserInfo loginUser);

    /// <summary>
    /// '农机类型' 字段的数据源
    /// </summary>
    public List<KVDataInfo> NongjigongdanEdit_leixing_DataSource(NongjigongdanEditUpdateRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '接单农机' 字段的数据源
    /// </summary>
    public List<KVDataInfo> NongjigongdanEdit_chuli_DataSource(NongjigongdanEditUpdateRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '状态' 字段的数据源
    /// </summary>
    public List<KVDataInfo> NongjigongdanEdit_zhuangtai_DataSource();

    /// <summary>
    /// '结算状态' 字段的数据源
    /// </summary>
    public List<KVDataInfo> NongjigongdanEdit_jeisuanzhuangtai_DataSource();

    /// <summary>
    /// 详情
    /// </summary>
    public NongjigongdanDetailResponse NongjigongdanDetail_GetValue(NongjigongdanDetailQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// '农机类型' 字段的数据源
    /// </summary>
    public List<KVDataInfo> NongjigongdanDetail_leixing_DataSource(NongjigongdanDetailResponse_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '接单农机' 字段的数据源
    /// </summary>
    public List<KVDataInfo> NongjigongdanDetail_chuli_DataSource(NongjigongdanDetailResponse_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '状态' 字段的数据源
    /// </summary>
    public List<KVDataInfo> NongjigongdanDetail_zhuangtai_DataSource();

    /// <summary>
    /// '结算状态' 字段的数据源
    /// </summary>
    public List<KVDataInfo> NongjigongdanDetail_jeisuanzhuangtai_DataSource();

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult NongjigongdanDelete(NongjigongdanDeleteRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<NongjigongdanSearchResponse> NongjigongdanSearch(NongjigongdanSearchQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// </summary>
    public NongjigongdanSearchDefaultValueResponse NongjigongdanSearch_GetDefaultValue(NongjigongdanSearchPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '农机类型' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> NongjigongdanSearch_leixing_View_DataSource(NongjigongdanSearchPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '接单农机' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> NongjigongdanSearch_chuli_View_DataSource(NongjigongdanSearchPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '状态' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> NongjigongdanSearch_zhuangtai_View_DataSource();

    /// <summary>
    /// '结算状态' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> NongjigongdanSearch_jeisuanzhuangtai_View_DataSource();

  }
}
