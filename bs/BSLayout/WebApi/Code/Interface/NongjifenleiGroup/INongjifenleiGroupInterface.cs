using System;
using WebApi.Code.Interface.NongjifenleiGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;

namespace WebApi.Code.Interface.NongjifenleiGroup
{
  public interface INongjifenleiGroupInterface
  {
    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult NongjifenleiAdd(NongjifenleiAddRequest request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public NongjifenleiAddDefaultValueResponse NongjifenleiAdd_GetDefaultValue(NongjifenleiAddPageparameter request, UserInfo loginUser);

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult NongjifenleiDelete(NongjifenleiDeleteRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<NongjifenleiSearchResponse> NongjifenleiSearch(NongjifenleiSearchQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// </summary>
    public NongjifenleiSearchDefaultValueResponse NongjifenleiSearch_GetDefaultValue(NongjifenleiSearchPageparameter request, UserInfo loginUser);

  }
}
