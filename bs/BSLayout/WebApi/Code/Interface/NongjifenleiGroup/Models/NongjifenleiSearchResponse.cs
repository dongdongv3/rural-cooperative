using System;

namespace WebApi.Code.Interface.NongjifenleiGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 查询
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class NongjifenleiSearchResponse : NongjifenleiSearchPageparameter
  {
    /// <summary>
    /// 农机分类 - 类型名
    /// </summary>
    public string? Nongjifenlei_Name { get; set; }
    /// <summary>
    /// 农机分类 - Id
    /// </summary>
    public Guid Nongjifenlei_Id { get; set; }
  }
}
