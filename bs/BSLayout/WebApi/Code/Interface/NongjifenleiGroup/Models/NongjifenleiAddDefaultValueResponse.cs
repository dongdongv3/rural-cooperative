using System;

namespace WebApi.Code.Interface.NongjifenleiGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// DefaultValue 输出模型
  /// /// </summary>
  /// </summary>
  public class NongjifenleiAddDefaultValueResponse : NongjifenleiAddPageparameter
  {
    /// <summary>
    /// 类型名
    /// </summary>
    public string? Name { get; set; }
  }
}
