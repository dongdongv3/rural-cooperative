using System;

namespace WebApi.Code.Interface.NongjifenleiGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 删除
  /// /// 入参模型
  /// /// </summary>
  /// </summary>
  public class NongjifenleiDeleteRequest : NongjifenleiDeletePageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
  }
}
