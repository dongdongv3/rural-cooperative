using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Code.Interface.NongjifenleiGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// 入参模型
  /// /// </summary>
  /// </summary>
  public class NongjifenleiAddRequest : NongjifenleiAddPageparameter
  {
    /// <summary>
    /// 类型名
    /// </summary>
    [MaxLength(50)]
    public string? Name { get; set; }
  }
}
