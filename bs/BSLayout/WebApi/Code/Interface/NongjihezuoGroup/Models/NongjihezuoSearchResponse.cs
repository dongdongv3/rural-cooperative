using System;

namespace WebApi.Code.Interface.NongjihezuoGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 查询
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class NongjihezuoSearchResponse : NongjihezuoSearchPageparameter
  {
    /// <summary>
    /// 农机合作 - 合作人名称
    /// </summary>
    public string? Nongjihezuo_Username { get; set; }
    /// <summary>
    /// 农机合作 - 合作人电话
    /// </summary>
    public string? Nongjihezuo_PhoneNumber { get; set; }
    /// <summary>
    /// 农机合作 - 农机类型
    /// </summary>
    public string? Nongjihezuo_Leixing { get; set; }
    /// <summary>
    /// 农机合作 - 农机型号
    /// </summary>
    public string? Nongjihezuo_Xinghao { get; set; }
    /// <summary>
    /// 农机合作 - 签约价(每亩)
    /// </summary>
    public decimal? Nongjihezuo_Qianyuejia { get; set; }
    /// <summary>
    /// 农机合作 - Id
    /// </summary>
    public Guid Nongjihezuo_Id { get; set; }
  }
}
