using System;

namespace WebApi.Code.Interface.NongjihezuoGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 编辑
  /// /// 查询输入模型
  /// /// </summary>
  /// </summary>
  public class NongjihezuoEditQueryRequest : NongjihezuoEditPageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
  }
}
