using System;

namespace WebApi.Code.Interface.NongjihezuoGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 编辑
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class NongjihezuoEditResponse : NongjihezuoEditPageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
    /// <summary>
    /// 合作人名称
    /// </summary>
    public string? Username { get; set; }
    /// <summary>
    /// 合作人电话
    /// </summary>
    public string? PhoneNumber { get; set; }
    /// <summary>
    /// 农机类型
    /// </summary>
    public string? Leixing { get; set; }
    /// <summary>
    /// 农机型号
    /// </summary>
    public string? Xinghao { get; set; }
    /// <summary>
    /// 签约价(每亩)
    /// </summary>
    public decimal? Qianyuejia { get; set; }
  }
}
