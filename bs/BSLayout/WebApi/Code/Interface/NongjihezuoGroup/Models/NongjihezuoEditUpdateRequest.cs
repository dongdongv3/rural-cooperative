using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Code.Interface.NongjihezuoGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 编辑
  /// /// 更新输入模型
  /// /// </summary>
  /// </summary>
  public class NongjihezuoEditUpdateRequest : NongjihezuoEditPageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
    /// <summary>
    /// 合作人名称
    /// </summary>
    [MaxLength(50)]
    public string? Username { get; set; }
    /// <summary>
    /// 合作人电话
    /// </summary>
    [MaxLength(50)]
    public string? PhoneNumber { get; set; }
    /// <summary>
    /// 农机类型
    /// </summary>
    public string? Leixing { get; set; }
    /// <summary>
    /// 农机型号
    /// </summary>
    [MaxLength(50)]
    public string? Xinghao { get; set; }
    /// <summary>
    /// 签约价(每亩)
    /// </summary>
    public decimal? Qianyuejia { get; set; }
  }
}
