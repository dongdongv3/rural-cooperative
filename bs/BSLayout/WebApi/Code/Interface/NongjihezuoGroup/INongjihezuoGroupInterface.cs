using System;
using WebApi.Code.Interface.NongjihezuoGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Code.Interface.NongjihezuoGroup
{
  public interface INongjihezuoGroupInterface
  {
    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult NongjihezuoAdd(NongjihezuoAddRequest request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public NongjihezuoAddDefaultValueResponse NongjihezuoAdd_GetDefaultValue(NongjihezuoAddPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '农机类型' 字段的数据源
    /// </summary>
    public List<KVDataInfo> NongjihezuoAdd_leixing_DataSource(NongjihezuoAddRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// </summary>
    public NongjihezuoEditResponse NongjihezuoEdit_GetValue(NongjihezuoEditQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult NongjihezuoEdit(NongjihezuoEditUpdateRequest request, UserInfo loginUser);

    /// <summary>
    /// '农机类型' 字段的数据源
    /// </summary>
    public List<KVDataInfo> NongjihezuoEdit_leixing_DataSource(NongjihezuoEditUpdateRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 详情
    /// </summary>
    public NongjihezuoDetailResponse NongjihezuoDetail_GetValue(NongjihezuoDetailQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// '农机类型' 字段的数据源
    /// </summary>
    public List<KVDataInfo> NongjihezuoDetail_leixing_DataSource(NongjihezuoDetailResponse_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult NongjihezuoDelete(NongjihezuoDeleteRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<NongjihezuoSearchResponse> NongjihezuoSearch(NongjihezuoSearchQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// </summary>
    public NongjihezuoSearchDefaultValueResponse NongjihezuoSearch_GetDefaultValue(NongjihezuoSearchPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '农机类型' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> NongjihezuoSearch_leixing_View_DataSource(NongjihezuoSearchPageparameter request, UserInfo loginUser);

  }
}
