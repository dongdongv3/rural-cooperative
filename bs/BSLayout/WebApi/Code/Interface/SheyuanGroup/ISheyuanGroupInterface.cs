using System;
using WebApi.Code.Interface.SheyuanGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Code.Interface.SheyuanGroup
{
  public interface ISheyuanGroupInterface
  {
    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult SheyuanAdd(SheyuanAddRequest request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public SheyuanAddDefaultValueResponse SheyuanAdd_GetDefaultValue(SheyuanAddPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '片区' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SheyuanAdd_Panqu_DataSource(SheyuanAddRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '状态' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SheyuanAdd_Status_DataSource();

    /// <summary>
    /// 编辑
    /// </summary>
    public SheyuanEditResponse SheyuanEdit_GetValue(SheyuanEditQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult SheyuanEdit(SheyuanEditUpdateRequest request, UserInfo loginUser);

    /// <summary>
    /// '片区' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SheyuanEdit_Panqu_DataSource(SheyuanEditUpdateRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '状态' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SheyuanEdit_Status_DataSource();

    /// <summary>
    /// 详情
    /// </summary>
    public SheyuanDetailResponse SheyuanDetail_GetValue(SheyuanDetailQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// '片区' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SheyuanDetail_Panqu_DataSource(SheyuanDetailResponse_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '状态' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SheyuanDetail_Status_DataSource();

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult SheyuanDelete(SheyuanDeleteRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<SheyuanSearchResponse> SheyuanSearch(SheyuanSearchQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// </summary>
    public SheyuanSearchDefaultValueResponse SheyuanSearch_GetDefaultValue(SheyuanSearchPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '片区' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> SheyuanSearch_Panqu_View_DataSource(SheyuanSearchPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '状态' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> SheyuanSearch_Status_View_DataSource();

  }
}
