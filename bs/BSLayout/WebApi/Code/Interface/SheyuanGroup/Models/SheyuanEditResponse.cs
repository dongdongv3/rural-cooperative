using System;
using WebApi.Code.Common.ElModels;

namespace WebApi.Code.Interface.SheyuanGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 编辑
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class SheyuanEditResponse : SheyuanEditPageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
    /// <summary>
    /// 姓名
    /// </summary>
    public string? UserName { get; set; }
    /// <summary>
    /// 身份证
    /// </summary>
    public string? UserCode { get; set; }
    /// <summary>
    /// 联系电话
    /// </summary>
    public string? PhoneNumber { get; set; }
    /// <summary>
    /// 加入日期
    /// </summary>
    public dateMonth_Element JoinTime { get; set; }
    /// <summary>
    /// 片区
    /// </summary>
    public string? Panqu { get; set; }
    /// <summary>
    /// 面积
    /// </summary>
    public double? Miangji { get; set; }
    /// <summary>
    /// 状态
    /// </summary>
    public string? Status { get; set; }
    /// <summary>
    /// 账户余额
    /// </summary>
    public decimal? Yue { get; set; }
  }
}
