using System;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;

namespace WebApi.Code.Interface.SheyuanGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// 入参模型
  /// /// </summary>
  /// </summary>
  [ValidateNever]
  public class SheyuanAddRequest_ValidateNever : SheyuanAddRequest
  {
  }
}
