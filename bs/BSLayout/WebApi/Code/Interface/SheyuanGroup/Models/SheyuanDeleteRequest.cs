using System;

namespace WebApi.Code.Interface.SheyuanGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 删除
  /// /// 入参模型
  /// /// </summary>
  /// </summary>
  public class SheyuanDeleteRequest : SheyuanDeletePageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
  }
}
