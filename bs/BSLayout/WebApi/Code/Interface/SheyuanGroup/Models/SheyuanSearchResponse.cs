using System;
using WebApi.Code.Common.ElModels;

namespace WebApi.Code.Interface.SheyuanGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 查询
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class SheyuanSearchResponse : SheyuanSearchPageparameter
  {
    /// <summary>
    /// 社员 - 姓名
    /// </summary>
    public string? Sheyuan_UserName { get; set; }
    /// <summary>
    /// 社员 - 身份证
    /// </summary>
    public string? Sheyuan_UserCode { get; set; }
    /// <summary>
    /// 社员 - 联系电话
    /// </summary>
    public string? Sheyuan_PhoneNumber { get; set; }
    /// <summary>
    /// 社员 - 加入日期
    /// </summary>
    public dateMonth_Element Sheyuan_JoinTime { get; set; }
    /// <summary>
    /// 社员 - 片区
    /// </summary>
    public string? Sheyuan_Panqu { get; set; }
    /// <summary>
    /// 社员 - 面积
    /// </summary>
    public double? Sheyuan_Miangji { get; set; }
    /// <summary>
    /// 社员 - 状态
    /// </summary>
    public string? Sheyuan_Status { get; set; }
    /// <summary>
    /// 社员 - 账户余额
    /// </summary>
    public decimal? Sheyuan_Yue { get; set; }
    /// <summary>
    /// 社员 - Id
    /// </summary>
    public Guid Sheyuan_Id { get; set; }
  }
}
