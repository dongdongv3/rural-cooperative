using System;
using System.ComponentModel.DataAnnotations;
using WebApi.Code.Common.ElModels;

namespace WebApi.Code.Interface.SheyuanGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// 入参模型
  /// /// </summary>
  /// </summary>
  public class SheyuanAddRequest : SheyuanAddPageparameter
  {
    /// <summary>
    /// 姓名
    /// </summary>
    [MaxLength(10)]
    public string? UserName { get; set; }
    /// <summary>
    /// 身份证
    /// </summary>
    [MaxLength(50)]
    public string? UserCode { get; set; }
    /// <summary>
    /// 联系电话
    /// </summary>
    [MaxLength(50)]
    public string? PhoneNumber { get; set; }
    /// <summary>
    /// 加入日期
    /// </summary>
    public dateMonth_Element JoinTime { get; set; }
    /// <summary>
    /// 片区
    /// </summary>
    public string? Panqu { get; set; }
    /// <summary>
    /// 面积
    /// </summary>
    [Range(0, 10000000)]
    public double? Miangji { get; set; }
    /// <summary>
    /// 状态
    /// </summary>
    public string? Status { get; set; }
    /// <summary>
    /// 账户余额
    /// </summary>
    public decimal? Yue { get; set; }
  }
}
