using System;

namespace WebApi.Code.Interface.KucunTypeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 查询
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class KucunTypeSearchResponse : KucunTypeSearchPageparameter
  {
    /// <summary>
    /// 库存分类 - 名称
    /// </summary>
    public string? KucunType_Name { get; set; }
    /// <summary>
    /// 库存分类 - Id
    /// </summary>
    public Guid KucunType_Id { get; set; }
  }
}
