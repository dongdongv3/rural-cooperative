using System;
using WebApi.Code.Interface.KucunTypeGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;

namespace WebApi.Code.Interface.KucunTypeGroup
{
  public interface IKucunTypeGroupInterface
  {
    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult KucunTypeAdd(KucunTypeAddRequest request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public KucunTypeAddDefaultValueResponse KucunTypeAdd_GetDefaultValue(KucunTypeAddPageparameter request, UserInfo loginUser);

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult KucunTypeDelete(KucunTypeDeleteRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<KucunTypeSearchResponse> KucunTypeSearch(KucunTypeSearchQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// </summary>
    public KucunTypeSearchDefaultValueResponse KucunTypeSearch_GetDefaultValue(KucunTypeSearchPageparameter request, UserInfo loginUser);

  }
}
