using System;
using WebApi.Code.Interface.SysUserGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Code.Interface.SysUserGroup
{
  public interface ISysUserGroupInterface
  {
    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult SysUser_AccountAndPasswordAuth1AddSysUser(SysUser_AccountAndPasswordAuth1AddSysUserRequest request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public SysUser_AccountAndPasswordAuth1AddSysUserDefaultValueResponse SysUser_AccountAndPasswordAuth1AddSysUser_GetDefaultValue(SysUser_AccountAndPasswordAuth1AddSysUserPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_AccountAndPasswordAuth1AddSysUser_RefSysUserId_DataSource(SysUser_AccountAndPasswordAuth1AddSysUserRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// </summary>
    public SysUser_AccountAndPasswordAuth1EditSysUserResponse SysUser_AccountAndPasswordAuth1EditSysUser_GetValue(SysUser_AccountAndPasswordAuth1EditSysUserQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult SysUser_AccountAndPasswordAuth1EditSysUser(SysUser_AccountAndPasswordAuth1EditSysUserUpdateRequest request, UserInfo loginUser);

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_AccountAndPasswordAuth1EditSysUser_RefSysUserId_DataSource(SysUser_AccountAndPasswordAuth1EditSysUserUpdateRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult SysUser_AccountAndPasswordAuth1Delete(SysUser_AccountAndPasswordAuth1DeleteRequest request, UserInfo loginUser);

    /// <summary>
    /// 详情
    /// </summary>
    public SysUser_AccountAndPasswordAuth1DetailResponse SysUser_AccountAndPasswordAuth1Detail_GetValue(SysUser_AccountAndPasswordAuth1DetailQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_AccountAndPasswordAuth1Detail_RefSysUserId_DataSource(SysUser_AccountAndPasswordAuth1DetailResponse_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<SysUser_AccountAndPasswordAuth1SearchSysUserResponse> SysUser_AccountAndPasswordAuth1SearchSysUser(SysUser_AccountAndPasswordAuth1SearchSysUserQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// </summary>
    public SysUser_AccountAndPasswordAuth1SearchSysUserDefaultValueResponse SysUser_AccountAndPasswordAuth1SearchSysUser_GetDefaultValue(SysUser_AccountAndPasswordAuth1SearchSysUserPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> SysUser_AccountAndPasswordAuth1SearchSysUser_RefSysUserId_View_DataSource(SysUser_AccountAndPasswordAuth1SearchSysUserPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源-where
    /// </summary>
    public List<KVDataInfo> SysUser_AccountAndPasswordAuth1SearchSysUser_RefSysUserId_Where_DataSource(SysUser_AccountAndPasswordAuth1SearchSysUserQueryRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult SysUser_UserRoleMapAddSysUser(SysUser_UserRoleMapAddSysUserRequest request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public SysUser_UserRoleMapAddSysUserDefaultValueResponse SysUser_UserRoleMapAddSysUser_GetDefaultValue(SysUser_UserRoleMapAddSysUserPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_UserRoleMapAddSysUser_RefSysUserId_DataSource(SysUser_UserRoleMapAddSysUserRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_UserRoleMapAddSysUser_RefSysUser_RoleId_DataSource(SysUser_UserRoleMapAddSysUserRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult SysUser_UserRoleMapAddSysUser_Role(SysUser_UserRoleMapAddSysUser_RoleRequest request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public SysUser_UserRoleMapAddSysUser_RoleDefaultValueResponse SysUser_UserRoleMapAddSysUser_Role_GetDefaultValue(SysUser_UserRoleMapAddSysUser_RolePageparameter request, UserInfo loginUser);

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_UserRoleMapAddSysUser_Role_RefSysUserId_DataSource(SysUser_UserRoleMapAddSysUser_RoleRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_UserRoleMapAddSysUser_Role_RefSysUser_RoleId_DataSource(SysUser_UserRoleMapAddSysUser_RoleRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// </summary>
    public SysUser_UserRoleMapEditSysUserResponse SysUser_UserRoleMapEditSysUser_GetValue(SysUser_UserRoleMapEditSysUserQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult SysUser_UserRoleMapEditSysUser(SysUser_UserRoleMapEditSysUserUpdateRequest request, UserInfo loginUser);

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_UserRoleMapEditSysUser_RefSysUserId_DataSource(SysUser_UserRoleMapEditSysUserUpdateRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_UserRoleMapEditSysUser_RefSysUser_RoleId_DataSource(SysUser_UserRoleMapEditSysUserUpdateRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// </summary>
    public SysUser_UserRoleMapEditSysUser_RoleResponse SysUser_UserRoleMapEditSysUser_Role_GetValue(SysUser_UserRoleMapEditSysUser_RoleQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult SysUser_UserRoleMapEditSysUser_Role(SysUser_UserRoleMapEditSysUser_RoleUpdateRequest request, UserInfo loginUser);

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_UserRoleMapEditSysUser_Role_RefSysUserId_DataSource(SysUser_UserRoleMapEditSysUser_RoleUpdateRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_UserRoleMapEditSysUser_Role_RefSysUser_RoleId_DataSource(SysUser_UserRoleMapEditSysUser_RoleUpdateRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult SysUser_UserRoleMapDelete(SysUser_UserRoleMapDeleteRequest request, UserInfo loginUser);

    /// <summary>
    /// 详情
    /// </summary>
    public SysUser_UserRoleMapDetailResponse SysUser_UserRoleMapDetail_GetValue(SysUser_UserRoleMapDetailQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_UserRoleMapDetail_RefSysUserId_DataSource(SysUser_UserRoleMapDetailResponse_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_UserRoleMapDetail_RefSysUser_RoleId_DataSource(SysUser_UserRoleMapDetailResponse_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<SysUser_UserRoleMapSearchSysUserResponse> SysUser_UserRoleMapSearchSysUser(SysUser_UserRoleMapSearchSysUserQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// </summary>
    public SysUser_UserRoleMapSearchSysUserDefaultValueResponse SysUser_UserRoleMapSearchSysUser_GetDefaultValue(SysUser_UserRoleMapSearchSysUserPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> SysUser_UserRoleMapSearchSysUser_RefSysUserId_View_DataSource(SysUser_UserRoleMapSearchSysUserPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '角色-角色名称' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> SysUser_UserRoleMapSearchSysUser_RefSysUser_RoleId_View_DataSource(SysUser_UserRoleMapSearchSysUserPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源-where
    /// </summary>
    public List<KVDataInfo> SysUser_UserRoleMapSearchSysUser_RefSysUserId_Where_DataSource(SysUser_UserRoleMapSearchSysUserQueryRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<SysUser_UserRoleMapSearchSysUser_RoleResponse> SysUser_UserRoleMapSearchSysUser_Role(SysUser_UserRoleMapSearchSysUser_RoleQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// </summary>
    public SysUser_UserRoleMapSearchSysUser_RoleDefaultValueResponse SysUser_UserRoleMapSearchSysUser_Role_GetDefaultValue(SysUser_UserRoleMapSearchSysUser_RolePageparameter request, UserInfo loginUser);

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> SysUser_UserRoleMapSearchSysUser_Role_RefSysUserId_View_DataSource(SysUser_UserRoleMapSearchSysUser_RolePageparameter request, UserInfo loginUser);

    /// <summary>
    /// '角色-角色名称' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> SysUser_UserRoleMapSearchSysUser_Role_RefSysUser_RoleId_View_DataSource(SysUser_UserRoleMapSearchSysUser_RolePageparameter request, UserInfo loginUser);

    /// <summary>
    /// '角色-角色名称' 字段的数据源-where
    /// </summary>
    public List<KVDataInfo> SysUser_UserRoleMapSearchSysUser_Role_RefSysUser_RoleId_Where_DataSource(SysUser_UserRoleMapSearchSysUser_RoleQueryRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult SysUser_RoleAuthMapAddSysUser_Role(SysUser_RoleAuthMapAddSysUser_RoleRequest request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public SysUser_RoleAuthMapAddSysUser_RoleDefaultValueResponse SysUser_RoleAuthMapAddSysUser_Role_GetDefaultValue(SysUser_RoleAuthMapAddSysUser_RolePageparameter request, UserInfo loginUser);

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_RoleAuthMapAddSysUser_Role_RefSysUser_RoleId_DataSource(SysUser_RoleAuthMapAddSysUser_RoleRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '权限-权限名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_RoleAuthMapAddSysUser_Role_RefSysUser_AuthId_DataSource(SysUser_RoleAuthMapAddSysUser_RoleRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult SysUser_RoleAuthMapAddSysUser_Auth(SysUser_RoleAuthMapAddSysUser_AuthRequest request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public SysUser_RoleAuthMapAddSysUser_AuthDefaultValueResponse SysUser_RoleAuthMapAddSysUser_Auth_GetDefaultValue(SysUser_RoleAuthMapAddSysUser_AuthPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_RoleAuthMapAddSysUser_Auth_RefSysUser_RoleId_DataSource(SysUser_RoleAuthMapAddSysUser_AuthRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '权限-权限名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_RoleAuthMapAddSysUser_Auth_RefSysUser_AuthId_DataSource(SysUser_RoleAuthMapAddSysUser_AuthRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// </summary>
    public SysUser_RoleAuthMapEditSysUser_RoleResponse SysUser_RoleAuthMapEditSysUser_Role_GetValue(SysUser_RoleAuthMapEditSysUser_RoleQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult SysUser_RoleAuthMapEditSysUser_Role(SysUser_RoleAuthMapEditSysUser_RoleUpdateRequest request, UserInfo loginUser);

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_RoleAuthMapEditSysUser_Role_RefSysUser_RoleId_DataSource(SysUser_RoleAuthMapEditSysUser_RoleUpdateRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '权限-权限名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_RoleAuthMapEditSysUser_Role_RefSysUser_AuthId_DataSource(SysUser_RoleAuthMapEditSysUser_RoleUpdateRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// </summary>
    public SysUser_RoleAuthMapEditSysUser_AuthResponse SysUser_RoleAuthMapEditSysUser_Auth_GetValue(SysUser_RoleAuthMapEditSysUser_AuthQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult SysUser_RoleAuthMapEditSysUser_Auth(SysUser_RoleAuthMapEditSysUser_AuthUpdateRequest request, UserInfo loginUser);

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_RoleAuthMapEditSysUser_Auth_RefSysUser_RoleId_DataSource(SysUser_RoleAuthMapEditSysUser_AuthUpdateRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '权限-权限名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_RoleAuthMapEditSysUser_Auth_RefSysUser_AuthId_DataSource(SysUser_RoleAuthMapEditSysUser_AuthUpdateRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult SysUser_RoleAuthMapDelete(SysUser_RoleAuthMapDeleteRequest request, UserInfo loginUser);

    /// <summary>
    /// 详情
    /// </summary>
    public SysUser_RoleAuthMapDetailResponse SysUser_RoleAuthMapDetail_GetValue(SysUser_RoleAuthMapDetailQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_RoleAuthMapDetail_RefSysUser_RoleId_DataSource(SysUser_RoleAuthMapDetailResponse_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// '权限-权限名称' 字段的数据源
    /// </summary>
    public List<KVDataInfo> SysUser_RoleAuthMapDetail_RefSysUser_AuthId_DataSource(SysUser_RoleAuthMapDetailResponse_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<SysUser_RoleAuthMapSearchSysUser_RoleResponse> SysUser_RoleAuthMapSearchSysUser_Role(SysUser_RoleAuthMapSearchSysUser_RoleQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// </summary>
    public SysUser_RoleAuthMapSearchSysUser_RoleDefaultValueResponse SysUser_RoleAuthMapSearchSysUser_Role_GetDefaultValue(SysUser_RoleAuthMapSearchSysUser_RolePageparameter request, UserInfo loginUser);

    /// <summary>
    /// '角色-角色名称' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> SysUser_RoleAuthMapSearchSysUser_Role_RefSysUser_RoleId_View_DataSource(SysUser_RoleAuthMapSearchSysUser_RolePageparameter request, UserInfo loginUser);

    /// <summary>
    /// '权限-权限名称' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> SysUser_RoleAuthMapSearchSysUser_Role_RefSysUser_AuthId_View_DataSource(SysUser_RoleAuthMapSearchSysUser_RolePageparameter request, UserInfo loginUser);

    /// <summary>
    /// '角色-角色名称' 字段的数据源-where
    /// </summary>
    public List<KVDataInfo> SysUser_RoleAuthMapSearchSysUser_Role_RefSysUser_RoleId_Where_DataSource(SysUser_RoleAuthMapSearchSysUser_RoleQueryRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<SysUser_RoleAuthMapSearchSysUser_AuthResponse> SysUser_RoleAuthMapSearchSysUser_Auth(SysUser_RoleAuthMapSearchSysUser_AuthQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// </summary>
    public SysUser_RoleAuthMapSearchSysUser_AuthDefaultValueResponse SysUser_RoleAuthMapSearchSysUser_Auth_GetDefaultValue(SysUser_RoleAuthMapSearchSysUser_AuthPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '角色-角色名称' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> SysUser_RoleAuthMapSearchSysUser_Auth_RefSysUser_RoleId_View_DataSource(SysUser_RoleAuthMapSearchSysUser_AuthPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '权限-权限名称' 字段的数据源-view
    /// </summary>
    public List<KVDataInfo> SysUser_RoleAuthMapSearchSysUser_Auth_RefSysUser_AuthId_View_DataSource(SysUser_RoleAuthMapSearchSysUser_AuthPageparameter request, UserInfo loginUser);

    /// <summary>
    /// '权限-权限名称' 字段的数据源-where
    /// </summary>
    public List<KVDataInfo> SysUser_RoleAuthMapSearchSysUser_Auth_RefSysUser_AuthId_Where_DataSource(SysUser_RoleAuthMapSearchSysUser_AuthQueryRequest_ValidateNever request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult SysUser_AuthAdd(SysUser_AuthAddRequest request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public SysUser_AuthAddDefaultValueResponse SysUser_AuthAdd_GetDefaultValue(SysUser_AuthAddPageparameter request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// </summary>
    public SysUser_AuthEditResponse SysUser_AuthEdit_GetValue(SysUser_AuthEditQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult SysUser_AuthEdit(SysUser_AuthEditUpdateRequest request, UserInfo loginUser);

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult SysUser_AuthDelete(SysUser_AuthDeleteRequest request, UserInfo loginUser);

    /// <summary>
    /// 详情
    /// </summary>
    public SysUser_AuthDetailResponse SysUser_AuthDetail_GetValue(SysUser_AuthDetailQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<SysUser_AuthSearchResponse> SysUser_AuthSearch(SysUser_AuthSearchQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// </summary>
    public SysUser_AuthSearchDefaultValueResponse SysUser_AuthSearch_GetDefaultValue(SysUser_AuthSearchPageparameter request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult SysUser_RoleAdd(SysUser_RoleAddRequest request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public SysUser_RoleAddDefaultValueResponse SysUser_RoleAdd_GetDefaultValue(SysUser_RoleAddPageparameter request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// </summary>
    public SysUser_RoleEditResponse SysUser_RoleEdit_GetValue(SysUser_RoleEditQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult SysUser_RoleEdit(SysUser_RoleEditUpdateRequest request, UserInfo loginUser);

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult SysUser_RoleDelete(SysUser_RoleDeleteRequest request, UserInfo loginUser);

    /// <summary>
    /// 详情
    /// </summary>
    public SysUser_RoleDetailResponse SysUser_RoleDetail_GetValue(SysUser_RoleDetailQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<SysUser_RoleSearchResponse> SysUser_RoleSearch(SysUser_RoleSearchQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// </summary>
    public SysUser_RoleSearchDefaultValueResponse SysUser_RoleSearch_GetDefaultValue(SysUser_RoleSearchPageparameter request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult SysUserAdd(SysUserAddRequest request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public SysUserAddDefaultValueResponse SysUserAdd_GetDefaultValue(SysUserAddPageparameter request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// </summary>
    public SysUserEditResponse SysUserEdit_GetValue(SysUserEditQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    public OptionsResoult SysUserEdit(SysUserEditUpdateRequest request, UserInfo loginUser);

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult SysUserDelete(SysUserDeleteRequest request, UserInfo loginUser);

    /// <summary>
    /// 详情
    /// </summary>
    public SysUserDetailResponse SysUserDetail_GetValue(SysUserDetailQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<SysUserSearchResponse> SysUserSearch(SysUserSearchQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// </summary>
    public SysUserSearchDefaultValueResponse SysUserSearch_GetDefaultValue(SysUserSearchPageparameter request, UserInfo loginUser);

  }
}
