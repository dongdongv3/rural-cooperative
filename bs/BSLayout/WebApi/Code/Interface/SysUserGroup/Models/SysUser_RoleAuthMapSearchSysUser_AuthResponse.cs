using System;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 查询
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class SysUser_RoleAuthMapSearchSysUser_AuthResponse : SysUser_RoleAuthMapSearchSysUser_AuthPageparameter
  {
    /// <summary>
    /// 角色权限映射表 - Id
    /// </summary>
    public Guid SysUser_RoleAuthMap_Id { get; set; }
    /// <summary>
    /// 角色权限映射表 - 角色-角色名称
    /// </summary>
    public Guid SysUser_RoleAuthMap_RefSysUser_RoleId { get; set; }
    /// <summary>
    /// 角色权限映射表 - 权限-权限名称
    /// </summary>
    public Guid SysUser_RoleAuthMap_RefSysUser_AuthId { get; set; }
  }
}
