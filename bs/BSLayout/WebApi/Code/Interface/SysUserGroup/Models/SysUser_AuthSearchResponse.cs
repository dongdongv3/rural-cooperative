using System;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 查询
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class SysUser_AuthSearchResponse : SysUser_AuthSearchPageparameter
  {
    /// <summary>
    /// 权限 - 权限名称
    /// </summary>
    public string? SysUser_Auth_AuthName { get; set; }
    /// <summary>
    /// 权限 - Id
    /// </summary>
    public Guid SysUser_Auth_Id { get; set; }
  }
}
