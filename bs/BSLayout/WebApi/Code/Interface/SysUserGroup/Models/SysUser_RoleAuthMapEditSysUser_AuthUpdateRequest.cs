using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 编辑
  /// /// 更新输入模型
  /// /// </summary>
  /// </summary>
  public class SysUser_RoleAuthMapEditSysUser_AuthUpdateRequest : SysUser_RoleAuthMapEditSysUser_AuthPageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
    /// <summary>
    /// 角色-角色名称
    /// </summary>
    public Guid RefSysUser_RoleId { get; set; }
  }
}
