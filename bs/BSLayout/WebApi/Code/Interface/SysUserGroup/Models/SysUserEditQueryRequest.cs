using System;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 编辑
  /// /// 查询输入模型
  /// /// </summary>
  /// </summary>
  public class SysUserEditQueryRequest : SysUserEditPageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public string Id { get; set; }
  }
}
