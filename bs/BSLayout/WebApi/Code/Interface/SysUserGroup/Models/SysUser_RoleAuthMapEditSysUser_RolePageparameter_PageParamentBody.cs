using System;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// </summary>
  public class SysUser_RoleAuthMapEditSysUser_RolePageparameter_PageParamentBody
  {
    public Guid Id { get; set; }
    public Guid RefSysUser_RoleId { get; set; }
  }
}
