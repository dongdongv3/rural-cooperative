using System;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 查询
  /// /// 查询输入模型
  /// /// </summary>
  /// </summary>
  [ValidateNever]
  public class SysUser_RoleAuthMapSearchSysUser_RoleQueryRequest_ValidateNever : SysUser_RoleAuthMapSearchSysUser_RoleQueryRequest
  {
  }
}
