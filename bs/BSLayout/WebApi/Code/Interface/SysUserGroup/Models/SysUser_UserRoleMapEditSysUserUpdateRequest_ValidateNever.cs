using System;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 编辑
  /// /// 更新输入模型
  /// /// </summary>
  /// </summary>
  [ValidateNever]
  public class SysUser_UserRoleMapEditSysUserUpdateRequest_ValidateNever : SysUser_UserRoleMapEditSysUserUpdateRequest
  {
  }
}
