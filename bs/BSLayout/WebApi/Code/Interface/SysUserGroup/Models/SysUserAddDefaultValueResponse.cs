using System;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// DefaultValue 输出模型
  /// /// </summary>
  /// </summary>
  public class SysUserAddDefaultValueResponse : SysUserAddPageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public string Id { get; set; }
    /// <summary>
    /// 用户名
    /// </summary>
    public string? UserName { get; set; }
  }
}
