using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// 入参模型
  /// /// </summary>
  /// </summary>
  public class SysUser_RoleAuthMapAddSysUser_RoleRequest : SysUser_RoleAuthMapAddSysUser_RolePageparameter
  {
    /// <summary>
    /// 权限-权限名称
    /// </summary>
    public Guid RefSysUser_AuthId { get; set; }
  }
}
