using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 编辑
  /// /// 更新输入模型
  /// /// </summary>
  /// </summary>
  public class SysUser_AuthEditUpdateRequest : SysUser_AuthEditPageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
    /// <summary>
    /// 权限名称
    /// </summary>
    [MaxLength(255)]
    public string? AuthName { get; set; }
  }
}
