using System;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 编辑
  /// /// 查询输入模型
  /// /// </summary>
  /// </summary>
  public class SysUser_RoleAuthMapEditSysUser_AuthQueryRequest : SysUser_RoleAuthMapEditSysUser_AuthPageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
  }
}
