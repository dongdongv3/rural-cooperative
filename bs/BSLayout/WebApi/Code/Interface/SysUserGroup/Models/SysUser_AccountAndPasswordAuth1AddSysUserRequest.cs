using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// 入参模型
  /// /// </summary>
  /// </summary>
  public class SysUser_AccountAndPasswordAuth1AddSysUserRequest : SysUser_AccountAndPasswordAuth1AddSysUserPageparameter
  {
    /// <summary>
    /// 登录账户
    /// </summary>
    [MaxLength(50)]
    public string? LoginID { get; set; }
    /// <summary>
    /// 密码
    /// </summary>
    public string? Pwd { get; set; }
  }
}
