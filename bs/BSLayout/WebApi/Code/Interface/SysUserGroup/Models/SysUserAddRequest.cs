using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// 入参模型
  /// /// </summary>
  /// </summary>
  public class SysUserAddRequest : SysUserAddPageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    [MaxLength(50)]
    public string Id { get; set; }
    /// <summary>
    /// 用户名
    /// </summary>
    [MaxLength(50)]
    public string? UserName { get; set; }
  }
}
