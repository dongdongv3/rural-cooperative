using System;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 详情
  /// /// 查询输入模型
  /// /// </summary>
  /// </summary>
  public class SysUserDetailQueryRequest : SysUserDetailPageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public string Id { get; set; }
  }
}
