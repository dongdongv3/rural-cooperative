using System;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 查询
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class SysUser_UserRoleMapSearchSysUserResponse : SysUser_UserRoleMapSearchSysUserPageparameter
  {
    /// <summary>
    /// 用户角色映射表 - Id
    /// </summary>
    public Guid SysUser_UserRoleMap_Id { get; set; }
    /// <summary>
    /// 用户角色映射表 - 系统用户-用户信息-用户名
    /// </summary>
    public string SysUser_UserRoleMap_RefSysUserId { get; set; }
    /// <summary>
    /// 用户角色映射表 - 角色-角色名称
    /// </summary>
    public Guid SysUser_UserRoleMap_RefSysUser_RoleId { get; set; }
  }
}
