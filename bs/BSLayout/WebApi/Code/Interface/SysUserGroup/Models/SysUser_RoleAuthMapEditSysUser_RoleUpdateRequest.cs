using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 编辑
  /// /// 更新输入模型
  /// /// </summary>
  /// </summary>
  public class SysUser_RoleAuthMapEditSysUser_RoleUpdateRequest : SysUser_RoleAuthMapEditSysUser_RolePageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
    /// <summary>
    /// 权限-权限名称
    /// </summary>
    public Guid RefSysUser_AuthId { get; set; }
  }
}
