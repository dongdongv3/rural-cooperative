using System;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// DefaultValue 输出模型
  /// /// </summary>
  /// </summary>
  public class SysUser_UserRoleMapAddSysUser_RoleDefaultValueResponse : SysUser_UserRoleMapAddSysUser_RolePageparameter
  {
    /// <summary>
    /// 系统用户-用户信息-用户名
    /// </summary>
    public string RefSysUserId { get; set; }
  }
}
