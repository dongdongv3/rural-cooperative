using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 编辑
  /// /// 更新输入模型
  /// /// </summary>
  /// </summary>
  public class SysUser_UserRoleMapEditSysUser_RoleUpdateRequest : SysUser_UserRoleMapEditSysUser_RolePageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
    /// <summary>
    /// 系统用户-用户信息-用户名
    /// </summary>
    public string RefSysUserId { get; set; }
  }
}
