using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// 入参模型
  /// /// </summary>
  /// </summary>
  public class SysUser_RoleAuthMapAddSysUser_AuthRequest : SysUser_RoleAuthMapAddSysUser_AuthPageparameter
  {
    /// <summary>
    /// 角色-角色名称
    /// </summary>
    public Guid RefSysUser_RoleId { get; set; }
  }
}
