using System;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// </summary>
  public class SysUser_UserRoleMapEditSysUserPageparameter_PageParamentBody
  {
    public Guid Id { get; set; }
    public string RefSysUserId { get; set; }
  }
}
