using System;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// </summary>
  public class SysUser_RoleAuthMapEditSysUser_AuthPageparameter_PageParamentBody
  {
    public Guid Id { get; set; }
    public Guid RefSysUser_AuthId { get; set; }
  }
}
