using System;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// 入参模型
  /// /// </summary>
  /// </summary>
  [ValidateNever]
  public class SysUser_UserRoleMapAddSysUserRequest_ValidateNever : SysUser_UserRoleMapAddSysUserRequest
  {
  }
}
