using System;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 详情
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class SysUser_RoleAuthMapDetailResponse : SysUser_RoleAuthMapDetailPageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
    /// <summary>
    /// 角色-角色名称
    /// </summary>
    public Guid RefSysUser_RoleId { get; set; }
    /// <summary>
    /// 权限-权限名称
    /// </summary>
    public Guid RefSysUser_AuthId { get; set; }
  }
}
