using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 编辑
  /// /// 更新输入模型
  /// /// </summary>
  /// </summary>
  public class SysUser_RoleEditUpdateRequest : SysUser_RoleEditPageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
    /// <summary>
    /// 角色名称
    /// </summary>
    [MaxLength(50)]
    public string? RoleName { get; set; }
  }
}
