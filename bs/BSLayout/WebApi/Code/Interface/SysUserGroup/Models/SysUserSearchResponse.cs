using System;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 查询
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class SysUserSearchResponse : SysUserSearchPageparameter
  {
    /// <summary>
    /// 系统用户-用户信息 - 用户名
    /// </summary>
    public string? SysUser_UserName { get; set; }
    /// <summary>
    /// 系统用户-用户信息 - Id
    /// </summary>
    public string SysUser_Id { get; set; }
  }
}
