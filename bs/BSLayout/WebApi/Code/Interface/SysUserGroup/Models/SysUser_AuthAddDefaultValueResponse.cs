using System;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// DefaultValue 输出模型
  /// /// </summary>
  /// </summary>
  public class SysUser_AuthAddDefaultValueResponse : SysUser_AuthAddPageparameter
  {
    /// <summary>
    /// 权限名称
    /// </summary>
    public string? AuthName { get; set; }
  }
}
