using System;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 查询
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class SysUser_RoleSearchResponse : SysUser_RoleSearchPageparameter
  {
    /// <summary>
    /// 角色 - 角色名称
    /// </summary>
    public string? SysUser_Role_RoleName { get; set; }
    /// <summary>
    /// 角色 - Id
    /// </summary>
    public Guid SysUser_Role_Id { get; set; }
  }
}
