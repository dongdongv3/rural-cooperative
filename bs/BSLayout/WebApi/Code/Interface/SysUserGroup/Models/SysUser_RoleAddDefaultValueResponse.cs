using System;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// DefaultValue 输出模型
  /// /// </summary>
  /// </summary>
  public class SysUser_RoleAddDefaultValueResponse : SysUser_RoleAddPageparameter
  {
    /// <summary>
    /// 角色名称
    /// </summary>
    public string? RoleName { get; set; }
  }
}
