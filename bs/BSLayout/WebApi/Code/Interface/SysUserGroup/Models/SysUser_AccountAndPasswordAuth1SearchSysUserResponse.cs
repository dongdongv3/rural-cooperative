using System;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 查询
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class SysUser_AccountAndPasswordAuth1SearchSysUserResponse : SysUser_AccountAndPasswordAuth1SearchSysUserPageparameter
  {
    /// <summary>
    /// 账户密码认证1-认证通道 - 登录账户
    /// </summary>
    public string? SysUser_AccountAndPasswordAuth1_LoginID { get; set; }
    /// <summary>
    /// 账户密码认证1-认证通道 - 密码
    /// </summary>
    public string? SysUser_AccountAndPasswordAuth1_Pwd { get; set; }
    /// <summary>
    /// 账户密码认证1-认证通道 - Id
    /// </summary>
    public Guid SysUser_AccountAndPasswordAuth1_Id { get; set; }
    /// <summary>
    /// 账户密码认证1-认证通道 - 系统用户-用户信息-用户名
    /// </summary>
    public string SysUser_AccountAndPasswordAuth1_RefSysUserId { get; set; }
  }
}
