using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// 入参模型
  /// /// </summary>
  /// </summary>
  public class SysUser_UserRoleMapAddSysUser_RoleRequest : SysUser_UserRoleMapAddSysUser_RolePageparameter
  {
    /// <summary>
    /// 系统用户-用户信息-用户名
    /// </summary>
    public string RefSysUserId { get; set; }
  }
}
