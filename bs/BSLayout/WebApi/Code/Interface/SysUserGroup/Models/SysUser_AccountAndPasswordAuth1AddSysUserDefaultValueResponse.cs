using System;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// DefaultValue 输出模型
  /// /// </summary>
  /// </summary>
  public class SysUser_AccountAndPasswordAuth1AddSysUserDefaultValueResponse : SysUser_AccountAndPasswordAuth1AddSysUserPageparameter
  {
    /// <summary>
    /// 登录账户
    /// </summary>
    public string? LoginID { get; set; }
    /// <summary>
    /// 密码
    /// </summary>
    public string? Pwd { get; set; }
  }
}
