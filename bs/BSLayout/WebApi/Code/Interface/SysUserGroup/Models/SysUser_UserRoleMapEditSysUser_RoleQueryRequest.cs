using System;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 编辑
  /// /// 查询输入模型
  /// /// </summary>
  /// </summary>
  public class SysUser_UserRoleMapEditSysUser_RoleQueryRequest : SysUser_UserRoleMapEditSysUser_RolePageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
  }
}
