using System;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 详情
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class SysUser_AccountAndPasswordAuth1DetailResponse : SysUser_AccountAndPasswordAuth1DetailPageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public Guid Id { get; set; }
    /// <summary>
    /// 登录账户
    /// </summary>
    public string? LoginID { get; set; }
    /// <summary>
    /// 密码
    /// </summary>
    public string? Pwd { get; set; }
    /// <summary>
    /// 系统用户-用户信息-用户名
    /// </summary>
    public string RefSysUserId { get; set; }
  }
}
