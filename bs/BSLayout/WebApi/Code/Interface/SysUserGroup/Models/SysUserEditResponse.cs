using System;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 编辑
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class SysUserEditResponse : SysUserEditPageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public string Id { get; set; }
    /// <summary>
    /// 用户名
    /// </summary>
    public string? UserName { get; set; }
  }
}
