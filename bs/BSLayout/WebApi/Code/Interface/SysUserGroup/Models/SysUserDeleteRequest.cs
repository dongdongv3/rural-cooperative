using System;

namespace WebApi.Code.Interface.SysUserGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 删除
  /// /// 入参模型
  /// /// </summary>
  /// </summary>
  public class SysUserDeleteRequest : SysUserDeletePageparameter
  {
    /// <summary>
    /// Id
    /// </summary>
    public string Id { get; set; }
  }
}
