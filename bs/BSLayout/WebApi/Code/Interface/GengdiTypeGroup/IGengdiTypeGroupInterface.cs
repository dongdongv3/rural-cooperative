using System;
using WebApi.Code.Interface.GengdiTypeGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;

namespace WebApi.Code.Interface.GengdiTypeGroup
{
  public interface IGengdiTypeGroupInterface
  {
    /// <summary>
    /// 添加
    /// </summary>
    public OptionsResoult GengdiTypeAdd(GengdiTypeAddRequest request, UserInfo loginUser);

    /// <summary>
    /// 添加
    /// </summary>
    public GengdiTypeAddDefaultValueResponse GengdiTypeAdd_GetDefaultValue(GengdiTypeAddPageparameter request, UserInfo loginUser);

    /// <summary>
    /// 删除
    /// </summary>
    public OptionsResoult GengdiTypeDelete(GengdiTypeDeleteRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    public PagerInfo<GengdiTypeSearchResponse> GengdiTypeSearch(GengdiTypeSearchQueryRequest request, UserInfo loginUser);

    /// <summary>
    /// 查询
    /// </summary>
    public GengdiTypeSearchDefaultValueResponse GengdiTypeSearch_GetDefaultValue(GengdiTypeSearchPageparameter request, UserInfo loginUser);

  }
}
