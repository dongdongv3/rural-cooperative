using System;

namespace WebApi.Code.Interface.GengdiTypeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// DefaultValue 输出模型
  /// /// </summary>
  /// </summary>
  public class GengdiTypeAddDefaultValueResponse : GengdiTypeAddPageparameter
  {
    /// <summary>
    /// 片区名
    /// </summary>
    public string? PianquName { get; set; }
  }
}
