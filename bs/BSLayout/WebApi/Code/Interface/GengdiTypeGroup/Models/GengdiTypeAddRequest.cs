using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Code.Interface.GengdiTypeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 添加
  /// /// 入参模型
  /// /// </summary>
  /// </summary>
  public class GengdiTypeAddRequest : GengdiTypeAddPageparameter
  {
    /// <summary>
    /// 片区名
    /// </summary>
    [MaxLength(50)]
    public string? PianquName { get; set; }
  }
}
