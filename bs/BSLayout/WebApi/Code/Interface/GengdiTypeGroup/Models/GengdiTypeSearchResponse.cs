using System;

namespace WebApi.Code.Interface.GengdiTypeGroup.Models
{
  /// <summary>
  /// /// <summary>
  /// /// 查询
  /// /// 查询输出模型
  /// /// </summary>
  /// </summary>
  public class GengdiTypeSearchResponse : GengdiTypeSearchPageparameter
  {
    /// <summary>
    /// 耕地片区 - 片区名
    /// </summary>
    public string? GengdiType_PianquName { get; set; }
    /// <summary>
    /// 耕地片区 - Id
    /// </summary>
    public Guid GengdiType_Id { get; set; }
  }
}
