using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System.IO;
using System.Collections.Generic;
using IdentityServer4;
using WebApi.Code.Data;

namespace WebApi
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }
    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      services.AddControllersWithViews().AddNewtonsoftJson();


      var builder = services.AddIdentityServer(options =>
      {
        options.Events.RaiseErrorEvents = true;
        options.Events.RaiseInformationEvents = true;
        options.Events.RaiseFailureEvents = true;
        options.Events.RaiseSuccessEvents = true;

        // see https://identityserver4.readthedocs.io/en/latest/topics/resources.html
        options.EmitStaticAudienceClaim = true;
      })
          .AddInMemoryIdentityResources(Config.IdentityResources)
          .AddInMemoryApiScopes(Config.ApiScopes)
          .AddInMemoryClients(Config.Clients(Configuration));

      // not recommended for production - you need to store your key material somewhere secure
      builder.AddDeveloperSigningCredential();

      services.AddAuthentication();

      services.AddLocalApiAuthentication();

      // In production, the React files will be served from this directory
      services.AddSpaStaticFiles(configuration =>
      {
        configuration.RootPath = "wwwroot";
      });
      services.AddSwaggerGen(c =>
      {
        c.SwaggerDoc("v1", new OpenApiInfo { Title = "WebApi", Version = "v1" });
        var file = Path.Combine(AppContext.BaseDirectory, "WebApi.xml");  // xml??????
        var path = Path.Combine(AppContext.BaseDirectory, file); // xml??????
        c.IncludeXmlComments(path, true); // true : ????????
        c.OrderActionsBy(o => o.RelativePath); // ?action???????????????????????
        c.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
        {
          Type = SecuritySchemeType.OAuth2,
          Flows = new OpenApiOAuthFlows
          {
            Implicit = new OpenApiOAuthFlow
            {
              AuthorizationUrl = new Uri("https://localhost:5011/connect/authorize"),
              Scopes = new Dictionary<string, string>
                      {
                                                        { IdentityServerConstants.LocalApi.ScopeName,IdentityServerConstants.LocalApi.ScopeName}
                      }
            }
          }
        });

        c.AddSecurityRequirement(new OpenApiSecurityRequirement
          {
                                            {
                                                new OpenApiSecurityScheme
                                                {
                                                    Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "oauth2" }
                                                },
                                                new[] { IdentityServerConstants.LocalApi.ScopeName }
                                            }
          });
      });
      services.AddDbContext<HeZuosheDB>(options => options.UseSqlite(Configuration.GetConnectionString("defaultConnection_HeZuosheDB")));
      services.AddScoped<WebApi.Code.Common.Authentication.IUserReadInterface, WebApi.Code.Service.Authentication.UserReadProvider_SysUser>();
      services.AddScoped<WebApi.Code.Common.Authentication.IAuthenticationProviderInterface, WebApi.Code.Service.Authentication.AccountAndPasswordAuth1AuthenticationProvider>();
      services.AddScoped<WebApi.Code.Interface.SysUserGroup.ISysUserGroupInterface, WebApi.Code.Service.SysUserGroup.SysUserGroupService>();
      services.AddScoped<WebApi.Code.Interface.NongjifenleiGroup.INongjifenleiGroupInterface, WebApi.Code.Service.NongjifenleiGroup.NongjifenleiGroupService>();
      services.AddScoped<WebApi.Code.Interface.NongjihezuoGroup.INongjihezuoGroupInterface, WebApi.Code.Service.NongjihezuoGroup.NongjihezuoGroupService>();
      services.AddScoped<WebApi.Code.Interface.NongjigongdanGroup.INongjigongdanGroupInterface, WebApi.Code.Service.NongjigongdanGroup.NongjigongdanGroupService>();
      services.AddScoped<WebApi.Code.Interface.GengdiTypeGroup.IGengdiTypeGroupInterface, WebApi.Code.Service.GengdiTypeGroup.GengdiTypeGroupService>();
      services.AddScoped<WebApi.Code.Interface.KucunTypeGroup.IKucunTypeGroupInterface, WebApi.Code.Service.KucunTypeGroup.KucunTypeGroupService>();
      services.AddScoped<WebApi.Code.Interface.KucunMingxiGroup.IKucunMingxiGroupInterface, WebApi.Code.Service.KucunMingxiGroup.KucunMingxiGroupService>();
      services.AddScoped<WebApi.Code.Interface.ChurukujiluGroup.IChurukujiluGroupInterface, WebApi.Code.Service.ChurukujiluGroup.ChurukujiluGroupService>();
      services.AddScoped<WebApi.Code.Interface.ZhangHuLogGroup.IZhangHuLogGroupInterface, WebApi.Code.Service.ZhangHuLogGroup.ZhangHuLogGroupService>();
      services.AddScoped<WebApi.Code.Interface.SheyuanGroup.ISheyuanGroupInterface, WebApi.Code.Service.SheyuanGroup.SheyuanGroupService>();
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
      if (env.IsDevelopment())
      {
        {
          app.UseDeveloperExceptionPage();
          app.UseSwagger();
          app.UseSwaggerUI(c =>
          {
            c.SwaggerEndpoint("/swagger/v1/swagger.json", "WebApplication1 v1");
            c.OAuthClientId("js3");
            c.OAuthClientSecret("abcd");
            c.OAuthScopeSeparator(" ");

            //c.OAuthAdditionalQueryStringParams(new Dictionary<string, string> { { "foo", "bar" } });
            //accessCode flow 模式  对应ids4中的Implicit模式
            c.OAuthUseBasicAuthenticationWithAccessCodeGrant();
            //authorizatonCode flows 模式
            //c.OAuthUsePkce();
          });
        }
      }
      else
      {
        {
          app.UseExceptionHandler("/Error");
          // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
          app.UseHsts();
        }
      }

      app.UseIdentityServer();

      app.UseHttpsRedirection();
      app.UseStaticFiles();
      app.UseSpaStaticFiles();


      app.UseRouting();
      app.UseAuthentication();
      app.UseAuthorization();

      app.UseEndpoints(endpoints =>
      {
        {
          endpoints.MapControllerRoute(name: "default", pattern: "{controller}/{action}/{id?}");
        }
      });

      app.UseSpa(spa =>
      {
        spa.Options.SourcePath = "wwwroot";
      });
    }
  }
}
