using System;
using WebApi.Code.Interface.GengdiTypeGroup;
using Microsoft.AspNetCore.Mvc;
using WebApi.Code.Interface.GengdiTypeGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;

namespace WebApi.Controllers.GengdiTypeGroup
{
  [Route("api/[controller]/[action]")]
  public class GengdiTypeGroupServiceController : ApiControllerBase
  {
    public readonly IGengdiTypeGroupInterface _GengdiTypeGroupService;

    public GengdiTypeGroupServiceController(IGengdiTypeGroupInterface _GengdiTypeGroupService)
    {
      this._GengdiTypeGroupService = _GengdiTypeGroupService;
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/GengdiTypeGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult GengdiTypeAdd([FromBody] GengdiTypeAddRequest request)
    {
      return _GengdiTypeGroupService.GengdiTypeAdd(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/GengdiTypeGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public GengdiTypeAddDefaultValueResponse GengdiTypeAdd_GetDefaultValue([FromBody] GengdiTypeAddPageparameter request)
    {
      return _GengdiTypeGroupService.GengdiTypeAdd_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 删除
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/GengdiTypeGroup/删除", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult GengdiTypeDelete([FromBody] GengdiTypeDeleteRequest request)
    {
      return _GengdiTypeGroupService.GengdiTypeDelete(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/GengdiTypeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public PagerInfo<GengdiTypeSearchResponse> GengdiTypeSearch([FromBody] GengdiTypeSearchQueryRequest request)
    {
      return _GengdiTypeGroupService.GengdiTypeSearch(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/GengdiTypeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public GengdiTypeSearchDefaultValueResponse GengdiTypeSearch_GetDefaultValue([FromBody] GengdiTypeSearchPageparameter request)
    {
      return _GengdiTypeGroupService.GengdiTypeSearch_GetDefaultValue(request, HttpContext.UserInfo());
    }

  }
}
