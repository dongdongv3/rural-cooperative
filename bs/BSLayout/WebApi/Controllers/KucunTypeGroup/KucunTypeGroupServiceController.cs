using System;
using WebApi.Code.Interface.KucunTypeGroup;
using Microsoft.AspNetCore.Mvc;
using WebApi.Code.Interface.KucunTypeGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;

namespace WebApi.Controllers.KucunTypeGroup
{
  [Route("api/[controller]/[action]")]
  public class KucunTypeGroupServiceController : ApiControllerBase
  {
    public readonly IKucunTypeGroupInterface _KucunTypeGroupService;

    public KucunTypeGroupServiceController(IKucunTypeGroupInterface _KucunTypeGroupService)
    {
      this._KucunTypeGroupService = _KucunTypeGroupService;
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/KucunTypeGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult KucunTypeAdd([FromBody] KucunTypeAddRequest request)
    {
      return _KucunTypeGroupService.KucunTypeAdd(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/KucunTypeGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public KucunTypeAddDefaultValueResponse KucunTypeAdd_GetDefaultValue([FromBody] KucunTypeAddPageparameter request)
    {
      return _KucunTypeGroupService.KucunTypeAdd_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 删除
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/KucunTypeGroup/删除", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult KucunTypeDelete([FromBody] KucunTypeDeleteRequest request)
    {
      return _KucunTypeGroupService.KucunTypeDelete(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/KucunTypeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public PagerInfo<KucunTypeSearchResponse> KucunTypeSearch([FromBody] KucunTypeSearchQueryRequest request)
    {
      return _KucunTypeGroupService.KucunTypeSearch(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/KucunTypeGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public KucunTypeSearchDefaultValueResponse KucunTypeSearch_GetDefaultValue([FromBody] KucunTypeSearchPageparameter request)
    {
      return _KucunTypeGroupService.KucunTypeSearch_GetDefaultValue(request, HttpContext.UserInfo());
    }

  }
}
