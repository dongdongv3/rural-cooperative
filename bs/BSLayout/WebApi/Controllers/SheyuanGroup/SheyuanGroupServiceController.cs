using System;
using WebApi.Code.Interface.SheyuanGroup;
using Microsoft.AspNetCore.Mvc;
using WebApi.Code.Interface.SheyuanGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Controllers.SheyuanGroup
{
  [Route("api/[controller]/[action]")]
  public class SheyuanGroupServiceController : ApiControllerBase
  {
    public readonly ISheyuanGroupInterface _SheyuanGroupService;

    public SheyuanGroupServiceController(ISheyuanGroupInterface _SheyuanGroupService)
    {
      this._SheyuanGroupService = _SheyuanGroupService;
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SheyuanGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult SheyuanAdd([FromBody] SheyuanAddRequest request)
    {
      return _SheyuanGroupService.SheyuanAdd(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SheyuanGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public SheyuanAddDefaultValueResponse SheyuanAdd_GetDefaultValue([FromBody] SheyuanAddPageparameter request)
    {
      return _SheyuanGroupService.SheyuanAdd_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '片区' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SheyuanGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SheyuanAdd_Panqu_DataSource([FromQuery] SheyuanAddRequest_ValidateNever request)
    {
      return _SheyuanGroupService.SheyuanAdd_Panqu_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '状态' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SheyuanGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SheyuanAdd_Status_DataSource()
    {
      return _SheyuanGroupService.SheyuanAdd_Status_DataSource();
    }

    /// <summary>
    /// 编辑
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SheyuanGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public SheyuanEditResponse SheyuanEdit_GetValue([FromBody] SheyuanEditQueryRequest request)
    {
      return _SheyuanGroupService.SheyuanEdit_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SheyuanGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult SheyuanEdit([FromBody] SheyuanEditUpdateRequest request)
    {
      return _SheyuanGroupService.SheyuanEdit(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '片区' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SheyuanGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SheyuanEdit_Panqu_DataSource([FromQuery] SheyuanEditUpdateRequest_ValidateNever request)
    {
      return _SheyuanGroupService.SheyuanEdit_Panqu_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '状态' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SheyuanGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SheyuanEdit_Status_DataSource()
    {
      return _SheyuanGroupService.SheyuanEdit_Status_DataSource();
    }

    /// <summary>
    /// 详情
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SheyuanGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public SheyuanDetailResponse SheyuanDetail_GetValue([FromBody] SheyuanDetailQueryRequest request)
    {
      return _SheyuanGroupService.SheyuanDetail_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '片区' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SheyuanGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SheyuanDetail_Panqu_DataSource([FromQuery] SheyuanDetailResponse_ValidateNever request)
    {
      return _SheyuanGroupService.SheyuanDetail_Panqu_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '状态' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SheyuanGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SheyuanDetail_Status_DataSource()
    {
      return _SheyuanGroupService.SheyuanDetail_Status_DataSource();
    }

    /// <summary>
    /// 删除
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SheyuanGroup/删除", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult SheyuanDelete([FromBody] SheyuanDeleteRequest request)
    {
      return _SheyuanGroupService.SheyuanDelete(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SheyuanGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public PagerInfo<SheyuanSearchResponse> SheyuanSearch([FromBody] SheyuanSearchQueryRequest request)
    {
      return _SheyuanGroupService.SheyuanSearch(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SheyuanGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public SheyuanSearchDefaultValueResponse SheyuanSearch_GetDefaultValue([FromBody] SheyuanSearchPageparameter request)
    {
      return _SheyuanGroupService.SheyuanSearch_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '片区' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SheyuanGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SheyuanSearch_Panqu_View_DataSource([FromQuery] SheyuanSearchPageparameter request)
    {
      return _SheyuanGroupService.SheyuanSearch_Panqu_View_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '状态' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SheyuanGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SheyuanSearch_Status_View_DataSource()
    {
      return _SheyuanGroupService.SheyuanSearch_Status_View_DataSource();
    }

  }
}
