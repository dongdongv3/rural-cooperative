using System;
using WebApi.Code.Interface.SysUserGroup;
using Microsoft.AspNetCore.Mvc;
using WebApi.Code.Interface.SysUserGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Controllers.SysUserGroup
{
  [Route("api/[controller]/[action]")]
  public class SysUserGroupServiceController : ApiControllerBase
  {
    public readonly ISysUserGroupInterface _SysUserGroupService;

    public SysUserGroupServiceController(ISysUserGroupInterface _SysUserGroupService)
    {
      this._SysUserGroupService = _SysUserGroupService;
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult SysUser_AccountAndPasswordAuth1AddSysUser([FromBody] SysUser_AccountAndPasswordAuth1AddSysUserRequest request)
    {
      return _SysUserGroupService.SysUser_AccountAndPasswordAuth1AddSysUser(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public SysUser_AccountAndPasswordAuth1AddSysUserDefaultValueResponse SysUser_AccountAndPasswordAuth1AddSysUser_GetDefaultValue([FromBody] SysUser_AccountAndPasswordAuth1AddSysUserPageparameter request)
    {
      return _SysUserGroupService.SysUser_AccountAndPasswordAuth1AddSysUser_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_AccountAndPasswordAuth1AddSysUser_RefSysUserId_DataSource([FromQuery] SysUser_AccountAndPasswordAuth1AddSysUserRequest_ValidateNever request)
    {
      return _SysUserGroupService.SysUser_AccountAndPasswordAuth1AddSysUser_RefSysUserId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public SysUser_AccountAndPasswordAuth1EditSysUserResponse SysUser_AccountAndPasswordAuth1EditSysUser_GetValue([FromBody] SysUser_AccountAndPasswordAuth1EditSysUserQueryRequest request)
    {
      return _SysUserGroupService.SysUser_AccountAndPasswordAuth1EditSysUser_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult SysUser_AccountAndPasswordAuth1EditSysUser([FromBody] SysUser_AccountAndPasswordAuth1EditSysUserUpdateRequest request)
    {
      return _SysUserGroupService.SysUser_AccountAndPasswordAuth1EditSysUser(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_AccountAndPasswordAuth1EditSysUser_RefSysUserId_DataSource([FromQuery] SysUser_AccountAndPasswordAuth1EditSysUserUpdateRequest_ValidateNever request)
    {
      return _SysUserGroupService.SysUser_AccountAndPasswordAuth1EditSysUser_RefSysUserId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 删除
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/删除", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult SysUser_AccountAndPasswordAuth1Delete([FromBody] SysUser_AccountAndPasswordAuth1DeleteRequest request)
    {
      return _SysUserGroupService.SysUser_AccountAndPasswordAuth1Delete(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 详情
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public SysUser_AccountAndPasswordAuth1DetailResponse SysUser_AccountAndPasswordAuth1Detail_GetValue([FromBody] SysUser_AccountAndPasswordAuth1DetailQueryRequest request)
    {
      return _SysUserGroupService.SysUser_AccountAndPasswordAuth1Detail_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_AccountAndPasswordAuth1Detail_RefSysUserId_DataSource([FromQuery] SysUser_AccountAndPasswordAuth1DetailResponse_ValidateNever request)
    {
      return _SysUserGroupService.SysUser_AccountAndPasswordAuth1Detail_RefSysUserId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public PagerInfo<SysUser_AccountAndPasswordAuth1SearchSysUserResponse> SysUser_AccountAndPasswordAuth1SearchSysUser([FromBody] SysUser_AccountAndPasswordAuth1SearchSysUserQueryRequest request)
    {
      return _SysUserGroupService.SysUser_AccountAndPasswordAuth1SearchSysUser(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public SysUser_AccountAndPasswordAuth1SearchSysUserDefaultValueResponse SysUser_AccountAndPasswordAuth1SearchSysUser_GetDefaultValue([FromBody] SysUser_AccountAndPasswordAuth1SearchSysUserPageparameter request)
    {
      return _SysUserGroupService.SysUser_AccountAndPasswordAuth1SearchSysUser_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_AccountAndPasswordAuth1SearchSysUser_RefSysUserId_View_DataSource([FromQuery] SysUser_AccountAndPasswordAuth1SearchSysUserPageparameter request)
    {
      return _SysUserGroupService.SysUser_AccountAndPasswordAuth1SearchSysUser_RefSysUserId_View_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源-where
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_AccountAndPasswordAuth1SearchSysUser_RefSysUserId_Where_DataSource([FromQuery] SysUser_AccountAndPasswordAuth1SearchSysUserQueryRequest_ValidateNever request)
    {
      return _SysUserGroupService.SysUser_AccountAndPasswordAuth1SearchSysUser_RefSysUserId_Where_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult SysUser_UserRoleMapAddSysUser([FromBody] SysUser_UserRoleMapAddSysUserRequest request)
    {
      return _SysUserGroupService.SysUser_UserRoleMapAddSysUser(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public SysUser_UserRoleMapAddSysUserDefaultValueResponse SysUser_UserRoleMapAddSysUser_GetDefaultValue([FromBody] SysUser_UserRoleMapAddSysUserPageparameter request)
    {
      return _SysUserGroupService.SysUser_UserRoleMapAddSysUser_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_UserRoleMapAddSysUser_RefSysUserId_DataSource([FromQuery] SysUser_UserRoleMapAddSysUserRequest_ValidateNever request)
    {
      return _SysUserGroupService.SysUser_UserRoleMapAddSysUser_RefSysUserId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_UserRoleMapAddSysUser_RefSysUser_RoleId_DataSource([FromQuery] SysUser_UserRoleMapAddSysUserRequest_ValidateNever request)
    {
      return _SysUserGroupService.SysUser_UserRoleMapAddSysUser_RefSysUser_RoleId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult SysUser_UserRoleMapAddSysUser_Role([FromBody] SysUser_UserRoleMapAddSysUser_RoleRequest request)
    {
      return _SysUserGroupService.SysUser_UserRoleMapAddSysUser_Role(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public SysUser_UserRoleMapAddSysUser_RoleDefaultValueResponse SysUser_UserRoleMapAddSysUser_Role_GetDefaultValue([FromBody] SysUser_UserRoleMapAddSysUser_RolePageparameter request)
    {
      return _SysUserGroupService.SysUser_UserRoleMapAddSysUser_Role_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_UserRoleMapAddSysUser_Role_RefSysUserId_DataSource([FromQuery] SysUser_UserRoleMapAddSysUser_RoleRequest_ValidateNever request)
    {
      return _SysUserGroupService.SysUser_UserRoleMapAddSysUser_Role_RefSysUserId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_UserRoleMapAddSysUser_Role_RefSysUser_RoleId_DataSource([FromQuery] SysUser_UserRoleMapAddSysUser_RoleRequest_ValidateNever request)
    {
      return _SysUserGroupService.SysUser_UserRoleMapAddSysUser_Role_RefSysUser_RoleId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public SysUser_UserRoleMapEditSysUserResponse SysUser_UserRoleMapEditSysUser_GetValue([FromBody] SysUser_UserRoleMapEditSysUserQueryRequest request)
    {
      return _SysUserGroupService.SysUser_UserRoleMapEditSysUser_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult SysUser_UserRoleMapEditSysUser([FromBody] SysUser_UserRoleMapEditSysUserUpdateRequest request)
    {
      return _SysUserGroupService.SysUser_UserRoleMapEditSysUser(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_UserRoleMapEditSysUser_RefSysUserId_DataSource([FromQuery] SysUser_UserRoleMapEditSysUserUpdateRequest_ValidateNever request)
    {
      return _SysUserGroupService.SysUser_UserRoleMapEditSysUser_RefSysUserId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_UserRoleMapEditSysUser_RefSysUser_RoleId_DataSource([FromQuery] SysUser_UserRoleMapEditSysUserUpdateRequest_ValidateNever request)
    {
      return _SysUserGroupService.SysUser_UserRoleMapEditSysUser_RefSysUser_RoleId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public SysUser_UserRoleMapEditSysUser_RoleResponse SysUser_UserRoleMapEditSysUser_Role_GetValue([FromBody] SysUser_UserRoleMapEditSysUser_RoleQueryRequest request)
    {
      return _SysUserGroupService.SysUser_UserRoleMapEditSysUser_Role_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult SysUser_UserRoleMapEditSysUser_Role([FromBody] SysUser_UserRoleMapEditSysUser_RoleUpdateRequest request)
    {
      return _SysUserGroupService.SysUser_UserRoleMapEditSysUser_Role(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_UserRoleMapEditSysUser_Role_RefSysUserId_DataSource([FromQuery] SysUser_UserRoleMapEditSysUser_RoleUpdateRequest_ValidateNever request)
    {
      return _SysUserGroupService.SysUser_UserRoleMapEditSysUser_Role_RefSysUserId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_UserRoleMapEditSysUser_Role_RefSysUser_RoleId_DataSource([FromQuery] SysUser_UserRoleMapEditSysUser_RoleUpdateRequest_ValidateNever request)
    {
      return _SysUserGroupService.SysUser_UserRoleMapEditSysUser_Role_RefSysUser_RoleId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 删除
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/删除", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult SysUser_UserRoleMapDelete([FromBody] SysUser_UserRoleMapDeleteRequest request)
    {
      return _SysUserGroupService.SysUser_UserRoleMapDelete(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 详情
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public SysUser_UserRoleMapDetailResponse SysUser_UserRoleMapDetail_GetValue([FromBody] SysUser_UserRoleMapDetailQueryRequest request)
    {
      return _SysUserGroupService.SysUser_UserRoleMapDetail_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_UserRoleMapDetail_RefSysUserId_DataSource([FromQuery] SysUser_UserRoleMapDetailResponse_ValidateNever request)
    {
      return _SysUserGroupService.SysUser_UserRoleMapDetail_RefSysUserId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_UserRoleMapDetail_RefSysUser_RoleId_DataSource([FromQuery] SysUser_UserRoleMapDetailResponse_ValidateNever request)
    {
      return _SysUserGroupService.SysUser_UserRoleMapDetail_RefSysUser_RoleId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public PagerInfo<SysUser_UserRoleMapSearchSysUserResponse> SysUser_UserRoleMapSearchSysUser([FromBody] SysUser_UserRoleMapSearchSysUserQueryRequest request)
    {
      return _SysUserGroupService.SysUser_UserRoleMapSearchSysUser(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public SysUser_UserRoleMapSearchSysUserDefaultValueResponse SysUser_UserRoleMapSearchSysUser_GetDefaultValue([FromBody] SysUser_UserRoleMapSearchSysUserPageparameter request)
    {
      return _SysUserGroupService.SysUser_UserRoleMapSearchSysUser_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_UserRoleMapSearchSysUser_RefSysUserId_View_DataSource([FromQuery] SysUser_UserRoleMapSearchSysUserPageparameter request)
    {
      return _SysUserGroupService.SysUser_UserRoleMapSearchSysUser_RefSysUserId_View_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_UserRoleMapSearchSysUser_RefSysUser_RoleId_View_DataSource([FromQuery] SysUser_UserRoleMapSearchSysUserPageparameter request)
    {
      return _SysUserGroupService.SysUser_UserRoleMapSearchSysUser_RefSysUser_RoleId_View_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源-where
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_UserRoleMapSearchSysUser_RefSysUserId_Where_DataSource([FromQuery] SysUser_UserRoleMapSearchSysUserQueryRequest_ValidateNever request)
    {
      return _SysUserGroupService.SysUser_UserRoleMapSearchSysUser_RefSysUserId_Where_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public PagerInfo<SysUser_UserRoleMapSearchSysUser_RoleResponse> SysUser_UserRoleMapSearchSysUser_Role([FromBody] SysUser_UserRoleMapSearchSysUser_RoleQueryRequest request)
    {
      return _SysUserGroupService.SysUser_UserRoleMapSearchSysUser_Role(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public SysUser_UserRoleMapSearchSysUser_RoleDefaultValueResponse SysUser_UserRoleMapSearchSysUser_Role_GetDefaultValue([FromBody] SysUser_UserRoleMapSearchSysUser_RolePageparameter request)
    {
      return _SysUserGroupService.SysUser_UserRoleMapSearchSysUser_Role_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '系统用户-用户信息-用户名' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_UserRoleMapSearchSysUser_Role_RefSysUserId_View_DataSource([FromQuery] SysUser_UserRoleMapSearchSysUser_RolePageparameter request)
    {
      return _SysUserGroupService.SysUser_UserRoleMapSearchSysUser_Role_RefSysUserId_View_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_UserRoleMapSearchSysUser_Role_RefSysUser_RoleId_View_DataSource([FromQuery] SysUser_UserRoleMapSearchSysUser_RolePageparameter request)
    {
      return _SysUserGroupService.SysUser_UserRoleMapSearchSysUser_Role_RefSysUser_RoleId_View_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源-where
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_UserRoleMapSearchSysUser_Role_RefSysUser_RoleId_Where_DataSource([FromQuery] SysUser_UserRoleMapSearchSysUser_RoleQueryRequest_ValidateNever request)
    {
      return _SysUserGroupService.SysUser_UserRoleMapSearchSysUser_Role_RefSysUser_RoleId_Where_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult SysUser_RoleAuthMapAddSysUser_Role([FromBody] SysUser_RoleAuthMapAddSysUser_RoleRequest request)
    {
      return _SysUserGroupService.SysUser_RoleAuthMapAddSysUser_Role(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public SysUser_RoleAuthMapAddSysUser_RoleDefaultValueResponse SysUser_RoleAuthMapAddSysUser_Role_GetDefaultValue([FromBody] SysUser_RoleAuthMapAddSysUser_RolePageparameter request)
    {
      return _SysUserGroupService.SysUser_RoleAuthMapAddSysUser_Role_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_RoleAuthMapAddSysUser_Role_RefSysUser_RoleId_DataSource([FromQuery] SysUser_RoleAuthMapAddSysUser_RoleRequest_ValidateNever request)
    {
      return _SysUserGroupService.SysUser_RoleAuthMapAddSysUser_Role_RefSysUser_RoleId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '权限-权限名称' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_RoleAuthMapAddSysUser_Role_RefSysUser_AuthId_DataSource([FromQuery] SysUser_RoleAuthMapAddSysUser_RoleRequest_ValidateNever request)
    {
      return _SysUserGroupService.SysUser_RoleAuthMapAddSysUser_Role_RefSysUser_AuthId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult SysUser_RoleAuthMapAddSysUser_Auth([FromBody] SysUser_RoleAuthMapAddSysUser_AuthRequest request)
    {
      return _SysUserGroupService.SysUser_RoleAuthMapAddSysUser_Auth(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public SysUser_RoleAuthMapAddSysUser_AuthDefaultValueResponse SysUser_RoleAuthMapAddSysUser_Auth_GetDefaultValue([FromBody] SysUser_RoleAuthMapAddSysUser_AuthPageparameter request)
    {
      return _SysUserGroupService.SysUser_RoleAuthMapAddSysUser_Auth_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_RoleAuthMapAddSysUser_Auth_RefSysUser_RoleId_DataSource([FromQuery] SysUser_RoleAuthMapAddSysUser_AuthRequest_ValidateNever request)
    {
      return _SysUserGroupService.SysUser_RoleAuthMapAddSysUser_Auth_RefSysUser_RoleId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '权限-权限名称' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_RoleAuthMapAddSysUser_Auth_RefSysUser_AuthId_DataSource([FromQuery] SysUser_RoleAuthMapAddSysUser_AuthRequest_ValidateNever request)
    {
      return _SysUserGroupService.SysUser_RoleAuthMapAddSysUser_Auth_RefSysUser_AuthId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public SysUser_RoleAuthMapEditSysUser_RoleResponse SysUser_RoleAuthMapEditSysUser_Role_GetValue([FromBody] SysUser_RoleAuthMapEditSysUser_RoleQueryRequest request)
    {
      return _SysUserGroupService.SysUser_RoleAuthMapEditSysUser_Role_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult SysUser_RoleAuthMapEditSysUser_Role([FromBody] SysUser_RoleAuthMapEditSysUser_RoleUpdateRequest request)
    {
      return _SysUserGroupService.SysUser_RoleAuthMapEditSysUser_Role(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_RoleAuthMapEditSysUser_Role_RefSysUser_RoleId_DataSource([FromQuery] SysUser_RoleAuthMapEditSysUser_RoleUpdateRequest_ValidateNever request)
    {
      return _SysUserGroupService.SysUser_RoleAuthMapEditSysUser_Role_RefSysUser_RoleId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '权限-权限名称' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_RoleAuthMapEditSysUser_Role_RefSysUser_AuthId_DataSource([FromQuery] SysUser_RoleAuthMapEditSysUser_RoleUpdateRequest_ValidateNever request)
    {
      return _SysUserGroupService.SysUser_RoleAuthMapEditSysUser_Role_RefSysUser_AuthId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public SysUser_RoleAuthMapEditSysUser_AuthResponse SysUser_RoleAuthMapEditSysUser_Auth_GetValue([FromBody] SysUser_RoleAuthMapEditSysUser_AuthQueryRequest request)
    {
      return _SysUserGroupService.SysUser_RoleAuthMapEditSysUser_Auth_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult SysUser_RoleAuthMapEditSysUser_Auth([FromBody] SysUser_RoleAuthMapEditSysUser_AuthUpdateRequest request)
    {
      return _SysUserGroupService.SysUser_RoleAuthMapEditSysUser_Auth(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_RoleAuthMapEditSysUser_Auth_RefSysUser_RoleId_DataSource([FromQuery] SysUser_RoleAuthMapEditSysUser_AuthUpdateRequest_ValidateNever request)
    {
      return _SysUserGroupService.SysUser_RoleAuthMapEditSysUser_Auth_RefSysUser_RoleId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '权限-权限名称' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_RoleAuthMapEditSysUser_Auth_RefSysUser_AuthId_DataSource([FromQuery] SysUser_RoleAuthMapEditSysUser_AuthUpdateRequest_ValidateNever request)
    {
      return _SysUserGroupService.SysUser_RoleAuthMapEditSysUser_Auth_RefSysUser_AuthId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 删除
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/删除", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult SysUser_RoleAuthMapDelete([FromBody] SysUser_RoleAuthMapDeleteRequest request)
    {
      return _SysUserGroupService.SysUser_RoleAuthMapDelete(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 详情
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public SysUser_RoleAuthMapDetailResponse SysUser_RoleAuthMapDetail_GetValue([FromBody] SysUser_RoleAuthMapDetailQueryRequest request)
    {
      return _SysUserGroupService.SysUser_RoleAuthMapDetail_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_RoleAuthMapDetail_RefSysUser_RoleId_DataSource([FromQuery] SysUser_RoleAuthMapDetailResponse_ValidateNever request)
    {
      return _SysUserGroupService.SysUser_RoleAuthMapDetail_RefSysUser_RoleId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '权限-权限名称' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_RoleAuthMapDetail_RefSysUser_AuthId_DataSource([FromQuery] SysUser_RoleAuthMapDetailResponse_ValidateNever request)
    {
      return _SysUserGroupService.SysUser_RoleAuthMapDetail_RefSysUser_AuthId_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public PagerInfo<SysUser_RoleAuthMapSearchSysUser_RoleResponse> SysUser_RoleAuthMapSearchSysUser_Role([FromBody] SysUser_RoleAuthMapSearchSysUser_RoleQueryRequest request)
    {
      return _SysUserGroupService.SysUser_RoleAuthMapSearchSysUser_Role(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public SysUser_RoleAuthMapSearchSysUser_RoleDefaultValueResponse SysUser_RoleAuthMapSearchSysUser_Role_GetDefaultValue([FromBody] SysUser_RoleAuthMapSearchSysUser_RolePageparameter request)
    {
      return _SysUserGroupService.SysUser_RoleAuthMapSearchSysUser_Role_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_RoleAuthMapSearchSysUser_Role_RefSysUser_RoleId_View_DataSource([FromQuery] SysUser_RoleAuthMapSearchSysUser_RolePageparameter request)
    {
      return _SysUserGroupService.SysUser_RoleAuthMapSearchSysUser_Role_RefSysUser_RoleId_View_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '权限-权限名称' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_RoleAuthMapSearchSysUser_Role_RefSysUser_AuthId_View_DataSource([FromQuery] SysUser_RoleAuthMapSearchSysUser_RolePageparameter request)
    {
      return _SysUserGroupService.SysUser_RoleAuthMapSearchSysUser_Role_RefSysUser_AuthId_View_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源-where
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_RoleAuthMapSearchSysUser_Role_RefSysUser_RoleId_Where_DataSource([FromQuery] SysUser_RoleAuthMapSearchSysUser_RoleQueryRequest_ValidateNever request)
    {
      return _SysUserGroupService.SysUser_RoleAuthMapSearchSysUser_Role_RefSysUser_RoleId_Where_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public PagerInfo<SysUser_RoleAuthMapSearchSysUser_AuthResponse> SysUser_RoleAuthMapSearchSysUser_Auth([FromBody] SysUser_RoleAuthMapSearchSysUser_AuthQueryRequest request)
    {
      return _SysUserGroupService.SysUser_RoleAuthMapSearchSysUser_Auth(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public SysUser_RoleAuthMapSearchSysUser_AuthDefaultValueResponse SysUser_RoleAuthMapSearchSysUser_Auth_GetDefaultValue([FromBody] SysUser_RoleAuthMapSearchSysUser_AuthPageparameter request)
    {
      return _SysUserGroupService.SysUser_RoleAuthMapSearchSysUser_Auth_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '角色-角色名称' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_RoleAuthMapSearchSysUser_Auth_RefSysUser_RoleId_View_DataSource([FromQuery] SysUser_RoleAuthMapSearchSysUser_AuthPageparameter request)
    {
      return _SysUserGroupService.SysUser_RoleAuthMapSearchSysUser_Auth_RefSysUser_RoleId_View_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '权限-权限名称' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_RoleAuthMapSearchSysUser_Auth_RefSysUser_AuthId_View_DataSource([FromQuery] SysUser_RoleAuthMapSearchSysUser_AuthPageparameter request)
    {
      return _SysUserGroupService.SysUser_RoleAuthMapSearchSysUser_Auth_RefSysUser_AuthId_View_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '权限-权限名称' 字段的数据源-where
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> SysUser_RoleAuthMapSearchSysUser_Auth_RefSysUser_AuthId_Where_DataSource([FromQuery] SysUser_RoleAuthMapSearchSysUser_AuthQueryRequest_ValidateNever request)
    {
      return _SysUserGroupService.SysUser_RoleAuthMapSearchSysUser_Auth_RefSysUser_AuthId_Where_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult SysUser_AuthAdd([FromBody] SysUser_AuthAddRequest request)
    {
      return _SysUserGroupService.SysUser_AuthAdd(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public SysUser_AuthAddDefaultValueResponse SysUser_AuthAdd_GetDefaultValue([FromBody] SysUser_AuthAddPageparameter request)
    {
      return _SysUserGroupService.SysUser_AuthAdd_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public SysUser_AuthEditResponse SysUser_AuthEdit_GetValue([FromBody] SysUser_AuthEditQueryRequest request)
    {
      return _SysUserGroupService.SysUser_AuthEdit_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult SysUser_AuthEdit([FromBody] SysUser_AuthEditUpdateRequest request)
    {
      return _SysUserGroupService.SysUser_AuthEdit(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 删除
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/删除", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult SysUser_AuthDelete([FromBody] SysUser_AuthDeleteRequest request)
    {
      return _SysUserGroupService.SysUser_AuthDelete(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 详情
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public SysUser_AuthDetailResponse SysUser_AuthDetail_GetValue([FromBody] SysUser_AuthDetailQueryRequest request)
    {
      return _SysUserGroupService.SysUser_AuthDetail_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public PagerInfo<SysUser_AuthSearchResponse> SysUser_AuthSearch([FromBody] SysUser_AuthSearchQueryRequest request)
    {
      return _SysUserGroupService.SysUser_AuthSearch(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public SysUser_AuthSearchDefaultValueResponse SysUser_AuthSearch_GetDefaultValue([FromBody] SysUser_AuthSearchPageparameter request)
    {
      return _SysUserGroupService.SysUser_AuthSearch_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult SysUser_RoleAdd([FromBody] SysUser_RoleAddRequest request)
    {
      return _SysUserGroupService.SysUser_RoleAdd(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public SysUser_RoleAddDefaultValueResponse SysUser_RoleAdd_GetDefaultValue([FromBody] SysUser_RoleAddPageparameter request)
    {
      return _SysUserGroupService.SysUser_RoleAdd_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public SysUser_RoleEditResponse SysUser_RoleEdit_GetValue([FromBody] SysUser_RoleEditQueryRequest request)
    {
      return _SysUserGroupService.SysUser_RoleEdit_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult SysUser_RoleEdit([FromBody] SysUser_RoleEditUpdateRequest request)
    {
      return _SysUserGroupService.SysUser_RoleEdit(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 删除
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/删除", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult SysUser_RoleDelete([FromBody] SysUser_RoleDeleteRequest request)
    {
      return _SysUserGroupService.SysUser_RoleDelete(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 详情
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public SysUser_RoleDetailResponse SysUser_RoleDetail_GetValue([FromBody] SysUser_RoleDetailQueryRequest request)
    {
      return _SysUserGroupService.SysUser_RoleDetail_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public PagerInfo<SysUser_RoleSearchResponse> SysUser_RoleSearch([FromBody] SysUser_RoleSearchQueryRequest request)
    {
      return _SysUserGroupService.SysUser_RoleSearch(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public SysUser_RoleSearchDefaultValueResponse SysUser_RoleSearch_GetDefaultValue([FromBody] SysUser_RoleSearchPageparameter request)
    {
      return _SysUserGroupService.SysUser_RoleSearch_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult SysUserAdd([FromBody] SysUserAddRequest request)
    {
      return _SysUserGroupService.SysUserAdd(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public SysUserAddDefaultValueResponse SysUserAdd_GetDefaultValue([FromBody] SysUserAddPageparameter request)
    {
      return _SysUserGroupService.SysUserAdd_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public SysUserEditResponse SysUserEdit_GetValue([FromBody] SysUserEditQueryRequest request)
    {
      return _SysUserGroupService.SysUserEdit_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult SysUserEdit([FromBody] SysUserEditUpdateRequest request)
    {
      return _SysUserGroupService.SysUserEdit(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 删除
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/删除", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult SysUserDelete([FromBody] SysUserDeleteRequest request)
    {
      return _SysUserGroupService.SysUserDelete(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 详情
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public SysUserDetailResponse SysUserDetail_GetValue([FromBody] SysUserDetailQueryRequest request)
    {
      return _SysUserGroupService.SysUserDetail_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public PagerInfo<SysUserSearchResponse> SysUserSearch([FromBody] SysUserSearchQueryRequest request)
    {
      return _SysUserGroupService.SysUserSearch(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/SysUserGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public SysUserSearchDefaultValueResponse SysUserSearch_GetDefaultValue([FromBody] SysUserSearchPageparameter request)
    {
      return _SysUserGroupService.SysUserSearch_GetDefaultValue(request, HttpContext.UserInfo());
    }

  }
}
