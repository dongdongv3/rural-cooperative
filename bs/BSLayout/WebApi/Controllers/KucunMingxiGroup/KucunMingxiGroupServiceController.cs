using System;
using WebApi.Code.Interface.KucunMingxiGroup;
using Microsoft.AspNetCore.Mvc;
using WebApi.Code.Interface.KucunMingxiGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Controllers.KucunMingxiGroup
{
  [Route("api/[controller]/[action]")]
  public class KucunMingxiGroupServiceController : ApiControllerBase
  {
    public readonly IKucunMingxiGroupInterface _KucunMingxiGroupService;

    public KucunMingxiGroupServiceController(IKucunMingxiGroupInterface _KucunMingxiGroupService)
    {
      this._KucunMingxiGroupService = _KucunMingxiGroupService;
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/KucunMingxiGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult KucunMingxiAdd([FromBody] KucunMingxiAddRequest request)
    {
      return _KucunMingxiGroupService.KucunMingxiAdd(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/KucunMingxiGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public KucunMingxiAddDefaultValueResponse KucunMingxiAdd_GetDefaultValue([FromBody] KucunMingxiAddPageparameter request)
    {
      return _KucunMingxiGroupService.KucunMingxiAdd_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '分类' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/KucunMingxiGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> KucunMingxiAdd_fenlei_DataSource([FromQuery] KucunMingxiAddRequest_ValidateNever request)
    {
      return _KucunMingxiGroupService.KucunMingxiAdd_fenlei_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/KucunMingxiGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public KucunMingxiEditResponse KucunMingxiEdit_GetValue([FromBody] KucunMingxiEditQueryRequest request)
    {
      return _KucunMingxiGroupService.KucunMingxiEdit_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/KucunMingxiGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult KucunMingxiEdit([FromBody] KucunMingxiEditUpdateRequest request)
    {
      return _KucunMingxiGroupService.KucunMingxiEdit(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '分类' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/KucunMingxiGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> KucunMingxiEdit_fenlei_DataSource([FromQuery] KucunMingxiEditUpdateRequest_ValidateNever request)
    {
      return _KucunMingxiGroupService.KucunMingxiEdit_fenlei_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 详情
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/KucunMingxiGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public KucunMingxiDetailResponse KucunMingxiDetail_GetValue([FromBody] KucunMingxiDetailQueryRequest request)
    {
      return _KucunMingxiGroupService.KucunMingxiDetail_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '分类' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/KucunMingxiGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> KucunMingxiDetail_fenlei_DataSource([FromQuery] KucunMingxiDetailResponse_ValidateNever request)
    {
      return _KucunMingxiGroupService.KucunMingxiDetail_fenlei_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 删除
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/KucunMingxiGroup/删除", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult KucunMingxiDelete([FromBody] KucunMingxiDeleteRequest request)
    {
      return _KucunMingxiGroupService.KucunMingxiDelete(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/KucunMingxiGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public PagerInfo<KucunMingxiSearchResponse> KucunMingxiSearch([FromBody] KucunMingxiSearchQueryRequest request)
    {
      return _KucunMingxiGroupService.KucunMingxiSearch(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/KucunMingxiGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public KucunMingxiSearchDefaultValueResponse KucunMingxiSearch_GetDefaultValue([FromBody] KucunMingxiSearchPageparameter request)
    {
      return _KucunMingxiGroupService.KucunMingxiSearch_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '分类' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/KucunMingxiGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> KucunMingxiSearch_fenlei_View_DataSource([FromQuery] KucunMingxiSearchPageparameter request)
    {
      return _KucunMingxiGroupService.KucunMingxiSearch_fenlei_View_DataSource(request, HttpContext.UserInfo());
    }

  }
}
