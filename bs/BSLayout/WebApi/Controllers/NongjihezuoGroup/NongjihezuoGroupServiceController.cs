using System;
using WebApi.Code.Interface.NongjihezuoGroup;
using Microsoft.AspNetCore.Mvc;
using WebApi.Code.Interface.NongjihezuoGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Controllers.NongjihezuoGroup
{
  [Route("api/[controller]/[action]")]
  public class NongjihezuoGroupServiceController : ApiControllerBase
  {
    public readonly INongjihezuoGroupInterface _NongjihezuoGroupService;

    public NongjihezuoGroupServiceController(INongjihezuoGroupInterface _NongjihezuoGroupService)
    {
      this._NongjihezuoGroupService = _NongjihezuoGroupService;
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjihezuoGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult NongjihezuoAdd([FromBody] NongjihezuoAddRequest request)
    {
      return _NongjihezuoGroupService.NongjihezuoAdd(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjihezuoGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public NongjihezuoAddDefaultValueResponse NongjihezuoAdd_GetDefaultValue([FromBody] NongjihezuoAddPageparameter request)
    {
      return _NongjihezuoGroupService.NongjihezuoAdd_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '农机类型' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjihezuoGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> NongjihezuoAdd_leixing_DataSource([FromQuery] NongjihezuoAddRequest_ValidateNever request)
    {
      return _NongjihezuoGroupService.NongjihezuoAdd_leixing_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjihezuoGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public NongjihezuoEditResponse NongjihezuoEdit_GetValue([FromBody] NongjihezuoEditQueryRequest request)
    {
      return _NongjihezuoGroupService.NongjihezuoEdit_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjihezuoGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult NongjihezuoEdit([FromBody] NongjihezuoEditUpdateRequest request)
    {
      return _NongjihezuoGroupService.NongjihezuoEdit(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '农机类型' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjihezuoGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> NongjihezuoEdit_leixing_DataSource([FromQuery] NongjihezuoEditUpdateRequest_ValidateNever request)
    {
      return _NongjihezuoGroupService.NongjihezuoEdit_leixing_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 详情
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjihezuoGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public NongjihezuoDetailResponse NongjihezuoDetail_GetValue([FromBody] NongjihezuoDetailQueryRequest request)
    {
      return _NongjihezuoGroupService.NongjihezuoDetail_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '农机类型' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjihezuoGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> NongjihezuoDetail_leixing_DataSource([FromQuery] NongjihezuoDetailResponse_ValidateNever request)
    {
      return _NongjihezuoGroupService.NongjihezuoDetail_leixing_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 删除
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjihezuoGroup/删除", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult NongjihezuoDelete([FromBody] NongjihezuoDeleteRequest request)
    {
      return _NongjihezuoGroupService.NongjihezuoDelete(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjihezuoGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public PagerInfo<NongjihezuoSearchResponse> NongjihezuoSearch([FromBody] NongjihezuoSearchQueryRequest request)
    {
      return _NongjihezuoGroupService.NongjihezuoSearch(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjihezuoGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public NongjihezuoSearchDefaultValueResponse NongjihezuoSearch_GetDefaultValue([FromBody] NongjihezuoSearchPageparameter request)
    {
      return _NongjihezuoGroupService.NongjihezuoSearch_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '农机类型' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjihezuoGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> NongjihezuoSearch_leixing_View_DataSource([FromQuery] NongjihezuoSearchPageparameter request)
    {
      return _NongjihezuoGroupService.NongjihezuoSearch_leixing_View_DataSource(request, HttpContext.UserInfo());
    }

  }
}
