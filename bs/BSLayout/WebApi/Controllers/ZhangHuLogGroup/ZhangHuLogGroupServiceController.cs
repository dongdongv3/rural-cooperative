using System;
using WebApi.Code.Interface.ZhangHuLogGroup;
using Microsoft.AspNetCore.Mvc;
using WebApi.Code.Interface.ZhangHuLogGroup.Models;
using WebApi.Code.Common;
using System.Collections.Generic;
using WebApi.Code.Common.Authentication;

namespace WebApi.Controllers.ZhangHuLogGroup
{
  [Route("api/[controller]/[action]")]
  public class ZhangHuLogGroupServiceController : ApiControllerBase
  {
    public readonly IZhangHuLogGroupInterface _ZhangHuLogGroupService;

    public ZhangHuLogGroupServiceController(IZhangHuLogGroupInterface _ZhangHuLogGroupService)
    {
      this._ZhangHuLogGroupService = _ZhangHuLogGroupService;
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/ZhangHuLogGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public PagerInfo<ZhangHuLogSearchSheyuanResponse> ZhangHuLogSearchSheyuan([FromBody] ZhangHuLogSearchSheyuanQueryRequest request)
    {
      return _ZhangHuLogGroupService.ZhangHuLogSearchSheyuan(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/ZhangHuLogGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public ZhangHuLogSearchSheyuanDefaultValueResponse ZhangHuLogSearchSheyuan_GetDefaultValue([FromBody] ZhangHuLogSearchSheyuanPageparameter request)
    {
      return _ZhangHuLogGroupService.ZhangHuLogSearchSheyuan_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '社员-姓名' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/ZhangHuLogGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> ZhangHuLogSearchSheyuan_RefSheyuanId_View_DataSource([FromQuery] ZhangHuLogSearchSheyuanPageparameter request)
    {
      return _ZhangHuLogGroupService.ZhangHuLogSearchSheyuan_RefSheyuanId_View_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '社员-姓名' 字段的数据源-where
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/ZhangHuLogGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> ZhangHuLogSearchSheyuan_RefSheyuanId_Where_DataSource([FromQuery] ZhangHuLogSearchSheyuanQueryRequest_ValidateNever request)
    {
      return _ZhangHuLogGroupService.ZhangHuLogSearchSheyuan_RefSheyuanId_Where_DataSource(request, HttpContext.UserInfo());
    }

  }
}
