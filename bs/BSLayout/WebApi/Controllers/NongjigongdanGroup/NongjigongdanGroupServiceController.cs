using System;
using WebApi.Code.Interface.NongjigongdanGroup;
using Microsoft.AspNetCore.Mvc;
using WebApi.Code.Interface.NongjigongdanGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Controllers.NongjigongdanGroup
{
  [Route("api/[controller]/[action]")]
  public class NongjigongdanGroupServiceController : ApiControllerBase
  {
    public readonly INongjigongdanGroupInterface _NongjigongdanGroupService;

    public NongjigongdanGroupServiceController(INongjigongdanGroupInterface _NongjigongdanGroupService)
    {
      this._NongjigongdanGroupService = _NongjigongdanGroupService;
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjigongdanGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult NongjigongdanAdd([FromBody] NongjigongdanAddRequest request)
    {
      return _NongjigongdanGroupService.NongjigongdanAdd(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjigongdanGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public NongjigongdanAddDefaultValueResponse NongjigongdanAdd_GetDefaultValue([FromBody] NongjigongdanAddPageparameter request)
    {
      return _NongjigongdanGroupService.NongjigongdanAdd_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '农机类型' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjigongdanGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> NongjigongdanAdd_leixing_DataSource([FromQuery] NongjigongdanAddRequest_ValidateNever request)
    {
      return _NongjigongdanGroupService.NongjigongdanAdd_leixing_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '接单农机' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjigongdanGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> NongjigongdanAdd_chuli_DataSource([FromQuery] NongjigongdanAddRequest_ValidateNever request)
    {
      return _NongjigongdanGroupService.NongjigongdanAdd_chuli_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '状态' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjigongdanGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> NongjigongdanAdd_zhuangtai_DataSource()
    {
      return _NongjigongdanGroupService.NongjigongdanAdd_zhuangtai_DataSource();
    }

    /// <summary>
    /// '结算状态' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjigongdanGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> NongjigongdanAdd_jeisuanzhuangtai_DataSource()
    {
      return _NongjigongdanGroupService.NongjigongdanAdd_jeisuanzhuangtai_DataSource();
    }

    /// <summary>
    /// 编辑
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjigongdanGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public NongjigongdanEditResponse NongjigongdanEdit_GetValue([FromBody] NongjigongdanEditQueryRequest request)
    {
      return _NongjigongdanGroupService.NongjigongdanEdit_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjigongdanGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult NongjigongdanEdit([FromBody] NongjigongdanEditUpdateRequest request)
    {
      return _NongjigongdanGroupService.NongjigongdanEdit(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '农机类型' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjigongdanGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> NongjigongdanEdit_leixing_DataSource([FromQuery] NongjigongdanEditUpdateRequest_ValidateNever request)
    {
      return _NongjigongdanGroupService.NongjigongdanEdit_leixing_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '接单农机' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjigongdanGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> NongjigongdanEdit_chuli_DataSource([FromQuery] NongjigongdanEditUpdateRequest_ValidateNever request)
    {
      return _NongjigongdanGroupService.NongjigongdanEdit_chuli_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '状态' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjigongdanGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> NongjigongdanEdit_zhuangtai_DataSource()
    {
      return _NongjigongdanGroupService.NongjigongdanEdit_zhuangtai_DataSource();
    }

    /// <summary>
    /// '结算状态' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjigongdanGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> NongjigongdanEdit_jeisuanzhuangtai_DataSource()
    {
      return _NongjigongdanGroupService.NongjigongdanEdit_jeisuanzhuangtai_DataSource();
    }

    /// <summary>
    /// 详情
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjigongdanGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public NongjigongdanDetailResponse NongjigongdanDetail_GetValue([FromBody] NongjigongdanDetailQueryRequest request)
    {
      return _NongjigongdanGroupService.NongjigongdanDetail_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '农机类型' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjigongdanGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> NongjigongdanDetail_leixing_DataSource([FromQuery] NongjigongdanDetailResponse_ValidateNever request)
    {
      return _NongjigongdanGroupService.NongjigongdanDetail_leixing_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '接单农机' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjigongdanGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> NongjigongdanDetail_chuli_DataSource([FromQuery] NongjigongdanDetailResponse_ValidateNever request)
    {
      return _NongjigongdanGroupService.NongjigongdanDetail_chuli_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '状态' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjigongdanGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> NongjigongdanDetail_zhuangtai_DataSource()
    {
      return _NongjigongdanGroupService.NongjigongdanDetail_zhuangtai_DataSource();
    }

    /// <summary>
    /// '结算状态' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjigongdanGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> NongjigongdanDetail_jeisuanzhuangtai_DataSource()
    {
      return _NongjigongdanGroupService.NongjigongdanDetail_jeisuanzhuangtai_DataSource();
    }

    /// <summary>
    /// 删除
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjigongdanGroup/删除", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult NongjigongdanDelete([FromBody] NongjigongdanDeleteRequest request)
    {
      return _NongjigongdanGroupService.NongjigongdanDelete(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjigongdanGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public PagerInfo<NongjigongdanSearchResponse> NongjigongdanSearch([FromBody] NongjigongdanSearchQueryRequest request)
    {
      return _NongjigongdanGroupService.NongjigongdanSearch(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjigongdanGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public NongjigongdanSearchDefaultValueResponse NongjigongdanSearch_GetDefaultValue([FromBody] NongjigongdanSearchPageparameter request)
    {
      return _NongjigongdanGroupService.NongjigongdanSearch_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '农机类型' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjigongdanGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> NongjigongdanSearch_leixing_View_DataSource([FromQuery] NongjigongdanSearchPageparameter request)
    {
      return _NongjigongdanGroupService.NongjigongdanSearch_leixing_View_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '接单农机' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjigongdanGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> NongjigongdanSearch_chuli_View_DataSource([FromQuery] NongjigongdanSearchPageparameter request)
    {
      return _NongjigongdanGroupService.NongjigongdanSearch_chuli_View_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '状态' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjigongdanGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> NongjigongdanSearch_zhuangtai_View_DataSource()
    {
      return _NongjigongdanGroupService.NongjigongdanSearch_zhuangtai_View_DataSource();
    }

    /// <summary>
    /// '结算状态' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjigongdanGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> NongjigongdanSearch_jeisuanzhuangtai_View_DataSource()
    {
      return _NongjigongdanGroupService.NongjigongdanSearch_jeisuanzhuangtai_View_DataSource();
    }

  }
}
