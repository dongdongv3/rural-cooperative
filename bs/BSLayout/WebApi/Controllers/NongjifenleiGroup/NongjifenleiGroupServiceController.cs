using System;
using WebApi.Code.Interface.NongjifenleiGroup;
using Microsoft.AspNetCore.Mvc;
using WebApi.Code.Interface.NongjifenleiGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;

namespace WebApi.Controllers.NongjifenleiGroup
{
  [Route("api/[controller]/[action]")]
  public class NongjifenleiGroupServiceController : ApiControllerBase
  {
    public readonly INongjifenleiGroupInterface _NongjifenleiGroupService;

    public NongjifenleiGroupServiceController(INongjifenleiGroupInterface _NongjifenleiGroupService)
    {
      this._NongjifenleiGroupService = _NongjifenleiGroupService;
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjifenleiGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult NongjifenleiAdd([FromBody] NongjifenleiAddRequest request)
    {
      return _NongjifenleiGroupService.NongjifenleiAdd(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjifenleiGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public NongjifenleiAddDefaultValueResponse NongjifenleiAdd_GetDefaultValue([FromBody] NongjifenleiAddPageparameter request)
    {
      return _NongjifenleiGroupService.NongjifenleiAdd_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 删除
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjifenleiGroup/删除", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult NongjifenleiDelete([FromBody] NongjifenleiDeleteRequest request)
    {
      return _NongjifenleiGroupService.NongjifenleiDelete(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjifenleiGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public PagerInfo<NongjifenleiSearchResponse> NongjifenleiSearch([FromBody] NongjifenleiSearchQueryRequest request)
    {
      return _NongjifenleiGroupService.NongjifenleiSearch(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/NongjifenleiGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public NongjifenleiSearchDefaultValueResponse NongjifenleiSearch_GetDefaultValue([FromBody] NongjifenleiSearchPageparameter request)
    {
      return _NongjifenleiGroupService.NongjifenleiSearch_GetDefaultValue(request, HttpContext.UserInfo());
    }

  }
}
