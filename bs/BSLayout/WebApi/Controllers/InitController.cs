using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi.Code;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;

namespace WebApi.Controllers
{
  [Route("api/[controller]/[action]")]
  public class InitController : ApiControllerBase
  {
    public const string Title_Error = "初始化过程错误";
    public const string Title_Closed = "初始化已关闭";
    public const string Title_Complate = "初始化已完成";
    public static bool IsOpenWebInit = false;
    public static string errorMessage = "";
    public static string value = "";
    public static string key = "";
    public static List<KeyValuePair<string, string>> options = null;
    public static bool Required = true;
    //为 readsetting 提供输入完成的信号量
    public static System.Threading.EventWaitHandle eventWaitInputComplote = new System.Threading.EventWaitHandle(false, System.Threading.EventResetMode.AutoReset);
    //为 SetValue 提供输入已经读取完成的信号量
    public static System.Threading.EventWaitHandle eventWaitValueReaded = new System.Threading.EventWaitHandle(false, System.Threading.EventResetMode.AutoReset);
    //为 GetInputRequest 提供请求输入的信号量
    public static System.Threading.EventWaitHandle eventWaitInputRequest = new System.Threading.EventWaitHandle(false, System.Threading.EventResetMode.AutoReset);

    [HttpGet]
    public ResponseModel GetInputRequest()
    {
      if (!IsOpenWebInit)
      {
        return new ResponseModel
        {
          Title = Title_Closed
        };
      }
      while (string.IsNullOrEmpty(key))
      {
        eventWaitInputRequest.WaitOne();
      }
      if (key == Title_Error)
      {
        return new ResponseModel
        {
          Title = key,
          Required = Required,
          errorMessage = errorMessage
        };
      }
      if (key == Title_Complate)
      {
        return new ResponseModel
        {
          Title = key,
          Required = Required,
        };
      }
      return new ResponseModel
      {
        Title = key,
        Required = Required,
        Options = options
      };
    }

    [HttpGet]
    public string SetValue(string value)
    {
      key = "";
      InitController.value = value;
      eventWaitInputComplote.Set();
      eventWaitValueReaded.WaitOne();
      return errorMessage;
    }
  }

  public class ResponseModel
  {
    public string Title { get; set; }
    public bool Required { get; set; }
    public List<KeyValuePair<string, string>> Options { get; set; }
    public string errorMessage { get; set; }
  }
}
