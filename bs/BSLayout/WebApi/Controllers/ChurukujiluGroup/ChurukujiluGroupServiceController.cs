using System;
using WebApi.Code.Interface.ChurukujiluGroup;
using Microsoft.AspNetCore.Mvc;
using WebApi.Code.Interface.ChurukujiluGroup.Models;
using WebApi.Code.Common;
using WebApi.Code.Common.Authentication;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Controllers.ChurukujiluGroup
{
  [Route("api/[controller]/[action]")]
  public class ChurukujiluGroupServiceController : ApiControllerBase
  {
    public readonly IChurukujiluGroupInterface _ChurukujiluGroupService;

    public ChurukujiluGroupServiceController(IChurukujiluGroupInterface _ChurukujiluGroupService)
    {
      this._ChurukujiluGroupService = _ChurukujiluGroupService;
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/ChurukujiluGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult ChurukujiluAdd([FromBody] ChurukujiluAddRequest request)
    {
      return _ChurukujiluGroupService.ChurukujiluAdd(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 添加
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/ChurukujiluGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public ChurukujiluAddDefaultValueResponse ChurukujiluAdd_GetDefaultValue([FromBody] ChurukujiluAddPageparameter request)
    {
      return _ChurukujiluGroupService.ChurukujiluAdd_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '审批人' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/ChurukujiluGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> ChurukujiluAdd_ApprovedUser_DataSource([FromQuery] ChurukujiluAddRequest_ValidateNever request)
    {
      return _ChurukujiluGroupService.ChurukujiluAdd_ApprovedUser_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '类型' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/ChurukujiluGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> ChurukujiluAdd_leixing_DataSource()
    {
      return _ChurukujiluGroupService.ChurukujiluAdd_leixing_DataSource();
    }

    /// <summary>
    /// '库存分类' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/ChurukujiluGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> ChurukujiluAdd_fenlei_DataSource([FromQuery] ChurukujiluAddRequest_ValidateNever request)
    {
      return _ChurukujiluGroupService.ChurukujiluAdd_fenlei_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '物品' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/ChurukujiluGroup/添加", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> ChurukujiluAdd_wupin_DataSource([FromQuery] ChurukujiluAddRequest_ValidateNever request)
    {
      return _ChurukujiluGroupService.ChurukujiluAdd_wupin_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/ChurukujiluGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public ChurukujiluEditResponse ChurukujiluEdit_GetValue([FromBody] ChurukujiluEditQueryRequest request)
    {
      return _ChurukujiluGroupService.ChurukujiluEdit_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 编辑
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/ChurukujiluGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult ChurukujiluEdit([FromBody] ChurukujiluEditUpdateRequest request)
    {
      return _ChurukujiluGroupService.ChurukujiluEdit(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '审批人' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/ChurukujiluGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> ChurukujiluEdit_ApprovedUser_DataSource([FromQuery] ChurukujiluEditUpdateRequest_ValidateNever request)
    {
      return _ChurukujiluGroupService.ChurukujiluEdit_ApprovedUser_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '类型' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/ChurukujiluGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> ChurukujiluEdit_leixing_DataSource()
    {
      return _ChurukujiluGroupService.ChurukujiluEdit_leixing_DataSource();
    }

    /// <summary>
    /// '库存分类' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/ChurukujiluGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> ChurukujiluEdit_fenlei_DataSource([FromQuery] ChurukujiluEditUpdateRequest_ValidateNever request)
    {
      return _ChurukujiluGroupService.ChurukujiluEdit_fenlei_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '物品' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/ChurukujiluGroup/编辑", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> ChurukujiluEdit_wupin_DataSource([FromQuery] ChurukujiluEditUpdateRequest_ValidateNever request)
    {
      return _ChurukujiluGroupService.ChurukujiluEdit_wupin_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 详情
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/ChurukujiluGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public ChurukujiluDetailResponse ChurukujiluDetail_GetValue([FromBody] ChurukujiluDetailQueryRequest request)
    {
      return _ChurukujiluGroupService.ChurukujiluDetail_GetValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '审批人' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/ChurukujiluGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> ChurukujiluDetail_ApprovedUser_DataSource([FromQuery] ChurukujiluDetailResponse_ValidateNever request)
    {
      return _ChurukujiluGroupService.ChurukujiluDetail_ApprovedUser_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '类型' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/ChurukujiluGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> ChurukujiluDetail_leixing_DataSource()
    {
      return _ChurukujiluGroupService.ChurukujiluDetail_leixing_DataSource();
    }

    /// <summary>
    /// '库存分类' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/ChurukujiluGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> ChurukujiluDetail_fenlei_DataSource([FromQuery] ChurukujiluDetailResponse_ValidateNever request)
    {
      return _ChurukujiluGroupService.ChurukujiluDetail_fenlei_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '物品' 字段的数据源
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/ChurukujiluGroup/详情", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> ChurukujiluDetail_wupin_DataSource([FromQuery] ChurukujiluDetailResponse_ValidateNever request)
    {
      return _ChurukujiluGroupService.ChurukujiluDetail_wupin_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 删除
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/ChurukujiluGroup/删除", AuthorizationFilterAttribute.Vtype.权限)]
    public OptionsResoult ChurukujiluDelete([FromBody] ChurukujiluDeleteRequest request)
    {
      return _ChurukujiluGroupService.ChurukujiluDelete(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// update
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/ChurukujiluGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public PagerInfo<ChurukujiluSearchResponse> ChurukujiluSearch([FromBody] ChurukujiluSearchQueryRequest request)
    {
      return _ChurukujiluGroupService.ChurukujiluSearch(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// 查询
    /// </summary>
    [HttpPost]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/ChurukujiluGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public ChurukujiluSearchDefaultValueResponse ChurukujiluSearch_GetDefaultValue([FromBody] ChurukujiluSearchPageparameter request)
    {
      return _ChurukujiluGroupService.ChurukujiluSearch_GetDefaultValue(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '审批人' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/ChurukujiluGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> ChurukujiluSearch_ApprovedUser_View_DataSource([FromQuery] ChurukujiluSearchPageparameter request)
    {
      return _ChurukujiluGroupService.ChurukujiluSearch_ApprovedUser_View_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '类型' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/ChurukujiluGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> ChurukujiluSearch_leixing_View_DataSource()
    {
      return _ChurukujiluGroupService.ChurukujiluSearch_leixing_View_DataSource();
    }

    /// <summary>
    /// '库存分类' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/ChurukujiluGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> ChurukujiluSearch_fenlei_View_DataSource([FromQuery] ChurukujiluSearchPageparameter request)
    {
      return _ChurukujiluGroupService.ChurukujiluSearch_fenlei_View_DataSource(request, HttpContext.UserInfo());
    }

    /// <summary>
    /// '物品' 字段的数据源-view
    /// </summary>
    [HttpGet]
    [AuthenticationFilterAttribute]
    [AuthorizationFilter("/ChurukujiluGroup/查询", AuthorizationFilterAttribute.Vtype.权限)]
    public List<KVDataInfo> ChurukujiluSearch_wupin_View_DataSource([FromQuery] ChurukujiluSearchPageparameter request)
    {
      return _ChurukujiluGroupService.ChurukujiluSearch_wupin_View_DataSource(request, HttpContext.UserInfo());
    }

  }
}
