using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApi.Migrations
{
  public partial class Gen20230823232028 : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.CreateTable(
          name: "FileUpLoad",
          columns: table => new
          {
            ID = table.Column<string>(type: "TEXT", maxLength: 50, nullable: false),
            FileName = table.Column<string>(type: "TEXT", maxLength: 255, nullable: true),
            Ext = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
            UserID = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
            CreateTime = table.Column<DateTime>(type: "TEXT", nullable: true),
            IsUse = table.Column<bool>(type: "INTEGER", nullable: true)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_FileUpLoad", x => x.ID);
          });

      migrationBuilder.CreateTable(
          name: "SysUser",
          columns: table => new
          {
            Id = table.Column<string>(type: "TEXT", maxLength: 50, nullable: false),
            UserName = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_SysUser", x => x.Id);
          });

      migrationBuilder.CreateTable(
          name: "SysUser_Auth",
          columns: table => new
          {
            Id = table.Column<Guid>(type: "TEXT", nullable: false),
            AuthName = table.Column<string>(type: "TEXT", maxLength: 255, nullable: true)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_SysUser_Auth", x => x.Id);
          });

      migrationBuilder.CreateTable(
          name: "SysUser_Role",
          columns: table => new
          {
            Id = table.Column<Guid>(type: "TEXT", nullable: false),
            RoleName = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_SysUser_Role", x => x.Id);
          });

      migrationBuilder.CreateTable(
          name: "SysUser_AccountAndPasswordAuth1",
          columns: table => new
          {
            Id = table.Column<Guid>(type: "TEXT", nullable: false),
            LoginID = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
            Pwd = table.Column<string>(type: "TEXT", maxLength: 100, nullable: true),
            RefSysUserId = table.Column<string>(type: "TEXT", nullable: true)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_SysUser_AccountAndPasswordAuth1", x => x.Id);
            table.ForeignKey(
                      name: "FK_SysUser_AccountAndPasswordAuth1_SysUser_RefSysUserId",
                      column: x => x.RefSysUserId,
                      principalTable: "SysUser",
                      principalColumn: "Id",
                      onDelete: ReferentialAction.Restrict);
          });

      migrationBuilder.CreateTable(
          name: "SysUser_RoleAuthMap",
          columns: table => new
          {
            Id = table.Column<Guid>(type: "TEXT", nullable: false),
            RefSysUser_RoleId = table.Column<Guid>(type: "TEXT", nullable: false),
            RefSysUser_AuthId = table.Column<Guid>(type: "TEXT", nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_SysUser_RoleAuthMap", x => x.Id);
            table.ForeignKey(
                      name: "FK_SysUser_RoleAuthMap_SysUser_Auth_RefSysUser_AuthId",
                      column: x => x.RefSysUser_AuthId,
                      principalTable: "SysUser_Auth",
                      principalColumn: "Id",
                      onDelete: ReferentialAction.Cascade);
            table.ForeignKey(
                      name: "FK_SysUser_RoleAuthMap_SysUser_Role_RefSysUser_RoleId",
                      column: x => x.RefSysUser_RoleId,
                      principalTable: "SysUser_Role",
                      principalColumn: "Id",
                      onDelete: ReferentialAction.Cascade);
          });

      migrationBuilder.CreateTable(
          name: "SysUser_UserRoleMap",
          columns: table => new
          {
            Id = table.Column<Guid>(type: "TEXT", nullable: false),
            RefSysUserId = table.Column<string>(type: "TEXT", nullable: true),
            RefSysUser_RoleId = table.Column<Guid>(type: "TEXT", nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_SysUser_UserRoleMap", x => x.Id);
            table.ForeignKey(
                      name: "FK_SysUser_UserRoleMap_SysUser_RefSysUserId",
                      column: x => x.RefSysUserId,
                      principalTable: "SysUser",
                      principalColumn: "Id",
                      onDelete: ReferentialAction.Restrict);
            table.ForeignKey(
                      name: "FK_SysUser_UserRoleMap_SysUser_Role_RefSysUser_RoleId",
                      column: x => x.RefSysUser_RoleId,
                      principalTable: "SysUser_Role",
                      principalColumn: "Id",
                      onDelete: ReferentialAction.Cascade);
          });

      migrationBuilder.CreateIndex(
          name: "IX_SysUser_AccountAndPasswordAuth1_RefSysUserId",
          table: "SysUser_AccountAndPasswordAuth1",
          column: "RefSysUserId",
          unique: true);

      migrationBuilder.CreateIndex(
          name: "IX_SysUser_RoleAuthMap_RefSysUser_AuthId",
          table: "SysUser_RoleAuthMap",
          column: "RefSysUser_AuthId");

      migrationBuilder.CreateIndex(
          name: "IX_SysUser_RoleAuthMap_RefSysUser_RoleId",
          table: "SysUser_RoleAuthMap",
          column: "RefSysUser_RoleId");

      migrationBuilder.CreateIndex(
          name: "IX_SysUser_UserRoleMap_RefSysUser_RoleId",
          table: "SysUser_UserRoleMap",
          column: "RefSysUser_RoleId");

      migrationBuilder.CreateIndex(
          name: "IX_SysUser_UserRoleMap_RefSysUserId",
          table: "SysUser_UserRoleMap",
          column: "RefSysUserId");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropTable(
          name: "FileUpLoad");

      migrationBuilder.DropTable(
          name: "SysUser_AccountAndPasswordAuth1");

      migrationBuilder.DropTable(
          name: "SysUser_RoleAuthMap");

      migrationBuilder.DropTable(
          name: "SysUser_UserRoleMap");

      migrationBuilder.DropTable(
          name: "SysUser_Auth");

      migrationBuilder.DropTable(
          name: "SysUser");

      migrationBuilder.DropTable(
          name: "SysUser_Role");
    }
  }
}
