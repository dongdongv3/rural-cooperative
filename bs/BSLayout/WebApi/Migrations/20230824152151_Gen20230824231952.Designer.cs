﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using WebApi.Code.Data;

namespace WebApi.Migrations
{
    [DbContext(typeof(HeZuosheDB))]
    [Migration("20230824152151_Gen20230824231952")]
    partial class Gen20230824231952
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "5.0.13");

            modelBuilder.Entity("WebApi.Code.Data.FileUpLoad", b =>
                {
                    b.Property<string>("ID")
                        .HasMaxLength(50)
                        .HasColumnType("TEXT");

                    b.Property<DateTime?>("CreateTime")
                        .HasColumnType("TEXT");

                    b.Property<string>("Ext")
                        .HasMaxLength(50)
                        .HasColumnType("TEXT");

                    b.Property<string>("FileName")
                        .HasMaxLength(255)
                        .HasColumnType("TEXT");

                    b.Property<bool?>("IsUse")
                        .HasColumnType("INTEGER");

                    b.Property<string>("UserID")
                        .HasMaxLength(50)
                        .HasColumnType("TEXT");

                    b.HasKey("ID");

                    b.ToTable("FileUpLoad");
                });

            modelBuilder.Entity("WebApi.Code.Data.SysUser", b =>
                {
                    b.Property<string>("Id")
                        .HasMaxLength(50)
                        .HasColumnType("TEXT");

                    b.Property<string>("UserName")
                        .HasMaxLength(50)
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.ToTable("SysUser");
                });

            modelBuilder.Entity("WebApi.Code.Data.SysUser_AccountAndPasswordAuth1", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<string>("LoginID")
                        .HasMaxLength(50)
                        .HasColumnType("TEXT");

                    b.Property<string>("Pwd")
                        .HasMaxLength(100)
                        .HasColumnType("TEXT");

                    b.Property<string>("RefSysUserId")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.HasIndex("RefSysUserId")
                        .IsUnique();

                    b.ToTable("SysUser_AccountAndPasswordAuth1");
                });

            modelBuilder.Entity("WebApi.Code.Data.SysUser_Auth", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<string>("AuthName")
                        .HasMaxLength(255)
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.ToTable("SysUser_Auth");
                });

            modelBuilder.Entity("WebApi.Code.Data.SysUser_Role", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<string>("RoleName")
                        .HasMaxLength(50)
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.ToTable("SysUser_Role");
                });

            modelBuilder.Entity("WebApi.Code.Data.SysUser_RoleAuthMap", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<Guid>("RefSysUser_AuthId")
                        .HasColumnType("TEXT");

                    b.Property<Guid>("RefSysUser_RoleId")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.HasIndex("RefSysUser_AuthId");

                    b.HasIndex("RefSysUser_RoleId");

                    b.ToTable("SysUser_RoleAuthMap");
                });

            modelBuilder.Entity("WebApi.Code.Data.SysUser_UserRoleMap", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<string>("RefSysUserId")
                        .HasColumnType("TEXT");

                    b.Property<Guid>("RefSysUser_RoleId")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.HasIndex("RefSysUserId");

                    b.HasIndex("RefSysUser_RoleId");

                    b.ToTable("SysUser_UserRoleMap");
                });

            modelBuilder.Entity("WebApi.Code.Data.SysUser_AccountAndPasswordAuth1", b =>
                {
                    b.HasOne("WebApi.Code.Data.SysUser", "RefSysUser")
                        .WithOne("SubSysUser_AccountAndPasswordAuth1")
                        .HasForeignKey("WebApi.Code.Data.SysUser_AccountAndPasswordAuth1", "RefSysUserId");

                    b.Navigation("RefSysUser");
                });

            modelBuilder.Entity("WebApi.Code.Data.SysUser_RoleAuthMap", b =>
                {
                    b.HasOne("WebApi.Code.Data.SysUser_Auth", "RefSysUser_Auth")
                        .WithMany("SubSysUser_RoleAuthMap")
                        .HasForeignKey("RefSysUser_AuthId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("WebApi.Code.Data.SysUser_Role", "RefSysUser_Role")
                        .WithMany("SubSysUser_RoleAuthMap")
                        .HasForeignKey("RefSysUser_RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("RefSysUser_Auth");

                    b.Navigation("RefSysUser_Role");
                });

            modelBuilder.Entity("WebApi.Code.Data.SysUser_UserRoleMap", b =>
                {
                    b.HasOne("WebApi.Code.Data.SysUser", "RefSysUser")
                        .WithMany("SubSysUser_UserRoleMap")
                        .HasForeignKey("RefSysUserId");

                    b.HasOne("WebApi.Code.Data.SysUser_Role", "RefSysUser_Role")
                        .WithMany("SubSysUser_UserRoleMap")
                        .HasForeignKey("RefSysUser_RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("RefSysUser");

                    b.Navigation("RefSysUser_Role");
                });

            modelBuilder.Entity("WebApi.Code.Data.SysUser", b =>
                {
                    b.Navigation("SubSysUser_AccountAndPasswordAuth1");

                    b.Navigation("SubSysUser_UserRoleMap");
                });

            modelBuilder.Entity("WebApi.Code.Data.SysUser_Auth", b =>
                {
                    b.Navigation("SubSysUser_RoleAuthMap");
                });

            modelBuilder.Entity("WebApi.Code.Data.SysUser_Role", b =>
                {
                    b.Navigation("SubSysUser_RoleAuthMap");

                    b.Navigation("SubSysUser_UserRoleMap");
                });
#pragma warning restore 612, 618
        }
    }
}
