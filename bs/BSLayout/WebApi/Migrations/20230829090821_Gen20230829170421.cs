using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApi.Migrations
{
  public partial class Gen20230829170421 : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.CreateTable(
          name: "Churukujilu",
          columns: table => new
          {
            Id = table.Column<Guid>(type: "TEXT", nullable: false),
            Createtime = table.Column<DateTime>(type: "TEXT", nullable: true),
            Shengqingren = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
            ApprovedUser = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
            Leixing = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
            Fenlei = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
            Wupin = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
            Shuliang = table.Column<int>(type: "INTEGER", nullable: true)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_Churukujilu", x => x.Id);
          });

      migrationBuilder.CreateTable(
          name: "GengdiType",
          columns: table => new
          {
            Id = table.Column<Guid>(type: "TEXT", nullable: false),
            PianquName = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_GengdiType", x => x.Id);
          });

      migrationBuilder.CreateTable(
          name: "KucunMingxi",
          columns: table => new
          {
            Id = table.Column<Guid>(type: "TEXT", nullable: false),
            WupinName = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
            Count = table.Column<int>(type: "INTEGER", nullable: true),
            Danwei = table.Column<string>(type: "TEXT", maxLength: 10, nullable: true),
            Fenlei = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_KucunMingxi", x => x.Id);
          });

      migrationBuilder.CreateTable(
          name: "KucunType",
          columns: table => new
          {
            Id = table.Column<Guid>(type: "TEXT", nullable: false),
            Name = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_KucunType", x => x.Id);
          });

      migrationBuilder.CreateTable(
          name: "Nongjifenlei",
          columns: table => new
          {
            Id = table.Column<Guid>(type: "TEXT", nullable: false),
            Name = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_Nongjifenlei", x => x.Id);
          });

      migrationBuilder.CreateTable(
          name: "Nongjigongdan",
          columns: table => new
          {
            Id = table.Column<Guid>(type: "TEXT", nullable: false),
            Createtimre = table.Column<DateTime>(type: "TEXT", nullable: true),
            BeginTime = table.Column<DateTime>(type: "TEXT", nullable: true),
            Miangji = table.Column<int>(type: "INTEGER", nullable: true),
            Leixing = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
            Chuli = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
            Zongjia = table.Column<decimal>(type: "TEXT", nullable: true),
            Zhuangtai = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
            Jeisuanzhuangtai = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_Nongjigongdan", x => x.Id);
          });

      migrationBuilder.CreateTable(
          name: "Nongjihezuo",
          columns: table => new
          {
            Id = table.Column<Guid>(type: "TEXT", nullable: false),
            Username = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
            PhoneNumber = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
            Leixing = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
            Xinghao = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
            Qianyuejia = table.Column<decimal>(type: "TEXT", nullable: true)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_Nongjihezuo", x => x.Id);
          });

      migrationBuilder.CreateTable(
          name: "Sheyuan",
          columns: table => new
          {
            Id = table.Column<Guid>(type: "TEXT", nullable: false),
            UserName = table.Column<string>(type: "TEXT", maxLength: 10, nullable: true),
            UserCode = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
            PhoneNumber = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
            JoinTime_Year = table.Column<int>(type: "INTEGER", nullable: true),
            JoinTime_Month = table.Column<int>(type: "INTEGER", nullable: true),
            Panqu = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
            Miangji = table.Column<double>(type: "REAL", nullable: true),
            Status = table.Column<string>(type: "TEXT", maxLength: 50, nullable: true),
            Yue = table.Column<decimal>(type: "TEXT", nullable: true)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_Sheyuan", x => x.Id);
          });

      migrationBuilder.CreateTable(
          name: "ZhangHuLog",
          columns: table => new
          {
            Id = table.Column<Guid>(type: "TEXT", nullable: false),
            CreateTime = table.Column<DateTime>(type: "TEXT", nullable: true),
            ActionMoney = table.Column<decimal>(type: "TEXT", nullable: true),
            Remark = table.Column<string>(type: "TEXT", maxLength: 200, nullable: true),
            RefSheyuanId = table.Column<Guid>(type: "TEXT", nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_ZhangHuLog", x => x.Id);
            table.ForeignKey(
                      name: "FK_ZhangHuLog_Sheyuan_RefSheyuanId",
                      column: x => x.RefSheyuanId,
                      principalTable: "Sheyuan",
                      principalColumn: "Id",
                      onDelete: ReferentialAction.Cascade);
          });

      migrationBuilder.CreateIndex(
          name: "IX_ZhangHuLog_RefSheyuanId",
          table: "ZhangHuLog",
          column: "RefSheyuanId");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropTable(
          name: "Churukujilu");

      migrationBuilder.DropTable(
          name: "GengdiType");

      migrationBuilder.DropTable(
          name: "KucunMingxi");

      migrationBuilder.DropTable(
          name: "KucunType");

      migrationBuilder.DropTable(
          name: "Nongjifenlei");

      migrationBuilder.DropTable(
          name: "Nongjigongdan");

      migrationBuilder.DropTable(
          name: "Nongjihezuo");

      migrationBuilder.DropTable(
          name: "ZhangHuLog");

      migrationBuilder.DropTable(
          name: "Sheyuan");
    }
  }
}
