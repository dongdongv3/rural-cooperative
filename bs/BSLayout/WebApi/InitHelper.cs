using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;

namespace WebApi
{
  public class InitHelper
  {
    public static void Run(string[] args, IServiceScope serviceScope)
    {
      if (args.Length > 0 && args[0] == "init")
      {
        if (args.ToList().Any(x => x == "-openInitWebUI"))
        {
          Console.WriteLine("-openInitWebUI");
          Controllers.InitController.IsOpenWebInit = true;
        }
        var func = new Action(() =>
        {
          //执行初始化
          var filename = "";

          if (args.ToList().Any(x => x.StartsWith("file:")))
          {
            filename = args.ToList().First(x => x.StartsWith("file:")).Replace("file:", "");
            ReadSetting.LoadSettingFromFile(filename);
          }

          try
          {
            InitData.DatInit(serviceScope);
            if (Controllers.InitController.IsOpenWebInit)
            {
              Controllers.InitController.key = Controllers.InitController.Title_Complate;
              Controllers.InitController.eventWaitInputRequest.Set();
              Controllers.InitController.eventWaitValueReaded.Set();
              Controllers.InitController.eventWaitInputComplote.Set();
            }
          }
          catch (Exception e)
          {
            Console.WriteLine(e.Message);
            if (Controllers.InitController.IsOpenWebInit)
            {
              Controllers.InitController.errorMessage = e.Message;
              Controllers.InitController.key = Controllers.InitController.Title_Error;
              Controllers.InitController.eventWaitInputRequest.Set();
              Controllers.InitController.eventWaitValueReaded.Set();
              Controllers.InitController.eventWaitInputComplote.Set();
            }
          }

          if (string.IsNullOrEmpty(filename) && !Controllers.InitController.IsOpenWebInit)
          {
            Console.WriteLine("请输入文件名用来存储当前配置，如果不输入则不保存配置:");
            var tmpinput = Console.ReadLine();
            if (!string.IsNullOrEmpty(tmpinput))
            {
              ReadSetting.SaveSettingToFile(tmpinput);
            }
          }
          serviceScope.Dispose();
        });
        if (Controllers.InitController.IsOpenWebInit)
        {
          new System.Threading.Thread(() => { func(); }).Start();
        }
        else
        {
          func();
        }
      }
    }
  }

}
