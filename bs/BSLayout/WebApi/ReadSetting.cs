using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace WebApi
{
  public class ReadSetting
  {
    private static Dictionary<string, string> setting { get; set; } = new Dictionary<string, string>();
    public static void LoadSettingFromFile(string fileName)
    {
      if (!File.Exists(fileName))
      {
        throw new Exception($"文件{fileName}不存在");
      }
      var data = File.ReadAllText(fileName);
      var d = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
      setting = d;
    }

    public static void SaveSettingToFile(string fileName)
    {
      File.WriteAllText(fileName, Newtonsoft.Json.JsonConvert.SerializeObject(setting));
    }

    public static T GetValue<T>(string initTitle, string inputName, string[] selectItem = null, bool Required = true)
    {
      Console.InputEncoding = Encoding.Unicode;
      var key = $"{initTitle}-{inputName}";
      while (true)
      {
        try
        {
          string valuestr = null;
          if (setting.ContainsKey(key))
          {
            valuestr = setting[key];
          }
          else
          {
            if (Controllers.InitController.IsOpenWebInit)
            {
              Controllers.InitController.key = key;
              Controllers.InitController.options = selectItem?.ToList().Select(x => new KeyValuePair<string, string>(x, x)).ToList();
              Controllers.InitController.Required = Required;
              Controllers.InitController.eventWaitInputRequest.Set();
              Controllers.InitController.eventWaitInputComplote.Reset();
              Controllers.InitController.eventWaitInputComplote.WaitOne();
              valuestr = Controllers.InitController.value;
            }
            else
            {
              Console.WriteLine(key);
              if (selectItem != null)
              {
                Console.WriteLine("允许的值列表：");
                selectItem?.ToList().ForEach(i =>
                {
                  Console.WriteLine(i);
                });
              }
              Console.Write("请输入:");
              valuestr = Console.ReadLine();
              Console.WriteLine(valuestr);
            }
          }

          object output = null;
          if (typeof(T) == typeof(int))
          {
            int tmp;
            if (int.TryParse(valuestr, out tmp))
            {
              output = tmp;
            }
            else
            {
              throw new Exception($"值'{valuestr}' 不是有效的int");
            }
          }
          else if (typeof(T) == typeof(double))
          {
            double tmp;
            if (double.TryParse(valuestr, out tmp))
            {
              output = tmp;
            }
            else
            {
              throw new Exception($"值'{valuestr}' 不是有效的double");
            }
          }
          else if (typeof(T) == typeof(decimal))
          {
            decimal tmp;
            if (decimal.TryParse(valuestr, out tmp))
            {
              output = tmp;
            }
            else
            {
              throw new Exception($"值'{valuestr}' 不是有效的decimal");
            }
          }
          else if (typeof(T) == typeof(Guid))
          {
            Guid tmp;
            if (Guid.TryParse(valuestr, out tmp))
            {
              output = tmp;
            }
            else
            {
              throw new Exception($"值'{valuestr}' 不是有效的Guid");
            }
          }
          else if (typeof(T) == typeof(DateTime))
          {
            DateTime tmp;
            if (DateTime.TryParse(valuestr, out tmp))
            {
              output = tmp;
            }
            else
            {
              throw new Exception($"值'{valuestr}' 不是有效的Guid");
            }
          }
          else
          {
            output = valuestr;
          }
          setting[key] = valuestr;
          if (Controllers.InitController.IsOpenWebInit)
          {
            Controllers.InitController.errorMessage = "";
            Controllers.InitController.eventWaitValueReaded.Set();
          }
          return (T)output;
        }
        catch (Exception e)
        {
          if (Controllers.InitController.IsOpenWebInit)
          {
            Controllers.InitController.errorMessage = e.Message;
            Controllers.InitController.eventWaitValueReaded.Set();
          }
          Console.WriteLine(e.Message);
        }
      }
    }
  }
}
